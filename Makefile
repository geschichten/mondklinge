SHORT_TITLE = IBDM
CHAPTERS := \
	Vorwort\
	SanftesEntrinnen\
	DasEndeDerWelt\
	Silberblick\
	Ketten\
	Erkennen\
	LogikAmMorgen\
	ProbierenUndStudieren\
	Mondscheinhypothesen\
	WissenschaftDesTodes\
	Jagd\
	Brennen\
	LaudenLeonideVonHorstenfels\
	OffLimits\
	DieSchlangeDieSchildkroeteUndDieLoechrigeFlunder\
	DerSchneefuchsUndDerSchmetterling\
	AuseinanderUndZusammenNehmenUndSetzen\
	Verloren\
	VorRueckNachAnAbZuverUndUebersicht\
	DieMondklinge\
	EineSchlechtErzaehlteGeschichte\
	EinScheiterhaufen\
	FieseLiebesbriefe\
	DieVerschiedenheitDerWeltrettungsplaene\
	DasReissenDerZeit\
	Epilog
INPUTS := $(CHAPTERS:%=chapters/%.md) metadata.yaml
OUTPUTS := ImBannDerMondklinge.pdf ImBannDerMondklinge.epub ImBannDerMondklinge.tex ImBannDerMondklinge.html ContentNotes.tex $(SHORT_TITLE)/ContentNotes.md $(SHORT_TITLE)
SPBUCHSATZV := SPBuchsatz_Projekte_1_4_9

all: $(OUTPUTS)

ImBannDerMondklinge.html: $(INPUTS)
	pandoc \
		$(INPUTS) \
		-o $@


ImBannDerMondklinge.epub: $(INPUTS)
	pandoc \
		-t epub2\
		--css epub.css\
		-L SPEpub.lua\
		$(INPUTS) \
		-o $@

ImBannDerMondklinge.tex: $(INPUTS) SPBuchsatz.lua
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$(INPUTS) \
		-o $@

ContentNotes.tex: chapters/ContentNotes.md
	~/.local/bin/pandoc \
		--top-level-division=chapter \
		-t SPBuchsatz.lua \
		$^ \
		-o $@


# changed Ifstr to ifstr in SPBuchsatz_System/Helferlein/Hilfsfunktionen.tex 
ImBannDerMondklinge.pdf: ImBannDerMondklinge.tex ContentNotes.tex TitleAuthor.tex
	cp ImBannDerMondklinge.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/Haupttext.tex
	cp ContentNotes.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/ContentNotes.tex
	cp TitleAuthor.tex \
		deps/$(SPBUCHSATZV)/Buch/tex/TitleAuthor.tex
	cd deps/$(SPBUCHSATZV)/Buch/build/ ; \
		lualatex ../tex/Buch.tex
	cp deps/$(SPBUCHSATZV)/Buch/build/Buch.pdf ImBannDerMondklinge.pdf

$(SHORT_TITLE)/titlelist.txt: Makefile
	mkdir -p $(shell dirname $@)
	@echo '$(CHAPTERS)' | xargs -n 1 | cat -n >$@

get_chapter_number = $(shell \
	grep -E '^[[:digit:][:space:]]+$(1:chapters/%.md=%)$$' $(SHORT_TITLE)/titlelist.txt | \
		awk '{ print $$1 - 1 }' \
)


chapters-epub/%.epub: chapters/%.md $(SHORT_TITLE)/titlelist.txt
	pandoc \
		-t epub2\
		--css epub.css\
		--metadata title="$(call get_chapter_number,$<) - $(shell \
			sed -n '3{p;q}' '$<' \
		)"\
		$< \
		-o $@

$(SHORT_TITLE)/%.md: chapters/%.md $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "$(call get_chapter_number,$<) - $(shell \
			sed -n '3{p;q}' '$<' \
		)"'
	@echo >>$@ 'number: $(call get_chapter_number,$<)'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	pandoc \
		-L SPmarkdown.lua\
		$< \
		-o tmp-md-to-md.md
	@cat tmp-md-to-md.md >>$@
	rm tmp-md-to-md.md

$(SHORT_TITLE)/ContentNotes.md: chapters/ContentNotes.md $(SHORT_TITLE)/titlelist.txt
	@echo '$@'
	@echo >$@ '---'
	@echo >>$@ 'layout: $(SHORT_TITLE)-chapters'
	@echo >>$@ 'page-category: story-chapters-$(SHORT_TITLE)'
	@echo >>$@ 'title: "Content-Notes"'
	@echo >>$@ 'number: 0'
	@echo >>$@ 'lang: de'
	@echo >>$@ '---'
	@echo >>$@ ''
	@cat $< >>$@

$(SHORT_TITLE): $(CHAPTERS:%=$(SHORT_TITLE)/%.md) ;

clean:
	rm -rf $(OUTPUTS)

.PHONY: clean $(SHORT_TITLE)
