pandoc.utils = require 'pandoc.utils'

-- return {
--   {
--     Str = function (elem)
--       if elem.text == "K\\exdose" then
-- 
--         return pandoc.Emph {pandoc.Str "Kexdose"}
--       else
--         return elem
--       end
--     end,
--   }
-- }

-- function RawBlock(elem)
--   if elem.format ~= "tex" then
--     return elem.text
--   elseif elem.format ~= "epub" then
--     if string.match(elem.text or '', "BefehlVorKapitelIBDM") == "BefehlVorKapitelIBDM" then
--       return (elem.text or ''):gmatch "{.-}"(1)..(elem.text or ''):gmatch "{.-}"(2)
-- --      return string.match(elem.text or '', "{.-}")
--     else
--       return string.match(elem.text or '', "Be")
--     end
--   else
-- --    return string.match(str or '',"%a") .. "kex"
--     return elem.text
-- --    if str then
-- --	    return "dose"
-- --    else
-- --	    return "kex"
-- --    end
--   end
-- end

function RawBlock(elem)
	if elem.format ~= 'tex' then
		return
	end

	local cmd, rest = string.match(elem.text, '^\\([^{]+)(.+)$')

	local args = {}
	for arg in string.gmatch(rest, '{([^}]*)}') do
		table.insert(args, arg)
	end

	if cmd == 'BefehlVorKapitelIBDM' then
                return {pandoc.Image(args[1], "/home/kaulquake/Fischtopf/Geschichten/ImBannDerMondklinge/img/" .. args[1]), pandoc.BlockQuote("Bildbeschreibung: " .. args[2]), pandoc.HorizontalRule(), pandoc.Strong("Content Notes:"), pandoc.Str(args[3])}
	elseif cmd == 'Beitext' then
		return {pandoc.Emph(args[1]), pandoc.HorizontalRule()}
	else
		return elem.text
	end
end
