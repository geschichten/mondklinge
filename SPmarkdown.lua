pandoc.utils = require 'pandoc.utils'

function Image(elem)
	return pandoc.Image("../" .. string.match(elem.src,"%a-%.png"),"../" .. string.match(elem.src,"%a-%.png"))
end


function RawBlock(elem)
	if elem.format ~= 'tex' then
		return
	end

	local cmd, rest = string.match(elem.text, '^\\([^{]+)(.+)$')

	local args = {}
	for arg in string.gmatch(rest, '{([^}]*)}') do
		table.insert(args, arg)
	end

	if cmd == 'BefehlVorKapitelIBDM' then
                return {pandoc.Image(args[1], args[1]), pandoc.Str("\n"), pandoc.Emph("Bildbeschreibung:"), pandoc.Str(args[2]), pandoc.Str("\n"), pandoc.Strong("Content Notes:"), pandoc.Str(args[3]), pandoc.HorizontalRule()}
	elseif cmd == 'Beitext' then
		return {pandoc.Emph(args[1])}
	else
		return elem.text
	end
end

