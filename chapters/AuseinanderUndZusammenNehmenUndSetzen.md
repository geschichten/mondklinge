\BefehlVorKapitelIBDM{AuseinanderUndZusammenNehmenUndSetzen.png}{Ein Kurzflügel -- das Musikinstrument.}{Fesseln, BDSM erwähnt, ein Witz, der Ableismus auf die Schippe nimmt, und bei dem ich noch nicht entscheiden kann, ob das eher hilfreich ist oder mir nicht zusteht, emotionaler Druck, Dissoziation.}

Auseinander- und Zusammen--nehmen und -setzen
=============================================

\Beitext{Viele}

Es duftet nach Keksen im ganzen Haus. Es ist Samstag, früher Nachmittag, und
Marcin ist gerade damit fertig, das Wohnzimmer aufzuräumen. Kendra ist an einen
Schreibtischstuhl mit Rollen daran gefesselt. Ja, das ist fast eine Formulierung
(an den Rollstuhl gefesselt), die im Allgemeinen eher
nicht okay ist, weil sie Behinderungen bemitleidenswert und entmündigend
darstellt, aber in Kendras Fall ist das wörtlich zu verstehen. Damit Kendra nicht mitaufräumt,
wozu sie einen starken Drang hat, hat Marcin Kendra auf ihren eigenen Wunsch
hin daran festgeschnürt. Sie hat heute bereits eigentlich mehr mit ihrem
Körper getan, als gut für sie ist. Sie haben für diese Fesselung allerdings
nicht die Metallschellen verwendet, mit denen sie Luna fixiert haben, sondern
weiche Fließfesseln für Gliedmaßen, die Léonide aus ebenso unbekannten Gründen
auch irgendwo im Haus rumfliegen hat.

Luna trägt ein Blech Kekse ins Wohnzimmer, das in der Küche keinen Platz
hat, und betrachtet Kendra grinsend.

"Das ist kein Kink von mir oder so!", protestiert Kendra proaktiv, einfach
falls Luna solche Gedanken hat.

Luna kichert. "Meiner war es auch nicht", gibt sie zu. "Es war ganz
interessant, aber mehr auch nicht."

Kendra nickt. "Dasselbe bei mir."

Luna erinnert sich an ihre erste Begegnung mit Kendra zurück. Als sie Kendra
gebissen hat. Da war etwas zwischen ihnen gewesen, aber es stimmt, das hatte
weniger mit Machtgefälle zu tun. Und wenn, dann im Gegenteil, hatte Kendra
eher mit Lunas Wünschen gespielt. Das war auch interessant und vielleicht
nicht nur ein sachliches, sondern ein schönes Interessant.

"Luna, ich hätte dich vermisst, wenn du draufgegangen wärest", informiert
Léonide unvermittelt.

Luna wendet sich iem hastig zu und schmeißt dabei einen Krug vom Tisch, den
sie allerdings irgendwie mit dem Fuß erwischt, sodass er heile in ihrer Hand
statt auf dem Boden landet. "Oh", sagt sie. Dann setzt sie sich schnaubend. "Leude,
ich bin eine Mörderin. Mir wurde in der Nacht im Wald schon ein Liebesgeständnis
gemacht. Ich gebe mir nun nicht sonderlich Mühe, sympathisch zu sein. Warum
zum Gott der Unglaubwürdigung mögt ihr mich?"

Léonide greift sich einen der heißen Kekse, der noch sehr biegsam ist,
pustet und schmunzelt dabei. "Ach, Luna", sagt as. "Ich gebe
mir auch keine Mühe, sympathisch zu sein. Mit
dir fällt im Gespräch immer so ein ärgerlicher Gesprächslayer des Kalibers
'das macht man so' weg. Das macht dich zu einer der wenigen Personen, in deren
Gegenwart ich mich entspannt beim Reden fühle."

Luna fühlt sich innerlich mit einem Mal wohlig weich. "Es ist für mich sehr
bedeutend, auf euch diese Wirkung zu haben, mylauden."

In die gefühlsgeladene Atmosphäre hinein erklingt ein Ton aus dem
Kurzflügel.

"Endlich", haucht Léonide. "Er hat sich getraut."

Kurz darauf hört Léonide das Geräusch der Klappe über dem Griffbrett, wie diese
die letzten Milimenter auf das Holz darunter fällt und alle Saiten sachte
zum Schwingen bringt.

"Doch nicht", flüstert Luna.

"Spiel ruhig!", ruft Léonide. Sie sitzen wieder erhöht um die Ecke, die
vom Kurzflügel aus nur über die Glasfront zum Garten und das Fenster an
der Essnische einzusehen ist.

"Aber die Monster!", argumentiert Marcin.

"Lass los!", ruft Luna. "Wir werden ein Inferno schon gemanaget kriegen."

Marcin klappt die Klappe wieder auf. Er schaut auf die Tasten, die einen solchen
Sog auf ihn haben. Dieser Teil vom Gehirn, der in diesem Raum bisher einfach nur damit
ausgelastet gewesen ist, sich zu wünschen, zu spielen, den er bisher immer wegignoriert
hat. Aber nun lässt er ihn vorsichtig ein Stück los, verteilt die Finger auf der
Tastatur. Schon passiert es: Ein leiser, fast haptikfreier, schwarzer Schmetterling
aus schwarzen Schlieren, fast wie Wasser, lässt sich auf seinem linken Mittelfinger
nieder. Sehnsucht.

Marcin atmet zitternd ein und aus. Ihm ist sehr bewusst, dass alle ihm hier ihre
Aufmerksamkeit schenken, selbst wenn Kendra auf den Boden blickt. Er versucht,
sich mehr auf sich zu konzentrieren. Auf sich und den Kurzflügel. Er hat
doch noch gar keine Ahnung, was er vor sich hat. Ist er gestimmt? Muss er vorsichtig
sein, weil jede Nuance zu viel Druck den Ton gleich um ein vielfaches lauter
machen würde?

Marcin spielt langsam einen gebrochenen Cis-Moll-Akkord. Der Schmetterling
bewegt dabei die Flügel und sie hinterlassen schwarze Muster wie Schwimmbewegungen
in der Luft, die wieder verblassen. Es ist so schön. Ein so zartes Geschöpf voller
zierlicher Zerstörung.

Marcin fällt. Er weiß nicht, was davon in seinem Kopf ist und was die anderen
miterleben: Er sieht durch die schwarze Dichte kaum zu den Tasten hindurch. Unmenschliche
Geräusche begleiten sein Spiel. Er untermalt das akustisch schlierige Quietschseufzen
mit den hohen, plingigen Tönen, die ein akustisches
Schillern in die Schlieren drängen und darin verwehen. Der ganze Raum ist erfüllt
von Musik und von Ungeheuern, die auf ihre gespenstische Art tanzen und alles
einhüllen. Sie rinnen über seinen Körper wie warmes und kaltes Wasser. Er schließt
die Augen und spielt einfach weiter. Akkorde in den tiefen Tasten, klangvolle,
rhythmische, pompöse Aufstiege. Wie lange hat er nicht mehr so gespielt? Dass er es überhaupt
noch kann! Aber sein Muskelgedächtnis leistet zuverlässig vor sich hin, während
sein Körper leidenschaftliche Dynamik in die Töne legt.

Als er den letzten Ton verklingen lässt, blickt er sich ängstlich um. Er spürt,
wie nass sein Gesicht vom unbemerkten Weinen ist. Die Schlieren verwehen. Außer einer sehr langen,
hageren Gestalt im hinteren Bereich des Raums. Es lässt sich nicht sinnvoll
einem Tier zuordnen. Am ehesten ist es eine Kreuzung aus schmaler, hoher Tanne
und norwegischer Waldkatze, sehr haarig, etwas unscharf,
sich bewegend, als wollte es etwas. Es ist vielleicht das unheimlichste,
was Marcin je produziert hat, aber er blickt die Gestalt mutig an. Sie
bleibt dort einfach, streckt nur gelegentlich unsicher eine Art Arm aus, nur ein
wenig.

"Es war schön", sagt Luna. Sie hat sich in den Wohnbereich geschlichen und
lehnt an der Wand. "Deine Ungeheuer sind wunderschön."

Kendra nickt. "Finde ich auch", sagt sie.

"Selbst die?", fragt Marcin und deutet auf die bleichschwarze
Gestalt, die fast bis an die Decke reicht.

Auf die Frage hin tritt die Gestalt weiter
in den Raum. Es fällt ihr sichtlich schwer, sie kann sich auf ihren
zu kleinen, zu unscharfen Füßchen kaum aufrecht halten.

Marcin stellt sich vor, dass Kendra ihr wahrscheinlich anbieten würde,
sie zu halten, wenn sie nicht gerade gefesselt wäre. Immer noch, fällt
Marcin auf.

"Ja", sagt Kendra. Sie sieht das haarige, dunkle Etwas freundlich an. "Was
ist es für eine Emotion."

"Mit Verletzlichkeit akzeptiert werden wollen", flüstert Marcin. Er
sinkt neben Kendra auf den Boden und weint.

"Das tue ich, Marcin", sagt Kendra leise.

Die Türglocke bimmelt. Luna springt auf, um die nächste Fuhre Kekse aus
dem Ofen zu holen und dann die Tür zu öffnen. Marcin beeilt sich, Kendra zu
entfesseln.

Als Paolo das Haus betritt, steht der Schreibtischstuhl wieder am
Sekretär neben dem Regal, wo die Gestalt gestanden hat. Die Gestalt ist
verschwunden. Marcin versucht sich nur mäßig erfolgreich das Gesicht trocken
zu wischen, bevor er sich zu Paolo umdreht, aber dieser braucht ohnehin noch
ein wenig. Schal abwickeln, Jacke aufhängen, Luna und Léonide begrüßen,
letzterem ein Besuchsgeschenk überreichen.

"Unfug", murmelt Léonide und nimmt es entgegen. "Wenn ich es nicht brauchen
kann, nimmst du es wieder mit?"

Kendra lacht und Marcin kann sich ob dieser Unverfrorenheit auch ein Grinsen
nicht verkneifen.

"Selbstverständlich." Paolo schluckt es einfach und lächelt auch.

Léonide öffnet das kleine Tütchen und holt eine Dose mit einer Gewürzmischung
heraus. "Die muss ich ja auch noch probieren, bevor ich entscheiden kann, ob
ich sie brauchen kann."

"Wenn Sie sie loswerden wollen, komme ich auch ohne Murren vorbei und hole
sie wieder ab. Und nehme Kritik entgegen, um eine passendere Mischung zu
erstellen", verspricht Paolo.

Marcin kommt ihm entgegen, um ihm einen Arm um den Rücken zu legen. Es
muss sich seltsam für Paolo anfühlen, er muss verunsichert sein. Aber
er hat vergessen, dass Paolo ihm dann ins Gesicht sehen wird, was auch
passiert.

"Was ist mit dir?", fragt Paolo. Er bietet eine Umarmung an.

Marcin zögert, aber nimmt sie dann doch entgegen. Er antwortet nicht. Zu
viel auf einmal ist gerade in seinem Kopf. Er möchte die Beziehung mit Paolo
auflösen. Wenn er es ihm jetzt sagt, denkt Paolo, Marcin habe deswegen geweint. Einen
Moment denkt Marcin darüber nach, ob das von Vorteil ist, aber er verwirft
den Gedanken, weil er ehrlich sein möchte.

Paolo fragt nicht noch einmal nach und streichelt ihm über den
Rücken, sortiert sein Haar. Er macht es so anders, als Kendra das macht. Von
beiden fühlt es sich schön an.

Marcin fühlt sich falsch, weil er die Beziehung gleich trennen möchte, aber
nun die Berührung genießt. Ob Paolo ihn noch umarmen möchte, wenn sie kein
Beziehungspaar mehr sind?

Marcin löst sich aus der Umarmung und führt Paolo zum Tisch. "Ich koche
noch Tee und Kaffee."

"Ich kann auch gern helfen!" Paolo will wieder aufstehen, aber Marcin
drückt ihn sanft zurück auf den Stuhl. "Lass mir die paar Minuten
Ruhe für mich in der Küche, ja?" Hat er das gerade wirklich so direkt
geäußert?

Paolo sieht ihn überrascht an, aber nickt schließlich. "Klar, Marcin", sagt
er. "Ich habe dich lieb."

In der Küche trifft Marcin nur auf Luna. "Du hattest recht", murmelt er.

"Die Sache mit den Ungeheuern?", fragt Luna.

Marcin nickt. "Sie tun weh, aber nur, wenn wir den Schmerz mögen und
brauchen", wiederholt er, was sie ihm einst am Waldesrand gesagt
hat. "Sie sind ungefährlich, wenn sie nicht zur Verteidigung da
sind. Sie sind wichtig und ich fühle mich besser, wenn sie bei mir
sind, auch wenn ich dabei innerlich reiße. Aber ich fühle dabei."

"Ist es sehr ungünstig, dass Paolo gerade jetzt gekommen
ist?", fragt Luna. "Soll ich ihn hinausbefördern?"

Marcin lächelt leise und schüttelt den Kopf. "Ich fühle mich zwar nicht
bereit, aber ich möchte es hinter mich bringen."

---

Sie sitzen alle gemeinsam am Tisch. Tageslicht scheint durch die UV-Vorhänge, die
sie inzwischen besorgt haben, und weil das alles doch etwas sehr dimmt, haben sie
romantischer Weise Kerzen aufgestellt. Tee, Kaffee und Kakao stehen in Kannen
auf dem Tisch. In der Mitte ein riesiger Teller mit gemischten Keksen.

"Ihr habt mich eingeladen, und so nett das hier ist, glaube ich nicht,
dass ihr das aus reiner Nettigkeit tut", sagt Paolo, nachdem sie alle mit
ihrer Komplimentiererei durch sind.

"Ich möchte meine Beziehung mit dir trennen", sagt Marcin. "Ich weiß nicht,
wie ich vorbereitende Dinge dazu hätte sagen sollen."

Paolo blickt ihn lange an. Er legt seinen angebissenen Keks auf seine
Serviette. "Das klingt endgültig", sagt er schließlich. "Ich hatte mich
schon gefragt, ob du mit Kendra etwas anfangen möchtest. Wenn es das ist,
das wäre okay für mich. Ich habe schon länger mal über Polyamorie
nachgedacht."

Marcin schüttelt den Kopf. "Das ist es nicht", sagt er. "Ich mag
zugeben, dass ich sie geküsst habe. Im Rahmen eines wissenschaftlichen
Experiments. Aber dabei habe ich rausgefunden, dass ich, hm, sex
repulsed bin, denke ich. Asexuell. Ich mag kuscheln. Aber ich mag
eigentlich gar nicht küssen."

"Oh", macht Paolo. "Uffz."

Marcin kämpft die Angst nieder, die in ihm aufkommt. Die Angst, Paolo könne
ihn dafür verurteilen. Entweder, weil Marcin so ist, oder weil er doch
so lange nichts dazu gesagt hat.

"Das tut mir leid", sagt Paolo. "War ich, wie frage ich das, habe
ich dich unbeabsichtigt zu Dingen gedrängt, die du nicht wolltest? Muss
ich dann ja."

Marcin sieht Paolo an, dass er sich gleich in eine Spirale aus Schuldgefühlen
verheddern könnte. "Nein, das ist nicht die Schlussfolgerung daraus", sagt
er. "Ich dachte, ich wollte. Ich wollte wollen. Mir war oft vielleicht
sogar weniger klar als dir, dass ich eigentlich nicht will, glaube ich."

Paolo schüttelt langsam den Kopf. "Ich habe versucht, immer Konsens sicher
zu stellen, aber ich habe nicht in dich reingucken können, nein."

"Mach dir jedenfalls keine Sorge, du könntest mich bedrängt haben", sagt
Marcin.

Paolo isst langsam den Rest seines Kekses. Anschließend tupft er sich
mit der Rückseite der Serviette die Augen. "Es tut mir leid!" Das
Weinen ist in seiner leisen Stimme hörbar. "Ich weiß gerade nicht, was
ich machen soll. Ich habe das Gefühl mir bricht alles weg. Und ich
mache alles falsch."

Marcin merkt es dieses Mal, fühlt, wie es passiert. Wie er nur noch funktioniert,
und das auch nur sehr gelähmt. Luna hat es ihm erklärt. Es ist sicher
keine Absicht von Paolo, aber wenn sich eine andere Person von Paolo
abgrenzt, so tut es Paolo sehr weh und er zentriert sich dann
mit seinem Schmerz so dermaßen, dass es schwer ist, die Grenze aufrecht
zu erhalten. Marcin gerät in solchen Momenten in einen Zustand, in dem
er einen großen Teil seiner Emotionen abschneidet und nicht mehr
wahrnimmt. Übrig bleibt eine irgendwie funktionierende Hülle, die ihn
möglichst sicher durch die Situation navigiert.

"Es kommen noch weitere unschöne Botschaften auf dich zu", sagt
Luna an Paolo gewandt.

Irgendwie schafft sie es damit, Paolo aus seiner Spirale zu reißen, obwohl
es sonst bloß Anerkennung und eine horrende Menge Trost tut. Paolo
blickt auf.

"Sonja möchte, dass ich dich ermorde", sagt Luna.

"Das ist völliger Bullshit. Sonja will, dass *ich dich* töte", widerspricht
Paolo.

"Weshalb du zu dem Unterfangen irgendwann in meinen Wald kommen wirst, weshalb
ich dich nach meinen Regeln ermorden würde", fasst Luna zusammen und
zuckt mit den Schultern. "Du hast nicht die geringste Chance, mich zu töten."

"Doch, habe ich!", widerspricht Paolo.

Léonide lacht los. "Hier am Tisch sitzen zwei Leute, die sich gegenseitig
töten möchten, und reden darüber. Sie meinen es ernst. Ich fasse es nicht!"

Luna hebt korrigierend einen Finger. "Ich möchte Paolo nicht töten", sagt
sie. "Sonja will, dass ich ihn töte. Sie sagte zu mir, sie dachte, ich würde
mich vielleicht freuen, wenn sie mir ein Opfer schickte, um dass ich es
nicht schade fände."

"Aber du fändest es um mich doch schade?", fragt Paolo.

Luna zerschmettert das frische Keimchen Hoffnung in Paolo wieder, doch
eine überraschende Wichtigkeit für sie zu haben. "Nein, kein Stück." Sie
holt tief Luft und seufzt. "Aber ich will mir auch nicht sagen lassen, wen
ich zu töten hätte. Was denkt Sonja sich dabei eigentlich?"

"Darf ich einen Erklärungsversuch wagen?", fragt Léonide.

"Das wäre mir willkommen, mylauden", lädt Luna ein.

Paolo wundert sich über die Höflichkeit, sagt aber nichts dazu.

"Ich glaube, Sonja möchte herausfinden, was du tust, wenn sie dir
einen Grenzfall bastelt", sagt Léonide.

"Also, sie schickt mir eine Person in den Wald, die da willentlich
und mit Absichten, die mich ärgern werden, hineinspaziert, aber
mir ist dabei bekannt, dass diese
manipuliert worden ist. Und sie will rausfinden, was ich tue?", fragt
Luna. "Meint ihr das, euer Lauden?"

Léonide zuckt mit den Schultern. "Es wäre, wenn ich mich so
an andere Erzählungen von dir über sie erinnere, nicht das erste Mal, dass
sie versucht, ein Psychospiel mit dir zu spielen. Eines, in dem sie eine
gewisse Macht gegen dich hat und dich in eine Zwickmühle bringt."

"Aber auf Kosten eines Jungspundes", grummelt Luna und wedelt in Paolos
Richtung. "Das ist schon neu."

"Es gibt nicht so viele Leute im Dorf, die sich leicht zu so etwas
manipulieren lassen", mutmaßt Léonide.

"Ich lasse mich nicht leicht manipulieren!", platzt es aus Paolo
heraus. Zu Léonide gewandt ergänzt er: "Euer Lauden."

Léonide schnaubt. "Du kannst den Formalkram gern weglassen. Duzen
ist mir eigentlich am liebsten. Ich bin Léonide. Bei Luna mache
ich dahingehend eine Ausnahme, weil wir uns nahe stehen."

Das ist auch mal interessant, denkt Kendra. Zwei Leute, die den Formalitätsgrad
zwischen sich *erhöhen*, weil sie sich nahestehen.

Paolo wird am Abend mit seiner Mutter kuscheln. Er wünscht sich so sehr,
Marcin nahezustehen. Oder vielleicht sogar Kendra. Irgendwann vielleicht. Oder
Sonja. Er glaubt eigentlich, Sonja nahezustehen. Er wird sie beim nächsten Treffen
zur Rede stellen und fragen, was an Lunas Gerede dran ist. Und er wird
sich nicht manipulieren lassen!

Angela aber liebt Paolo wirklich ohne irgendwelche Einschränkungen und
für immer. Egal was er tut. Egal was für Unsinn er anstellt. Sie ist
einfach da, mit ihren Armen, mit ihren warmen Worten, wann immer er es
braucht, und heute wird er es endlich mal wieder brauchen. Das gesteht
Paolo sich ein.
