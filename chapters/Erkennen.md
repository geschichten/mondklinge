\BefehlVorKapitelIBDM{Erkennen.png}{Eine digitale Fotokamera mit einem Rad für Einstellungen von Funktionen und mit Objektiv mit Ringen, an denen sich der Fokus oder der Zoom einstellen lassen.}{Reden über Tod.}

Erkennen
========

\Beitext{Kendra}

Das Zimmer ist klein und möbliert. Und was für Möbel: Das Bett und ein Regal sind
aus hellblau angestrichenem Holz mit einem Blumen-Schörkelmuster darauf. Durch
die verzierenden Rillen darin schwer staubzuwischen. Das Bett hat zwei urige Schubladen im Bettkasten mit
jeweils zwei messingartigen Knäufen, die aber so schwergängig sind, dass Kendra in ihrem Zustand
keine Chance hätte, sie zu öffnen. Allerdings macht das nichts, denn sie hätte sie nur
dann gebrauchen können, wenn sie etwas zu verräumen gehabt hätte, aber es scheitert
bei ihr schon daran, den eigenen Koffer
zu öffnen. Sie fällt einfach aufs immerhin bezogene Bett, hat vorher ein Fenster geöffnet,
um den abgestandenen Geruch durch den der Frühjahrskälte zu ersetzen, was vielleicht
ein Fehler ist. Sie wärmt bereits die Decke an. Ob sie nochmal aus dem Bett kommt, wenn das
Bett kuschelig warm und das Zimmer kühl ist, bleibt abzuwarten.

Kendra sortiert ihre neuen Erkenntnisse. Paolo ist also echt. Und zufällig die Person, bei der
ein Zimmer im Haus frei war. Ist das des Zufalls zu viel?

Einen Moment fragt Kendra sich, ob Paolo erst seit dem zweiten Traum existiert. Dagegen spricht,
dass sich hier niemand über ihn wundert. Auf der anderen Seite wundert sich hier im Dorf auch
niemand über einen schwarz nebelnden Drachen auf der Schulter einer Person, die einen Koffer
durch die Gegend zerrt. Eben genauso wenig, wie sich Paolo über Kendras Anwesenheit auf der
Bank gewundert hat. Kendra hat seine Geistesgegenwärtigkeit, ihr eine Adresse der Schule
zu geben, zunächst für zu abstruse Traumlogik gehalten. Tatsächlich hat sie wohl eher unterschätzt,
wie normal Magieausbrüche in einem Dorf mit einer Schule sein mögen, in der Magie unterrichtet
wird. Vielleicht bedingt sich das ja auch gegenseitig: Hier haben sich Leute mit Magie gesammelt, also gibt
es eine entsprechende Schule, also sammeln sich umso mehr Leute mit Magie. So etwa.

Paolo scheint man hier aber schon länger als zwei Wochen zu kennen. Wenn
Paolo also echt ist, wieviel spricht dafür, dass der Ort mit der Bank auch echt ist? Ist
sie Paolo in dessen Traum begegnet oder war sie nachts mit ihm an diesem Ort? Aber warum
sollte sich Paolo nachts in der Nähe eines Waldes auf einer Bank aufhalten? Das war, was
sie bei ihren letzten Überlegungen schon dazu gebracht hat, eher von einem Traum auszugehen. Nun
hinterfragt sie es trotzdem noch einmal.

Sie erinnert sich, dass sie von dem Ort schon einmal geträumt hat, -- oder dort gewesen ist --, und
ihr zu dem Zeitpunkt der Ort auch bekannt vorgekommen war, aber nicht so, als wäre sie
tatsächlich dort gewesen. Er ist ihr eher so bekannt vorgekommen wie von einem Bild, von einer Postkarte
vielleicht, die ihr geschickt worden ist. Hat sie im Internet schon einmal ein Foto
von dieser Klippe gesehen? Es ist ein schöner Ort. Hat sie vielleicht schon einmal
nach Schulen gesucht und ist bei der Recherche auf ein Foto der Klippe gestoßen? Und ihrem
Unterbewusstsein hat es so sehr gefallen, dass es ihr die Schule ausgesucht hat?

Es *ist* ein schöner Ort. Und es drängt sie durchaus, ihn wiederzufinden, ob nun in
einem Semi-Traum oder in der Realität. Sie verbindet ihn
mit einem bestimmten Gefühl, einer behaglichen Traurigkeit oder melancholischen Romantik
vielleicht.

Sie fängt an, ihre Browserhistory zu durchsuchen, aber noch während sie sich fragt, in welchem
Zeitraum sie nach Schulen recherchiert haben könnte, erinnert sie sich an eine ganz andere mögliche
Quelle. Ein Mastodon-Account, der bei ihr exakt dieses Gefühl melancholischer Romantik
auslöst, und den sie daher sehr mag. Gefunden hat sie ihn eigentlich, weil sie Aktivismus
marginalisierter Minderheiten lesen gewollt hat, und somit dieser eingewanderten trans maskulinen Person
gefolgt ist, die zudem eine magische Fähigkeit hat. Saine Posts über entsprechend
politische Themen sind zwar rar, aber Kendra ist dem Account ohne Zögern trotzdem gefolgt, weil
*Eska* Fotos, selbst gemalte Bilder und Musik postet, die sie sehr ansprechen. Eska ist
wahrscheinlich nicht der Klarname, und nun vermutet Kendra Marcin dahinter. Obwohl Marcins
Pronomen 'er/sein/ihm/ihn' sind und Eskas Pronomen 'as/sain/iem/as'.

Sie sucht in dem Account eine gute Stunde rückwärts, bis sie ein Foto genau dieser Klippe
findet. Sie klickt den Reply-Button, wählt aus, dass nur die getaggten Personen, also nur
Eska und sie, die Nachricht lesen können, und formuliert ins Textfeld:

> Dort habe ich mich aus Versehen hinteleportiert, nachdem ich dieses Foto
> gesehen habe. Wohl weil ich es schön fand. Also indirekt bist du wohl
> Schuld daran, dass ich jetzt hier bin.

Sie schickt es noch nicht ab. Das ist auch nicht unbedingt ihre Intention gewesen, -- erst
einmal so tun als ob und dann entscheiden, ob es sich gut anfühlen würde. Das tut es. Im
Prinzip. Nur hat Kendra Angst, dass es vorwurfsvoll rüberkommen könnte. Also fügt
sie hinzu:

> Es ist kein Vorwurf, eher etwas scherzhaft. Ich denke, wir kennen uns inzwischen, was meinst
> du?

Sie schickt es ab und badet einen kurzen Moment im Gefühl von Horrorszenarien, dass sie
es doch public gesendet hätte, dass es sich doch nicht um Marcin sondern um Paolo
handeln könnte (wäre das überhaupt schlimm?), oder um eine ganz andere Person (aber
in dem Fall lernt sie hier vielleicht einfach noch eine Person kennen), oder dass
Marcin sie dafür hassen könnte.

Warum will sie nicht, dass es Paolo ist? Paolo hat ihr zu essen gekocht, und das sogar
zweimal. Und was für Essen! Kendra hätte nicht erwartet, dass er, nachdem
er frustriert darüber gewesen ist, dass sie die vegetarische Pasta verschmäht hat, nicht
bloß eine schnelle Sache für sie nachgereicht hätte, sondern ein wirklich durchdachtes
Rezept mit sämiger Linsenpaste, selbstgemacht, gekocht hat. (Sie verheddert sich in
Gedanken in den Versuchen, ihre Anerkennung zum Ausdruck zu bringen. Sie
hat Paolo jedenfalls die Mühe und das Hobby
angemerkt.) Eigentlich hat Kendra nicht damit gerechnet, dass überhaupt für
sie gekocht wird. Sie wohnt zur Untermiete in einem Zimmer, Küchenmitbenutzung, sie hat
damit gerechnet, sich selbst kümmern zu müssen. Und sie ist sehr, sehr froh, dass
sie das heute nicht mehr muss.

Paolo hat sie ansonsten gemieden. Seine Mutter Angela hat ihr das Essen
hinaufgebracht. Angela ist alt und schön. Sie hat schwarzes, dickes Haar
mit einzelnen grauen Strähnen dazwischen, sodass es wie leicht versilbert
aussieht, furchtbar freundliche Runzeln und ein einladendes Gemüt.

Kendra möchte Paolo durchaus kennen lernen. Die Sache mit den silbernen Augen
fasziniert sie, überhaupt die ganze erste Begegnung. Vielleicht ist es sogar der
Grund, warum sie sich wünscht, dass Eska nicht Paolo ist: Sie wünscht sich
mit Eska eine weniger schicksalshaft anmutende Bekanntschaft. Oder sie kann sich einfach
vorstellen, dass die Fotos und gemalten Bilder, sowie die Musik, zu Marcin passen,
und sie möchte Recht behalten.

Sie erhält eine private Nachricht von Eska zurück. Kendra mag
Mastodon dafür, auf Posts privat reagieren zu können.

Eska schreibt:

> Ich glaube auch, dass wir uns kennen. Ich möchte dir gern Dinge schreiben, die
> mir zu persönlich für PNs auf Mastodon sind. Hast du signal? Oder
> *eska*liert dir das zu schnell?

In Klammern hat Eska saine Nummer hinzugefügt. Kendra gibt sie in ihren signal-Desktop-Client
ein. Das Smartphone ist zu weit weg und außerhalb des Bettes ist es zu kalt. Sie schreibt:

> Offenbar habe ich signal.

Eine einfallslose Nachricht, aber sie hat nicht so lange warten wollen, bis ihr etwas
Eloquenteres einfällt.

Eska fragt:

> Kommst du damit klar, wenn ich mit Türen in Häuser falle?

Kendra antwortet:

> Ich komme gut damit klar. Mir wäre aber vorher noch lieb, wenn wir uns
> einmal bestätigen, wer wir sind. Ich bin Kendra.

Eska bestätigt, dass er Marcin ist und eröffnet:

> Das Zimmer, in dem du schläfst, war früher Brans Zimmer. Meine Schwester.
> Bran ist tot.

Kendra schmunzelt, was wahrscheinlich eine denkbar unpassende Reaktion ist, aber
es sieht ja niemand: Das ist wohl etwas, was gesellschaftlich eher nicht per
Messanger verschickt wird. Sie findet durchaus sympathisch, dass Marcin kein
Problem damit hat, mit dieser Gesellschaftsnorm zu brechen. Es geht dabei sogar
um seine Schwester.

Der Name Bran kommt Kendra bekannt vor.

> Bran Wojtkowska? Die Physiotherapeutin?

Marcin bestätigt es.

> Shit, brauchst du Physiotherapie? Das ist hier gerade sehr rar. Es
> muss ein beschissener Anfang für dich hier sein.

Kendra schnaubt und schreibt:

> Ich werde schon klar kommen. Ich finde es ja schon ein wenig verkehrte
> Welt, dass du mir gegenüber Empathie zeigst, weil deine Schwester
> gestorben ist. Ist das der Grund, warum du gerade nicht in diesem
> Zimmer sein kannst?

Dieses Mal muss Kendra eine Weile auf eine Antwort warten. Als sie ungeduldig wird,
nutzt sie die Untätigkeit doch, um sich aus dem Bett zu quälen und das Fenster
zu schließen. Als sie wiederkommt, ist eine längere Nachricht von Marcin bei
ihr eingegangen:

> Es fällt mir sehr schwer einzuschätzen, ob ich es wirklich nicht kann. Ich
> würde es gern probieren, weil ich nicht mag, dass du die vielen Treppen
> steigen musst. Aber ich bin psychisch nicht sehr stabil. Wahrscheinlich
> ist es besser, es nicht zu riskieren. Kennst du diese Entscheidungen, die
> man über das eigene Leben fällen muss, zu denen es eigentlich viel zu
> wenige Daten gibt, um es sinnvoll tun zu können? Und liebmeinende Leute
> geben irgendwelche Ratschläge und du denkst, sie könnten ja Recht haben
> und dich besser einschätzen als du dich selbst?
> 
> Ich habe von Bran einiges gezeigt bekommen, was Physiotherapie angeht. Ich
> ersetze sicher keine Physiotherapieperson, aber falls du mir recht genau
> erklären kannst, was du brauchst, biete ich mich gern als Unterstützung
> an.
> 
> Ich habe das Gefühl, ich schreibe viel zu persönliches Zeug. Wie geht
> es dir damit?

Noch während Kendra antwortet, fügt Marcin hinzu:

> Überhaupt, wenn du irgendetwas brauchst, Wärmflasche, was zu trinken,
> veganen Süßkram, sag gern Bescheid. Ich muss mich bestimmt nicht
> vom Zimmer völlig fernhalten, ich kann dir was bringen.

Wärmflasche, denkt Kendra. Das klingt gut, aber auch luxuriös. Sie
ist selbst immer viel zu faul, sich eine zu machen. Aber Wärme könnte
gegen die Krämpfe helfen. Sie seufzt.

> Ich möchte gerade eigentlich lieber niemanden treffen. Und ich kann
> von dir vermutlich kaum verlangen, eine Wärmflasche vor meiner Tür
> abzulegen.

Sie grinst und schüttelt den Kopf über sich selbst. Wenn sie das abschickt, hat
sie es quasi getan.

> Ich weiß sehr genau, was für Krankengymnastik ich brauche. Muskalapsie
> ist keine verbreitete Behinderung. Ich habe es nicht nur einmal einer
> Vertretung erklärt. Wir können das probieren, auch wenn mir eine Fachkraft
> auf Dauer aus vielen Gründen lieber wäre.

Weil ihr erstmal nichts weiter einfällt, schickt sie die Nachricht ab.

> Ich lege dir eine Wärmflasche vor deine Tür und klopfe, wenn sie da ist. Wenn
> du ein bisschen wartest, bevor du sie abholst, bekommst du von mir nichts
> weiter mit.

Kendra hat fast Tränen in den Augen, als sie schreibt, dass sie einverstanden
ist. Ja, eine schmale Treppe zum Dachboden ist mit einer Gehbehinderung nicht
das Gelbe vom Ei und erst recht kein brauchbares veganes Ersatzprodukt dafür. Aber sie
wird in einer Weise umsorgt, mit der sie nicht gerechnet hätte. Sie ist nicht
allein, und sie muss sich nicht einmal gesellschaftskonform benehmen dafür.

Sie schreibt noch:

> Ich mag deine Kunst sehr!

Und dann noch:

> Ich komme mit dem Persönlichen gut zurecht, wenn du nicht entsprechend von
> mir erwartest, dass ich auch "overshare". Oversharen in Anführungsstricheln,
> weil ich nicht finde, dass du oversharest, aber du vielleicht.

Dann holt sie sich die Wärmflasche von vor der Tür und krümelt sich ins
Bett. Morgen ist erster Schultag. Und sie möchte rausfinden, wo
dieser Ort mit der Bank ist.
