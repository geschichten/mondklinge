\BefehlVorKapitelIBDM{SanftesEntrinnen.png}{Ein Halskettenanhänger in Form eines halben Herzens, in das ein M eingraviert ist.}{Suizid, Mord, Blut, Beißen, Herzprobleme, Atemprobleme, Erotik.}

Sanftes Entrinnen
=================

\Beitext{Bran und Luna}

Im Geisterwald. Wurzeln ragen aus dem fadenscheinigen Bodennebel. Es ist kalt. Auf
den herabgefallenen Laubblättern glitzert der Frost. Auf einem
umgefallenen Baum mit ebenso angefrorenem Moos darauf sitzt Bran. Ihr
frisch gewaschenes, äschernes Haar ist steif vor Kälte. Sie merkt
es kaum. Niemand, der den Wald betritt, kommt lebend wieder heraus, denkt
sie. Und doch ist es ihr dreimal gelungen. Sie hat niemandem davon
erzählt.

Durch den Nebel naht eine Gestalt. Uneilig. Schwarzes Spitzenkleid, fahlgraue
Haut, schulterlanges, schwarzes Haar, Seitenscheitel und die Spange, die
es an einer Seite zurückhält, in Form eines Schmetterlings. Wie immer.

Sie blicken einander an. Gegenseitiges Erkennen.

"Allmählich schwant mir, dass du dich nicht aus Versehen in diesen
Wald verirrst", sagt die Gestalt.

"Luna, nicht wahr?" Bran erinnert sich halb verschwommen an vergangene
Konversationen. Es ist nie angenehm gewesen. Bisher.

Luna nickt. "Möchtest du wieder hinausbegleitet werden?"

Bran schüttelt den Kopf. "Ich bin müde", sagt sie. "Und du bist auch
nicht zufällig zum vierten Mal hier, um mich aus dem Wald zu
führen."

"Nicht zufällig", bestätigt Luna. Sie sehen sich einen langen Moment
in die Augen. "Möchtest du sterben?"

Bran überlegt, ob sie es bejahen soll, aber zögert noch. "Wieso
überlebst du den Wald? Was weißt du über die Gefahr des Waldes?"

"Ich bin die Gefahr des Waldes", antwortet Luna. Es fühlt sich
selten so gut an, das auszusprechen. Etwas in ihr reißt sachte
ein. Sie mag den inneren Schmerz.

"Tötest du Menschen, die sich in den Wald verirren?", fragt
Bran weiter. Sie macht auf ihrem Baum Platz und, mit
einer zittrigen, unsicheren Handgeste, lädt sie Luna ein, sich
neben sie zu setzen. Diese Konversation hat jetzt schon einen
angenehmeren Charakter. Direkter. Weniger etwas sonst Unaussprechliches
umschiffend.

Luna atmet flach und voller Erwartung ein, langsam. Beim Ausatmen
tritt sie näher. "Keine Verirrten. Ich töte Menschen, die beabsichtigen,
mich hier zu stören." Sie dreht sich um und nimmt behutsam neben
Bran Platz. "Und Menschen, die schon lange sterben möchten."

Bran nickt. Erleichtert. Lächelt nicht. "Ich wollte schon beim ersten
Mal, als wir uns trafen, sterben. Endlich", sagt sie. "Ich habe mir
nicht ausgesucht, zu leben, und ich möchte mir aussuchen, zu sterben."

Luna betrachtet sie einfach. Entspannt und ruhig. Sie mag die
Aufgabe. Sie mag es, zu morden. Sie weiß, dass es Hoffnung geben
könnte. Vielleicht. Und doch sind ihr jene Morde am liebsten, die sich die
Verlorenen selber von ihr wünschen. Sie sehnt sich danach, die Hand
auszustrecken und das ergraute Haar zu berühren. Die Haut darunter. Aber
sie genießt die Erwartung noch ein wenig.

"Ich möchte durch meinen Tod niemanden traumatisieren", fährt
Bran fort. "Und ich traue mich nicht, selbst einen Abzug zu betätigen. Daher
hatte ich gehofft, in diesem Wald, nun ja, von einem Raubtier überfallen
zu werden oder von einer gespenstischen Macht ermordet." Brans Stimme
zittert ein wenig. "Tut es dir weh, zu töten?"

Luna hält es nicht mehr aus. "Darf ich dich berühren?"

Bran nickt verwirrt. "Ich hätte jetzt nicht damit gerechnet, dass
du mich aus der Ferne tötest. Wenn du es tust. Ich verheddere mich."
Die Zeile 'mit dem Schießgewehr' aus einem Kinderlied dröhnt ungefragt
und störend durch ihr Hirn.

Luna streckt die Hand aus und berührt Brans Haar, fühlt, nimmt
die Haptik in sich auf. Dann lässt sie die Hand wieder sinken
und lächelt. "So, meine ich."

Bran zuckt mit den Schultern. "Mir egal", sagt sie. Sie rückt
ein Stück näher an Luna heran und seufzt. "Es ist auch ganz
schön, das ist schon okay." Eigentlich mag sie es sogar, aber will
es nicht zugeben, weil sie zu oft gehört hat, dass sie ja
genießen könne, wenn sie von ihrem Todeswunsch gesprochen
hat. Zu oft versuchen Leute, sie zu überreden, doch leben zu
wollen, aber sie ist all die Überlegungen schon durch. So
oft. Zu oft. "Beantwortest du mir meine Frage noch?"

Luna nickt und streckt die Hand abermals aus. Dieses Mal
streicht sie das halblange Haar in den Nacken, berührt
Brans Hals oberhalb des Halstuchs zart mit den kühlen Fingern. "Es
tut weh, aber ich mag den Schmerz", antwortet sie ehrlich. "Ich
genieße ihn."

Bran atmet rascher. Ein
Gefühlsgemisch von Erleichterung und Aufregung durchströmt sie. Letzteres
verwirrt sie noch. Es löst außerdem den unangenehmen Gedanken aus, dass sie
rascher atmen würde, um sich an ihre letzten Atemzüge zu klammern.

Die Sache ist die, dass sie durchaus Angst vorm Sterbevorgang selbst
hat. Aber nicht vorm Tod. "Magst du mich", -- sie zögert und
entscheidet sich für die klanglich sanftere Vokabel --, "ermorden?"
Das Wort 'töten' hat zu scharfe Konsonanten. "Sanft?" Wieso
hat sie das hinzugefügt?

Luna lächelt und nickt. "Und zärtlich?", fragt sie.

Bran sieht in ihre dunklen Augen. Und nickt. "Du siehst so
aus, als wolltest du mich küssen." Sie spricht es aus und
fühlt sich unverschämt dafür. So etwas kann man Leuten nicht
ansehen. Aber es ist alles egal. Luna wird damit schon umgehen
können.

"Möchtest du?", fragt Luna überrascht. Das ist selten, dass
Menschen zugleich getötet und diese Form von romantischer
Zuneigung haben wollen.

Brans Kopf zuckt nur kurz, aber es ist als Nicken für Luna
erkennbar. "Vielleicht muss ich dann weniger denken", gibt
sie zu, und hat gleich Angst, dass Luna das diskutieren möchte.

Aber Luna rückt näher an sie heran, berührt Brans ihr abgewandte Wange
mit den so angenehm kühlen Fingern. Sie beugt sich vor und legt ihre
kalten Lippen auf Brans warme. Bran lehnt sich vorsichtig
in den Kuss, spürt, wie Lunas Lippen sich auf ihren weich
bewegen.

Interessant, denkt Luna. Sie hofft, dass es Bran gefällt. Für sie
fühlt es sich brauchbar angenehm an, aber mehr auch nicht. Sie
löst die Lippen für einen kurzen Moment, dann drückt sie ihre wieder
auf die fremden, fädelt die kühlen Finger in das Haar an der Seite des
Kopfs. Sie lauscht auf den rascher werdenden, fremden Atem und das
ist, was sie schließlich catcht. Mit der Zungenspitze streicht
sie über die anderen Lippen, bis die Haut noch weicher ist, und beißt
dann sehr vorsichtig in Brans Unterlippe.

Bran atmet erschreckt auf, aber hält still, mit zitterndem Atem, als
Luna ihr das Blut von den Lippen leckt. Luna beißt kein zweites
Mal, aber löst sich eine ganze Weile nicht davon, Bran das frische
Blut von den Lippen zu lecken und das Beben des Körpers vor
sich zu erfühlen. Sie rückt noch näher an Bran heran, bis sich
ihre Körper fast berühren, und lässt dann von ihren Lippen ab. "Möchtest
du immer noch sterben?", fragt sie.

"Ja", antwortet Bran ohne Zögern. "Bitte!"

"Gefällt dir die Todesart bislang?", fragt Luna.

Bran nickt, und unromantischerweise schlägt dabei ihre Stirn
gegen Lunas. Sie zittert und ignoriert das Ungeschick. "Was
muss ich tun, damit es dir gefällt?", fragt sie. "Soll ich
dich auch anfassen?"

"Du musst nichts und darfst alles", erklärt Luna.

"Darf ich in deinen Armen sterben?", bittet Bran.

Luna legt als Antwort die Arme um sie. "So hatte ich mir das auch
vorgestellt."

"Sterbe ich an Blutarmut?", fragt Bran.

"Ich denke, das heißt so", antwortet Luna nachdenklich. "Also, ich
gedenke, dich auszutrinken. Vermutlich knapp zwei Liter? Bis dein
Körper kollabiert und nichts mehr hergibt."

"Haben Menschen nur zwei Liter Blut?", fragt Bran verwirrt. Eigentlich
ist es ihr egal.

"Es ist unmöglich, alles auszusaugen. Es gerinnt und irgendwann mangelt
es an Blutdruck", erklärt Luna. "Möchtest du mehr Details wissen?"

Bran schüttelt den Kopf. "Eigentlich nicht. Ich mochte die Stimmung
vorher."

Luna dreht Bran in ihren Armen, sodass Bran halb mit dem Rücken an ihr
lehnt. Mit dem einen Arm umschließt sie den Brustkorb und hält Bran
fest, unnachgiebig und zugleich sanft. Die andere Hand führt sie wieder
durch Brans Haar. Sie sortiert es auf eine Seite. Dann beginnt sie uneilig
das Halstuch zu entknoten, aber Brans Hände kommen ihr zuvor.

Bran löst es und lässt es in ihren Schoß sinken. "Was passiert mit meiner
Leiche?", fragt sie.

"Traditionell verbrenne ich sie", antwortet Luna. "Die Asche lasse ich aufs
Meer hinauswehen. Dabei nehme ich Abschied. Möchtest du es anders?"

Bran zittert noch einmal und niemand der beiden weiß, ob es vor Kälte oder
durch die Berührung der Worte kommt. "Das ist schön. Das möchte ich."

Luna fädelt die Finger in Brans Haar seitlich am Kopf und dieses Mal
fixiert sie ihn mit festem Griff. Sie berührt die Kehle mit
ihren Lippen. Eine sehr gute Haltung. Ihr Kopf
passt genau dort hin.

Bran kann sich kaum mehr rühren und der Atem zittert wieder. "Ich habe
keine Fragen mehr", sagt sie.

"Dann lass los", flüstert Luna gegen ihren Hals. Sie spürt, wie der
Körper unter ihr entspannt, als hätte er auf diese Aufforderung
gewartet. Nun hält Luna das Leben dieser Person in ihren Armen und
wird es ihr entziehen.

Sie atmet rascher, berührt die Haut des Halses wieder mit den Lippen. Alte
Haut. Luna mag Haut, ob alt oder jung, alles davon. Sie schließt die
Augen und formt noch einen Kuss auf dem Hals aus, bevor sie die Zähne
auf der Hauptschlagader platziert. Sie spürt, wie der Körper in ihren
Armen stärker zittert, fast bebt. Diese interessante Kreuzung aus Lust
und Todeswunsch. Der sehnliche Wunsch, verzehrt zu werden.

Bran gibt einen kurzen Laut von sich, als die Zähne einen halben Milimenter
in ihre Haut eindringen. So kontrolliert, zart und sanft. Und dann lösen sie
sich doch viel zu schnell wieder. Es fühlt sich für sie an, als würde
ihr der Boden unter den Füßen weggezogen, der da das erste Mal
seit Jahrzehnten überhaupt war.

"Ich führe das gleich zu Ende", verspricht Luna. "Wie heißt du?"

"Bran", antwortet Bran. Sie möchte, dass Luna fortfährt. Erst, als
Lunas Zähne wieder an Ort und Stelle sind, fühlt sie sich wieder
orientiert. Gehalten. Als wäre die Welt wieder in Ordnung. Endlich.

Luna lässt sich in das Gefühl fallen und drückt ihre Zähne durch
die Haut, dieses Mal, bis sie den Blutfluss kontrollieren
kann. Es strömt von selbst aus den Wunden in ihren Mund. Die
ersten zwei Male, als sie
schluckt und sie das Rot in ihren Körper aufsaugt, fühlt es sich
noch bloß wie ein Versprechen an, das gebrochen werden könnte. Ein
süßes Versprechen, das sie sehnsuchtsvoll genießt. Aber
sie fühlt, wie Bran sich hingibt. Sie zieht den Körper noch fester
in ihre Umarmung, atmet schneller. Brans Brustkorb hebt und senkt
sich zitternd. Noch voller Kraft.

Luna hat ihr nicht gesagt, wie lange es dauern wird. Aber das macht
nichts. Sie spürt, wie Bran es mag. Wie Bran nicht losgelassen werden
möchte, während ihr Leben aus ihr herausrinnt. Das Mondlicht bricht
durch die Wolken, wirft geisterhafte Schatten auf sie, während Luna
trinkt. Ihr wird allmählich wärmer, während Bran an Wärme verliert. Lunas
Körper braucht vom Blut nur die Inhaltsstoffe, nicht die Flüssigkeit,
und beginnt diese auszudampfen. Der Dampf gesellt sich zum Bodennebel.

Dann, es mag eine Viertel- bis Halbe Stunde voller Genuss vergangen sein, -- Genuss
des Vergehens --, lässt Brans Kraft allmählich nach. Luna merkt, wie
sie den Blutfluss konstant halten möchte und deshalb mehr saugt. Sie
zittern beide, vor Erregung, -- und es ist nicht unbedingt eine sexuelle. Vielleicht
eher die Erregung des Wünscheerfüllens. Brans Wunsch zu sterben und Lunas Wunsch
zu morden. Wieder reißt etwas in Luna, als sie an Brans Schwäche und
Pulsaussätzern merkt, dass die Entscheidung nicht mehr zurückgenommen
werden kann. Sie hält Bran nun sanfter. Ihre Hand, die nicht den Kopf
fixiert, sucht nach einer von Brans. 

Bran nimmt Lunas lauwarme Hand schwach entgegen. Wie um 'Danke' zu sagen. Sie will
es auch sagen, aber sie kann kaum denken und spürt ihren Körper
nur gedämpft, oder verwirrend anders. Ob es schmerzen sollte? Ob Luna irgendetwas
tut, damit es nicht weh tut? Oder ist es einfach die Zärtlichkeit und Erotik, die jeden
Schmerz verdrängt. Bran schmeckt Blut auf den Lippen, dort, wo
Luna sie gebissen hat. Sie mag den Geschmack. Bevor ihre Gedanken
ganz wegkippen, nimmt sie noch einmal alles an Leben, was sie noch
hat, zusammen und sagt es: "Danke."

Luna sagt nichts, schon allein, weil ihre Zähne in Brans Hals stecken
und sie sie jetzt bestimmt nicht noch einmal herausziehen möchte, bevor
Bran stirbt. Aber ihr Biss wird weicher. Tränen sammeln sich in ihren
geschlossenen Augen. Brans
Dank, und dass sie es auf sich genommen hat, ihn auszusprechen, berühren
Luna zutiefst. Sie zieht noch ein paar tiefe Schlucke genussvoll in sich
ein, und dann, den nächsten sehr langsam. Den, bei dem sie weiß, dass
es bald soweit ist. Bran atmet kaum mehr. Noch ein langsamer, tiefer
Schluck und das Herz versagt. Und Luna durchströmt ein Gefühl von
Liebe und roter Trauer. Sie verharrt andächtig einen Moment, lässt
die tote Hand los, streicht noch einmal über die Wange, bevor sie
weitertrinkt, bis der Körper nichts mehr hergibt. Sie leckt über die
Halswunden, als sie fertig ist, die nicht mehr heilen werden, und
behält den toten Körper noch eine Weile einfach im Arm.

Bran. Was für ein schöner Name. Was für eine mutige Person. Wieviel
Liebe in einem Tod stecken kann. Danke, denkt Luna.
