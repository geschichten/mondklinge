\BefehlVorKapitelIBDM{EinScheiterhaufen.png}{Eine Tasse mit Tee darin. Die Tasse hat ein altmodisches Design mit Blümchen und eigentlich ist sie prädestiniert dazu, auf einer Untertasse mit gleichem Muster zu stehen, aber das tut sie nicht.}{Stalking, Tötungsabsichten, Einbruch, Fesselung.}

Ein Scheiterhaufen
==================

\Beitext{Sonja, Léonide, Marcin, Luna, Angela}

Die selbe mondscheinhelle Nacht. Wind, der dünne Wolken über den
Himmel fegt, bevor das erste Morgengrauen sie rosa anfärben wird. Lauden
Léonide von Horstenfels' Villa ist nicht so zugewuchert wie Lunas Haus
im Wald. Sie liegt auch nicht in einem unheimlichen, kühlen Geisterwald,
sondern zwischen den anderen Häusern im Dorf. Aber derzeit ist es fast
genau so ruhig.

Sonja, in Fuchsform, nähert sich dem Gebäude über den Garten. Das Haus
schläft. Nur eine Lampe erhellt ein Zimmer im oberen Stockwerk. Marcins
Zimmer, denkt Sonja. Paolo hat ihr beschrieben, wo Marcin jetzt wohnt. Marcin
kann wohl nicht schlafen, weil er so besorgt um Paolo ist, mutmaßt
Sonja.

Sonja hüpft am Efeu entlang einen verzweigten Baum neben dem Wohnzimmer
hinauf auf das Dach. Es verursacht kein Geräusch. Beziehungsweise ein so
leises, dass höchstens Luna es hören würde. Sonja weiß nicht, wie Lunas
Sinne funktionieren. Vielleicht ist es auch mehr ein Fühlen. Sie weiß nur,
dass es im Wald um vieles besser funktioniert als im Dorf.

Sonja fühlt sich leicht ängstlich, aber sie weiß, dass sie eigentlich nicht
von Leuten entdeckt werden kann, von denen sie nicht entdeckt werden will. Luna
ist nicht in der Nähe, sondern im Wald mit ihrer Beute beschäftigt. Der Klang
der Mondklinge sirrt noch in Sonjas Körper nach. So angenehm. So gern würde
sie wieder, wieder in Lunas Armen liegen, wenn sie ihn hört, und dabei ihr zum
Opfer fallen. Aber derzeit glaubt sie, dass Luna ihr den Ton nie wieder vorspielen
wird. Ihr nicht. Sadistisches Aas, das sie ist.

Sonja balanciert über eine Fensterbank direkt am einzig noch leeren Zimmer
vorbei, das irgendwann vielleicht von Kendras Mutter bezogen wird. Dann kommt der
schwierige Teil: Der Sprung zum Fenster mit dem Licht, in dem Marcin ist. Sonja
peilt, atmet, springt, und... verheddert sich. Das ist ihr noch nie passiert. Sie
strauchelt, versucht sich zu halten. Irgendwas stimmt hier nicht, etwas ist anders
als sonst. Es ist glatt wie Eis. Eine Schlinge, von der sie nicht versteht, wo sie
herkommt, schließt sich um ihren Fuß, als sie in die Tiefe stürzt und von selbiger
aufgefangen wird. Sie baumelt am Seil, kommt nicht rauf, kommt nicht runter,
strampelt. Sie überlegt, sich in die menschlichere Form zu wandeln, aber hat Angst,
dass die Schlinge ihr dann völlig den Fuß abschnürt, weil sie ihren dünnen Fuchsfuß
eng umschließt.

Gerade als sie sich doch dazu entscheiden möchte, geht das Licht im Fenster hinter ihr
an. Lauden Léonide von Horstenfels tritt in einem Nachtgewand vor die Scheibe und beobachtet
Sonja beim Schaukeln.

As öffnet das Fenster. "Wenn du willst, dass ich dich losmache, halt still!", befielt as.

Sonja braucht noch einen Moment, bis sie sich davon abhalten kann, panisch Halt
zu suchen. Als sie endlich versteht, nimmt Léonide sie in saine Arme, als wäre as
geübt in so etwas, löst die Schlinge, und wirft den Schneefuchs auf sain Bett. Ehe
Sonja verschwinden kann, schließt Léonide das Fenster.

Sonja wandelt sich nun endgültig in ihre menschlichere Form. Sie fühlt sich so
verletzlich in einem fremden Haus auf einem Bett in einem Zimmer dieses Laudens, nicht
im Zimmer von Marcin. Sie kennt das Lauden doch gar nicht. "Es war eine Falle", flüstert
sie das erste, was ihr in den Sinn kommt. Sie ist noch nicht ganz aus dem
Schneefuchsdenken zurück.

"Ich weiß", sagt Léonide bloß. "Ich habe sie selbst installiert."

Sonja starrt as an. "Was? Warum?"

Léonide nimmt erschöpft auf sainem Schlafzimmersessel Platz. "Eine Vorsichtsmaßnahme", sagt
as. "Du hast Paolo dazu manipuliert, eventuell in den Wald zu gehen. Ich habe mich
einfach gefragt, was du als nächstes probieren wirst. Und da war naheliegend, dass du
dich an Marcin wagst. Er ist nicht so leicht zu manipulieren, aber du hast bei ihm vermutlich
die besten Chancen, ihn in den Wald zu locken, indem du ihm sagst, Paolo wäre da."

"Für so bösartig hältst du mich?", fragt Sonja.

"Ja, ich denke schon", antwortet Lauden Léonide von Horstenfels auch noch.

Sonja blickt as mit zusammengekniffenen Augen an. Sie kennt as nicht, aber
bei dieser Reaktion erschließt sich ihr sofort, warum Luna as mag. Sonja
seufzt. Wenn sie sich wünscht, dass Luna sich mit ihr irgendwann verstehen
soll, dann muss sie sich wohl mit Lunas Herzenspersonen verstehen. Und das geht
wahrscheinlich nur über diese merkwürdige Form von Direktheit oder Ehrlichkeit,
die sich Sonja noch nicht ganz erschließt. "Ja, bin ich wohl."

"Huch?", macht Léonide verwundert. "Traust du dich nicht, mir gegenüber
alle Kaliber von Taktik zu ziehen, um zu kriegen, was du willst?"

"Ich will Luna", entgegnet Sonja. Gefühlsachterbahn. Ein erbärmliches Häufchen
Elend ist sie doch.

"Luna gehört sich selbst", sagt Léonide. "Sie wird niemals jemandem gehören."

"Aber sie mag dich", sagt Sonja. "Sie hat irgendeine Art Freundschaft oder
Bindung mit dir. So etwas reicht mir schon. Denke ich."

"Hm", brummt Léonide. "Setzen wir uns ins Wohnzimmer? Ich ziehe mir einen
Morgenmantel an und du machst uns Tee? Mir ist nämlich recht frisch
außerhalb meines Bettes."

Sonja nickt. "Ich, ja", sagt sie. "Ich habe eigentlich überhaupt keine
Chancen, dass Luna sich je für mich interessieren wird, oder?"

"Eins nach dem anderen." Léonide lässt sich nicht aus der Ruhe bringen.

---

Als Sonja in der Küche hantiert, taucht Marcin doch auf, und zwar nicht von
oben, sondern aus Kendras Zimmer. Wenn sie von Paolo nichts Genaueres wüsste,
was sie eigentlich nichts angeht, hätte sie nun vielleicht gedacht, Marcin
ließe nichts anbrennen, -- und schämt sich für den Gedanken. Über all die
Jahrhunderte Leben hat Sonja die Gelegenheit doch nicht genutzt, sich die
Diversität unter den Menschen genauer anzusehen. Misanthrop von den Haarspitzen
bis in die Zehen ist sie. Menschen sind ihr einfach zuwider.

"Ich habe wohl die Absicht gehabt, Luna dich auch umbringen zu
lassen", gibt sie ihm gegenüber zu. Es wird durch Lauden Léonide von Horstenfels
gleich ohnehin herauskommen.

"Das überrascht mich nicht", sagt Marcin.

Auf seiner Schulter sitzt ein kleiner, schwarzer Drache, den Sonja eben noch nicht
gesehen hat, und faucht sie an. Er erinnert sie an Luna. Sie muss lächeln.

Auch Kendra sitzt schlaftrunken mit am Tisch, als Sonja und Marcin den Tee
liefern, -- und einen Kakao für Kendra. Die zwei sehen schon ein wenig aus
wie Liebende, findet Sonja. Sie versucht, keine Schlüsse zu ziehen.

"Kommen wir direkt zur Sache", sagt Léonide. "Du möchtest eine Chance haben, mit
Luna eine ungefähr beliebige Beziehung anzufangen, solange es keine reine Hassbeziehung
ist. Soweit richtig?"

Sonja lächelt resigniert. "So etwa, ja. Eine, in der sie mich mag, wäre optimal. Oder, Moment,
das wäre das beste, was ich zu hoffen wage. Optimal wäre, wenn sie mich mindestens so mag
wie euch. Vielleicht ein bisschen liebt." Sie seufzt. "Aber ich mache mir da nicht viele
Hoffnungen. Ich spiele Spiele mit ihr, die sie in die Enge treiben. Weil ich mal nicht
durch sie in die Enge getrieben sein will." Sonja verheddert sich in Gedanken fast so
sehr wie in dem Seil vorhin. "Es ist eine lange, komplexe Geschichte."

"Ich lasse das mal so im Raum stehen", sagt Léonide. "Das andere Thema ist Paolo. Dass
du gerade heute Nacht zu Marcin wolltest, legt nahe, dass Paolo im Wald ist."

Sonja seufzt sehr leicht. "Ja, deshalb bin ich hier. Er befindet sich in Lunas Fängen. Und sie
lässt sich offenbar Zeit. Beziehungsweise ließ sich zumindest zu dem Zeitpunkt noch
Zeit, als ich noch in der Nähe war."

"Ich fasse dein Verhalten mal etwas unverblümter zusammen, ja?", mischt sich
Kendra ein. "Du legst es in deinen Konversationen mit Paolo darauf an, dass
er irgendwann in den Wald spaziert, um Luna zu töten. Mir erschließt sich noch
nicht ganz, warum er dann an einem Abend in den Wald spaziert, an dem Luna
vermeintlich die Nacht hier verbringen wird, aber sei das mal kurz
dahingestellt. Vielleicht muss sich Paolo auch sicher genug fühlen, um es zu
tun. Deine Absicht dabei ist aber eigentlich, dass Luna ihn entweder tötet, oder
in die Bredouille gerät, eine Person nicht zu töten, die in ihren Wald
kommt, um sie zu ärgern. Damit nicht genug beobachtest du Paolo oder Luna
so lange, bis du herausfindest, dass Paolo dort
ist. Und dann gehst du zu Marcin, um ihm von deinen Beobachtungen zu
erzählen. In der Absicht, dass auch er zu Luna in den Wald geht, was sie
in eine noch komplexere Bedrullie bringen wird, weil sie Marcin sogar
sympathisch findet."

Sonja blickt Kendra eine Weile an. "Ja", gibt sie zu. "Da lässt sich nicht
viel dran schönreden." Sie seufzt. "Ich habe Paolo erzählt, dass die Mondklinge
in Lunas Wald liegt und das einzige Instrument wäre, das sie töten könne. Die
Mondklinge ist dort, aber sie ist nicht dazu da, irgendwen zu töten, und ich
hätte die Mondklinge gern für mich. Es ist so eine Art Musikinstrument."

Marcin horcht auf und grinst sofort über sich selbst. Er hat bisher sehr
wenig gesagt.

Obwohl es um seinen Freund geht. Getrennt oder nicht, sie
haben sich gegenseitig sehr gern. Hat Sonja bis jetzt zumindest geglaubt. Und
wenn Menschen sich mögen, wollen sie sich meistens irgendwie retten.

"Jedenfalls denke ich, dass ich die beste Strategie für uns alle habe, zu
erreichen, was wir jeweils wollen", sagt Léonide. "Luna lässt nicht gern
mit sich spielen. Also wird sie nicht glücklich darüber sein, wenn Marcin
als nächstes den Wald betritt. Es ist am sinnvollsten, wenn das eine Person
tut, die Luna nicht töten kann."

"Ich", schaltet Sonja sofort. "Aber..."

"Du kannst zu ihr gehen und ihr klar sagen, was du willst. Meiner
Erfahrung nach ist mit Luna am besten Kirschen essen, wenn man ihr
erklärt, welches Machtgefälle existiert und gemeinsam angegangen
werden muss", führt Léonide fort, ohne Sonja
zu Wort kommen zu lassen. "Und du kannst
als einzige Person von uns überhaupt zu einer gewissen Wahrscheinlichkeit
Paolo retten. Wenn es noch nicht zu spät ist."

Kendra hätte mit ihren Teleportationsfähigkeiten vielleicht eine
Chance, aber dass diese noch nicht ausreichend ausgebildet sind, wissen
sie alle.

"Luna hat mir versichert, dass, wenn sie die Absicht haben sollte, Paolo etwas
anzutun, sie es ab dem Moment nur um so schneller tun werde, wenn ich
den Wald betrete", sagt Marcin. "Mit anderen Worten, *ich* kann nichts für
Paolo tun. Daher würde ich dich bitten, zu sehen, was du tun kannst."

Sonja wägt den Vorschlag ab. Sofort fragt sie sich, was für sie bei dem
Plan rausspringt. Aber vielleicht sollte sie sich das nicht fragen. Viel
bohrender ist die Frage, was sie dabei verliert. Sie hat dann ein Spiel
gegen Luna aufgegeben. Ist ihr unterlegen. Sie will ihr nicht in allen
Punkten unterlegen sein. Sie will ihr in *manchen* Punkten unterlegen
sein. Und auch das zerreißt sie innerlich oft. Weil sie nicht bloß ein
Opfer für Luna sein will. Sie will auch selbstbestimmt sein. Luna ist
so einschüchternd.

Sie seufzt. "Ich tue es." Es ist ein großer, mutiger Schritt von ihr. "Wenn
Luna mir ausweicht und nicht mit mir reden mag, dann", sie zögert, "ich
will sie eigentlich nicht bedrängen." Sie löst schweren Herzens die Spange
aus ihrem Haar und legt sie auf den Tisch. "Sagt ihr 'Danke' von mir, für
das große Geschenk, das dieser Teil von ihr eine Weile bei mir sein durfte."

Sie weint ein wenig, als sie die Villa verlässt.

---

Ein schiefes Reihenhaus, das mal sehr lebendig war, im Dorf. In einem Zimmer
brennt noch Licht. Ein Sorgenseufzen so tief, dass es das ganze Haus
spürt.

Luna bleibt aus Respekt noch einen Moment stehen, bis das Seufzen sich verloren
hat. Erst dann nähert sie sich dem Schlafzimmerfenster im ersten Stock
über das Mäuerchen zum Hof und klopft sachte.

Angela springt erschreckt von ihrem Tisch auf, atmet einige Male zur Beruhigung
ein und aus und tritt ans Fenster. Ihre Miene verliert wieder an Hoffnung. Es ist
nicht die Person, auf die sie gehofft hat. Sie öffnet das Fenster trotzdem. "Wenn du ihn
umgebracht hast, werde ich einen Weg finden, um es dir heimzuzahlen. Verlass
dich darauf!", schleudert sie der Vampirkreatur entgegen.

"Ich weiß", sagt Luna schlicht.

Einen Moment sehen sie sich einfach bloß an. Dann macht Angela Platz und Luna
springt von der Mauer durchs geöffnete Fenster. Sie landet leichtfüßig vor
Angela. Dicht. Spürt die Schwere ihrer Gefühle. Sie hält sich mühsam davon
ab, sie zu berühren. Es wäre eine Berührung mit Trost darin geworden, aber
auch mit Mordlust. Stattdessen schließt sie das Fenster. Sie tut es langsam
und betrachtet noch ein paar Momente den verwahrlosten Hof, um Angela den Raum
zu geben, für sich zu weinen.
