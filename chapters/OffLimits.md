\BefehlVorKapitelIBDM{OffLimits.png}{Ein Eimer.}{Vergiften, Ausbluten, Fäkalien, konsensuelles Foltern, Blut, Beißen, Fesseln.}

Off Limits
==========

\Beitext{Gemischt}

So viel los im Wohnzimmer. Grünliches Licht fällt durch die Fensterfront
herein und flutet den großen Raum. Léonide sitzt erschöpft im Ohrensessel und
schaut hinaus auf die im Nordsturm raschelnden Blätter des ganzen Gestrüpps
da draußen. Luna und Marcin packen das Paket aus und Kendra telefoniert. Letzteres ist
vielleicht das störendste für Léonide. Erschöpft ist as, weil as gerade draußen war, um
dieses riesige Paket entgegenzunehmen, beziehungsweise dafür zu unterschreiben, dass
as volljährig ist. Physisch entgegengenommen hat es Luna. Es enthält eine Liege
aus dem BDSM-Bedarf mit sehr stabilen Ösen für metallene, gepolsterte Schellen.

Kendra klickt das Telefonat weg. "Die Liege ist zu hart."

Luna drückt einen Finger in das Kunstleder. Es gibt nach. "Ich mag vorsichtig
daran erinnern, dass du gedenkst, mich zu foltern. Ich mag es weich."

"Die Liege ist erst einmal nicht direkter Teil des Folterns, sondern dafür
da, dass du fixiert bist", widerspricht Kendra. "Leg dich mal drauf."

Marcin zieht die letzte Schraube fest, die den Rahmen stabilisiert
und entfernt sich einen Schritt.

Luna legt sich behutsam auf die Liege. Sie ist eigentlich ganz gemütlich.

Kendra rappelt sich vom Sofa hoch und geht zur Liege. Sie umfasst Lunas
Handgelenk an einer der Stellen, wo es später eine Schelle tun wird. "Versuch,
dich dagegen zu stemmen", sagt sie.

Luna runzelt die Stirn. "Ernsthaft? Also, ich gehe davon aus, dass du weißt,
dass du für mich keinen Widerstand darstellst. Wozu das Ganze also?" Sie
tut es trotzdem. Nicht mit übertrieben viel Kraft. Sie drückt mit kaum mehr
als der Kraft zum Keksteigkneten gegen Kendras wehrlose Hand.

In dem Augenblick in dem sie es tut, berührt Kendras andere Hand sie in der
Gegend ihres Ellenbogens auf dem Polster. "Du brauchst den Gegendruck dort,
um dich gegen meine Hand zu wehren. Du wirst ihn erst recht brauchen, um dich
gegen eine Schelle zu wehren. Daher sollte es hier extrem weich sein."

Luna lässt den Arm sinken. "Du gefällst mir so", sagt sie. "Was sagt deine
Mutter zu der Idee, mit einem Vampir in eine WG zu ziehen?" Das Gespräch eben
mit Kendras Mutter war dazu dagewesen, eben jenes abzuklären.

"Sie ist überrascht, dass ich doch so schnell eine Möglichkeit gefunden habe",
berichtet Kendra. Sie wirkt aufgewühlt. "Sie
fragt mich, ob ich noch einen Monat ohne sie auskomme. Sie hat ein Date. Und sie
möchte rausfinden, ob sie mit dem Date zusammenziehen ausprobieren möchte oder
nicht. Zu der Sache mit dem Vampir ist sie sich noch nicht ganz sicher. Sie
hat weniger Probleme damit, dass du ein Vampir bist, als damit, dass du Menschen
auf dem Gewissen hast."

"Wie fühlst du dich damit?", fragt Marcin. Es ist das erste, was er seit Langem
sagt, aber er wirkt auf Luna deshalb nicht still. Das ist interessant.

"Eigentlich freue ich mich über die Sache mit dem Date. Das wollte sie schon
lange und hat sich meinetwegen nicht getraut", erklärt Kendra. "Trotzdem
fällt es mir, glaube ich, schwer, damit gerade jetzt umzugehen." Sie blickt lange
in Marcins ruhiges Gesicht. Schließt die Augen, atmet, öffnet sie wieder. "Ich
vermisse Umarmungen von Menschen, die ich mag."

"Ist Vampir statt Mensch auch okay?", fragt Luna. "Oh, und ich weiß gar nicht, ob du
mich magst." Sie richtet sich wieder auf.

Kendra nickt, drückt Luna zurück auf die Liege, -- was sie natürlich nur kann,
weil Luna das zulässt --, und kuschelt sich zu ihr darauf. Sie *müssen* auf
diese Art kuscheln, es wäre für alles andere zu eng. "Ist das in Ordnung für dich?"

Luna schließt die Arme sanft um Kendra. "Es ist in Ordnung für mich." Den ungebetenen
Gedanken, dass sie auch noch wärmen könnte, wenn sie Kendra nur ein bisschen aussaugen
würde, verdrängt sie rasch. Es geht hier um Support, eine Art von Support, bei der
Erotik hinderlich wäre.

Es braucht für Kendra eine Weile, bis sich der Knoten in ihr löst, der sich da
untergründig vor sich hingebildet hat, weil ihr Bedürfnis nach Zärtlichkeit zu
wenig gedeckt gewesen ist. Aber schließlich fängt sie an, sich zu entspannen. In
den Armen einer Mörderin, die sie gedenkt, konsensuell schwer zu verletzen... Es
klingt absurder, als es ist.

---

Während Kendra und Luna sich darüber Gedanken gemacht haben, was sie noch brauchen, um
Luna sicher zu fixieren, hat Marcin sich Notizen gemacht. Dann hat er die Fahrt
zum Baumarkt und zurück mit dem Fahrrad auf sich genommen, um in einem großen Rucksack
den Satz zusätzlicher Schellen, Schrauben und Muttern zu besorgen, sowie Schaumstoff, den
er auf dem Gepäckträger mit Spanngummis befestigt hat. Als sie sich Gedanken
gemacht haben, wie sie es genau haben wollen, muss also jemand bohren. Niemand
der Jungspunde traut sich so richtig an die Bohrmaschine ran, also macht Léonide es
schließlich. Ein bisschen ist as stolz darauf. Das erlauchte Lauden vom Dorfe
macht die technische Arbeit. Aber das ist natürlich albern.

Als alles fertig ist, testen sie aus, ob Luna sich selbst befreien kann, nachdem sie
sie dort festgeschnallt haben. Léonide sieht Luna an, dass sie die Erfahrung, mal
auf der wehrloseren Seite zu sein, durchaus fasziniert, vielleicht sogar gefällt. Sie
versucht, die Ketten zu sprengen, aber das Material gibt kein Stück nach.

"Ich weiß, wir haben das alle mit viel Arbeit gebaut", sagt Kendra. "Ich befürchte, du
könntest deshalb Hemmungen haben, dass es kaputt geht. Aber uns ist nicht geholfen, wenn
du es dann kaputtmachst, wenn du verzweifelt bist. Also geh in die vollen, ja?"

Luna lässt stattdessen lockerer. "Vor allem habe ich Angst, dass durch die zerberstenden
Schrauben jemand verletzt wird. Oder der Flügel Kratzer kriegt."

Sie behängen also den Kurzflügel mit Bettzeug und gehen alle aus dem Weg. Was Luna jetzt an
weiterer Kraft herausholt, ist ihr am Körper durchaus anzusehen, aber das Gebilde
gibt immer noch kein Stück nach. Gut, denkt Léonide. Kendra versteht was von Physik. As
kann verstehen, dass Luna an ihr Gefallen findet. Sie ist souverän und nicht angeberisch
dabei.

Dann geht es ans Experimentieren. Kendra fragt Luna aus, was sie schon alles probiert
hat. Und da Gifte darin nicht so viel vorkommen, flößt Kendra Luna als erstes
verschiedene ungesunde Flüssigkeiten aus dem Baumarkt ein. Darunter Benzin. Sie
haben einen Feuerlöscher neben der Liege stehen. Das Licht, das durch die Fensterfront
hineinfällt, ist eigentlich durch die Blätter und durch das Glas genug gefiltert, sodass
Luna nicht ankokelt, aber sollte sie doch irgendwann aus Versehen ein wenig anfackeln, ist
das mit Benzin gefährlich.

Nichts passiert. Trotzdem ermahnt Léonide etwas verspätet, die leicht brennbaren
Dinge besser nach Sonnenuntergang zu probieren.

Anschließend schneidet Kendra Luna an und beobachtet, wie sie heilt. Dazu borgt sie
sich von Léonide ein Mikroskop. Es ist nicht so leicht, es so umzufunktionieren, dass
sie damit Lunas nicht losgelöste Haut unter die Superlupe nehmen kann.

"Dein Blut ist eher rosa und ziemlich flüssig. Gehört das so, oder ist es doch eine
Auswirkung der Giftmischung?", fragt Kendra.

"Das ist es immer, wenn ich nicht gerade frisch wen ausgesaugt habe", antwortet
Luna. Sie wirkt erstaunlich wenig gequält.

"Mein nächster Plan wäre, *dich* auszusaugen", verkündet Kendra.

"Uh! Du möchtest dich in meine Perspektive versetzen?", fragt
Luna. "Ich würde das mit dem Trinken aber an deiner Stelle eher auf übermorgen oder
so verschieben. Es ist nicht ausgeschlossen, dass ich größere Mengen ungesundes Gift
in meinen Adern habe."

Kendra kichert. "Ich hatte keineswegs die Absicht, dich auszutrinken. Ich wollte
dich eher in einen Eimer entleeren."

"Das käme mir, ehrlich gesagt, gerade auch sehr entgegen. Ich fühle mich etwas
gefüllt mit Flüssigkeiten", stimmt Luna zu. "Ich habe nur Bedenken, dass
die Flüssigkeiten in meinem Magen und nicht in meinen Adern sind."

"Es geht hier nicht um dein Wohlergehen", erinnert Kendra.

"Es geht hier mehr um mein Unwohlergehen", kommentiert Luna trocken.

Marcin macht sich auf den Weg, ein paar große Eimer zu holen.

"Ich hatte Mal einen Katheter. Den müsste ich noch rumliegen haben. Klingt
der hilfreich?", fragt Léonide.

Kendra nickt zögernd. Sie will Léonide nicht herumscheuchen, aber es passiert
doch. Da Léonide nicht sicher ist, wo as den aufbewahrt, muss as selbst ran. Als
as wieder ins Wohnzimmer kommt, weist Luna Marcin gerade an, die Liege so
zu kippen, dass ihre Füße erhöht liegen, damit die Schwerkraft beim Leersaugen
nachhelfe. Sie hat da Expertise.

Dann setzt Kendra den Schnitt durch eine der Halsschlagadern. Sie zögert dafür
nur einen Moment. Léonide bewundert es, wie leicht es Kendra fällt. Der
zischende Laut, der Luna dabei
entfernt, überrascht as. As hat Luna immer für so beherrscht gehalten, dass
ihr niemals Leid angesehen werden könnte. Léonide wird vom Hinsehen leicht
schwummrig und as kann doch nicht wegsehen.

Damit der Schnitt nicht direkt wieder verheilt, schiebt Kendra ein Stück
Klarsichthülle dazwischen. Dann schiebt sie den Katheterschlauch in Lunas
Hals, sodass das dünnflüssige Blut erst einmal darüber in den Eimer
abfließt und keine Sauerei auf dem Boden hinterlässt. Sie haben ihn mit Mülltüten
und Betttüchern ausgelegt, aber trotzdem ist es nicht der Hit, in Blut
zu stehen.

Die Prozedur macht Luna tatsächlich zu schaffen. Sie wirkt fahler und
schlapper. "Shit", nuschelt sie schließlich.

"Hätten wir ein Safeword ausmachen sollen?", fragt Kendra.

"Wieso?", fragt Luna schwach. "Hast du vor, einfach weiterzumachen, wenn
ich dich bitte, aufzuhören?"

Kendra blickt Luna ein paar Momente schweigend an. Es ist naheliegend, dass sie
eigentlich nicht vorhatte, Luna eine Wahl zu lassen. "Ich hätte vorher
ausmachen müssen, ob es ein Tunnelspiel wird", sagt sie. "Wenn du mich dieses
Mal darum bittest, höre ich auf. Beim nächsten Mal nicht mehr."

Luna versucht zu nicken, aber schafft es nicht. Ihre Augenlieder flattern. "Ich
möchte ja Grenzen testen", flüstert sie. Und schließt die Augen.

"Willst du aufhören?", richtet sich Marcin an Kendra.

Kendra schüttelt den Kopf. "Ich bin wirklich gespannt, woraus Lunas Körper
Flüssigkeit nachgenerieren will. Und er braucht offenbar dieses Blut, oder
diese Alternativplörre in ihrem Körper, um zu heilen? Ich bin so gespannt, was
passiert!"

Marcin zuckt mit den Schultern und wechselt den Eimer. Er ist weiterhin sehr
ruhig. Kendra mag das. Sie mag, bei der Arbeit nicht allein zu sein.

Léonide erwischt sich bei dem Gedanken, dass as es durchaus schade fände, wenn
Luna dieses Experiment dahinraffen würde. As glaubt nicht so recht daran. Diese
Vampirkreatur hat immerhin Köpfen überstanden. Aber vielleicht hat Kendra recht,
dass es was anderes ist, ihr sämtliches Blut zu entfernen. Léonide mag Luna
eigentlich. "Wir können auch einen halben Tag warten, und wenn sich nichts
tut, Wasser nachfüllen. Vielleicht belebt sie das wieder", schlägt as vor.

Kendra nickt. "Sowas hatte ich mir gedacht", sagt sie. Aber in diesem Moment fängt
sie erst einmal an, auch die Unterdruckpumpe zu nehmen, die beim Katheter dabei
war, um Luna noch gründlicher leer zu pumpem. Es ist nicht mehr viel zu holen. Luna
reagiert nicht.

Wenn jetzt jemand zum Fenster reinsieht...

---

Léonide sitzt um die Ecke auf der Empore, um den jungen Leuten etwas Raum für
sich zu geben. Marcin und Kendra liegen auf dem Boden auf einer weichen Wolldecke,
und warten ab.

"Würdest du auch meine Magie durchexperimentieren?", fragt Marcin.

Kendra mustert ihn nachdenklich. Dann runzelt sie über sich selbst die
Stirn. "Warum erscheint mir das riskanter, als ein Vampir zu töten zu
versuchen?"

Marcin lächelt. "Vielleicht hast du sogar Recht damit. Ich weiß es nicht", sagt
er. "Luna hat sich immerhin über Jahre selbst getestet und weiß, was sie ist. Ich
nicht. Ich produziere Ungeheuer, die Leute sehr gruseln."

"Mich hat der Drache nicht gegruselt", sagt Kendra.

"Ich weiß", sagt Marcin. "Ich glaube, ich mag dich dafür."

"Ich kann auch nicht abstreiten, neugierig auf ein größeres Ungeheuer zu
sein", murmelt sie. "Hast du irgendeine Kontrolle?"

Marcin seufzt. Und schüttelt den Kopf. "Eigentlich nicht", sagt er. "Wenn
Paolo und ich uns küssen, reiße ich mich immer zusammen, nicht so viel zu
fühlen, damit keines entsteht. Am Anfang ist mir manchmal eine Schlange
entstanden. Sie hat nichts getan und ist verschwunden, bevor Paolo überhaupt
dazu kam, mit ihr zu kämpfen."

"Also", überlegt Kendra, "meinst du, es ist halbwegs safe, wenn ich dich
küsse, und du dich weniger kontrollierst? Dass eine Schlange auftaucht
und mir nichts tut?"

Marcin schluckt. Und nickt. "Würdest du mich für die Wissenschaft
küssen?"

"Meinst du, es passiert was, wenn wir es unromantisch tun?", fragt Kendra. "Weil
mein Verständnis war, dass sie entstehen, wenn du fühlst."

Marcin nickt. "Für die Wissenschaft heißt für mich nicht ohne Gefühle", sagt
er. "Vielleicht frage ich viel zu viel von dir. Es tut mir leid. Ich möchte
dich nicht bedrängen."

Kendra streckt die Hand nach Marcins Wange aus, um ihn beschwichtigend zu
berühren.

Marcin nimmt ihre Hand in seine. "Und jetzt habe ich Angst, dass du
zuvorkommend und einladend reagierst, weil ich mich zu reflexartig
zurückgezogen habe und du die Reaktion nicht gut fandest und
überkompensierst."

"Marcin, ich möchte dich eigentlich schon länger küssen", gibt Kendra zu. Irgendwas
in ihrer Brustkorbgegend flattert, als sie es sagt. "Probleme, die ich damit hatte. A: Du
bist in einer Beziehung, von der ich angenommen habe, dass sie monogam ist. B: Wir
kennen uns kaum. Mein Bedürfnis ist oberflächlich. Ich habe einfach lange nicht mehr
geküsst und du siehst für mich sehr kissable aus." Kendra wird sehr heiß, als
sie das sagt. Sie hat schon Angst davor, für ihre Oberflächlichkeit nicht gerade
wertgeschätzt zu werden.

"Ich weiß nicht einmal, ob meine Beziehung monogam ist", gesteht Marcin. "Aber
ich ordne es nicht als Fremdgehen ein, wenn mich wer küsst, weil wir mich
untersuchen wollen, und nicht als etwas, was mit einer Beziehung zu tun hat. Ist
es für dich okay, wenn es das nicht hat?"

Kendra nickt. Ein Gefühl von Enttäuschung testet, ob es angebracht ist, weil
Kendra weiß, dass es ein in solchen Situationen zu anderen passendes Gefühl
wäre, aber es findet keinen Halt. "Das passt für mich sehr gut", sagt sie. "Ich
habe etwas Sorge, dass Paolo das anders sieht, aber ich erlaube mir, das als
dein Problem zu deklarieren."

"Ja", sagt Marcin. "Ich verstehe die Sorge, ich habe sie auch ein wenig. Aber
ich bin mir auch sicher, dass ich irgendwann gern mit mir und meinen Ungeheuern
umzugehen lernen will. Und ich will wissen, wie sich für mich ein Kuss anfühlt, bei
dem ich nicht versuche, mich zu kontrollieren. Ich will das für mich rausfinden. Es
hat nichts mit dir zu tun. Und damit muss er wohl klarkommen."

"Wirst du es ihm sagen?", fragt Kendra.

"Abhängig davon, was du willst", antwortet Marcin. "Ich möchte dich nicht in etwas
reinziehen, wo du Bedenken hast, dass Paolo anschließend noch gemeiner zu dir ist. Ich
tendiere vor allem deshalb dazu, es für mich zu behalten. Bis ich ihm vertraue, dass
er sich nicht mies dir gegenüber verhält."

Kendra nickt. Sie weiß gar nicht genau, was sie ethisch hier für richtig
halten würde. Und sie denkt, dass es nicht ihr Problem ist. Aber vielleicht ist es das, weil
sie mit Marcin eine Freundschaft anfangen will. Sie rückt näher an ihn heran. "Ich
mache es langsam, ja?"

Marcin nickt. Er löst den Griff von ihrem Handgelenk und streicht sanft darüber. Er
ist aufgeregt, als sie sich noch weiter nähert. So weit, dass sie sich nicht mehr scharf
sehen können. Er spürt ihren warmen Atem. Ein Teil von ihm schnappt reflexartig in
einen Kontrolliermodus ein. Marcin versucht, ihn loszulassen.

Kendra setzt ihre Brille ab. Marcin wirkt, als käme er etwas perplex über die Existenz
von so etwas Banalem wie Brillen aus seinem Gedankenuniversum zurück und setzt seine
eigene auch ab. Als sie sie oberhalb ihrer Köpfe platzieren, berühren sich ihre Hände. Kendra
nutzt es aus und streift zart mit den Fingern über Marcins Handgelenk. Sie beobachtet
trotz der Nähe, wie Marcin die Augen schließt und sein Gesicht einen genießenden
Ausdruck annimmt. Als nächstes legt sie ihre Hände an seine Wangen, kann nicht
widerstehen, ihm Haar vom Ohr zu streichen, das eigentlich gar nicht im Weg
ist, und berührt vorsichtig seine Lippen mit ihren.

Es ist ungefähr so, wie sie es sich vorgestellt hat. Seine Lippen sind sehr kissable. Weich,
sich leicht wie von selber öffnend, sodass sie sie zwischen ihre nehmen kann, oder ihre
Zungenspitze vorsichtig dazwischen schieben. Er nimmt sie in den Arm, während sie sich
küssen. Aber irgendwas an ihm ist noch angespannt, lässt nicht los. Also versucht
Kendra es mit mehr Leidenschaft. Sie legt auch ihre Arme um ihn, eine davon
unter das Haar in seinen Nacken, und küsst ihn ungestümer. Sie merkt wie sein Atem
ein wenig schneller dabei wird. Aber kein Ungeheuer erscheint.

Irgendwann, als sie sich weniger hungrig nach so etwas fühlt und es sich immer
noch nichts Ungeheuertechnisches tut, hört sie auf, hält ihn aber noch umarmt.
Es ist schön, in seinen Armen zu liegen. Er hat ein Bein über ihr Becken gelegt
und streichelt ihre Wange. Ist es doch mehr als Wissenschaft?

"Wie fühlst du dich?", fragt sie. "Wie war das mit dem Kontrollverlust? Ich
habe den Eindruck, du hast nicht so recht loslassen können."

"Doch." Marcins Stimme ist leise. "Anfangs nicht, dann schon." Er löst eine Hand
von ihr, um nach seiner Brille zu tasten. "Oh!", macht er.

Als Kendra ihre Brille aufsetzt und seinem Blick folgt, sieht sie, was er meint. Ein
kleiner, schwarzer Schildkrötenpanzer liegt über ihnen auf dem Boden. Schwarzer Nebel,
wieder vom Typ Flüssigstickstoff, wabert aus seinen Öffnungen. Tief im Inneren entdeckt
Kendra einen sehr verschrumpelten, kleinen Schädel. "Darf ich versuchen, es
anzufassen?", fragt sie.

Marcin zögert und nickt schließlich.

Aber als Kendra sich mit den Fingern nähert, materialsiert durch ein Zusammenfusseln
schwarzer Schlieren aus dem Nichts der kleine Drache, den Kendra schon kennt, und breitet
schützend die Flügel über der Schildkröte aus. Kendra zieht den Finger zurück und
nickt. "In Ordnung", flüstert sie.

Dann überschlagen sich die Ereignisse. Luna hat es irgendwie geschafft, trotz ihrer
Fesselungssituation die Liege umzukippen.

Wie sie das getan hat, ist Kendra
schleierhaft. "Du verstößt gegen die Naturgesetze", sagt sie.

Luna hat durch den Boden mehr Widerstand als durch die Liege, und bekommt es hin, mit
dem unteren Arm beide Schellen zu sprengen, mit denen dieser befestigt ist.

Marcin und Kendra beeilen sich, die Liege aufzurichten, weil das Luna am schnellsten in die
wehrlosest mögliche Lage bringen wird. Aber kaum steht diese wieder, packt
Luna Kendras Handgelenk mit einem unnachgiebigen Griff, von dem Kendra hofft, dass
ihre Knochen nicht dadurch zerbersten. Blaue Flecken werden entstehen. Aber wahrscheinlich
sollte sich Kendra mehr Gedanken darüber machen, dass Luna das Handgelenk langsam, als
wäre sie doch nicht völlig überzeugt, zu ihrem Mund zieht. Oder sie nimmt an, dass
Kendra sich ohnehin nicht wehren kann, und kostet die Situation aus.

Unabhängig von den Motiven ist ihr Zögern hilfreich. Auf Lunas Brust manifestiert sich
ein schwarzer Sturm aus Staub. Marcin hat etwas wirklich Großes, Wehrhaftes ins Leben
gerufen. Ein Flederwesen, zerfledderte Flügel, sechs Krallen an Gliedmaßen mit jeweils
zwei Ellbögen, der Kopf eines Hirschskellets. Oder eines Rentierskeletts?

Luna lässt von Kendra ab und befasst sich mit dem Ungeheuer. Kendra und Marcin schauen
sich an und fragen sich beide, was sie als nächstes tun sollen.

Léonide kommt um die Ecke gehumpelt und betrachtet stirnrunzelnd das Spektakel. "Luna,
wenn du Blut brauchst, lass die Jungspunde in Ruhe und bedien dich an mir, okay?"

Luna antwortet nicht, aber sie wird ruhiger. Sie weint, kann Kendra erkennen. Marcins
Ungeheuer verschwindet. Nichts bleibt von ihm zurück.

"Könntest du es wieder entstehen lassen, wenn wir es brauchen?", fragt Kendra leise.

Marcin holt zischend etwas wütend Luft. "Ich sagte dir, ich kann nichts
kontrollieren!" Ruhiger fügt er hinzu. "Das wollte ich nicht. Meinen
Frust an dir auslassen. Ich denke, es entsteht, weil ich Angst und Beschützenwollen
gefühlt habe. Das kann wieder passieren, wenn wieder Gefahr droht."

Kendra greift Marcin kurz bei der Hand.

"Ich glaube, drei bis fünf Schlucke Blut würden genügen, um mich wieder in einen
kontrollierteren Zustand zu versetzen", schätzt Luna.

Marcin tritt ohne Zögern zu ihr. "Ich kann Blutverlust von uns allen am besten
wegstecken", begründet er.

"Aber vielleicht sollten wir es dir abnehmen, sodass du nicht in direktem Kontakt
zu ihr stehst", überlegt Kendra.

"Wo sie recht hat", murmelt Luna. "Ich denke, ich kriege mich beherrscht, aber das
gerade, das war das Unbeherrschteste von mir seit einem Jahrhundert oder sowas." Sie
weint immer noch. Sie wirkt ermattet.

"Haben wir Dinge zum Blutabnehmen da?", fragt Marcin.

Kendra blickt sich zu Léonide um.

As schüttelt den Kopf. "Ich bin trotzdem dafür, dass du mein Blut nehmen solltest."

Luna schüttelt den Kopf und greift Marcins Handgelenk. Sanft. "Euch töte ich vielleicht,
weil es euch nicht so viel ausmacht, mylauden." Und versenkt die Zähne zitternd eine Fingerlänge
unter Marcins Daumenansatz durch die Haut.

Während Luna trinkt, taucht Marcins Drache wieder auf seiner Schulter auf und schmiegt
sich an seine Wange.

Irgendwie, denkt Kendra, wirken alle von Marcins Ungeheuern auf sie nicht besonders
gefährlich. Eher beschützend. Vielleicht maximal so gefährlich wie so ein Wolf oder
Wildschwein. Sie tun nur was, wenn sie sich bedroht fühlen, oder weil es die Natur
eines sich verteidigenden oder versorgenden Tiers ist. Nicht, weil sie per se
böse oder menschenfeindlich wären.

Luna zählt fünf Schlucke. Sie will eigentlich mehr, aber sie hat ihre Beherrschung
zurück. Ihr geht es beschissen. Gelinde gesagt. Sie könnte sich losreißen, das weiß
sie jetzt. Was sie einmal geschafft hat, schafft sie auch ein zweites Mal. Sie
lässt Marcins Hand los. Das Blut macht es etwas besser. Sie zupft sich das Stück
Klarsichthülle aus dem Hals, das irgendwie geschafft hat, dort noch zu
kleben. Den Katheter haben wohl Kendra oder eine der anderen Personen nach getaner
Arbeit entfernt. Woher Lunas neues Blut kommt, weiß sie nicht, aber es rauscht
durch ihren Körper und füllt sich mit den Stoffen aus Marcins Blut, das sie
braucht. Luna spürt es.

Sie schluchzt einmal auf. Zieht die Nase hoch. "Macht ihr mich los?"

Kendra hadert, aber entscheidet dann, dass sie den Wunsch des Vampirs am besten
erfüllt. Sie tritt an die Liege heran und öffnet die Verschlüsse.

Luna bleibt einen Moment zitternd liegen, reibt sich die Handgelenke.

"Musst du aufs Klo gehen?", fragt Kendra.

Luna ist so durch, dass sie nicht einmal kichern kann. Sie nickt.

"Ich fände sinnvoll, denke ich, wenn du es in irgendeinen Behälter für
Schadstoff tust. Ich kann mir vorstellen, dass es nicht so erbaulich
ist, Benzin in die Kanalisation zu kippen", erklärt sie.

Luna mag ihre trockene Art. Sie erdet. Sie greift sich den letzten leeren
Eimer und geht damit ins Bad. Als sie entleert wiederkommt, geht es
ihr trotzdem matt. Sie greift sehr sanft
Kendras Handgelenk und führt es, weil es freiwillig mitkommt, zu ihrem
Mund. Sie küsst es und blickt Kendra fragend an.

"Zwei", sagt Kendra trocken.

Luna streichelt mit ihren geschundenen Händen, die dieses Mal etwas länger
zum Heilen brauchen, über Kendras Arm, schließt die Augen und beißt sehr
zart hinein. Sie merkt, wie es Kendra gefällt, vielleicht daran, wie sich Kendra
zu ihr beugt und ihr das Haar aus dem Gesicht streicht. Aber dabei kommt
auch Kendras Hals näher. Und der ungeduldige Wunsch danach, ihre Zähne dort
hineinzuversenken, lässt sie vergessen zu zählen. Sie merkt an Kendras Zucken, dass
sie dreimal geschluckt hat, lässt los. Ihr kommen wieder ungefragt Tränen. "Es tut mir
leid. Ich bin ein bisschen überm Limit."

"Ein bisschen", widerholt Léonide sarkastisch.

"Ich gehe in den Wald, ja? Ich komme wieder, wenn ich mich im Griff
habe", verspricht sie.

Die anderen nehmen es hin. Marcin öffnet ihr die Glastür zur Terrasse. Luna
überlegt, ob sie noch etwas sagen soll, aber sie hat zu große Angst, dabei
wieder die Fassung zu verlieren, also verschwindet sie. So schnell, dass
die anderen nicht einmal sehen, dass sie dabei fast auf den Bettlaken über
dem Plastik ausrutscht.

---

Luna fühlt sich in ihrem Wald augenblicklich wohler. Er rauscht, weil der Nordwind
stark genug ist, seinen Weg hindurchzufinden. Regen tröpfelt aus dem Blätterdach
in Wehen auf Lunas geschundenen Körper. Und trotzdem denkt sie darüber
nach, hier nicht zu bleiben. Irgendwo hoch oben auf den Klippen wohnt Sonja
in einem kleinen Haus mit Kellergewölbe darunter. Luna spielt eine ganze Weile
mit dem Gedanken, sie aufzusuchen und sie zu bitten, ob sie von ihr trinken
darf. Sie möchte eigentlich sicher nicht in Sonjas Schuld stehen. Aber
auf der anderen Seite ist Sonja eine treue Person gewesen. Sie hat nie
'nein' gesagt. Luna hegt schon länger den Verdacht, dass sie es eigentlich
will.

Sie könnte ja wenigstens bis zu Sonjas Behausung gehen. Aber kurz bevor Luna
sich in Bewegung setzt, merkt sie, dass sie zu erschöpft war, um ihren Sinnen
zu trauen. Sonja befindet sich, dem Geruch nach zu urteilen, längst im Wald.
