\BefehlVorKapitelIBDM{Mondscheinhypothesen.png}{Ein Wolkenmeer, durch das der Mond hindurchleuchtet, dessen Form selbst aber nicht zu erkennen ist.}{Mord und Suizid als Gesprächsthema, Köpfen, Bedrohung, Vampir, Blut, Kanibalismus - assoziierbar, Erotik, Beißen, wirbellose Tiere - erwähnt.}

Mondscheinhypothesen
====================

\Beitext{Kendra und Luna}

Eine Bank an der Klippe dicht am Geisterwald. Nebel wabert aus dem Wald
hinaus und die Klippe herab. Abenddämmerung, aber noch ungeröteter Himmel. Vielleicht
ist der hellere Fleck am Himmel der schon aufgegangene Halbmond hinter
den Wolken. Kendra sitzt auf besagter Bank, sonst bisher niemand. Sie starrt aufs Meer,
auch wenn sie es durch den Nebel kaum ausmachen kann. Eiderenten geben ihre
geisterhaften Schuhuu-Geräusche von sich.

Aus dem Geisterwald tritt eine Gestalt. Uneilig. Ihr Haar ist schulterlang,
schwarz und matt. Obwohl es dämmert, der Himmel bleich ist, hätte
das Haar einen gewissen Glanz aufweisen sollen, tut
es aber nicht. Es reflektiert nur eine Spange mit einem
schwarzen, filigranen Schmetterling darauf das fahle Licht, und hält das Haar
auf einer Seite davon ab, ins Gesicht zu fallen. "Du magst Abgründe?", fragt
die Gestalt.

Kendra löst ihren Blick für ein paar Momente vom Nebelmeer über dem anderen
Meer, betrachtet die Person und nickt. Als sie wieder in die Ferne sieht,
lächelt sie.

"Gestattest du, dass ich mich zu dir setze?", fragt die Gestalt.

Kendra nickt wieder. "Du kommst aus dem Geisterwald."

Die Gestalt setzt sich behutsam neben Kendra auf die Bank und streicht
sich über die mehrfach geflickte, schwarze Kordhose. "Das ist mir
auch aufgefallen."

Kendra gluckst. "Aus dem Wald kommt niemand lebend raus, heißt es", sagt
sie. "Ich schließe, entweder ist an dem Gerücht weniger dran, als sich
ach so viele Leute sicher sind. Oder du hast es gerade so geschafft,
zu entkommen, aber sonderlich aus der Puste wirkst du nicht. Oder
du bist die Gefahr des Waldes, die mordlüstige Gewalt, durch
die der Geisterwald so unsicher wird."

"Ich bin Luna", antwortet die Gestalt mit dem Ansatz eines Lächelns. "Würdest
du letzteres für möglich halten?"

Kendra blickt sich noch einmal zu ihr um. "Warum nicht?"

"Wirke ich auf dich so, als wären andere wehrlos gegen mich?", fragt Luna. Sie
fügt dem Klang ihrer Stimme einen gespielt bedrohlichen und ebenso albernen
Unterton hinzu.

"Ich wäre gegen dich sicherlich wehrlos!" Kendra fuchtelt zum Gehstock, der
neben ihr an der Bank lehnt. "Muskelkrankheit."

"Oh", sagt Luna. Nicht mitleidig, mehr überrascht. "Ich hätte damit gerechnet,
dass wer anders den hier vergessen hätte."

"Weil ich zu jung aussehe, um behindert sein zu dürfen?" Kendra schnaubt
bitter.

"Das scheinst du oft zu hören zu bekommen", mutmaßt Luna. "Das tut mir
leid."

Kendra kichert. "Dass ich jung aussehe, dass ich den Spruch immer mal abbekomme
oder dass ich behindert bin?"

"Das mittlere. Beim Rest weiß ich noch nicht, wie du damit klar kommst. Wie
lebt es sich mit der Behinderung?", fragt Luna. "Wenn du denkst, dass es mich
etwas anginge."

"Na ja, ich bin es gewohnt. Eine Assistenz wäre schon nett zu haben, aber
ist halt nicht." Kendra seufzt. "Es ist nicht der Hit, aber mit Assistenz
würde ich nichts vermissen, denke ich."

Luna beobachtet Kendra eine ganze Weile still und neugierig. Sie schweigen. Die
Eiderenten schuhuhen aufgeregt durch den Nebel. Kendra, deren Namen sie noch nicht
kennt, ist Luna sympathisch und so etwas kommt nicht häufig vor. "Kannst du dir mich als
deine Assistenz vorstellen?", fragt sie schließlich. Und zu sich selbst
denkt Luna: Warum nicht mal wieder ein Sprung ins kalte Wasser.

"Die mordlüstige Gewalt des Gespensterwaldes als meine Assistenz?" Kendra kichert.

"Scheust du dich vor Monstern wie mir?", fragt Luna sachlich.

"Schon, denke ich", gibt Kendra zu.

"Und doch wolltest du mich kennen lernen."

Luna hat es nicht als Frage formuliert, aber Kendra blickt sie sehr verwirrt
an. "Nein?"

"Warum kommst du dann so oft hier her?", fragt Luna.

"Es ist ein schöner Ort!", betont Kendra. "Der Nebel, die Äste, die daraus hervorragen,
das Meer, das Rauschen, der Wind, die Eiderenten. Die Möwen, der Geruch, der Abgrund, ich
mag einfach alles an diesem Ort. Es fehlen nur noch Kerzen, aber ich kann nicht viel tragen."

Luna setzt einen Fuß mit auf die Bank, legt den Ellbogen darauf und blickt nun auch
in die Ferne. Sie nickt. "Es ist wunderschön hier. In der Tat. Deshalb habe ich mir
ja auch den Wald geclaimt."

"Und da fängt mein Unverständnis an." Kendra zieht den Reißverschluss der roten
Fließjacke weiter zu, weil ihr fröstelig ist. Es ist ein Muster mit schwarzen
Sternen und Monden darauf. "Wieso denkst du, dass du
dir so etwas wie einen Wald einfach claimen darfst? Und Leute, die ihn dir nicht
zugestehen wollen, mit dem Tode bestrafst? So heißt es jedenfalls. Korrigiere mich
gern."

Luna seufzt. "Da gibt es nicht viel zu korrigieren", sagt sie resigniert. Sie
befürchtet, dass Kendra gleich gehen wird. "Ich könnte mich nun rauszureden
versuchen: Menschen haben den Wald roden wollen um Felder dahinzumachen. Dabei
haben sie ihn auch als ihren zu claimen versucht und zwar abhängig davon, wieviel Macht
sie hatten. Durch Geld oder durch Armeen oder was weiß ich. Ich mache es sicher
nicht besser, wenn ich sage, ich bin alt, ich möchte mein Dasein in einer
einsamen Waldhütte fristen, lasst mich in Ruhe, und diese Bedürfnisse über
die der Felderbauerei stelle. Und zusätzlich auch noch klarmache, dass ich abmurxe,
wer mich nicht in Ruhe dort abhängen lässt. Es mag durch letzteres ethisch
fragwürdiger sein. Aber immerhin wissen die Leute, was sie erwartet, wenn sie
den Wald betreten."

"Wissen sie es wirklich alle?", fragt Kendra. "Kann sich nicht mal ein Tourist
hierher verirren?"

"Darauf gebe ich schon Acht, dass ich nur die Leute esse, die sich nicht aus Versehen
in den Wald verirrt haben", räumt Luna ein. "Ich verwickele sie in ein Gespräch,
informiere sie über die Gefahr des Waldes und weise ihnen den Weg hinaus. Verirrte
Leute folgen den Hinweisen meist gern."

"Und wenn sie ihnen nicht folgen oder sich über die Warnungen bewusst hinwegsetzen,
fällst du über sie her, wie sich Leute das bei Raubtieren so vorstellen?" Kendra
fragt dies ohne erkennbare Regung der Mimik.

Luna weiß nicht so genau, wie sie darauf reagieren soll und fragt
schließlich: "Kaufst du mir ab, dass ich das kann?"

"Auf den Punkt wollte ich auch zurückkommen:", sagt Kendra. "Du scheinst generell
damit zu rechnen, dass Leute dich für ungefährlicher halten, als du bist. Hältst
du es für gerechetfertigt, Menschen umzubringen, die sich in dir diesbezüglich
irren?"

Luna seufzt abermals. "Aber du hast all die Gerüchte über den Wald gehört, nicht
wahr? Dass darin eine Gewalt lauert, die ihn gefährlich macht. Dass niemand lebend
herauskommt."

Kendra nickt. "Schon."

"Auch wenn sie die Gefahr nicht in mir vermuten, ist ihnen doch bewusst, dass
im Wald eine tödliche Gefahr lauert", argumentiert Luna. Sie überlegt, ob sie noch
etwas hinzufügen möchte, lässt es aber doch sein. Sie hat oft das Bedürfnis, sich
zu rechtfertigen, sieht aber durchaus, dass es nur bis zu einem gewissen Punkt
geht.

"Das allgemeine Wissen, dass diesen Wald zu betreten meist tödlich endet,
muss aber auch irgendwo herkommen", wendet Kendra schließlich ein. "Sprich,
irgendwann musst du damit angefangen haben, ohne dass Leuten die tödliche
Gefahr allzu bewusst gewesen ist. Richtig?"

Luna nickt. "Ich war einst noch mehr Monster als heute", sagt sie. "Es ist
lange her."

"Wie lange?", fragt Kendra.

"200 Jahre?", schätzt Luna.

Es ist das erste Mal in diesem Gespräch, dass Kendra eine Spur überrascht
wirkt. "Bist du unsterblich?"

"Sieht so aus", sagt Luna nachdenklich. "Warum von allen Dingen, die ich dir
erzählt habe, macht dich ausgerechnet das stutzig?"

"Ich mag Wissenschaft, ich lese viel über Natur, Tiere und alles", erklärt
Kendra. "Am nächsten an Unsterblichkeit heran kommt die Qualle, denke ich. Aber
das ist ein ganz anderer Organismus. Hast du je versucht, dich umzubringen?"

Luna kichert. "Du fragst mich hier casual über meine Suizidalität aus", sagt
sie. "Interessanterweise mag ich das. Und, ja, ich habe es versucht."

"Na ja, du hast casual über dein Mordverhalten geredet. Da dachte ich, ich bin mit dem
Thema mal ähnlich locker. Was hast du alles probiert?", fragt Kendra einfach
weiter. Schließlich hat Luna gesagt, dass sie es mag.

"Das wohl krasseste bisher war köpfen, denke ich." Luna grinst sehr breit
und erwartet irgendein Zucken, aber es bleibt aus.

Kendra wartet ein paar Atemzüge ab, ob Luna weiter ausführt, aber als
diese das nicht tut, fragt sie: "Und der Kopf ist einfach wieder
angewachsen?"

"So einfach war das nicht", widerspricht Luna. Sie grinst immer noch, aber
inzwischen fühlt sie sich etwas unbehaglich dabei. Trotzdem erzählt sie. Kendra
fasziniert sie viel zu sehr, um hier nicht auch aus der eigenen
Comfort Zone zu gehen. "Das Problem ist, dass ich meine Gliedmaßen am Rumpf
nur steuern kann, wenn der Kopf mit ihm bereits verbunden ist. Ich konnte mit
meinem Kopf zwar solche Befehle geben, aber nichts davon hat mein Rumpf umgesetzt."

"Hat es weh getan?", fragt Kendra.

"Am Anfang ein bisschen. Aber ich habe mich rasch daran gewöhnt", murmelt
Luna. "Schlimmer war das Jucken. Aber vor allem war schlimm, ein knappes Jahr
in meinem eigenen Kopf gefangen zu sein. Das war kontraproduktiv, was
das Loswerden meines Suizidwunschs anging."

"Fast ein Jahr?", rückversichert sich Kendra, dieses Mal wieder überrascht.

Sind es Zeitangaben, die das mit ihr machen?, überlegt Luna. "Na ja, die Angst
vor dem Wald hat dazu geführt, dass sich keine lokal hier ansässige Person
in den Wald getraut hat. Es brauchte einen verirrten, ahnungslosen
Touristen, der mich schließlich gefunden hat", berichtet Luna. "Und dieser Jemand war so
freundlich, den Kopf an den Hals zu legen, vermutlich aus ästhetischen
Gründen. Dann ist er wieder angewachsen."

"Hast du die Person leben lassen?", bohrt Kendra nach.

Luna schüttelt den Kopf. "Das war ein Mord, den ich bereue", gibt sie zu. Sie
seufzt. "Ich hatte so lange nicht mehr gespeist."

"Gespeist?", fragt Kendra. "Du sprachst vorhin schon von essen." 

Luna betrachtet Kendras Gesicht kritisch. Es wirkt wirklich nicht schockiert
oder so etwas. "Austrinken trifft es eher."

"Bist du ein Vampir?", fragt Kendra sachlich.

"Dann hätten wir das wohl geklärt. Ja, bin ich." Luna lehnt sich an die Bank
zurück. Und doch fragt sie nach kurzem Zögern nach: "Glaubst du es mir?"

"Ich kalkuliere es zumindest ein", sagt Kendra. Sie mustert Luna
ausgiebig, bevor sie wieder etwas fragt. "Tötest du gern?"

Luna nickt und schließt die Augen. Sie atmet tief durch -- und bekommt dabei
Kendras Geruch in die Nase geweht, den sie schon eine Weile halb wahrgenommen
hat. Sie riecht so jung und lebendig, und nach Erde und Kräutern, nicht sehr
blumig. "Ich töte sehr gern. Macht dir das Angst?"

"Sollte es wahrscheinlich", murmelt Kendra. "Aber nein. Möchtest du mich denn
töten?"

"Bislang nicht", sagt Luna. "Bislang bist du der interessanteste Mensch, dem ich
seit ungefähr 70 Jahren begegne, und ich würde mich freuen, mit dir mehr zu
tun haben zu dürfen." Dieses Mal beobachtet Luna keine Überraschung ob der
Zeitangabe, als sie flüchtig zu Kendra hinüberlinst. "Aber
es zählt für meine Entscheidung nicht nur, was ich möchte. Es
ist kein Reflex, ich bin nicht aufs Töten angewiesen. Ich bin nicht
sehr impulsiv. Ich töte dich, wenn du bewusst in meinen Wald spazieren würdest. Oder wenn
es dir für einen zu langen Zeitraum ein wirkliches Bedürfnis ist, dass ich das tue." Luna
seufzt und versichert eindringlich: "Selbst wenn es irgendwann dazu kommen sollte,
dass ich große Lust verspürte, dich zu töten, weiß ich es doch zu
vermeiden, wenn keine der beiden Dinge zutreffen."

Ein Kribbeln durchfährt Kendras Körper. Sie mag nunmal Abgründe, -- auch solche, scheint
es. "Es sei denn, dein Kopf ist zu lange nicht an deinem Körper befestigt."

Luna sagt eine zu lange Weile nichts, und als Kendra sich ihr zuwendet, sieht sie,
wie ihre Augen feucht sind und eine Träne über ihre Wange rollt. Eine
Person, die mordet und darüber Trauer fühlt, denkt Kendra, und dass sie
das wohl nicht so verwunderlich finden sollte.

"Tötest du quasi als Coping Strategie gern, weil du töten musst, weil du
Blut brauchst?", fragt Kendra vorsichtig.

Luna schüttelt den Kopf. "Das Blut ist optional. Ich komme auch ohne aus." Sie
seufzt abermals. "Es kommt auch vor, dass mir Leute ihr Blut freiwillig zur
Verfügung stellen." Sie beobachtet Kendra aufmerksam, als sie es
sagt. "Du wärest nicht abgeneigt, oder?"

"Es kommt sehr drauf an." Kendra zuckt sachte mit den Schultern, um
einen Schauer zu kaschieren, der ihren Körper kurz durchrüttelt. "Schon, denke
ich. Aus Neugierde. Aus Forschungstrieb. Aber ich möchte nicht sterben."

"Wie nah möchtest du dem Sterben dabei kommen?", fragt Luna sanft.

Kendra runzelt die Stirn. "Überhaupt nicht nah." Und zögert. "Zumindest
beim ersten Mal."

Luna schnaubt kichernd. "Also nehme ich höchstens einen Schluck", sagt
sie. "Dieses Mal." Sich besinnend beginnt sie zu faseln: "Und
auch nur, wenn du wirklich willst. Ich habe das Gefühl, ich habe
gedrängt. Wir können das gern für dieses Treffen ausschließen. Oder auch
für jedes Mal. Ich sagte, für dieses Mal, weil ich klar machen wollte,
dass dir nichts davonläuft, wenn du willst."

"Wie hoch ist ein Infektionsrisiko?", unterbricht Kendra Lunas Redefluss.

"Ja, nun, also:" Luna breitet die Worte langgezogen und breit vor
ihnen aus. "Bisher ist es interessanterweise noch nie zu einer Entzündung
oder so etwas gekommen. Im Gegenteil. Die Bisswunden schienen ungewöhnlich
zügig zu heilen. Es ist auch noch niemand an irgendwas Mysteriösem erkrankt
oder gar zum Vampir geworden. Aber das sind meine Erfahrungen und
keine Forschungserkenntnisse."

Kendra lächelt. "Wie groß ist deine Stichprobenmenge?"

"Ich wusste, dass die Frage jetzt kommt!", jubelt Luna. "Etwa 40."

"Über einen Zeitraum von 200 Jahren kommt mir das nicht so viel
vor", überlegt Kendra.

"Nun", antwortet Luna bloß. Und erst, nachdem sich Kendras Blick in sie
hineinbohrt, fügt sie hinzu: "Es könnte da Vertrauens-Issues mir gegenüber
geben. Ich meine, ich gehe recht freizügig mit der Information um, dass ich
eine Mörderin bin."

Kendra unterdrückt den Impuls, zu sagen, dass es dafür in der Tat vielleicht
doch ganz schön viele Menschen wären, und nickt bloß. "Ergibt Sinn."

Eine Weile sitzen sie schweigend beieinander. Kendra sieht abwechselnd nachdenklich,
fast verloren aufs Meer hinaus und in Lunas wachsame, dunkle Augen. Luna beobachtet
Kendras Mimik. Riecht ihren Duft. Das Blut unter der Haut. Sie kann nicht leugnen,
sich sehr danach zu sehnen, einen Schluck davon zu sich zu nehmen. Ihre
Atemfrequenz erhöht sich, -- ein wenig, denn Luna versucht sehr, sich zurückzuhalten. Es
ist nicht an ihr, noch einmal danach zu fragen. Sie schließt einen Moment die
Augen und unterdrückt das Atmen ganz, konzentriert sich.

Bis auf das eine Mal, als dieser Mensch sie gerettet hat, hat sie nie ein
Problem mit Impulskontrolle gehabt, und so auch jetzt nicht. Zumindest, was ihren
Körper betrifft. Ihre Gedanken wollen spazieren gehen. Wenn Kendra nicht bald
etwas sagt, sollte Luna das Thema wechseln, um nicht mehr daran zu denken, die
Haut zu berühren, unter der Leben in Form von roter Flüssigkeit entlanggepumpt wird,
die Luna auf der Zunge und an den Zähnen mehr genießt als alles andere.

"Ob du möchtest, brauche ich dich wahrscheinlich nicht zu fragen", stellt
Kendra trocken fest.

Luna öffnet die Augen wieder und setzt sich gerader hin. Sie nickt. "Darauf
kommt es nicht an."

Kendra kichert. "Ich weiß halt nicht genau, wie ich dich frage, ob du es
tun wirst. Mir kam die Frage 'Möchtest du?' in den Sinn. Und die ist
Unfug, weil ich denke, dass die Antwort offensichtlich ist."

"Ich tue es, wenn du mir sagst, dass du es willst." Lunas Stimme ist leise. Sie
versucht so sehr, nicht zu drängen.

"Ich möchte es", flüstert Kendra. "Das Risiko ist mir klein genug. Ich möchte
gern ohne Schaden hier rauskommen, der über die Größenordnung von
Schrammen hinausgeht, aber ich möchte auch wissen, wie es ist."

"Es kann sehr verschieden sein", sagt Luna. Sie bemüht sich, aus dem Rausch der
Gedanken, in die sie gerade gefallen ist, wieder aufzutauchen. "Möchtest du in
den Hals gebissen werden? Oder eher ins Handgelenk zum Beispiel."

"Wie wäre es mit dem Fußgelenk?", scherzt Kendra.

"Würde ich auch nehmen", antwortet Luna ohne Zögern.

Kendra kichert. "Hast du einen Vorzug?"

Lunas Blick verrät sie sicherlich bereits. "Hals." Sie atmet kaum.

"Mir kommt das glatt vor, als hätte ich dich gerade im Griff, nur
weil du etwas möchtest, das", Kendra zögert, sucht die richtigen
Worte und grinst dann, "in mir ist." Ihr wird ein bisschen schwindelig.

"Sagen wir, du stellst mir etwas in Aussicht, was ich sehr mag", raunt
sie dunkel. "Aber ich komme auch ohne aus."

Kendra geht nicht weiter darauf ein, -- auch wenn es sie nicht kalt
lässt. Sie fühlt sich begehrt, und das fühlt sich interessanterweise
sehr gut an. "Wie läuft das ab? Wenn du mich in den Hals beißt und es
so tust, wie du es am liebsten hättest."

Luna atmet rasch ein und riecht abermals den kräuterigen Geruch und
den nach Kendras Schweiß. Ein wenig Erotik schwingt auch im Duft mit. Kendra
mag den Gedanken, erkennt Luna. Das ist schön. "Ich würde mir,
wenn du mich lässt, das Haar vom Hals streichen, mich dir
langsam nähern, dich vorsichtig beißen und einen Schluck
genießen. Dann würde ich vielleicht ein bisschen in der Position verharren
und mich in das Gefühl fallen lassen. Ich würde über die Bisswunden lecken
und dich wieder freigeben." Lunas Stimme zittert bei der Aufführung nicht, was
sie ein wenig wundert.

"Freigeben klingt, als würdest du mich gefangen halten", erwidert
Kendra.

"Ich würde dich festhalten, damit du nicht unwillkührlich wegzuckst, wenn
meine Zähne gerade in deiner Haut stecken", erläutert Luna. "Wenn ich merke,
dass du wegzucken würdest, gebe ich dich sofort frei, aber ich möchte dich nicht
unnötig verletzen."

Kendra nickt. "Ich denke, ich bin vorbereitet genug."

"Möchtest du es jetzt?", fragt Luna vorsichtshalber.

Kendra nickt abermals.

Luna schließt einen Moment die Augen, lässt sich darauf ein, dass sie
darf. Dass das hier wirklich passiert. Sie öffnet sie wieder und nähert
sich Kendras Körper. Kendra zuckt nicht zurück, als sie mit kühlen Fingern
ihr braunes Haar vom Hals streicht, die Fließjacke wieder ein Stück
öffnet, den Stoff zur Seite schiebt und so die Kehle offen legt. Luna fädelt die
Finger sachte ins Nackenhaar ein, sodass ihr Griff den Kopf bewegungsunfähig
fixieren könnte, was sie nicht tut. Sie führt ihn in
eine gekipptere Position, sachte, und hält nur sehr zart fest. Nun
liegt der Hals bar vor ihr. Sie sieht die Hauptschlagader unter der Haut
pulsieren. Die sie nicht beißen wird, das ist nicht so gesund. Aber die sie
sehr gern beobachtet und nun, sich langsam nähernd, mit der Nase berührt. Sie
atmet Kendras Geruch tief in sich ein. Den dünnen Schweißfilm auf der Haut,
der Kendras Aufregung, und auch ihre Erregung verrät. Das Gemisch aus Salz
und Süße des Lebens, das Blut.

Luna berührt Kendras Hals mit den Lippen und spürt das Beben, das den Körper
unter ihr durchrinnt. Sie mag es, wenn Körper so reagieren.

Kendra merkt, wie Luna genießt, schon bevor die Zähne ihre Haut überhaupt
berühren. Sie ist aufgeregt. Und sie kann nicht leugnen, dass der Situation
eine Erotik innewohnt, die sie zu überwältigen droht. Sie atmet schneller, spürt
Lunas Atem auf der Haut. Muss diese Vampirkreatur atmen? Oder tut sie es, weil
es schön ist? Sie spürt die zitternden, verlangenden Lippen auf ihrem
Hals, so zart. Der Mund öffnet sich und Feuchte schlägt auf den Hals. Langsam
und sehr sanft drücken sich Zähne gegen die Haut. Kendras Atemfrequenz erhöht
sich noch mehr. Was, wenn Luna sie in eine Extase bringt, in der Kendra
die Frage, ob sie ausgesaugt werden möchte, bejahen würde? Aber Luna
hat vorhin von einem lang anhaltendem Todeswunsch gesprochen.

Kendra merkt, dass die Zähne an ihrer Haut schärfer und spitzer sind als
Menschenzähne für gewöhnlich. Sie merkt den Moment, in dem sie
die Haut durchdringen, und es fühlt sich unbeschreiblich schön an. Es
tut nur wenig weh. Sie merkt den leichten Sog, die Zunge auf der Haut,
den raschen Atem und den Genuss im zarten, sensuchtsvollen Ton, der
Luna entweicht, zu sanft, um Seufzen oder Stöhnen genannt zu werden. Sie
genießt es, genossen zu werden.

Die Zähne lösen sich aus Kendras Haut wieder. Luna schluckt. Ihre Finger an
Kendras Kopf sind zärtlich. Sie hat Wort gehalten, denkt Kendra, und ist fast
enttäuscht. Eigentlich nicht nur fast.

Lunas Lippen bleiben an Kendras Haut liegen. Ihre Zunge streicht zart über
die Bissspuren. Dann verharrt sie regungslos. Nur der Atem weht in
raschen Impulsen über die Haut oberhalb der Lippen. Kendra fühlt, dass
Lunas Verlangen noch lange nicht gestillt ist. Ihres auch nicht. "Noch ein
Schluck?", fragt sie flüsternd. "Meinst du, wir schaffen es, dabei zu
bleiben?"

"Ja", flüstert Luna gegen ihren Hals. "Darf ich deinen Hals auch küssen?"

Kendra atmet wieder rascher, so rasch, dass sie fast nicht mehr antworten
kann. "Ja!" Ihr wird schwindelig.

Lunas Hand in ihrem Haar hält sie fester, ohne zu ziepen. Sie legt
den anderen Arm um Kendra und zieht sie an sich heran. Kendras Arm wandert
fast reflexartig über Lunas Rücken in deren Nacken, streicht darüber.

Luna küsst Kendras Hals, zart, zweimal, und nach einer kurzen Pause noch zwei
mal, bevor ihre spitzen Zähne sanft über die Haut streichen, anders als beim
letzten mal. Sie sucht die gleichen Löcher wieder, in denen sie schon gesteckt
haben. Dieses Mal wimmert auch Kendra exstatisch auf, als Luna sie abermals
zart beißt und sie den Sog spürt, durch den sie einen weiteren Schluck Blut
verliert.

Lunas Zähne stecken noch in Kendras Hals, als sie schluckt, ihr Atem weht erleichtert
durch Kendras Haar. Es folgt ein angenehmes Gefühl von Entspannung. Erst
dann zieht Luna die Zähne wieder aus der Haut, leckt noch einmal über die Wunden,
küsst sie, küsst noch eine Stelle weiter unten am Hals, fast am Schulteransatz
und legt ihren Kopf dort ab. "Danke."


