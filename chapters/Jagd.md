\BefehlVorKapitelIBDM{Jagd.png}{Ein Betäubungsgewehr.}{Erschießen, Morbidität}

»Jagd«
======

\Beitext{Paolo}

Hä?

Was zum?

Also erstmal, nein, Paolo ist nicht einfach so davon ausgegangen, dass sein
Silberblick funktioniert. Er hat es für wahrscheinlich gehalten, aber er ist
nicht davon ausgegangen. Das hätte er präziser vermitteln können, aber wozu?

Er ist definitiv nicht davon ausgegangen, dass Kendra ein Vampir ins Haus
schleppt. Ins Haus, in dem Bran gewohnt hat und Brans Geschwister noch wohnt. Was
zur Hölle einfach!

Was, wenn Marcin davon erfährt?

Aber vielleicht ist das nicht, worüber Paolo sich als erstes Gedanken machen
sollte. Vielleicht hat Kendra Recht und Marcin ist in dieser Hinsicht stabiler,
als Paolo vermutet hat. Manchmal braucht es den Blick einer außenstehenden
Person, um Seiten einer Person zu verstehen, die zu lange direkt vor der
eigenen Nase lebt. Es wäre nur nett gewesen, wenn Kendra ihn generell etwas
behutsamer auf Dinge stoßen würde, und nicht so, als wäre er der absolute Noob.

Paolo seufzt innerlich. Er geht langsamer, stellt er fest. Obwohl er Sonja
eigentlich immer gern trifft, möchte er heute nicht so schnell bei der Bank
ankommen. Um sich vorher zu sammeln. Und weil er sich darüber Gedanken macht, ob
er doch versuchen sollte, eine Freundschaft mit Kendra anzufangen.

Paolo hat sich noch nie leicht damit getan, Freundschaften anzufangen. Irgendwas
an seiner Art, wie er mit Leuten umgeht, stört jene. Vielleicht weil es Paolo nicht
leicht fällt, Komplimente zu machen. Er weiß es nicht genau.

Kendra war schroff zu ihm, bisher. Er hat sich teils von ihr verspottet gefühlt. Aber
vielleicht geht es ihr einfach ähnlich wie ihm. Vielleicht führt einfach etwas
an ihrer Art dazu, dass sie nicht so leicht Freundschaften aufbauen kann, aber sie
weiß auch nicht, was es ist. Vielleicht ist ihr nicht bewusst, dass ihre Art diese
Wirkung hat.

Paolo hat sich schon einmal Gedanken darüber gemacht, ob es sich lohnt, Energie
darein zu investieren, besser mit Kendra klarzukommen. Aber er will eigentlich
auch seine Energie für sein Vorhaben bündeln, das Vampir zu töten. Während er
am Anfang wenig Interesse hatte, Kendra näher zu kommen, weil ihn das von
seinem Hauptprojekt abgelenkt hätte, wächst es nun wieder, weil Kendra dasselbe
will. Luna töten. Diese kleine Person mit den kurzen, schwarzen Haaren, die
eben in das Haus seiner Familie gegangen ist. Warum will Kendra es?

Nun wird ihm mit einem Mal klar, warum er eigentlich nicht zu Sonja gehen
möchte: Ihm ist nicht geheuer bei dem Gedanken dass Luna mit seiner Familie im
Haus ist. Was, wenn sie auch Marcin töten möchte? Weil Marcins Blut Brans ähnlich
ist, zum Beispiel. Eigentlich glaubt er nicht, dass Luna zum Morden da ist. Es
ist angeblich noch nie vorgekommen, dass Luna eine Person außerhalb des Waldes getötet
hat. Aber vielleicht lockt sie Marcin dorthin!

Paolo hat eigentlich schon umkehren gewollt, da hat er das Dorf noch gar nicht
verlassen. Aber davon hält ihn wiederum ab, was er denn machen soll. Eigentlich
ist es klar: Luna töten. Aber er hat auch Angst davor. Wenn es nicht klappt, greift
Luna ihn dann doch an? Und wenn er nicht direkt Luna tötet, sondern erst
Pläne für das Wie sammelt, wie soll er sich verhalten? Sollte er mit Marcin
darüber reden? Oder mit Kendra? Die Luna immerhin auch töten möchte. Was
sie in Lunas Gegenwart geäußert hat, wird Paolo bewusst. Und Luna hat nichts
dagegen gesagt. Also weiß Luna selbst, dass sie eine Abnorm der Natur ist, und
möchte gern sterben! Wenn es einen Grund gibt, sterben zu wollen, dann ist es,
dass man selber weiß, wie gefährlich man für die Welt ist oder wird.

Paolo kehrt um. Er hat sich bereits Gedanken darüber gemacht, wie er ein Vampir
am besten tötet. Es gibt, außer dieser einen Geschichte, in der Vampire glitzern,
kaum eine Quelle, in der Vampire nicht mit Holzpflöcken getötet werden können. Er
hat Holzminen für das alte Gewehr seines Großvaters geschnitzt. Als er wieder
daheim ist, holt es aus seinem Zimmer von unter seinem Bett, steigt die Treppen
zu Kendras Zimmer hinauf und klopft.

"Herein?", hört er Kendras Stimme durch die Tür.

Er öffnet sie. Kendra sitzt auf ihrem Bett, Luna am Schreibtisch und macht
Notizen. Paolo schließt die Tür hinter sich und richtet das Gewehr auf
Luna. Luna dreht sich nicht einmal um. Irgendwie fühlt sich das für Paolo
unhöflich an. Aber mit einer Waffe auf eine Person zu zielen, ist sicher
auch nicht gerade höflich.

"Auf die Knie!", sagt er. Seine Stimme zittert nicht.

Luna rückt den Holzstuhl herum, sodass die Füße auf dem Boden reiben, bis
sie Paolo im Blick hat. "Du musst mich nicht bedrohen, um vor mir knien zu
dürfen", sagt sie trocken.

"Es sind Holzpatronen im Gewehr", informiert Paolo. Er fragt sich, was er sich
dadurch erhofft, das zu sagen. Vielleicht, dass sie ihn ernster nimmt.

"Ah", sagt Luna. "Silber ist nun also out. Du machst jetzt was mit Holz."
