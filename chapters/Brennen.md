\BefehlVorKapitelIBDM{Brennen.png}{Eine Schnürung auf der Rückseite eines Kleides, Typ Gothic.}{Erschießen, Brandverletzungen, Blutsaugen.}

Brennen
=======

\Beitext{Kendra, Paolo und Luna}

In Kendras kleinem Zimmer mit den hellblau gestrichenen Möbeln mit
Muster. Draußen ist es feucht und kalt und dunkel. Das Deckenlicht
leuchtet. Luna wünscht sich eine Kerze. "Ich habe richtig verstanden,
Kendra", sagt sie. "Du würdest eigentlich gern woanders wohnen, wo
deine Mutter auch Platz hat, richtig?"

Kendra nickt. "Sie würde dann, sobald wie möglich, nachziehen."

"Wollt ihr mich einfach ignorieren?", fragt Paolo. Er steht mit
dem albernen Gewehr in der Hand vor der Zimmertür im Zimmer.

"Ganz und gar nicht", entgegnet Luna. Sie versucht einen
freundlich reservierten Ton. "Kendra, was hältst du davon, wenn
ich dich morgen nach der Schule abhole, dich mit zu einem alten
Herzwesen von mir nehme und frage, ob jenes noch Platz hat und
ob wir uns alle verstehen?"

Kendra hebt die Brauen und nickt vorsichtig. "Können wir machen. Ich
käme mir dabei sicher merkwürdig vor, eine fremde Person zu fragen, ob
sie Platz für mich hat, aber wenn du sie kennst, und als meine
Assistenz die Kommunikation übernimmst, klingt das interessant." Seit
Luna in ihrem Leben aufgetaucht ist, was gar nicht so lange her ist, wirkt
es auf Kendra, als hätte es die Bahnen des Üblichen verlassen. Alles
ist neu, alles ist aufregend und abgefahren. Sie mag das.

Luna wendet sich wieder Paolo zu. "Ich denke, wenn du hier drinnen
schießt, ruft das Leute auf den Plan, mit denen zu dealen dann
stressig ist. Außerdem könnten Dinge zu Bruch gehen. Mein Vorschlag
ist, ich bringe dich zur Bank, wo du eh hinwolltest, und bevor ich mich
in den Wald verabschiede, darfst du mich ausnahmsweise einmal durchschießen."

Paolo lässt endlich das Gewehr sinken und nickt. Luna will tatsächlich
sterben, realisiert er. Sie weiß nur nicht wie. Und Kendra möchte
ihr helfen. Paolo sollte sich definitiv doch mit Kendra anfreunden. "Möchtest
du mitkommen?", richtet er sich an Kendra.

Kendra schüttelt den Kopf. "Ich bin erschöpft, ich ruhe mich aus." Sie
geht fest davon aus, dass Luna eine Holzpatrone oder auch mehrere überleben
wird. Der Anblick hört sich nicht lohnend an. Sie wird vermutlich
bald spannendere Dinge ausprobieren dürfen.

---

Es ist genau das Szenario, vor dem Horrorfilme quasi warnen: Das Vampir
sieht harmlos aus, noch. Ein Gesicht, das zugleich kindlich und alt wirkt. Es
vermittelt Vertrauen. Und dann, irgendwann unvermittelt auf dem Weg, stellt
sich Paolo vor, wird Luna ihn in den Wald zerren, sich in ein Monster
verwandeln und ihn ermorden.

Es ist nicht so wahrscheinlich, sonst würde Paolo sich darauf nicht einlassen, mit
dem Vampir bei Nacht einen einsamen Weg zu einer Bank an einer Klippe zu
spazieren. Es wird nicht diese Nacht passieren. Sie wird ihn erst in Sicherheit
wiegen. Und wenn das passiert, muss er im richtigen Moment schneller
sein. Für den Fall, dass sie doch nicht sterben will.

Es fühlt sich alles so falsch an. Es fühlt sich zu gefährlich an, neben einer
schweigsamen Mörderin diesen Pfad zu gehen. Auf der anderen Seite hat
er sich auf so etwas ohnehin eingelassen. Es ist nicht gefährlicher, als Nacht für Nacht
auf der Bank zu sitzen und zu beobachten. Nun wird er also das erste Mal
sehen, wie sie in den Wald verschwindet.

Als sie die Bank erreichen, ist der Ort nicht verlassen. Sonja sitzt dort und
schaut in die Ferne. Kopfhörer auf den Ohren und zur Musik wippend, sonst hätte
sie sie vielleicht kommen gehört.

"Sonja", sagt Luna leise.

Sie kennen sich also. Nun, dass Luna ein Wunderwesen nicht entgangen ist, damit
hätte Paolo rechnen können.

"Lässt du dich nun erschießen, oder nicht?", fragt Paolo. Er fühlt sich
eigentlich unsicher, aber es kommt irgendwie streng heraus.

"Durchs Herz möchtest du, nehme ich an?", fragt Luna trocken.

Paolo runzelt die Stirn und nickt.

"Wenn du kein Problem damit hast, mich gegebenenfalls zu ermorden, nehme
ich an, dass du auch kein Problem damit hast, wenn ich mich oben herum
frei mache?", fragt Luna.

Paolo runzelt die Stirn noch mehr. "Du willst nackt sterben?"

"Ich werde überhaupt nicht sterben", korrigiert Luna. "Aber ich habe
um die letzte Jahrhundertwende herum schon allzu viele Löcher in meiner
Kleidung stopfen müssen." Luna zuckt mit den Schultern.

Paolo sagt nichts weiter dazu, als sie das schulterlange Haar anhebt und
ihm den Rücken zuwendet. Es ist klar, was sie will. Paolo öffnet die
Schnürung. Lunas Haut ist kalt und fühlt sich papierern und gleichzeitig
irgendwie zart an. Paolo versucht, die Erotik der Situation zu verdrängen.
Als er erst halb fertig ist, zieht Luna den schwarzen Stoff mit der vielen
Spitze daran ihren Oberkörper herab, sodass sie quasi nur noch einen Rock
mit Gebamsel trägt, -- und einen Spitzen-BH. Den öffnet sie selbst.

Dann, durch eine Bewegung, die Paolo nur als Rascheln und Windzug wahrnimmt,
steht Luna auf einmal im Wald zwischen den ersten Bäumen.

"Das war so nicht abgemacht!", ruft er zu ihr.

Luna zuckt gelangweilt mit den Schultern. "Ich kann mich nicht erinnern,
dir mehr zugesagt zu haben, als dass ich dich hierher bringe und dass du mich
durchschießen darfst. Und ich halte letzteres für ein sehr krasses
Zugeständnis."

Das ist der Moment, in dem Sonja realisiert, dass sie nicht allein ist. Vielleicht
sind sie laut genug gewesen. Sonja nimmt die Kopfhörer ab, legt sie auf die Bank neben
sich und starrt Luna an. Luna grinst mit leicht zusammengekniffenen Augen.

Paolo legt an, bevor sie ganz verschwinden kann und schießt. Holz
splittert von einer Birke neben Luna ab. Ein Schwarm schwarzer Krähen
flattert aus Geäst in die dünn verregnete Luft.

"Komm näher, wenn du treffen willst", empfielt Luna.

"Damit du mich ermorden darfst, weil ich deinem Wald zu nah bin?" Paolo
weiß doch genau, was sie will, aber dennoch nähert er sich ein paar Schritte. Er
hat seine Zielfähigkeiten überschätzt.

"Bis vorm Waldrand bist du sicher", ermutigt Luna. "Und immerhin hat das mehr
Erfolgschancen, als ich vermutet hätte, dass du mich überhaupt durchbohrt
bekommst. Ist es ein Betäubungsgewehr?"

Paolo nickt und nähert sich der halbnackten Gestalt langsam weiter. Er
hat im Vorfeld herausgefunden, dass Holzpatronen in normalen Gewehren
zu weich sind und dabei zerreißen. Er *hat* geübt, aber er hat nur nahe
Testziele durchschossen. Er nähert sich zögerlicher. Ein Teil von
ihm hofft, dass Sonja ihm sagen wird, wann es genug ist. Wann es gefährlich
würde. Aber Sonja tut nichts dergleichen.

Als Paolo glaubt, dass er nun wirklich treffen müsste, zielt er gründlicher
und schießt erneut. Der Knall echot aus dem Wald, wieder flattern Vögel
erschreckt auf.

Luna strauchelt dieses mal, macht ein kurzes unwillkührliches
Schmerzensgeräusch, als hätte sie sich mit einer Nadel in den
Finger gepiekst. Die Kugel hat sie tatsächlich in Herzgegend
durchbohrt, aber Luna heilt so rasch dagegen an, dass Paolo klar
wird, dass Holzpatronen nicht der Weg sind. Er hat es schon eine
Weile angezweifelt, aber ein Teil von ihm hat es doch gehofft.

"Hast du genug? Darf ich mich wieder anziehen?", fragt Luna, als wäre
nichts gewesen.

Paolo nickt resigniert. "Klar. Ich habe ohnehin keine Patronen mehr." Er
ärgert sich über sich selbst. Was für ein Spruch. Er hat nicht das
Gefühl, sich heldenwürdig zu verhalten. Er hat einfach nichts mit
irgendwelchen Held\*innen in Fantasy-Büchern gemein. Nicht souverän, nicht
fertig durchgeplant, nichts klappt sofort. Aber so ist die Realität.

Luna zieht den BH wieder an. Sie bleibt dabei, wo sie ist. Sonja ist
aufgestanden und neben Paolo getreten. Eine Spannung entsteht, die
er nicht so recht einordnen kann. Warum rennt Luna nicht weg? Warum
ist Sonja so nervös?

Luna steckt die Arme wieder durch die Ärmel und dreht sich um, sodass
sie ihren Rücken sehen können. Sie fängt an, hinter dem Rücken zu
schnüren. "Sonja!", sagt sie.

"Hm hm?", macht Sonja.

"Willst du es wagen?" Etwas Dunkles hat sich in Lunas Stimme geschlichen.

Sonja zögert. Aber dann spaziert sie in den Wald hinein zu Luna und
befasst sich mit der Schnürung.

"Nein! Nicht!", schreit Paolo. Aber er kann nichts tun. Er überlegt, in
den Wald zu laufen, um Sonja zu retten. Sonja ist fragil. Zumindest
wirkt sie sehr zart. Ihr Griff in der Schnürung ist allerdings alles
andere als zart. Vielleicht plant sie etwas. Luna ein Gift zu
verabreichen oder im letzten Moment irgendwie zu töten.

Aber als die Schnürung festgezurrt ist und sie flink den Wald
verlassen will, hat Luna auf einmal ihr Handgelenk fest im Griff
und versenkt die Zähne in Sonjas Unterarm. Sonja gibt ein ähnlich
schmerzhaft zartes Geräusch von sich, wie Luna vorhin, als er
sie durchschossen hat. Sie wehrt sich, aber irgendwie nur
halbherzig.

Luna wird sie austrinken. Aber in dem Moment, in dem Paolo doch beschließt,
Sonja zu helfen zu versuchen, lässt Luna von ihr ab. Sie leckt über die
Bisswunde, schubst Sonja aus dem Wald und verschwindet selbst in seinen
dunklen, nebligen Tiefen, übernatürlich schnell.

Sonja taumelt aus dem Wald und setzt sich ermattet zurück auf die Bank. Sie
betrachtet ihren blutigen Unterarm und Paolo beobachtet, wie sich auch hier
die Wunden schließen. Ob das Lunas Magie ist? Dass auch Wunden, die sie
zufügt, rasch heilen?

Er setzt sich neben Sonja. "Was war das?", fragt er. "Was bedeutet es?"

"Eine alte Fehde zwischen uns", sagt Sonja. "Ich wusste, dass sie mich nicht
töten wird."

"Ist dein Blut schlecht für sie, weil es sozusagen rein ist?", fragt
Paolo. "Das Blut eines Wunderwesens, meine ich?"

Sonja blickt auf und lächelt. Sie hält immer viel von Paolos
Ideen. Und normalerweise mag Paolo das. Er fühlt sich dadurch
ernst genommen, und als hätte er tatsächlich ein Verständnis der
Dinge. Aber gerade zweifelt er. Kendra wegen.

"Vielleicht", antwortet Sonja.

"Weißt du, wie man Luna töten kann?", fragt Paolo.

"Oh, du kennst sie jetzt mit Namen!", stellt Sonja fest.

Paolo berichtet von den jüngsten Ereignissen, von all seinen
Zweifeln an sich selber, und Sonja hört gebannt zu. Sie mag
es, ihm zuzuhören. Sie hat wirklich Zeit für ihn und driftet
nie in eigene Gedanken ab.

"Es gibt ein Instrument, das heißt 'Mondklinge'. Es ist wahrscheinlich
in Lunas Hütte im Wald gelagert. Es ist das einzige Instrument, das
Luna töten kann", berichtet Sonja. "Wir müssen irgendwie daran
kommen. Und ich glaube, da ist der Aspekt, dass Luna jetzt
den Wald des öfteren verlässt, etwas, was uns sehr in die Hände spielt."

---

Fahles Sonnenlicht fällt durch die Wolkendecke zwischen den Eichenwipfeln
hindurch auf den Vorplatz vor dem Schulgebäude. Ein Ziegelsteinbau. Alle paar
Steinreihen ist eine Steinreihe grün lackiert und glänzt. Es nieselt.

Endlich gongt es, wieder, endlich ist unter den wenigen Schüler\*innen,
die die Schule verlassen, auch Kendra dabei. Sie ist langsamer als die
meisten anderen. Paolos Gesichtsausdruck, als er Luna sieht, weiß
nicht, in was er sich verziehen soll. Armer, verwirrter Paolo, denkt
Luna, aber verschwendet weiter keinen Gedanken an ihn, als er
davongeht.

Kendra kommt in Begleitung von Marcin. Marcin hat immer die
zärtesten und sanftmütigsten Gesichtsausdrücke, findet Luna. Sie
nickt ihm zu, er nickt zurück. Altvertraut. Aber es ist doch
eine etwas neue Situation, weil sie dieses Mal nicht unter sich
sind.

"Wäre es in irgendeiner Weise störend, wenn ich mitkäme? Oder
wäre ich willkommen?", fragt er. "Ich fühle mich nicht unhöflich
weggeschickt, seid bitte ehrlich."

Luna lächelt. Er ist stets so sehr darauf bedacht, dass sich alle
in seiner Gegenwart wohl und sicher fühlen. "Du und deine Ungeheuer
sind mehr als willkommen", versichert sie. Dann richtet sie sich an
Kendra. "Ich stehe hier seit einer Stunde mit dem Schirm in der Sonne, weil
ich verpeilt habe, dass du noch Magie-Unterricht hast. Das fängt ja
gut an mit meiner Assistenzfähigkeit."

"Hast du Selbstzweifel?", neckt Kendra.

"Ja, habe ich!" Als dürfe eine Person ab einem Alter von 200 oder so
keine Selbstzweifel mehr haben. "Ich meine, als Mörderin hätte
ich mir ohnehin schlechtere Karten bei meiner Bewerbung um den Posten
ausgerechnet."

"Sagen wir, du hast keine Konkurrenz.", sagt Kendra. "Beziehungsweise, es gibt
Leute, die den Job machen, aber damit die bezahlt werden, muss ich gefühlt
durch die Hölle."

"Eine größere, als mit einer Mörderin zusammenzuziehen wohl." Luna grinst.

"Definitiv!" Kendras Blick wandert zum schwarzen Spitzensonnenschirm. Er
schützt auch vor dem Nieselregen, aber vorwiegend vorm Tageslicht. "Verbrennst
du in der Sonne?"

Luna schnaubt. "Davon gehe ich aus."

"Ich denke, du bist unsterblich." Kendra runzelt die Stirn.

"Nun, also, ich gehe schon davon aus, dass ich sowas in der Art wie tot
wäre, wenn man mich in diese Fusionsdingsanlage, die die Sonne darstellt,
mitten hineinyeeten würde", hält Luna fest. "Das ist aber schwer
hinzukriegen." Luna holt tief Atem. "Im Sonnenlicht hingegen passiert
dies." Sie zieht einen Spitzenhandschuh aus, etwas ungalant, weil sie den
Sonnenschirm dabei festhält, und hält die Finger unter dem Schirm
hervor. Sie werfen zügig Blasen und fangen an zu brennen. "Sieht hübsch
aus, nicht?"

Kendra nickt. "Eine gewisse Ästhetik kann ich keinesfalls abstreiten." Sie
sieht genauer hin. "Du heilst dagegen an?"

"Tatsächlich gehe ich bei Tageslicht zwar in Flammen auf, aber
es ist in erster Linie schön. Im Sinne von ästhetisch, nicht in Sinne
von 'fühlt sich gut an.'", hält Luna fest. "Es ist nicht unbedingt besonders
angenehm, aber alles an mir regeneriert gegen das Feuer an, sodass ich
lediglich als Fackel rumlaufe und vor allem alle anderen gefährde. Daher
der Sonnenschirm."

Marcin kichert. Als Luna und Kendra ihm den Blick zuwenden, erklärt er: "So unpraktisch. Ich
dachte gerade, wenn es nachts zu dunkel ist, hast du direkt Licht dabei, aber
nachts funktioniert das ja nicht."

"Ja, finde ich auch", sagt Luna. "Gerade auch, was die Schönheit betrifft. Feuer
hat nachts so viel hübscheren Kontrast. Wollen wir?"
