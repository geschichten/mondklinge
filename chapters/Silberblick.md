\BefehlVorKapitelIBDM{Silberblick.png}{Links eine Birke, rechts eine Bank, beides auf einer Klippe mit Blick aufs Meer. Wolken sind am Himmel. Schemenhaft sind weitere Bäume hinter der Birke angedeutet.}{Keine bisher.}

Silberblick
===========

\Beitext{Kendra}

*Ein paar Wochen später.*

Eine Bank an einer Klippe dicht an einem Birkenwald. Nebel quillt zwischen den
geisterhaft weißen Bäumen hervor und wabert die Klippe herab. Es ist Nacht,
bewölkt, aber der Vollmond erhellt die Wolken von hinten. Es tröpfelt leicht.

Kendra sitzt auf besagter Bank, blickt hinaus aufs dunkle Meer. Sie hat keine
Ahnung, wo sie genau ist, obgleich sie den Ort kennt. Es wundert sie nicht, das
sie hier ist. Das ist so eine wiederkehrende Sache in Träumen, dass
sie irgendwo anfangen, während es sich nicht unvermittelt anfühlt. Sie
ist sich relativ sicher, dass es ein Traum ist, auch
wenn es sich übertrieben real anfühlt, aber sie hat noch diese Denkweise, die
sie kurz vorm Aufwachen in Träumen oder kurz nach dem Aufwachen in der Realität
hat, und letzteres ist angesichts des Ortes, an dem sie sich befindet, eher
unwahrscheinlich: Eine Bank in der Nacht, von der sie nicht weiß, wo auf der Welt sie sich
befindet, ist nicht so der typische Ort fürs Aufwachen.

Das erste Mal, als sie hier gewesen ist, ist ihr der Ort auch bekannt vorgekommen, aber
dieses Mal ist es anders. Damals war es, als hätte sie den Ort nur mal auf einer
Ansichtskarte gesehen oder so. Heute ist es schon mehr fast ein vertrautes Gefühl. Und
sie ist nicht allein.

Sie richtet ihren Blick auf die Person neben sich, die sie mit halb geöffnetem
Mund betrachtet. Verwunderung? Überraschung? Verwirrung? Soll sie die Person
ansprechen? Auf Søregisch? Ist sie noch in Søregen?

"Kommst du aus dem Wald?", fragt die Person, tatsächlich auf Søregisch, und
nimmt Kendra somit die Entscheidung ab.

Kendra wirft einen Blick zum Waldrand. Er ist nicht fern, aber es klingt trotzdem
nicht sehr realistisch, dass sie es von dort unbemerkt von dieser Person bis zur Bank
geschafft hätte. Aber wer weiß, in Träumen ist vieles möglich. "Hast du mich
aus dem Wald kommen sehen?"

Die Person schüttelt den Kopf. Sie hat braune Locken, keine feinen,
ein im Mondlicht hell reflektierendes Gesicht, eine Brille.

"Aber du glaubst, ich könne gegebenenfalls zu schnell gewesen sein, als
dass du mich aus dem Wald hättest kommen sehen können?", fragt Kendra.

Die Person macht den Mund zu, aber reagiert nicht. Es huscht ein Ausdruck über
ihr Gesicht, den Kendra vielleicht als mordlüstern einordnen würde. Zumindest
wütend oder bitter. Sie runzelt die Stirn. 

"Ich weiß genau, was du hier versuchst", sagt die Person.

"Ich nicht", erwidert Kendra prompt und lädt freundlich ein: "Fühl dich
frei, es mir zu erklären!"

Die Person wirkt mit einem Mal eher eingeschüchtert und schüttelt den Kopf. "Schon
gut", sagt sie. "Ich bin Paolo. Pronomen 'er'."

Kendra zuckt mit den Schultern. "Kendra, 'sie/ihr/ihr/sie'." Ehe Paolo
fragen kann, fragt Kendra: "Was machst du hier?"

"Erst einmal herausfinden, ob du vertrauenswürdig bist", antwortet Paolo.

Kendra hebt eine Augenbraue. Nach kurzem Schweigen ohne weitere Erklärungen
seitens Paolos zuckt sie abermals mit den Schultern. "Dann
versuche ich mal das selbe bei dir", sagt sie.

Das scheint nicht zu sein, was Paolo erwartet hat. "Du willst herausfinden,
ob *ich* vertrauenswürdig bin?"

"Ja, warum denn nicht?", fragt Kendra.

"Nachdem du hier einfach aus dem Nichts aufplöppst?", konkretisiert Paolo.

Kendra verengt die Augen leicht und nickt. Das ist die erste interessante
Information. "Oder zu rasch aus dem Wald gekommen bin, als dass du es
mitbekommen haben könntest", murmelt sie. "Was ob meiner Geh-Behinderung
eine hochtrabend alberne Vorstellung ist."

"Du bist behindert?", filtert Paolo heraus.

"Macht mich das nun vertrauenswürdiger?", fragt Kendra. "Oder bringst du, weil
wir in den ganzen Filmen und so stets die fragwürdigen Rollen bekommen, der
behinderten Person nun erst recht Misstrauen entgegen?"

"Die Probleme kenne ich", murmelt Paolo. "Mein Freund ist trans." Aus einem
dunklen Annorak mit Felleinsetzen zieht er einen Notitzblock und einen
Stift hervor, um sich etwas aufzuschreiben.

Kendra lauscht auf das Schreibgeräusch des Bleistifts, das in ihrem Nacken
kribbelt, und anschließend auf das Ratschen eines reißenden Zettels. Wie er
bei der Dunkelheit hat schreiben können, ist ihr nicht ganz klar.

Paolo kontrolliert, was er geschrieben hat, dann blickt er Kendra starr
ins Gesicht. Kendra sieht zurück und beobachtet das gruseligste, was sie
in diesem Traum bisher gesehen hat: Paolos Augen werden silbern. Die
ganzen Augenbälle. Für ein paar Momente. Zwei Wimpernschläge später ist
der Spuk auch schon wieder vorbei.

Er reicht ihr den Zettel. "Die Adresse einer Schule, in der wir zusätzlich
zum normalen Unterricht auch lernen, unsere magischen Fähigkeiten zu
kontrollieren", teilt er mit.

Kendra nimmt den Zettel entgegen und grinst, weil sie erwartet, dass sie
ihn nicht lesen kann, und das nicht nur wegen der Dunkelheit. Sie
kann nur sehr selten in Träumen lesen. Wenn
sie es kann, kommt Kauderwelsch heraus. Sie blickt hinab auf den Zettel
und kann ihn tatsächlich nicht lesen, was allerdings in erster
Linie daran liegt, dass ein Ruck ihren Körper durchfährt und sie zitternd
in ihrem Bett zu sich kommt.

Den Zettel hat sie noch in ihrer Hand.

---

In einem weichen, warmen Bett in einem kleinen Zimmer. Kendra liegt unter der
Decke, behaglich müde. Definitiv zu müde, um überwindungsfrei aufzustehen,
und zittert noch vor Kälte. Sie kann sich nur schwer vorstellen, unter den
wamen Decken hervorzukriechen. Sie hat außerdem generell Probleme damit, wieder
einzuschlafen, wenn sie erst einmal richtig wach ist. Der Zettel in ihren Händen
versucht sie zu überreden, dass es heute aber doch schon sinnvoll wäre. Der
Schlaf gewinnt trotzdem.

Und so findet sie den Zettel erst am nächsten Morgen auf dem Stuhl neben
ihrem Bett. Glücklicherweise ist Wochenende, weshalb sie nicht in die
Schule muss. Kendra startet ihre Morgenroutine, zu der Durchlüften
gehört, sich bekleiden und sich unten mit einem Müsli an den Tisch zu
setzen. Den Zettel nimmt sie mit hinab und legt ihn in sicherem Abstand,
auf dass er keine Hafermilch abbekäme, neben der Müslischale ab. Ihre
Mutter schläft noch. Sie hat gestern eine Nachtschicht gehabt und wird
vermutlich erst gegen Mittag aufstehen. Kendra liest die krakeligen Zeichen
ein weiteres Mal. Paolo hat so eine Handschrift, wie sie medizinischem
Fachpersonal stets zugeschrieben wird. Nur schwer leserlich. Aber
als behinderte Person ist Kendra einigermaßen geübt. Noch dazu
ist sie sehr wohl in der Lage, selbst zu recherchieren.

Sie hat also eine magische Fähigkeit, hat Paolo impliziert. Damit gehört
sie zu jenen 0.5% bis 1% der Bevölkerung, auf die das zutrifft. Es
gibt eine recht große Dunkelziffer. Viele merken es gar nicht oder
ihre magische Fähigkeit beeinflusst das Leben so wenig, dass sie nichts
an ihrem Leben ändern und es nicht melden. In der Schulklasse ihrer
Oma soll eine Person gewesen sein, die ihren Arsch sachte zum Leuchten
hat bringen können. Das ist jener Person in erster Linie peinlich
gewesen.

Kendras Fähigkeit ist schon interessanter, auch wenn sie sich nicht
ganz sicher ist, was es nun genau ist. Kann sie aus Träumen etwas
mitnehmen? Kann sie in die Zukunft sehen? Oder ist sie
an jenem Ort tatsächlich gewesen? Vielleicht kann sie über Träume,
die ein wenig in die Realität hineinschwappen, mit anderen reden, die
magische Fähigkeiten haben.

Kendra wägt die Möglichkeiten eine Weile ab und kommt zu dem Schluss,
dass sie schon für wahrscheinlich hält, dass ein Teil des Traums, nun,
wirklich Traum gewesen ist. Paolos seltsames Gebaren lässt sich
besser mit Traumlogik erklären als mit Realitätslogik.

Kendra findet bei ihrer Internetrecherche zwei Schulen mit
Unterrichtsfach Magie, die für sie in Frage
kommen. Auf Søregen gibt es fünf, aber drei schließt sie wegen Standortfaktoren
aus. Es darf keine allzu bergige Gegend sein, weil Kendra
nicht gut auf den Beinen ist. Sie verkraftet nur beschränkt viel
körperliche Anstrengung am Tag. Das schließt die im Runengebirge
in Mittelsøregen aus. Und es darf nicht die Hauptstadt sein,
was gleich zwei der Schulen ausschließt, weil ihre Mutter nicht gut mit
zu vielen Menschen zurechtkommt. Zumindest wenn sie mitkommt, kommen
diese nicht in Frage, und Kendra hofft darauf, nicht ohne sie den Ort
wechseln zu müssen. Nun, sie muss überhaupt nicht, aber sie weiß, dass
es schon ganz gut wäre, wenn sie magische Fähigkeiten hat, diese dann in
den Griff zu kriegen.

Für die übrigen zwei Schulen versucht sie die Lage bezüglich Krankengymnastik
und medizinischer Notversorgung zu checken. Das Dorf, zu dem die Schule
mit der dafür ausreichend leserlichen Anschrift auf Paolos
Zettel gehört, hat zwei
Physiotherapeutinnen und einen Orthopäden. Die kleine Stadt hat mehr im
Angebot, aber ist auch schon grenzwertig groß für Kendras Mutter. Nun. Zwei
Physiotherapeutinnen muss bei der Bevölkerungsdichte reichen. Vielleicht
war dieser Traum also so etwas wie ein guter Hinweis für ihre Recherche. Auch
wenn Kendra sich nicht gern von etwas Unbekannten in eine Richtung pushen
lässt, erleichtert es ihr die Entscheidung. Vielleicht kann
sie dadurch ja sogar mehr über diese Figur Paolo erfahren.

Als nächstes sucht Kendra nach Wohnungen, und das sieht, wenig überraschend,
in so einem kleinen Dorf eher mau aus. Sie findet ein kleines Zimmer
zur Untermiete. Ob sie da mit ihrer Mutter vorübergehend provisorisch
unterkommen kann? Viel Kram haben sie nicht und viel Platz haben sie auch
nie gehabt. Aber alles wird nicht in den kleinen Raum passen und Kendra
hat auch eigentlich keine Lust, ein Zimmer mit ihrer Mutter zu teilen.

Irgendwann, als Kendra Pizza zum Mittag aufbackt, wacht diese endlich auf. Kendra
bespricht mit ihr die Lage. Nach einigem Abgewäge kommen sie zu dem Schluss, dass
es gut wäre, wenn Kendra in zwei Wochen, wenn das Schulhalbjahr wechselt,
besagtes Zimmer bezieht, und Kendras Mutter nachkommt, sobald sie dort eine
Bleibe finden können. Kendras Mutter macht Support für eine IT-Firma, das
kann sie auch remote.

---

Die Reise ist abenteuerlich, wie stets, wenn es aufs Land geht. Kendra fährt mit
einem großen Rollkoffer erst Zug in den Norden Søregens und steigt dort nach zwei
Stunden Wartezeit in den einzigen Bus, der zweimal an Wochentagen in
besagtes Dorf fährt, und wartet dort, bis die wenigen Menschen, die mit ihr
ausgestiegen sind, sich in alle Richtungen verstreut haben. Übrig bleibt eine
Person, etwa so groß wie sie, langes, blondes Haar, schwarzer Mantel.

"Bist du Marcin? Der Mensch, der mich abholen wollte?", fragt sie.

"Das r und das c verschleifen sich zu einem tsch-Laut, wie in
Checkpoint", korrigiert Marcin Kendras Aussprache.

Kendra wiederholt den Namen und entschuldigt sich dafür, versäumt zu haben,
sich im Vorfeld über die Aussprache zu erkundigen. "Polnisch?"

Marcin nickt. "Ich bin als Kleinkind nach Deutschland eingewandert und vor
ein paar Jahren mit meiner Schwester nach Søregen gezogen."

"Ich wollte gar nicht so grausig persönliche Fragen stellen, es tut mir
leid", entschuldigt sich Kendra abermals. "Ich wollte eigentlich nur fragen,
ob ich den Namen richtig als polnisch einordne."

Marcin nickt abermals. "Ich werde oft bei meinen ersten Begegnungen
nach Genitalien und meiner Gebärfähigkeit ausgefragt. Dagegen bist du
harmlos!", sagt er, und beeilt sich hinzuzufügen: "Ich bin oft sehr
frei heraus. Damit kommen viele Leute nicht so gut klar."

Etwas Schwarzes flackert um seine Schulterpartie. Kendra blinzelt, aber es
ist auch schon wieder weg. "Es hatte fast die Form eines Drachens", murmelt
sie.

Marcin nickt, sagt dieses Mal nichts. Er wirkt vielleicht besorgt.

"Versucht du meinetwegen etwas zu unterdrücken?", fragt Kendra. Das
kommt ihr nun tatsächlich wie eine derbst persönliche Frage vor, noch
bevor sie die Antwort hat.

Marcin nickt ein weiteres Mal. "Ich will dir keine Angst machen."

"Es ist ziemlich challenging, mir Angst zu machen", sagt Kendra
leise. Sie versucht es beruhigend klingen zu lassen.

Einen Moment schweigen sie. Dann streckt Marcin die Hand nach dem Koffergriff
aus. "Darf ich?"

Kendra nickt dankbar und ermattet. Sie humpelt ziemlich, stützt sich sehr
auf ihren Gehstock, als sie sich in Bewegung setzen.

"Kann ich dich noch irgendwie stützen oder so etwas?", fragt Marcin.

"Nein, aber wenn du es aushältst, noch langsamer gehen", bittet Kendra.

Marcin nickt noch einmal. Ein Lächeln huscht über sein Gesicht. Dann
ist es auch wieder verschwunden. Die schwarze Form flackert noch einmal um
seinen Schulterbereich auf, wie als könne sie sich nicht so richtig
entscheiden, aber schließlich tut sie es. Kendra lächelt dem kleinen
Drachen zu, der auf Marcins Schulter hockt. Er nebelt. Kendra würde
das Schwarze, was vom Drachen herabwabert und sich im Nichts verliert,
von der Verhaltensweise her am ehesten mit Flüssigstickstoff
vergleichen. Das einzig nicht Schwarze und nicht leicht nebulöse am
Drachen sind die grauen Augenbälle.

"Macht es dir doch Angst?", fragt Marcin.

Kendra schüttelt den Kopf. "Kein bisschen bisher. Ist es deine
Magie?"

Marcin nickt einfach, wie so oft. "Ich habe sie nicht immer im
Griff", gibt er zu.

"Dazu sind wir ja hier", sagt Kendra zuversichtlich. Aber ihr schwant,
dass Marcin nicht so gute Erfahrungen damit gemacht hat, wenn es sich
bemerkbar gemacht hat, dass er es nicht im Griff hat.

Marcin sagt nichts dazu. Er führt sie langsam durch die Gassen des
Dorfes zu einem Reihenhaus mit nicht so ganz geraden Fassaden. Er hievt
ihren Koffer mühelos die drei Stufen zur Haustür hinauf und hält sie ihr
auf, und das eine ganze Weile, weil sie die Stufen nur mit großer
Mühe schafft. "Es gefällt mir überhaupt nicht, dass du ausgerechnet
das kleine Zimmer im Dachboden bekommst, weil du noch so viele Treppen
vor dir hast, aber ich kann da leider im Moment nicht an deiner statt
wohnen. Es tut mir leid."

Kendra sagt nichts dazu. Sie kann sich gerade kaum vorstellen, wie das
mit ihr und den Treppen auf Dauer gehen soll, aber auf der anderen Seite
ist sie heute nach der Fahrt auch besonders fertig.

"Willst du die Tür nicht langsam mal wieder zu machen? Es wird kalt!", hört
Kendra eine andere Stimme, eine leicht vertraute, mit der sie nicht gerechnet hat.

Marcins Drache verpufft ins Nichts. Eilige Schritte nähern sich und in der Tür
erscheint der mysteriöse Mensch mit den silbernen Augen aus ihrem Traum. Paolo.
