\BefehlVorKapitelIBDM{VorRueckNachAnAbZuverUndUebersicht.png}{Ein von innen gepolsterter Sarg. Die Klappe steht offen. Er hat Schnörkel und Griffe an den Seiten.}{Knochenbrüche, Morddrohungen.}

Vor-, Rück-, Nach-, An-, Ab-, Zuver- und Übersicht
==================================================

\Beitext{Marcin und Luna}

Eigentlich, findet Marcin, hat sich Paolo wirklich bemüht. Warum
sollte sein Ex perfekt darin sein müssen, nicht in die Opferrolle zu fallen,
dafür dass Marcin mit ihm klarkommt? Warum ist es so, dass Marcin in
diesen Situationen, in denen Paolo kritisiert wird oder Personen
sich von ihm abgrenzen, quasi nur darauf wartet, dass er in seine Spirale
stürzt, aus der er alleine kaum mehr herauskommt? Ist es böse von
Marcin, dass er da manchmal keine Kraft für hat?

Es ist früh in der Nacht. Er schleicht sich aus Kendras Zimmer, die neben ihm schläft,
die Treppe hinauf, an deren Rändern sich Bücherstapel türmen, zu Lunas
Zimmer. Luna öffnet die Tür ehe er klopfen kann, und macht eine
einladende Geste.

"Hast du mich gehört?", fragt er, als die Tür wieder geschlossen ist.

Luna nickt. Ihr skeptischer Blick verfängt sich wieder an dem Sarg im
Zimmer. "Léonide denkt irgendwie, ich könne mich über einen Schlafsarg
freuen", sagt Luna unüberzeugt. "Ein Schlaf*sack* hätte mir besser
gefallen."

"Findest du es zu makaber?", fragt Marcin.

"Ich?" Luna schnaubt. "Etwas *zu* makaber finden?"

Marcin kichert. "Er eignet sich nicht so gut, um
nebeneinanderzusitzen", kommentiert er.

"Und beim Beieinanderliegen müssten wir ziemlich kuscheln", ergänzt
Luna. Sie seufzt. "Ich würde auf diesem Holzstuhl Platz nehmen, wenn
du mit dem Sarg Vorlieb nehmen willst. Oder umgekehrt. Oder
wir setzen uns auf den Boden."

"Oder wir kuscheln im Sarg", wagt Marcin leise vorzuschlagen.

Luna schüttelt den Kopf. "Ich kann mich körperlich definitiv davon
abhalten, dich zu ermorden", versichert sie. "Aber wenn du mir heute nah
bist, wird einiges meines Denkens mit Mordfantasien ausgelastet
sein. Das möchte ich nicht."

Marcin grinst sie an. Er fühlt sich bei Luna vor allem wohl, weil sie
beide oft unwillkommene, sich aufzwängende Gedanken haben. Vernichtende
Gedanken. Während Luna viel über das Morden nachdenkt, zerstört Marcin
in Gedanken oft die ganze Welt. Infernös. Lässt sie in angenehmes Dunkel
versinken, in seine Schatten, wo sie zu Asche zerfällt.

Er kennt Luna schon länger, als Bran sie gekannt hat. Er hat sich an den
Waldrand gesetzt und seine düsteren Geschichten über Zerfall in den
Wald hinein vorgelesen. Weil er eigentlich wollte, dass jemand ihm
zuhört, aber sich vor keiner Person, die er kannte, getraut hat. Er hat
einige davon unter Pseudonym im Internet veröffentlicht, aber darauf kam
nie Rückmeldung und das hat ihm nicht gereicht. Er wollte sie vorlesen. Weil
beim Vorlesen die Ungeheuer in diese Welt glitschen, die er so liebt. Sie
waren bei ihm, solange er gelesen hat, und sind dann in den Wald entfleucht, wo
sie das Böse des Waldes entweder ergänzen, oder von der Gefahr verschlungen
werden. Nachts am Waldesrand hat Marcin sich sicher gefühlt, niemandem mit
ihnen zu schaden, als er noch glaubte, sie wären gefährlich.

Und dann ist Luna aufgetaucht. Sie hat gemeint, wenn sie schon zuhört, kann sie
es auch offiziell tun. Luna hat seine Ungeheuer gemocht. Und seine Kunst. Auch
wenn sie sie nicht unkritisiert gelassen hat. Aber die Kritik war wohltuend. Marcin
hat sich schreiberisch und auch anders künstlerisch schon immer mit Kritik
weiterentwickeln wollen, aber alle in seinem Umfeld bis dahin waren entweder
Typ 'Das kannst du doch nicht machen, du kannst nicht immer alles zerstören!' oder Typ
'Oh wow, deine Kunst ist super gut, so schön!'.

Luna hat sich mit ihm und seiner Kunst auseinandergesetzt. Einfach so.

Marcin ist schon halb dabei, sich auf dem Stuhl niederzulassen, aber überlegt
es sich anders. Der Sarg wirkt gemütlicher. Er gönnt anderen meistens das
Gemütlichere, aber heute möchte er für sich entscheiden. Also setzt er sich
in den Sarg, rückt ein Kissen an die Holzkante und lehnt sich an.

"Weise Entscheidung", sagt Luna und setzt sich auf den Stuhl. "Du hast deinen
Reader nicht dabei. Ich schließe, es gibt keine neue Geschichte?"

"Stattdessen eine Frage", erwidert Marcin. "Wenn ich in deinen Wald käme, würdest
du mich töten?"

Luna seufzt und nimmt die Füße mit auf den Stuhl. Sie umarmt die Beine und blickt
Marcin auf eine Weise an, die er fast niedlich findet. "Ich habe heute Abend irgendwie
mit der Frage gerechnet", sagt sie. "Du würdest in den Wald kommen, um Paolo
zu retten, falls eine Situation entsteht, in der das Sinn ergeben könnte, richtig?"

Marcin denkt einen Moment nach, ob er Lunas Frage beantworten möchte, bevor
sie seine beantwortet, aber nickt dann doch. "Ich weiß nicht, ob ich es tun
würde, aber ich wüsste gern, woran ich dann wäre."

"Ich möchte dir die Frage nicht beantworten", leitet Luna ein. "Ich komme
nicht davon weg, zu denken, wenn ich anfangen würde, Ausnahmen zu machen, dass dann
die Leute in meinen Wald strömen. Meinetwegen mit freundlichen Absichten, um mir
Kekse vorbeizubringen oder eben um eine Person zu retten." Sie streicht das Haar
hinters Ohr, auf der Seite, wo sonst immer eine Spange in Form eines Schmetterlings
es zurückhält, die fehlt. "Lass die Sache sein, Marcin. Wenn du meinen Wald
betrittst, während ich mich darin aufhalte, werde ich es wissen. Wenn ich die
Absicht haben sollte, Paolo etwas anzutun, dann werde ich es ab dem Moment nur um
so schneller tun."

"Du hast mir leider schon erzählt, dass du mindestens eine halbe Stunde, eher
länger brauchst, um eine Person so weit auszusaugen, dass sie stirbt", sagt
Marcin leise.

Lunas Gesicht entspannt sich und sie lächelt ein wenig, als sie noch leiser
informiert: "Es gibt auch andere Weisen des Tötens, als durch Blutsaugen. Sie machen
weniger Spaß, aber mehr als gar nicht zu morden."

---

Es ist Nacht. Der Vollmond ist so grell, dass Luna sich vorstellt, wie das
darin reflektierte Sonnenlicht unter ihrer Haut ein loderndes Kribbeln
auslösen könnte. Aber das tut es natürlich nicht.

Die Gesamtsituation stimmt Luna eher so semizufrieden. Auf dem negativen
Blatt steht die Sache mit Paolo und Sonja. Luna denkt, dass Lauden
Léonide von Horstenfels' Vermutungen schon richtig sein mögen: Sonja möchte
herausfinden, ob Luna ihre Regeln wirklich so fix befolgen würde, jede
Person zu ermorden, die mutwillig und um sie zu ärgern in ihren Wald käme, wenn
daran an Stellschrauben gerüttelt würde, die es ethisch noch fragwürdiger
machen. Luna öffnet das Fenster. Weit und breit niemand hier. Sie
stellt sich vor, wie Sonja auftaucht, sich auf ihr Fensterbrett
setzt und sie anlächelt. "Ich wollte dir zeigen, dass du es dir zu
einfach machst mit deinen Regeln", könnte sie sagen.

Luna seufzt. Sie mag Dinge einfach. Und zum Schlafen in einen Sarg zu
klettern, ist sicher nicht so einfach, wie in ein Bett. Sie probiert
es trotzdem. Aber sie mag es wirklich nicht. Dass im Liegen ihre Gliedmaßen
überall gegen stoßen können. Sie schläft gern mit einem angewinkelten
Bein. Aber... kann sie ein Geschenk einfach aus dem Fenster schmeißen? Eines
von einer Person, deren Gesellschaft sie mag? (Und das steht auf dem guten
Blatt: Die enstehende WG.)

Luna seufzt und klettert wieder aus dem Sarg. Sie hält den Kopf hinaus
in den Wind. Sie sehnt sich nach dem Waldboden unter den Füßen. Nach der
Einsamkeit des Waldes. Der Stille des Geraschels. Dem Flüstern in den
Wipfeln. Dem Leben im Gewurzel.

Sie springt aus dem Fenster. Als sie aufkommt, erschließt sich ihr, dass das
so keine optimale Idee war. Sie hört ihren spitzen Schrei im Wohnzimmer noch
nachhallen. Und es braucht einen Moment, bis die Knochen in ihren Beinen sich
wieder zusammenfügen. Sie ist noch nie aus dem ersten Stock gesprungen. Aus
höheren Höhen, um ihre Grenzen zu erforschen, schon, aber über die Ungünstigkeit
von nur einem Stockwerk hat sie sich einfach bisher nicht den Kopf zerbrochen.

Als sie sich mühsam wieder aufrichtet, öffnet Kendra die Terrassentür. Na gut,
vielleicht hat Luna auch auf sie gewartet, nachdem sie mitbekommen hat, dass sie
aufgewacht ist. Kendra nutzt ihren Stock mehr als sonst. Auch das hat
Luna mitbekommen.

"Ich tauge nicht als Assistenz", begrüßt sie Kendra. "Im Moment zumindest
nicht. Ich scheuche dich an so einem Tag aus Versehen aus dem Bett."

"Indem du dir die Beine brichst", ergänzt Kendra trocken.

Luna nickt. "Ich halte es in einem Haus nicht aus", sagt sie. "Und das
müsste ich auch als deine Assistenz. Aber vielleicht würde es mit einem
Bett anstelle eines Sarges gehen."

"Warum nutzt du dann einen Sarg und kein Bett?", fragt Kendra. "Eine
Frage des Stils?"

"Unfug." Luna fährt sich durchs Haar. Die Spange fehlt. Aber ein Teil von
ihr findet es gut. "Das ehrwürdige Lauden von Horstenfels hat ihn mir
besorgt. Ich bin kein Fan davon. Nun muss ich über ein Geschenk nölen."

"Ich verstehe." Kendra klingt tatsächlich einfühlsam.

"Ich komme morgen früh wieder. Und dann reden wir über die Zukunft", verspricht
Luna. "Es sei denn, ich finde Paolo im Wald."
