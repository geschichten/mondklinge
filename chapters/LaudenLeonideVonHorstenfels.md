\BefehlVorKapitelIBDM{LaudenLeonideVonHorstenfels.png}{Ein weicher Keks mit Schokoladenstückchen drin.}{Morddrohungen, Folter - erwähnt.}

Lauden Léonide von Horstenfels
==============================

\Beitext{Léonide, Luna und Kendra}

Léonide ist alt genug, um zu wissen, dass Alter nicht unbedingt
etwas mit Zahlen zu tun hat. As ist 70. Fast auf den Tag genau. Niemand
ist zum Gratulieren da gewesen, und das ist auch gut so. Léonide hat
es zu diesem Zweck über nun ungefähr 50 Jahre in diesem Dorf hinbekommen,
niemandem zu verraten, wann dieser olle Jahrestag ist. As ist kein
Fan von Glückwünschen. Und diese Eigenschaft hat sich mit dem Alter
eher verstärkt.

As ist alt. An Tagen wie diesen, nach so einem Einkauf für eine Woche, den
as mit einem Rollwägelchen nach Hause gezogen hat, das stets alle mitleidig
anstarren, wünscht as sich die Villa, in der as lebt, eher irgendwo in die
Pampa. Wo es richtig abgeschieden und ruhig ist. Vielleicht in diesen
Birkenwald, neben der Holzhütte, die Luna angeblich bewohnt, die aber
aus guten Gründen noch nie jemand gesehen hat. Aber das würde Luna
wohl nicht zulassen.

Luna hat sehr viele Jahre gelebt, aber Léonide ist älter. Müder und
altersgezeichneter. Etwas, was Léonide an sich durchaus mag. Luna
und as haben dadurch eine sehr interessante Verständnisebene.

Léonide jedenfalls wohnt nicht irgendwo ab vom Schuss, am Rande des Dorfes. Die Villa
steht auf einem immerhin brauchbar verwunschenen Grund mit Hecken und Efeu
und allem etwa 300 Meter von der Schule entfernt. Léonide hört früh morgens
schon das aufgeregte Geprappel (ist das ein Neologismus?) von jungen Leuten, die
zu früh morgens schon zu wach sind, und das dumpfe, nölende Geprappel jener, die
zu früh morgens schon zu wach sein müssen.

Léonide verstaut das Eingekaufte in Kühl- und Vorratsschrank und will sich gerade
aufs Ohr hauen, als es läutet. Léonide überlegt, die Türglocke zu ignorieren. In
der Vorweihnachtszeit gibt es öfter Kinder, die Süßkram wollen. Aber der Frühling
naht. Léonide schleppt sich ermattet zur Tür und öffnet. Wenn man vom Vampir
spricht...

"Wen hast du denn dabei?", sagt Léonide.

"Das sind Kendra und Marcin, mylauden." Luna verbeugt sich, nicht tief. Die beiden
Jungspunde, die sie begleiten, informiert sie: "Lauden von Horstenfels. Lauden fungiert als neutraler Eratz
für Lord und Lady. Wenn wir über Lauden Léonide von Horstenfels sprechen,
nutzen wir die Pronomen 'as/sain/iem/as'. Ist das noch richtig?"

Léonide nickt. "Gedenkst du, euch bei mir einzuladen?"

"Ich hoffte, dass ihr mir zuvorkommt, Lauden von Horstenfels", entgegnet
Luna.

"Und warum sollte ich das tun?" Vielleicht, überlegt Léonide, sollte
as gerade kein schäkerndes Spiel mit diesem Vampir spielen, während andere
daneben stehen und nicht verstehen, worum es geht.

"Ich backe euch Kekse?", schlägt Luna vor.

Es ergibt überhaupt keinen Sinn, dass Luna Léonide euchzt. Aber sie tut
es trotzdem. Bei wem hätte sie sonst die Gelegenheit, diese Form mal
zu nutzen, wenn nicht bei dem alterwürdigen Enby dieses Dorfes. Ein
Gentleenby, kurz Genby? Die nicht-binäre Gentleperson in town, jedenfalls.

Léonide seufzt und gibt den Eingang frei. "Du weißt immer, wie du mich kriegst." As
scheucht den Besuch in ein kleines, gemütliches Wohnzimmer mit Kamin. Und
Unordnung. Wen schert es.

Léonide nimmt auf dem Sofa Platz. Der niedrige Couchtisch davor, ein
filigranes Teil aus dem letzten Jahrhundert, hübsch lackiert, ist immerhin
gewischt. Luna nimmt im Ohrensessel Platz. Marcin bietet Kendra den zweiten
Sessel an, ein kleinerer in rosa. Léonide rechnet halb damit, dass also
Marcin in den sauren Apfel beißt und zu ihr aufs Sofa kommt, aber dieser nimmt
sich stattdessen den Klavierhocker vom Kurzflügel weg und stellt ihn an
den Tisch. Er hat auffällig lang am Kurzflügel verharrt. Ob er ihn spielen
kann?

"Was verschafft mir die Ehre?", fragt Léonide. "Gedenkst du mich zu töten?"

"Oh, möchtet ihr?", fragt Luna. Ihr Ausdruck ist freundlich und vermittelt
zugleich Bereitschaft.

Léonide verengt die Augen leicht. Sie mag das Spiel, aber das ist eine Grenze, wo
sie dann doch lieber ernst wird. "Das war eigentlich nicht die
Frage. Irgendwann vielleicht, wenn du willst. Ich möchte vorher noch
Testamentsdinge und so machen."

"Dabei kann ich gern behilflich sein", bietet Luna an.

Das ist neu, denkt Léonide. "Dass du es eiliger hast als ich, ist schon etwas creepy."

"Oh, in diesem Fall war mein Anliegen vor allem Übung mit Formalem zu gewinnen.", lenkt
Luna rasch ein. Ihre Überraschung über das Missverständnis wirkt
echt. "Ich will ja nun Assistenz sein. Von Kendra."

Léonide wirft letztgenannter einen abschätzenden Blick zu. "Du
bist für sie hier?"

"Kendra sucht eine Bleibe im Dorf. Eine, die besser kompatibel mit
ihrer Behinderung ist als Brans altes Dachgeschosszimmerchen, und eine,
wo auch ihre Mutter mit hinziehen kann.", legt Luna offen.

"Und ihre Assistenz, nehme ich an." Léonide betont es nicht als
Frage und hofft doch auf eine klare Antwort.

Luna nickt zögerlich. "Es wäre mir eine Ehre, mylauden. Wenn ihr die
Menge Zimmer übrig habt?"

Léonide schüttelt mit einem schnaubigen Grinsen den Kopf. "Ich brauche
die meisten Zimmer nicht so richtig, da hast du schon recht", sagt
sie. Sie zählt mit ihren Fingern die Zimmer in ihrer Vorstellung
ab. "Ich sollte vier frei machen können." Sie nickt Kendra zu und
dann zu einer Tür auf einer kleinen Empore im Hintergrund. "Das ist
mein Arbeitszimmer. Ich denke, wenn du hier einzögest, dann wäre
wichtig, das es unten und nah bei der Küche ist. Ich habe mich aus
den Gründen schon im alten Waschzimmer ausgebreitet und es entsprechend
an meine Bedürfnisse angepasst. Das sollte mit dem Zimmer auch
gehen. Ich arbeite nicht mehr so viel, dass ich ein Arbeitszimmer
dafür brauche."

"Wäre es etwas, was ihr bereit wäret, zu ermöglichen?", fragt
Luna.

"Ich vermute, ohne Bezahlung?", fragt Léonide.

"Ich zahle Miete für das Zimmer bei Angela", widerspricht Kendra. "Wir
können das erübrigen."

"Erübrigen!", betont Léonide. "Also ohne Bezahlung. Wenn du und deine
Mutter euch nicht zu doll dagegen wehrt. Marcin ist auch ohne Bezahlung
fein. Luna zahlt in Keksen. Sonst noch etwas?"

Kendra weiß nichts zu erwidern, aber sie fühlt sich sehr merkwürdig. Einfach
von einer fremden Person ein Zimmer aufgedrückt zu bekommen. Was das für
eine monatliche, finanzielle Entlastung darstellen würde! Es fühlt sich
ungehörig an.

"Ich hatte gar nicht nach einem Zimmer gefragt", hält Marcin fest.

Léonide zuckt mit den Schultern. "Ich gehe das jetzt auch erstmal
alles in Gedanken durch", erwidert as. "Ich denke, es ist halt so
aus sozialer Sicht sinnvoll, diese Sache zu machen, aber es ist
auch eine ganz schöne Umstellung für mich. Ich habe es eigentlich
gern ruhig. Aber ihr seid gute Leute, wenn Luna euch mitgebracht
hat." As seufzt. "War es das denn? Oder soll, keine Ahnung, noch irgendwie
ein Drache einziehen oder so."

"Ich dachte daran, euer Wohnzimmer als Folterkammer umzugestalten", wirft
Luna ein, bevor zu auffällig ist, dass Marcin sich erwischt fühlt. Wenn
er über seine Ungeheuer reden will, dann von ihm aus und nicht, weil er sich verrät.

"Jetzt wird es interessant!", betont Léonide.
