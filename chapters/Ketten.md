\BefehlVorKapitelIBDM{Ketten.png}{Die andere Hälfte des Kettenanhängers, die zu dem vorm ersten Kapitel gehört. In diese Herzhälfte ist ein B eingraviert.}{Erotik.}

Ketten
======

\Beitext{Paolo}

Paolo klopft an die Badezimmertür. "Kann ich reinkommen?"

Statt, dass Marcin antwortet, dreht er einfach den Schlüssel herum, sodass
Paolo die Tür öffnen kann. Er schließt sie direkt wieder hinter sich. Marcin
trägt nichts als eine Unterhose und einen Binder.

"Meinst du nicht doch, dass mir eher zumutbar ist, in Brans altem Zimmer
zu wohnen, als Kendra?", fragt Marcin.

Paolo schüttelt alarmiert den Kopf. "Der Unfall auf dem Markt ist doch
gerade mal einen Monat her", ermahnt er. "Willst du es gleich wieder
provozieren? Mit einer Fremden im Haus?"

"So fremd schien sie dir nicht", wendet Marcin ein.

Paolo nickt. Er hat nicht unbedingt damit gerechnet, dass die Person hier
auftauchen würde, die er vor zwei Wochen auf der Bank getroffen hat, auf
der er so oft den Wald beobachtet und hofft, dem Vampir irgendwann zu
begegnen, wenn es Gegenstände Verstorbener zurücklässt. Paolo hat Marcin
die Kette immer noch nicht gegeben. Er weiß, dass er es eigentlich tun
sollte, aber er bringt es nicht über sich. Brans Tod würde sich dadurch
noch endgültiger anfühlen. Für sie beide. Paolo hat Angst, was das
in Marcin auslösen wird. Aber er weiß, dass er es eigentlich nicht
wieder so lange rauszögern sollte wie die Information, dass es Mord
gewesen ist. Dafür ist es wohl jetzt zu spät.

"Es gibt keinen Grund zur
Eifersucht", sagt er stattdessen. "Soweit ich das verstehe, hat sie
Teleportationsmagie. Ich habe sie kurz hier im Dorf gesehen, gerade lang
genug, um mit ihr über die Schule hier zu reden. Seitdem nicht mehr."

"Ich fühle nicht den Hauch von Eifersucht", versichert Marcin.

"Gut!" Paolo glaubt ihm.

Marcin zieht den Binder über den Kopf, was nicht ganz einfach ist, aber
leider kann man ihm dabei kaum sinnvoll zur Hand gehen. Die
Kette mit dem halben Herz hat er bereits abgelegt. Unter dem engen
Kleidungsstück hat er kleine Brüste, -- das Ergebnis einer
Brustverkleinerung --, die Paolo sehr schön findet. Marcin
hat sich für eine Brustverkleinerung entschieden, statt wie so
viele trans Männer eine Mastektomie zu machen. Die Entscheidung dazu
war Ergebnis jahrelangen Abwägens, aber als er achzehn geworden ist,
hat er es direkt umgesetzt. Er trägt meistens weite Kleidung, und nur
an Tagen, an denen er neue Leute zum ersten Mal trifft, einen Binder.

Paolos Blick bleibt einen Moment an Marcins Brüsten hängen und
er macht einen Schritt auf ihn zu. "Manchmal wünschte ich, ich könnte
dich einfach küssen und anfassen und dabei sicher sein, dass
du nicht abrutschst und die Dämonen in diese Welt lässt."

Marcin bleibt ruhig und gelassen, als er Paolo ins Gesicht blickt. Nichts
flimmert schwarz. "Kannst du", sagt er leise.

Paolo hat Angst, dass er es nur sagt, weil er es so sehr will. Aber heute
ist kein Tag, an dem Paolo viel Selbstkontrolle übrig hat. Er hat nach
einem ganzen Vormittag auf der Bank am Birkenwald und einer Menge Hausaufgaben
für alle gekocht. Für Marcin, seine Mutter Angela und die Fremde hat er
ein komplexes Pasta-Gericht zubereitet, um dann festzustellen, dass
die Fremde ausgerechnet Kendra ist, und vegan. Manchmal reicht das, um zu
viel zu sein. Angela hat ihr das Alternativgericht raufgebracht, das
Paolo dann noch gezaubert hat. Paolo ist ganz froh, dass ein Gespräch
mit ihr nicht auch noch auf dem Tagesplan steht.

Er streckt die Hand aus und berührt Marcins Haut in der Nähe des
Schlüsselbeins. So zart. So warm. Er streichelt über Marcins Schulter,
sieht, wie dieser die Augen schließt, und berührt Marcins feuchtes, langes
Haar. Es ist noch ungekämmt. "Soll ich dich bürsten?"

Marcin nickt. Er behält die Augen dabei sanft geschlossen.

Paolo lässt sich noch Zeit damit, ihn zu küssen. Das Gesicht, das
er mit diesem entspannten Ausdruck und den geschlossenen Augen
so anziehend findet. Er dreht Marcin
herum, nimmt die Bürste vom Regal und fährt ihm langsam, von
unten nach oben stückchenweise immer mehr hinzunehmend, durchs nasse Haar, bis es glatt auf Marcins Rücken
liegt. Es ist dünn, aber dicht. Paolo sortiert es noch einmal, legt dabei Marcins Hals frei und
küsst ihn dort. Er hat ihm nicht gesagt, dass er nun auf Vampirjagd ist. Und
er sollte mit Kendra reden, bevor diese Marcin erzählt, wo sie sich
getroffen haben.

Er möchte kein unehrliches Verhältnis zu Marcin haben, aber es ist in
diesem Fall einfach seine Angelegenheit. Er hat darüber lange mit dem Schneefuchs
Sonja geredet. Sonja meint, dass es besser ist, dass so wenige wie
möglich von der eigentlichen Gefahr wissen, die von dem Vampir ausgeht. Das Vampir
tötet nicht bloß Menschen zum Vergnügen, was schlimm genug wäre, sondern
stellt durch seine Unsterblichkeit auch eine Anomalie
der Welt dar, ein Ungleichgewicht, das, wenn nicht gestoppt, die
Welt vernichten wird. Schon jetzt gibt es immer mehr Magie, bei der
das Böse in diese Welt schwappt, wie bei Marcin. Wenn zu viele Wesen wie das
Vampir zu lange leben, wird das Ungleichgewicht, basierend auf Gier, sich nicht mehr auflösen
lassen. Es wird zunehmen bis zu einem Kipppunkt, wie beim Klimawandel, der dann nicht mehr
umkehrbar ist. Aber gerade weil Marcin durch seine Magie zu tief drin
steckt, muss Paolo es sein, der sich kümmert. Wie immer.

Paolo küsst Marcin ein weiteres Mal, dieses Mal am Ohr. Ein leicht erregter Laut
entweicht Marcin. Paolo lächelt und küsst ihn ein weiteres Mal am Ohr. Er merkt, wie
er selber schneller atmet, als er Marcin von hinten sanft in einem Umarmung zieht und
mit den Händen von unten an Marcins Brüsten entlangstreichelt. Marcin gibt sich
Mühe, dass seine Laute kaum hörbar sind, aber sie entgehen Paolo nicht. Er dreht
Marcin wieder herum, schmiegt den warmen Körper an seinen, und küsst erst Marcins
Wangen, langsam, dann seine zarten Lippen.

Sie haben selten Sex. Das ergibt sich nicht so oft in einem Haushalt, in dem sie
bis zu Brans Tod zu viert gewohnt haben und nun mit Kendra wieder zu viert wohnen. Zumindest nicht, solange
es Marcin so unangenehm ist, gehört zu werden. Und auch seine Dämonen erschweren
die Sache. Es geht eigentlich nur, wenn er stabil ist. Und oft hat Marcin auch
keine Lust.

Paolo fragt sich, ob Marcin heute Lust haben mag. Es geht ohnehin nicht, weil sie
nicht allein im Haus sind, also fragt Paolo gar nicht erst. Aber
Paolo kann kaum ablassen davon, Marcin zu küssen. Seine
gelegentlichen zarten Geräusche, die ihm doch entweichen, in sich aufzunehmen.

Das wirklich Frustrierende ist allerdings, dass sich Paolo gegen jede Vernunft doch
manchmal wünscht, dass die Kreaturen auftauchen mögen, während er zärtlich zu
Marcin ist. Es würde bedeuten, dass Marcin stark fühlt. Und Paolo weiß, dass Marcin
es unterdrückt, dass er sich zusammenreißt, aber manchmal wünscht sich ein sehr
unvernünftiger Teil von Paolo, dass seine Zärtlichkeiten und Leidenschaft
einfach zu stark für Marcin wären. Das Marcin seinetwegen die Kontrolle
verliert.

In Wirklichkeit wünscht er sich natürlich, dass Marcin einfach frei wäre. Dass
er einfach vor sich hinfühlen könnte, was auch immer für ein Gefühl gerade
dran wäre, ohne auf Kontrolle von Dämonen achten zu müssen. Und das ist eine
Zukunft, für die Paolo kämpfen möchte. Für die Welt, aber besonders für Marcin.
