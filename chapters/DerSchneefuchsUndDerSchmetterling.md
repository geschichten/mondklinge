\BefehlVorKapitelIBDM{DerSchneefuchsUndDerSchmetterling.png}{Eine Haarspange in Form eines Schmetterlings. Er hat Glitzersteine und viele Schnörkel in den Flügeln.}{Mord, Blut, Blutarmut, Genitalien, Erregung, Erotik, psychologisches Sadismus-Spiel, Fixieren, Ausgeliefertsein.}

Der Schneefuchs und der Schmetterling
=====================================

\Beitext{Luna und Sonja}

Im Geisterwald. Der Wind schüttelt die dünnen Äste kahler Birken. Die
gespenstisch weiße Borke schimmert im letzten Abendlicht. Nebel wabert
im seichten Bodenwind durchs Unterholz.

Sonja ist leise. Sie hat Luna natürlich bemerkt. Und eigentlich hat sie
vermutet, dass es dadurch für sie gelaufen wäre. Aus. Lunas Fähigkeiten
im Wald aufzuspüren, wer nicht hineingehört, sind besser als Sonjas. Wenn
Sonja Luna bemerkt hat, gar nicht so weit von ihr, dann hat Luna auch Sonja
bemerkt. Aber aus Sonja unbekannten Gründen wird sie nicht sofort
angegriffen.

In ihrer Schneefuchsform schleicht sie zum nächsten Waldesrand. Nun,
nicht ganz zum nächsten, denn der endet direkt an einer Klippe in die
tosende Nordsee. Dann doch lieber einem Vampir zum Opfer fallen. Oder
ist es doch nicht die bessere Wahl und sie mag bloß die Nässe nicht? Oder
sie mag Luna?

Als Sonja schon nah am Waldrand ist, dort, wo ein Wanderweg ihn
begrenzt, hält sie noch einmal inne. Etwas stimmt nicht. Mit Luna stimmt
etwas nicht. Sorge. Sonja nimmt menschlichere Gestalt an. Es verändert
nichts an ihren Sinnesorganen, außer, dass sie nun höher liegen. Es
verändert etwas in ihrem Denken. Oder eher andersherum, das Denkuniversum,
in dem sie sich gerade befindet, beeinflusst unterbewusst ihre Form? In
der Schneefuchsform ist jedenfalls immer ein Teil ihres Denkens etwas
tranceartig. Nun, in menschlicherer Form erhofft sie sich, besser herausfinden
zu können, was nicht stimmt.

Ein Rascheln im Hintergrund erschreckt sie. Der Fluchtreflex setzt ein, sie
versucht, zum Waldrand zu rennen, aber ihr nach der Vermenschlichung langes
Haar verfängt sich in Birkenborke, so sehr, dass sie nicht weiterweiß.

"Kann ich dir helfen?" Lunas Stimme ist ruhig und dunkel.

Man könnte meinen, Luna klinge freundlich. Aber Sonja liest aus der Uneiligkeit,
dass Luna weiß, wie sehr sie überlegen ist, und nicht zögern wird, es
auszunutzen, wenn ihr danach ist. "Du möchtest mich töten", flüstert sie.

"Schon", sagt Luna, mit der selben Ruhe wie eben.

Sie hat sich überhaupt so leise genähert, dass Sonja es kaum mitbekommen
hat. Sie weiß nicht genau, wo Luna steht. Es ist unheimlich. (Und es gefällt
Sonja. Aber das will sie sich selbst gegenüber nicht so laut
zugeben, deshalb ist es in Klammern geschrieben.)

Es raschelt kaum, als sich Luna nähert. Sonja rührt sich nicht. Es ist zu
spät. Luna legt den einen Arm um Sonjas Brustkorb, gerade mit so viel Kraft,
dass es Sonja vermittelt, dass sie nicht wegkann. Mit der anderen löst sie
so sanft und zärtlich Sonjas Haar aus der Borke, dass es auf Sonjas Kopfhaut
kribbelt. Und in ihrem ganzen Körper. Kein Haar bleibt zurück. Nun hängt
sie nicht mehr hilflos am Baum, sondern wehrlos in Lunas Arm.

Luna berührt Sonjas Hand, in der sich ein glänzender, metallener Gegenstand
befindet. "Die Mondklinge also", raunt sie. Dann streicht sie Sonjas Haar
von derem Ohr, um genau hineinzusprechen. "Du bist in meinem Wald."

Sonja zittert. Ihr liegen viele alberne Sprüche auf der Zunge. 'Willst
du es nicht rasch hinter dich bringen?' Aber das will Sonja gar nicht. Warum
sollte sie es vorschlagen? 'Muss es so zärtlich sein?' Aber ist das nicht,
warum Sonja überhaupt hier ist? "Hm hm", sagt sie schließlich bestätigend. Mit
dünnem Stimmchen.

"Ich gebe dir eine Wahl", sagt Luna. "Option 1: Ich nehme dir jetzt die Mondklinge
ab und lasse dich einfach aus meinem Wald gehen."

Was?, denkt Sonja. Das sieht Luna gar nicht ähnlich. So etwas überhaupt zur
Wahl zu stellen. "Und Option 2?"

"Option 2: Du bringst die Mondklinge selbst zu meiner Hütte zurück. Ich warte
hier eine Weile, solange, bis ich denke, dass du eine knappe Chance hast, zu
entfliehen. Dann jage ich dich."

Sonjas Atem zittert bei dem Vorschlag. Luna ist so ein sadistisches Aas! "Warum
sollte ich Option 2 wählen?", fragt sie, denn eigentlich müsste aus Lunas Sicht
für Sonja alle Logik dagegensprechen.

"Ich weiß es nicht", raunt Luna dunkel. "Weil du das Bedürfnis hast, den Schaden,
den du angerichtet hast, selbst wieder in Ordnung zu bringen?", schlägt sie vor. Und
noch viel leiser flüstert sie in Sonjas Ohr: "Komm du ja nicht auf die Idee, dass
dir das auch eine Chance geben könnte, den Wald samt Mondklinge zu verlassen. Eine
Option, die dir anders nicht offen steht."

Stimmt, denkt Sonja. Daran hat sie tatsächlich nicht gedacht. "Und was hast du
davon?", fragt Sonja.

"Ich habe meine Gründe", sagt Luna einfach.

"Du spielst gern mit deiner Beute?", rät Sonja.

"Das auch", gibt Luna zu. Gier schwingt in ihrer Stimme mit.

Sonja wird weich in den Knien. Oh, wie sehr sie sich nach Lunas Gier sehnt. Es
ist so klar, wie sie entscheiden wird. "Wie viel Chance zu entfliehen lässt
du mir bei der zweiten Option?"

"Willst du handeln? Ich steige mit 1% ein." Lunas Stimme grinst.

"5%?", schlägt Sonja zitternd vor.

Luna schnaubt. "Ich dachte, ich müsste dich von irgendwas ab 90% runterhandeln. Was
sagt das über dich?" Lunas Finger streicht einmal unendlich sachte über
Sonjas Hals. "Ich nehme die 5% einfach! Ist das also der Deal? Nimmst du die
zweite Option?"

Wie Sonja mit diesen wabbeligen Beinen gleich überhaupt rennen soll, ist ihr
nicht klar. Sie hat das Gefühl, gleich umfallen zu müssen, als Luna sie
vorsichtig loslässt. Sie stützt sich an einem Baum ab. "Ich habe noch nicht
geantwortet", flüstert sie.

"Ich weiß", sagt Luna. "Ich habe nicht vor, für dich zu entscheiden. Selbst
wenn noch so naheliegend erscheint, was du willst. Aber ich würde eine Richtung,
die du jetzt bewusst, also nicht aus Versehen herumstrauchelnd, einschlägst, als
Entscheidung werten."

Sonja ist dankbar darum. Sie will es nicht aussprechen. Sie fühlt sich eh schon
emotional entblößt, weil sie sich mit den 5% verraten hat. Wie albern. Luna
hat sie längst durchschaut, nur deshalb lässt sie ihr eine scheinbare Wahl. Beziehungsweise
durchaus eine echte, eigentlich.

Sonja blickt gen Waldrand. Er dürfte nicht allzu weit sein. Sobald sie einen Schritt
dorthin machen würde, würde Luna ihr noch die Mondklinge entwenden, aber sie
gehen lassen. Luna hält Wort, das weiß Sonja. Aber dass sie die Mondklinge noch
in der Hand hält, realisiert Sonja, ist auch ein Zeichen dafür, dass
Luna weiß, was los ist. Wenn sie naheliegender empfunden
hätte, dass sich Sonja für die erste Option entschiede, hätte sie ihr die Mondklinge vielleicht
bereits abgenommen.

Sonja sieht hinab auf den glänzenden Gegenstand in ihrer Hand. Ein Gegenstand ihrer
Begierde. Wenn sie ihn behalten will, ist ihre einzige Wahl, den Weg zu Lunas
Hütte einzuschlagen. Aber sie muss dort nie ankommen! Sie kann zügig, nachdem sie den Weg
zu Lunas Hütte angepeilt hat, abbiegen, um den Wald samt Mondklinge zu verlassen. Sie
wandelt sich wieder in ihre Schneefuchsform, die Mondklinge in einer Pfote, die dafür noch
fingerigere Gliedmaßen hat, wie vorhin, und jagt in den Wald hinein.

Das war nicht abgesprochen, denkt Luna. Sie merkt sofort, dass Sonja nicht plant, die
Hütte zu erreichen. Sie lässt ihr trotzdem etwas Vorsprung. Sie lehnt sich grinsend
an einen Baum. Sie ist müde und zerschunden, aber sie kann sehr wohl noch klar genug
denken, sodass sie weiß, was das hier ist. Das Spiel einer Person, die sich ihr ausliefern
will, aber den Anschein haben möchte, als wäre es doch nicht freiwillig. Sonja hat
genug Wissen und Erfahrungen, dass ihr klar ist, dass Luna ihre Chancen zu entfliehen auf
null reduzieren wird, sobald sie den Vertrag bricht, den sie eigentlich haben. Das
ist die Absicht dahinter. Sonja rechnet nicht damit, mit der Mondklinge davonzukommen.

Luna setzt ihr nach, als sie nach ihren Hochrechnungen an der Stelle mit dem
Mooshügel im Wald auf Sonja treffen wird, der sich besonders gut eignet, Personen
auszusaugen. Sie rennt. Sie fühlt sich immer noch, als hätte sie
einige Drahtbürsten verschluckt und verdaut, und gleichzeitig so lebendig wie lange nicht
mehr. Sie liebt die Jagd. Sie liebt das Zappeln der Opfer. Sie liebt es, den
Todeskampf auszulösen und ihm beizuwohnen.

Sonjas Sinne sind geschärft. Dieses Mal hört sie Luna kommen. Irgendetwas stimmt
mit ihr gar nicht. Wieder sorgt Sonja sich. Wieso sorgt sie sich um diese
Vampirkreatur?

Der Boden unter ihren Füßen wird weicher und lässt sie, wie automatisch, ein wenig
langsamer werden. Das wäre ein guter Ort, um...

In diesem Moment passiert es. Sie hat Luna doch gehört, warum die letzte Näherung
dann wieder nicht? Wie aus dem Nichts stürzt sie sich auf Sonja und begräbt den
Schneefuchskörper unter sich im Moos, die Hand an Sonjas Kehle. Sonja nutzt den
kurzen Moment von Raum, den sie noch hat, um die Form wieder in eine menschlichere
zu wechseln und Luna lässt sie.

Luna greift ihr noch einmal um die Hüfte und rollt sich mit ihr über
das Moos, bis sie an einer Stelle liegen, die weich ist, und abschüssig. Sonjas
Kopf zeigt nach unten, ihre Beine liegen weich und erhöht, eingeklemmt zwischen
Lunas starken Beinen. "Ist es für erhöhten Blutdruck auf der Halsschlagader?", fragt
Sonja.

Luna nickt. Sie streicht Sonjas langes Haar zurecht, bis es um ihren Kopf verteilt
ausgebreitet auf dem Moos liegt.

Wie auf dem Servierteller, denkt Sonja. Sie atmet
schneller. Sie will, dass Luna sie noch einmal so unendlich sanft am
Hals berührt, mit der Hand, wie vorhin, wie um Darzulegen, was sie tun
wird. Wogegen sich Sonja nicht wehren kann. Jetzt nicht mehr. Sie möchte, dass
Luna sie zwischen den Beinen anfasst. Aber das ist nicht Lunas Stil.

Luna beugt sich herab und berührt tatsächlich Sonjas Hals mit dieser kaum aushaltbaren
Sanftheit, die sich Sonja gewünscht hat, aber mit den Lippen. Und flüstert ihr
dann ins Ohr: "Du möchtest also von mir ermordet werden. Damit hatte ich so
auch nicht gerechnet."

"Nicht?", fragt Sonja. "Muss es so offensichtlich sein, damit du es merkst?"

Lunas Hand um Sonjas Hals wird eine Spur sanfter. "Scheint so", flüstert
sie. Einen Moment wirkt sie dadurch verletzlich. "Es war mir noch nie so
bewusst, dass du dich mir freiwillig ausliefern willst und es nicht eher
tust, weil du ein etwas unsinniges Risikoverhalten an den Tag legst."

"Gefällt es dir deshalb jetzt weniger?", fragt Sonja ängstlich.

"Im Gegenteil." Lunas Stimme ist ein sanftes, gieriges Flüstern. "Ich
mag Konsens. Das waren meine Gründe, dir eine Wahl zu lassen und zu sehen,
was du aussuchst."

Sonja fragt sich, ob sie etwas dazu sagen soll. Ihr Herz pocht schnell. Sie
ist feucht. Sie will, dass Luna sie ausnutzt. Dass Luna alles von ihr
nimmt. Sie atmet tief ein und riecht, dass Luna sehr anders riecht als
sonst. Verwüstet. "Bist du krank? Oder verletzt?"

"Schon, denke ich", sagt Luna. "Ich glaube, das wird sich legen, wenn ich
dich aussauge."

"Ich möchte dir nicht sagen, dass du es tun sollst", sagt Sonja. Auch,
wenn sie will, dass Luna sie aussaugt. Ganz und gar. Das lässt sie
unausgesprochen.

"Das verstehe ich", sagt Luna. Die Hand an Sonjas Hals wandert auf eine
Seite, bohrt ihr einen Daumen unters Kinn und packt zu. Ein feiner
Grad zwischen sanft und unsanft. Die Bestimmtheit wiederspiegelnd, die
Sonja braucht, und die Zärtlichkeit darin, die Lunas Sadismus
ist. "Du möchtest, dass ich über dein Schicksal entscheide und nicht
du. Und das werde ich, Sonja, das werde ich!"

Sonjas ganzer Körper bebt, als Lunas Mund sich wieder von Sonjas Ohr
entfernt, ihre Lippen sehr langsam ihren überstreckten Hals hinabwandern
und an einer besonders empfindlichen Stelle verharren. Luna lässt sich die
Zeit, Sonjas Seufzen in sich aufzusaugen, Sonjas hilfloses Verlangen. Sie
mag Sonjas Sehnen danach, sich Luna hinzugeben. Luna öffnet den Mund. Ihre
Lippen bilden ein unwillkührlich belustigtes Lächeln ab, als das schon wieder
ein Fiepsen aus Sonja hervorlockt.

Sie kratzt teasend mit den Zähnen über Sonjas Hals. Es hinterlässt
Schrammen, die sofort heilen. Luna riecht das Blut trotzdem. Und das
Wissen, dass es ihr gleich so viel besser gehen wird, überwältigt
ihre Kontrolle. Ihr ganzer Körper spannt sich an, sie fixiert Sonja
unbeweglich und leicht schmerzhaft in ihre Lage und beißt in Sonjas
Hals. Sie zittert selbst, als sie das Blut in ihren Mund laufen
lässt und in sich hineinrinnen. Sie schließt die Augen. Sie hört
Sonjas sehnendes Wimmern, fühlt das Aufgeben des Körpers unter ihr,
das Fallenlassen.

Und dann hebt Sonja doch die Hand, streicht zärtlich über Lunas
Wange. Luna fühlt sich geliebt. Es ist ein schönes Gefühl. Damit
hätte sie nicht gerechnet.

Sonjas Hand fädelt sich in Lunas Haar ein, findet die Spange in
Form eines Schmetterlings und entfernt sie aus Lunas Haar. Lunas
Haar verselbstständigt sich und kitzelt. Verdattert lässt sie von Sonjas Hals
ab, richtet ihren Oberkörper etwas auf und schaut ihrem Opfer ins
Gesicht. "Was machst du da?"

Sonja befestigt die Spange in ihrem eigenen Haar. "Darf ich sie
haben, bis ich sterbe?", bittet sie.

Luna ist so perplex, dass sie erst nicht weiß, was sie sagen soll. Einen
Moment fühlt sie das Machtgefälle zwischen ihnen nicht. Sie fragt
sich, ob sie es völlig aufgibt, wenn sie nun zustimmt. Aber dann kommt
ihr der Gedanke albern vor. Sie nickt.

"Ich möchte dir noch etwas sagen, was dir vielleicht unangenehm ist, aber
es kommt mir ehrlich vor", sagt Sonja.

Luna nickt wieder.

"Ich bin erregt", flüstert Sonja. "Sehr."

Zu Sonjas Überraschung legt Luna die Hand, die nicht damit beschäftigt
ist, Sonjas Hals zu fixieren, an Sonjas Becken. Sie wandert langsam darum
herum, gibt Sonjas Beinen ein klein wenig Raum. Auf Sonjas Oberschenkel
hält Luna inne. Auf dem Fellkleid, dass Sonja aus dem Körper
wächst. Frustrierenderweise bleibt Lunas Hand da einfach liegen. Ist
es Teil von Teasen? Kann Luna sich nicht entscheiden?

Sonja hält die Anspannung kaum aus. Sie will, dass Luna zupackt. Dass sie
mit ihr macht, was sie will. Und wenn sie sie nicht anfassen will, okay,
darum ging es Sonja nicht. Sie will es, aber sie will eigentlich Luna
zur Verfügung stehen, mit was immer diese für Bedürfnisse hat, und
hat überhaupt nicht damit gerechnet, dass etwas mit Genitalien darunter
sein könnte. Sie fand es nur fair, ihr zu sagen, dass es für sie sexuell
ist.

Bei dem Gedanken öffnet sie ihre Beine, halb unwillkührlich, halb
einladend. Und wie, als hätte die Hand darauf gewartet, wandern Lunas
immer noch recht kühle Finger dazwischen. Sortieren zärtlich langes Fellhaar
zur Seite, bis sie zwischen Sonjas Vulvalippen glitschen können. Sonja
atmet mit einem Mal so schnell, als läge ihre versuchte Flucht erst gerade
hinter ihr. Sie blickt Luna an, weiß, dass sie verloren und verlangend
aussieht. Luna dagegen lächelt sachte. Sie belässt die Finger zwischen
Sonjas Beinen, als sie sich wieder zum Hals hinabbeugt. "Ist nun alles
geklärt?", flüstert sie dunkel.

"Ja", fiepst Sonja. "Ich habe dich rausgerissen oder?"

Luna küsst Sonjas Hals, was ihr ein weiteres sehnsüchtiges Fiepsen
einbringt. "Dafür stecken wir jetzt noch tiefer drin", raunt sie. "Du
hast dich dadurch noch ein Stück mehr ausgeliefert. Und ich mag
es, dich so im Griff zu haben."

Ehe Sonja auf die Idee kommen kann, etwas dazu zu sagen (es wäre
etwas Zustimmendes geworden), spürt sie, wie Luna abermals die
Zähne in ihren Hals versenkt. Sie liebt den Schmerz. Sie liebt das
Saugen. Den Sog. Sie liebt, wie sie den Blutverlust spürt, sich zunehmend weniger
rühren könnte, selbst wenn Luna sie losließe. Sie merkt, wie Luna
wärmer wird. Sie spürt den warmen Wasserdampf, der sich ausbreitet, den
Luna ausstrahlt. Es bedeutet, dass Luna von Sonjas Blut profitiert. Sie
spürt Lunas Gier. Ihr wird leicht schwummrig im
Kopf. Trinkt Luna so zügig? Auch, ja, aber... da sind auch ihre
Finger zwischen Sonjas Beinen. Die nun damit anfangen, ihre Vulvalippen
entlangzustreicheln. Sie langsam stimulieren. Sonja hat wirklich
nicht damit gerechnet, dass es Lunas Ding ist, aber sie kann gerade
ohnehin nichts anderes tun, als sich fallen zu lassen und aufzugeben, zu
fiepsen, gegen das Gefühl anzukämpfen, gleich das Bewusstsein zu
verlieren, um die Erregung noch etwas länger auszukosten.

Und dann zieht Luna überraschend ihre Zähne aus Sonjas Haut. Sonja vermisst
sofort das Sauggefühl, fühlt sich wie verloren. Lunas Mund wandert abermals
zu Sonjas Ohr. "Ich bringe es gleich zu Ende", verspricht sie. "Ich wollte
dir nur etwas sagen, damit du es weißt, weil ich es fair finde."

"Okay", fiepst Sonja.

"Ich mache das hier", -- Luna bewegt die Finger noch einmal an Sonjas
Vulvalippen entlang, so auf der Grenze von nicht genug, so sadistisch --, "weil
es dich in einen Zustand der Extase versetzt, in dem dein Herz schneller und
länger pumpt und ich mehr Blut aus dir herausbekommen werde."

Sonja winselt unwillkührlich, stöhnt beinahe auf. Es erregt sie
nur umso mehr.

Lunas Mund berührt ihr Ohr, als er wieder nach unten wandert. Auf dem Weg
nimmt Luna das Ohrläppchen in den Mund und beißt so brutal hinein, dass
Sonja denkt, dass es abfallen müsste. Sie schreit auf, aber mit kaum
Kraft. Ein Moment, indem sie versteht, dass Luna mit ihr auch nicht so schöne
Dinge machen könnte. Dass sie vollkommen in ihrer Gewalt ist. "Ich liebe
dich", flüstert sie.

Luna lässt sich ihre Verdutztheit dieses Mal nicht anmerken. Sie küsst die
blutige Stelle direkt unter dem Ohr, leckt darüber. Merkt, wie das Ohrläppchen
schon dabei ist, wieder zu heilen, als sie wieder in den Hals beißt.

Sie lauscht auf Atem und Herzschlag, als sie trinkt. Es war ein langes Spiel, aber
gleich ist es so weit. Sonjas Atem ist flach und immer noch sehr erregt. Und in
dem Moment, kurz bevor ihr die Sinne wegkippen, lässt Luna zwei ihrer Finger
in Sonjas Vagina gleiten.

Sonja verliert das Bewusstsein in einem Moment, der ihr wie der schönste in ihrem
Leben vorkommt. Sie fühlt sich gewollt, geliebt und ausgebeutet. Von der einzigen
Person, die ihr etwas bedeutet. Sie fühlt die Finger in sich, die Zähne in ihrem Hals
und im nächsten Augenblick ist da einfach tiefste Entspanntheit, Schwärze.

Ein weiteres Mal wabert sie doch wieder ins Bewusstsein. Lunas Finger liegen nun
unbewegt zwischen ihren Vulvalippen. Luna rechnet nicht damit, dass Sonja noch einmal
etwas wahrnimmt. Es wirkt, als wäre Luna einfach zu faul gewesen, sie dort
wegzunehmen. Sie hält Sonja nicht mehr so zangenartig fest. Ihre andere Hand liegt
liebevoll in Sonjas Haar, während sie genießend und selbstvergessen
Blut aus Sonjas Hals trinkt. Vielleicht ist das ein noch schönerer Moment. Weil
Luna glaubt, ihn für sich zu haben, und Sonjas Körper wirklich nur dient. Dann
kippt Sonjas Bewusstsein entgültig weg.

---

Der Himmel leuchtet im ersten Tageslicht vor Sonnenaufgang. Dieses magisch wirkende
Blau, das noch fast an Nachthimmel erinnert, aber gleichzeitig den Eindruck einer
Neonfarbe macht. Das Meer rauscht, der Nordwind weht und bringt Salz mit sich. Sonja
fühlt sich so tiefenentspannt, wie lange nicht mehr. Das ist
vielleicht das Einzige, in dem sie Luna überlegen ist: Wenn sie vom Tod zurückkommt, ist
sie stets ausgeruht, entspannt und erholt. Wenn Luna vom Tod zurückkommt, ist sie am Limit und
braucht eine Weile, bis sie wieder ganz Luna ist.

Etwas fühlt sich aber seltsam an Sonjas Körper an. Sie bewegt dem Gefühl folgend ihre
Hand zu ihrem Haar, betastet ihren Kopf und findet die Schmetterlingsspange. Luna
hat sie ihr überlassen!

Sie richtet sich auf. Sie befindet sich außerhalb des Waldes, auf dem Boden bei
der Klippe bei der Bank, wo Luna manchmal Überbleibsel oder Erinnerungen an
Tote hinterlässt. In diesem Fall wohl Sonjas ganze Leiche. Sonja blickt
zum Waldrand. Dort zwischen den Birken, vielleicht drei große Schritte in den
Wald hinein sitzt Luna in einem Schneidersitz und wartet. Sonja nähert sich ihr
und setzt sich außerhalb des Waldes auf den Boden. Sie schweigen sich eine Weile
an, wissen vielleicht nicht so recht, was sie sagen wollen.

"Du lässt mich gehen?", fragt Sonja schließlich.

"Ich hoffe, du verirrst dich mal wieder in meinen Wald", entgegnet Luna.

"Habe ich die Erlaubnis?", fragt Sonja.

"Niemand kommt lebend aus meinem Wald", erinnert Luna. "Es ist nicht
verboten, mich hier zu besuchen und dabei Sterben in Kauf zu nehmen."

"Aber ich komme zurück", murmelt Sonja. "Stört dich das nicht?"

Luna lächelt. "Es fühlt sich weniger nach Mord an, ja", gibt sie zu. "Aber
ich genieße es trotzdem."

Sie lächeln sich ein paar Momente schweigend an. Bis wieder Sonja
die Stille durchbricht. "Geht es dir besser?"

Luna nickt. "War dir irgendwas zu viel? Zu grenzwertig?"

Sonja hebt eine Braue. "Wieso würdest du so etwas fragen? Ist nicht vielmehr
Sinn der Sache, dass du machst, was *du* willst und dich nicht scherst, was
ich fühle?"

Luna schüttelt den Kopf. "Zum einen schert es mich, was du fühlst. Das ist Teil
von dem, was ich mir einverleibe, was ich genieße", erklärt Luna.

Sonja kann nicht abstreiten, dass es sie anmacht, und dass es sie sehr berührt.

"Und zum anderen gibt es Grenzen, die ich nicht überschreiten möchte", sagt
Luna. "Ich mache schon bewusst selten sexuelle Dinge. Das ist etwas, was ich
nicht machen werde, wenn du es nicht magst."

"Ich mochte es", flüstert Sonja. "Sehr."

"Auch, dass ich dir am Ende offen gelegt habe, dass ich es für den Blutdruck
bei dir tue?", bohrt Luna nach.

Sonja grinst. "Du hast schon krassere Dinge mit mir gemacht, um meinen Blutdruck
beim Aussaugen zu erhöhen." Sie lächeln sich wissend an. "Nein, ehrlich", fügt
Sonja hinzu. "Ich fand gerade den Moment sehr extrem, krass, gut, meine ich."

Luna grinst und kichert tonlos. "Dann habe ich noch eine Frage übrig. Was
soll die Sache mit Paolo? Wärest du bereit, ihn aus dem Spiel zu lassen?"

Sofort verfliegt Sonjas Leichtmütigkeit vollends. "Paolo", flüstert sie. "Ich
dachte, du würdest dich vielleicht freuen, wenn ich dir ein Opfer in den
Wald schicke, um das du es nicht schade finden würdest."

"Nicht so!", sagt Luna. Sie steht auf, und geht, ohne etwas weiteres hinzuzufügen.

Sonja bleibt wie vom Donner gerührt sitzen. Das Meer rauscht unterhalb der
Klippe. Der Wind weht durch ihr Haar. Es war bis gerade die schönste Nacht
ihres langen, unsterblichen Lebens. Aber jetzt, mit einem Mal, jetzt hasst sie
alles.
