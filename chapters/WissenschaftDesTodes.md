\BefehlVorKapitelIBDM{WissenschaftDestodes.png}{Ein Auge, das mit einer Silberschicht überzogen ist, umrahmt von Wimpern, schaut schräg nach oben.}{Mordversuche - thematisiert, Machtgefälle, Morbidität.}

Wissenschaft des Todes
======================

\Beitext{Luna und Kendra}

Kendras Körper wird allmählich kalt und Luna hat nicht genug Blut
getrunken, um mit eigener Körperwärme entgegenzusteuern. Eigentlich
hat sie eher die Befürchtung, dass sie Kendra in der Umarmung noch weiter herunterkühlt. Ein
kleiner Teil von ihr möchte mehr von Kendras Blut, ein Spiel am Rande des
Abgrunds spielen, so viel von ihr aussaugen, dass sie Kendra wärmen kann, aber
dann die Spannung spüren, ob Kendra das überlebt. Aber das ist nicht dran. Sie
will es auch nicht wirklich. Nur die Vorstellung ist schön.

"Soll ich dich nach Hause begleiten?", fragt Luna.

"Das wäre schon was, was ich von meiner Assistenz erwarten würde." Kendra
schmunzelt schelmisch, sodass Luna nicht sicher weiß, ob es ein
Scherz ist. Eigentlich wissen beide, dass Assistenzen meist nur
für Tätigkeiten genehmigt werden, die wirklich nicht ohne gehen. Es
wird, wenn, dann ohnehin ein Arbeitsverhältnis werden, dass der Staat
nicht kennt.

Luna lässt Kendra los und zögert eine Weile, bevor sie ihre Gedanken in
Worte kleidet. "Ich habe Bedenken, dass du dich, wenn ich deine Assistenz
wäre, in der Verpflichtung sehen könntest, mit Blut bezahlen zu müssen. Weil
ich mit anderer Bezahlung wie Geld nicht so viel anfangen kann."

"Du möchtest aber viel lieber unbezahlt für mich arbeiten?", mutmaßt Kendra.

Luna nickt lächelnd. "Oder sagen wir, die Bezahlung, dass ich dabei eine
neue, spannende Erfahrung mache, ist mir vollkommen ausreichend."

"Möchtest du mit mir jedes Klischee von ungesunden Beziehungen bedienen?", fragt
Kendra, wieder mit diesem Schalkschmunzeln.

"Möchtest du sie aufzählen? Vielleicht sind mir nicht alle bewusst", schlägt Luna
vor. Sie steht langsam auf und Kendra folgt ihrem Beispiel.

Kendra kommt dem Vorschlag ohne Umschweife nach. "Eine
erotische Beziehung zwischen Chef und angestellter Person. Du
bist Vampir und ich Mensch. Du bist um 200 und ich 17."

"17?", wiederholt Luna.

"Jop."

"Shit! Wow, das war mir nicht klar", sagt Luna. "Du könntest mich dafür anzeigen und mich
einbuchten lassen. Nicht, dass es viel bringen würde." Luna hat unzählige
Male im Knast gesessen, früher, viel früher. Wenn sie es satt gehabt hat, gejagt
zu werden. Aber es ist ihr stets irgendwann zu langweilig geworden.

Ganze Trupps von Menschen haben versucht, sie im Wald zu finden und zu
ermorden. Weil sie sie für eine Gefahr gehalten haben, -- womit sie natürlich
nicht Unrecht hatten. Über Jahrzehnte hat Luna versucht, klarzumachen, dass
sie nur im Wald eine Gefahr darstellt. Als Kompromiss. Sie hat gezeigt, dass
sie der Gewalt, der sie im Dorf begegnet, nichts entgegensetzt, außer zu
überleben, und schließlich eben aus Kerkern auszubrechen und sich in
den Wald zurückzuziehen. Sie weiß nicht mehr, wann die Lage zum Besseren für alle
gekippt ist. Nun kann sie üblicherweise ins Dorf gehen, ohne dass sie angegriffen
wird. Viele erkennen sie nicht einmal.

"Es ist dir nicht egal? Also, die paar Jahre Unterschied, bis es rechtlich
okay wäre?", fragt Kendra.

Luna taucht aus ihren porösen Erinnerungen grausamerer Zeiten wieder auf und
bedenkt Kendra mit einem langen Blick mit gerunzelter Stirn. "Meine Gründe
sind noch absurder eigentlich", sagt sie. "Ich weigere mich, mich freiwillig in
so eine Romanzensache zwischen uraltem Vampir, aka mir, und einer Person
zu verwickeln, die ausgerechnet 17 ist. In all diesen kitschigen,
übergriffigen Romanen und Filmen sind sie 17, weißt du?"

"16 wäre okay?"

Sie kichern beide, aber Luna schüttelt den Kopf. "Ich muss in mich
gehen. Ich fand es sehr schön. Es sprechen Dinge dagegen, es unter den Umständen
zu wiederholen. Und dann wiederum kenne ich mich mit toxischem Mist eigentlich
ziemlich gut aus und traue mir zu, mit dir ein gesünderes Beziehungsdings
zu haben, als wahrscheinlich alles wäre, was du mit irgendwelchen
in dieser Welt sozialisierten Gleichaltrigen erleben würdest."

"Ähm...", macht Kendra, sagt aber dann weiter nichts dazu.

"Fühl dich eingeladen, deine Bedenken auszuführen!", sagt Luna sanft.

Der Waldweg, den sie nehmen, knirscht und raschelt leise unter ihren Füßen.
Luna hat sich kurz gefragt, ob sie Kendra beim Gehen unterstützen sollte. Als
Assistenz. Aber wahrscheinlich ist das beste, was sie machen kann, sich Kendras
Tempo anzupassen. Sie machen den Bogen um den Wald herum Richtung Marktplatz.
Luna rechnet damit, dass noch nicht Paolos Zeit ist. Sie rechnet damit, dass er
später in der Nacht wieder ein paar Stunden auf der Bank verweilen wird. Von
Paolo ist Luna sich relativ sicher, dass er zu dem Zweck so oft auf der Bank sitzt,
um ihr aufzulauern, aber bei Kendra hat sie mit einer vergleichbaren Vermutung
falsch gelegen.

"Was hältst du von der Idee, dass ich versuche, dich zu töten?", fragt Kendra
in Lunas Gedanken hinein.

Luna lacht. Das hat sie lange nicht mehr. "Da wäre ich für zu haben. Das wird scheitern,
schätze ich. Warum willst du es? Hast du Mordlust und hättest gern, dass es
keine Konsequenzen hat?"

"Sagen wir, ich habe ein wissenschaftliches Interesse an dir", erklärt Kendra. "Ich
würde gern untersuchen, wie du so physikalisch funktionierst."

"Uh!" Luna wird ein wenig weich in den Knien. Das hat sie auch echt lange nicht
mehr gehabt. "Ich mag dich."

Kendra schnaubt. "Dann fügen wir diese ungesund anmutende Beziehungsebene auch noch
hinzu."

Sie erreichen das Dorf. Dünner Nebel aus dem Wald kriecht über das Pflaster. Es ist
nichts mehr los. Kendra führt Luna zum Reihenhaus, in dem Angela wohnt. Also auch
Paolo und Marcin. Und wo Bran gewohnt hat. Zufällig ausgerechnet das. Obwohl, vielleicht
doch nicht so zufällig: Es ergibt
Sinn, dass das Zimmer einer Verstorbenen frei wird. "Wohnst du ganz oben?" Luna
hat nach Brans Tod mit Angela gesprochen und weiß inzwischen ein bisschen mehr
über Brans Lebenssituation vor deren Tod. Angela ist auch zum Trauern zur Bank an der Klippe
neben dem Geisterwald gekommen.

Kendra nickt.

"Das ist eigentlich unzumutbar für dich, oder? Daran arbeiten wir", sagt Luna, und fügt
rasch hinzu: "Wenn du möchtest."

"Isst du eine passende Person auf, sodass ich deren Zimmer übernehmen kann?", fragt
Kendra.

Luna verzieht das Gesicht in einen Ausdruck zwischen Skepsis und Belustigung. "Ist
dir bewusst, dass du in dem Zimmer einer Person wohnst, die ich ermordet habe?"

Aus Kendras überraschter, doch tonloser Reaktion schließt Luna, dass ihr das nicht
bewusst gewesen ist. "Bran", sagt Kendra.

Luna nickt. "Eine mutige Person."

"Ist sie in den Wald gegangen? War das dein Grund, sie zu töten?", fragt Kendra.

Luna nickt abermals. "Sie wollte, dass ich sie töte." Wieder fühlt sie dieses
zarte, sehnsuchtsvolle Reißen in ihr. Sie blickt Kendra an und findet in derem
jungen Gesicht einen Ausdruck, der Luna gefällt. Etwas Ernstes und ein Stück
Liebe. Aber vielleicht überinterpretiert Luna hier auch.

"Willst du noch mit hochkommen?", fragt Kendra. "Beziehungsweise, wann sprechen
wir über die Details unserer Beziehungsebenen?"

Luna lächelt. "Es berührt mich, dass du immer noch magst", sagt sie. "Ich komme
gern mit hoch."

Kendra holt den Schlüssel aus der Tasche, aber bevor sie ihn ins Schloss stecken
kann, öffnet Paolo die Tür. Er ist in eine dicke Jacke und eine warme Kordhose
gekleidet. Kendras Blick wandert über seine Hose, die auf eine Weise Falten wirft,
als würde sie auf weiterem Stoff haften. Paolo trägt vielleicht eine Leggins
oder eine Strumpfhose unter der Kordhose. Es liegt also nahe, dass er wieder
lange nachts unterwegs sein wird.

"Wer ist das?", fragt Paolo Kendra und deutet mit einem Nicken auf Luna.

"Luna. Sie wird, wenn alles gut geht, meine Assistenz", fasst Kendra zusammen.

Paolo wirft einen skeptischen Blick auf Luna. Und... da ist es wieder. Es
wirkt, als würde er das Gesicht zum Weinen verziehen, aber statt dass
Tränen aus seinen Augen träten, verfärben sich die ganzen Augenbälle silbern. Er
blinzelt ein paar Mal und der Effekt verschwindet wieder.

"Beeindruckend", murmelt Luna. "Echtes Silber?"

Paolo nickt. "Natürlich, sonst würde es ja nicht funktionieren."

Kendra erinnert sich, dass Paolos Magie ist, Edelmetalle zu beeinflussen. Er
beherrst sie ziemlich gut verglichen mit anderen in der Klasse, aber bisher ist
er nicht sehr angetan von der Idee, Schmied für Schmuck zu werden, was ihm des
öfteren vorgeschlagen wird. Kendra hat für ihn außerdem an eine Karriere im
Bereich Medizin gedacht. Implantiertes Metall im Körper anzupassen, ohne die
Notwendigkeit, jene zu öffnen, wäre sicher ein guter Einsatz seiner Magie. Aber
sie hat noch nicht gewagt, ihm etwas vorzuschlagen, weil er ihr gegenüber oft
abweisend ist.

"Oh wow, ich wollte eigentlich einen Scherz machen." Luna runzelt belustigt
die Stirn. Sie kann diese Person einfach nicht so wirklich ernst nehmen. "Und
was funktioniert dadurch?"

"Ich kann damit sehen, ob jemand ein Vampir ist", erklärt Paolo. "Vampire
und Silber funktionieren nicht zusammen, richtig? Also sehe ich, wenn ich
meine Augen mit einer Silberschicht überziehe, einen Unterschied zwischen
Mensch und Vampir."

"Hast du Belege dafür, dass es funktioniert?", fragt Kendra.

"Du immer mit Belegen! Wie stellst du dir das vor?" Trotz
seiner offenkundig üblen Laune hält er Kendra
und Luna die Tür auf. "Wahrscheinlich sollte ich deiner Ansicht nach eine
Studie durchführen. Aber bislang ist mir nur ein Vampir bekannt. Und von dem
weiß ich nicht, wie es aussieht oder wie ich es finde. Erklär mir bitte die
Erfolgschancen einer Studie!"

Kendra wirft Luna einen Blick zu und fängt ihren amüsierten auf. "Ich
habe kein Problem mit einer unwissenschaftlichen Herangehensweise
mangels besserer Mittel", hält Kendra fest. "Ich habe ein Problem damit, wenn
du die Herangehensweise ohne irgendwelche Beweise unhinterfragt als wirksam einordnest. Aber
vielleicht sollte mir das auch egal sein, abhängig davon, ob etwas Wichtiges
davon abhängt oder nicht. Was willst du überhaupt machen,
wenn du über eine Person erfährst, dass sie ein Vampir ist?"

"Sie töten", informiert Paolo. "Hast du damit ein Problem?"

Kendra schnaubt. "Das Vorhaben teile ich immerhin."

"Du bist auf Vampirjagd?" Das erste Mal, seit sie Paolo kennt, fühlt Kendra
keine Ablehnung in seiner Reaktion.

"Ich will versuchen, ein Vampir zu töten. Von Jagen war keine Rede", antwortet
Kendra mit einem Lächeln. Sie deutet auf ihren Stock und bewegt sich endlich
in die Wärme des Hauses. Luna folgt ihr.

"Ich halte Ausschau!", sagt Paolo, deutet seinerseits auf seine Augen und zwinkert. Er
fügt hinzu: "Aber nichts für ungut: Du bist behindert. Ich weiß nicht, wie du
es mit einem Vampir aufnehmen willst."

"Da sind wir ja schon zwei, was es anbelangt, die jeweils andere Person
für nicht so fähig einschätzen, ihr Vorhaben umzusetzen." Kendra
grinst. "Ich traue deinem Silbertest nicht. Aus guten Gründen."

"Wieso? Ist jemand von euch beiden ein Vampir?" Paolo erwidert das Grinsen.

Luna nickt, blickt Kendra an, blickt Paolo an und deutet auf sich. "Meine
Wenigkeit."

Paolo starrt.

Kendra und Luna kichern.

"Kein Witz?", fragt Paolo.

"Ich finde das alles durchaus sehr witzig", sagt Luna. "Aber ich bin diese
Vampirperson, die du suchst. Das hast du schon richtig verstanden."
