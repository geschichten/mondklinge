\BefehlVorKapitelIBDM{DasEndeDerWelt.png}{Ein Löwengerippe, an dem noch die Mähne am Kopf ist und das außerdem transparente Flügel hat. Es ist auf allen Vieren und ist in einer sprungbereiten Position.}{Schüsse, Knochenbruch, Suizid - nicht in diesem Kapitel durchgeführt, aber Thema.}

Das Ende der Welt
=================

\Beitext{Paolo}

Mini-Eiskristalle glitzern in der Luft, zu leicht, um nach unten zu
fallen. Es riecht nach Fluffen, einem typischen Wintermarktgebäck
in Søregen. Dass es dieses frittierte Weichgeback nicht einfach überall
zu kaufen gibt, weiß Paolo von Marcin, der erst seit einigen Jahren in
Søregen wohnt.

Eigentlich passt Marcin wundervoll hierher. Søregen ist ein Inselland,
etwa so groß wie Dänemark aber ohne Verbindung zum Festland, auf dem das Wetter
die meiste Zeit über rau und grau ist. Marcins Grundstimmung passt
dazu. Seine Magie auch. Und das ist ein Problem. Über die Jahrhunderte
sind ganze Landstriche von Søregen Überflutungen und Sturm zum Opfer
gefallen, und wenn Marcin seine dunkle Magie nicht in den Griff bekommt,
wird sie sich entsprechend vereinnahmend und zerstörerisch verhalten, das
weiß Paolo. Deshalb ist dieser Tag durchgeplant. Und Marcin weiß das
nicht. Paolo wird es ihm heute sagen. Aber wie sagt man einem emotional
instabilem Jungen, oder jungen Mann, der vielleicht das Zeug dazu hat,
die Welt mit einem Wimpernschlag seiner schönen Wimpern maßlosem Terror
auszusetzen, dass seine große Schwester wahrscheinlich von einem Vampir
ermordet worden ist?

Als sie sich dem Marktplatz nähern, packt Marcin seine Fotokamera in seine
Umhängetasche. Alles an Marcin ist ästhetisch ansprechend. Die Art, wie
er ruhig durch die Kamera ein Motiv aussucht, in dessen Tiefe sich dann
stets auch immer Paolos Blick verliert. Das zarte Klicken, das Paolos
Aufmerksamkeit wieder auf Marcins sanften, geduldigen Finger lenkt. Marcin
trägt schwarze, fingerlose Strickhandschuhe, einen schwarzen, weichen Mantel, auf
dem ein paar der Eiskristalle glitzern, und langes, dünnes, helles Haar.

Paolo zieht einen der Fäustlinge aus und streckt Marcin die nackte
Hand entgegen, die Marcin mit einem schmalen Lächeln ergreift. Paolo erwartet,
dass sie die Finger verscheren, aber Marcin umschließt einfach Paolos
ganze Faust. Warm. Wie Marcins Hände bei der Kälte mit nur halben Handschuhen
bis auf die äußersten Fingerspitzen so warm sein können, ist Paolo
schleierhaft.

"Sind deine Hände gewachsen, seit du Hormone nimmst?", fragt Paolo.

Marcin hört auf zu lächeln und schüttelt kaum merklich den Kopf. "Ich kann
immer noch exakt genauso knapp Dezimen greifen wie eh und je."

"Entschuldige, war das Thema schlecht?", fragt Paolo. Er macht sich
deswegen nicht nur Sorgen, weil er ein guter Trans-Ally sein möchte, sondern
auch, weil eben heute so ein kritischer Tag ist, an dem Marcin nicht so
viel Negatives im Vorfeld fühlen sollte.

Marcin schüttelt abermals den Kopf, sagt aber weiter nichts dazu. Sein
Gesicht ist vor Kälte rosa um die Wangen. Es ist so schön, und Paolo
hätte es am liebsten geküsst, aber auch positive Emotionen sind gefährlich,
wenn sie stark sind. Mindestens aber wühlen sie auf, sodass die schlechten
dann nicht so leicht zu verarbeiten gewesen wären. Leicht würde es ohnehin
nicht werden. In weiser Voraussicht jedenfalls verzichtet Paolo darauf
und lehnt sich nur etwas gegen Marcins Schulter.

"Fluffen?", fragt Paolo, als sie den Markt erreichen.

Marcins Blick verschränkt sich mit Paolos und Paolo weiß in dem Moment, dass es
zu spät ist und dass etwas ganz und gar nicht stimmt. Die Luft um Marcin flimmert
stellenweise schwarz, wie Tintenschlieren in Wasser.

"Was ist los?", fragt Paolo.

"Das fragst du *mich*?" Marcins Hand lässt Paolos los.

Ja, wen sonst, denkt Paolo, aber provoziert wohlweißlich nicht. Sie sind auf
dem Wintermarkt! Das hat sich Paolo ausgesucht, um Marcin über den Mord zu
informieren, weil Marcin seine Gefühle in der Öffentlichkeit oft besser
im Griff hat. Aber nun kann das Desaster unter Menschen ausbrechen. Das war
nicht gut. "Wollen wir uns rasch Fluffen kaufen, Abstand zwischen uns und
den Markt bringen und dann reden? Hältst du es bis dahin aus?"

Marcin reagiert nicht sofort und als er es tut, ist es diese seltsame
Frage: "Wie wichtig sind dir die Fluffen?"

"Wir können auch ohne Fluffen gehen.", versichert Paolo. "Ich
dachte nur, du magst sie so gern!"

Abermals flimmert die Luft um sie herum, schwarze Schwaden, wie Rauch. Marcin
schließt die Augen und atmet langsam ein und aus, und sie vergehen wieder.

Paolo kann nicht leugnen, dass er auch diesen Teil von Marcin ästhetisch findet,
aber auch creepy. Paolo weiß, dass es hart für Marcin ist, nun ruhig zu bleiben. Er
hat keine Ahnung, was es ausgelöst hat, aber am liebsten würde er Marcin nun
in den Arm nehmen, oder wenigstens eine Hand auf seinen Rücken legen, aber er weiß,
dass Zärtlichkeit es bei Marcin im Moment nur schlimmer macht.

Paolo tut etwas, was ihm selbst weh tut zu tun, aber was bisher immer gut
funktioniert hat: Er greift Marcin fest am Oberarm, um ihn wegzuführen. "Komm,
ich weiß gerade, was gut für dich ist."

Aber dieses Mal schüttelt Marcin sich los. "Nein, weißt du nicht!" Seine
Stimme ist ruhig, aber ein scharfes Geräusch, nicht von dieser Welt
durchreißt die kühle, friedliche Luft, als ein schwarz triefendes
Ungeheuer in diese dringt. Es drängt sich zwischen Paolo und Marcin,
setzt eine Pranke auf Paolos Brust und wirft ihn zu Boden.

Paolo bleibt für einen Moment reglos und verdattert liegen. Wie aus weiter
Ferne dringt Gekreische der anderen Marktbesuchenden zu ihm durch. Dieses Monster
kennt er noch nicht. Ist es Wut? Es wirkt wie ein Löwengerippe, dem noch
die Mähne geblieben ist, denkt Paolo, als es mächtige Schwingen ausbreitet. Schwarzer
Rauch quillt aus den Nüstern und zwischen den Knochen her und verteilt sich
über den Boden des Marktplatzes.

"Ist es giftig?", schreit eine Person panisch, die erste Stimme, die Paolo aus
dem Gekreische herausfiltern kann. Die Kund\*innen haben längst Abstand genommen, aber
nun verlassen auch die Verkaufenden ihre Stände und rennen. Gut so. Ein Schuss
ertönt. Weniger gut.

"Feuer einhalten!", schreit Paolo über die Menge hinweg. Die Lähmung fällt
mit einem Mal von ihm ab. Er springt auf die Füße und stellt sich zwischen Marcin
und das Treiben. "Das ist Marcin, er kann nichts dafür!"

"Aber was sollen wir machen?" Die panische Stimme einer Person in Uniform, die
ein Gewehr auf Marcins Kreatur richtet.

"Ich erledige sie, das kenne ich schon!", behauptet Paolo. Aber die Kreaturen, gegen
die er bisher gekämpft hat, sind viel kleiner gewesen. Wie zum Beweise, dass diese
Situation anders ist, ertönt ein hohles, bedrohliches Quietschen hinter ihm, das
seine Glieder weich erzittern lässt. Er braucht wenigstens ein Messer. "Aber schießt nicht,
das ist viel zu gefährlich!" Paolo springt zwei Schritte nach hinten, greift sich das
flache, lange Gerät vom Fluffenstand, mit dem der Teig in kleine, würfelförmige Portionen
geschnitten wird, und stellt sich dem Löwenskelett gegenüber. Schwarzer Sabber rinnt
dem Wesen aus dem Mund, tropft auf den Boden und zerstiebt dort zu Dampf.

"Nicht", bittet Marcin.

Paolo hebt eine Augenbraue. "Du willst, dass das Ungetüm mich platt macht?"

Wie zur Bestätigung versucht eine für ein Skelett erstaunlich geschmeidige Pranke
Paolo die frisch erworbene Waffe aus der Hand zu schlagen, aber Paolo weicht geschickt
aus. Er soll nicht umsonst Martial Arts trainiert haben. Er duckt sich ein weiteres
Mal, als eine Pranke nach ihm ausholt, aber bevor diese sich wieder zurückziehen
kann, holt Paolo mit dem Messer aus und drischt es mit so viel Kraft wie ihm
möglich gegen den Knochen. Er knackst. Für einen Moment hängt die schwarze
Pranke nur an einer Sehne. Paolo wird fast schlecht von dem Anblick,
aber dann löst sich der Teil des Körpers, den das Ungeheuer nicht mehr brauchen
kann, in schwarzen Rauch auf. Paolo atmet erleichtert aus, als er realisiert,
dass es nicht nachwächst. Marcin hat auch schon Ungeheuer ins Leben gerufen, die
nachwachsende Gliedmaßen hatten. "Glaub du ja nicht, dass du mir nicht gewachsen
wärest, Marcins Wut!"

"Wut", wiederholt Marcin, als wäre ihm die Erkenntnis noch nicht gekommen.

Eine weitere, kleinere Kreatur ploppt aus dem Nichts in diese Welt. Ein
kleiner, schwarzer Drache auf Marcins Schulter. Das Wesen kennt Paolo schon, das
wird er später erledigen.

"Warum bist du wütend?", fragt Paolo in die kurze Stille hinein, die sofort
von einem weiteren unmenschlichen Quietschen der Kreatur vor ihm durchbrochen
wird.

"Ich weiß, dass sie tot ist, Paolo", sagt Marcin. "Und ich weiß, dass es
Suizid war."

"Es war kein Suizid!", widerspricht Paolo. Nein!, er sollte das nicht jetzt
sagen, nicht mitten in einem Desaster.

Die Löwenskelettkreatur setzt den verbliebenen Vorderfuß auf den Boden, nur
um zu einem Satz auszuholen, mit dem sie Paolo abermals auf die Steinplatten
wirft. Paolo versucht, ihr aus dem Weg zu rollen, aber die übrige Pranke
ist schneller und nagelt ihn auf dem Boden fest. Sie dringt zu einem Teil
in seinen Körper ein, das gruseligste aller Gefühle, und Paolo weiß noch
nicht, ob das auf Dauer Nachwirkungen haben wird. Kälte durchflutet ihn. Wieder
erschallt ein Schuss. Wieder schreit Paolo, dass sie nicht schießen sollen. Aber
nun auf dem Boden liegend, fragt er sich, ob er es wirklich alleine schaffen
wird. Die Kreatur zu vernichten, oder Marcin rechtzeitig zu sortieren, dass er
es selber könnte.

"Ich weiß, dass sie dazu in den Wald gegangen ist, und dass du mir erzählen
willst, dass es ein Vampir war", fügt Marcin hinzu. Er spricht immer ruhig, auch
wenn die Kreaturen über seine aufgewühlten Emotionen anderes beweisen. "Seit
Wochen weiß ich es."

"Woher?" Eigentlich fragt Paolo nur, um abzulenken. Seine linke Hand krallt sich um das
Schwert, um der Kreatur gleich damit den Kopf abzuschlagen.

"Ich habe den Abschiedsbrief gelesen", teilt Marcin mit.

Paolo schüttelt den Kopf. Den hat er als erster gefunden, und dann sofort
gut bei sich versteckt. In seinem Tagebuch. Da würde Marcin nie drangehen.

"Noch zu ihren Lebzeiten", fügt Marcin hinzu.

Scheiße, denkt Paolo. Ja, das kann sein. Obwohl? "Hätte ich denn dann nicht schon vorher
das Vergnügen haben müssen, diese Löwenkreatur kennen zu lernen?"

Sie bäumt sich abermals auf, womit Paolo gerechnet hat. Als sie sich zurück
auf ihn stürzt, hält er das Messer, oder den Spatel?, vor sich ausgestreckt
in die Höhe und die Kreatur stürzt sich bei dem Versuch, ihn wieder
anzugreifen, hinein. Paolo schlitzt ihr durch
den Oberkörper Richtung Hals. Knochen knacksen und brechen. Schwärze quillt
aus ihrem Brustkorb und stürzt in Fluten auf Paolos Körper hinab. Kalt. Es brennt wie
Eis auf der Haut. Aber als er am Kopf der Kreatur ankommt, löst sie sich ganz
und gar auf. Endlich.

"Weil ich mich jetzt nicht deswegen beschissen fühle, sondern wegen der
Sache, die du abziehst, um es mir zu sagen", erklärt Marcin. Er sinkt zu
Boden. In einen Schneidersitz, aber den Körper so weit vorgelehnt, dass sein
Gesicht fast den Boden berührt.

Paolo streckt nun doch eine Hand aus und legt sie auf Marcins Schulter. "Es
tut mir leid", sagt er sanft. "Aber du verstehst, warum ich es tue?"

"Schon", sagt Marcin. "Aber wenn die Wut so viel krasser ist als die
Verzweiflung über ihren Suizid, oder auch meinetwegen Mord, dann frage ich mich,
ob es das richtige Mittel ist."

Paolo nickt. "Wir müssen uns neue Gedanken machen", bestätigt er. "Ich hätte
wirklich damit gerechnet, dass du die Nachricht über den Mord schlecht auffassen
würdest."

"Also suchst du dir dafür den ersten sonnigen Tag seitdem dafür aus und gehst mit mir an einen
öffentlichen Ort", schließt Marcin richtig. Und als Paolo nickt, fügt er
hinzu: "Einen fucking Monat später. Sie war meine Schwester!"

Der kleine Drache auf Marcins Schulter macht sich kampfbereit, aber als Paolo
sich ihm mit seiner freien Hand nähert, um ihn zu zerquetschen, wie er es schon oft
getan hat, verschwindet der Drache von selbst. Marcin hat sich wieder im Griff. Aber
dieses Mal würde es vielleicht einiges an Überzeugungsarbeit brauchen, um der
Dorfwacht klar zu machen, dass von ihm im Allgemeinen keine Gefahr ausginge.

"Manchmal frage ich mich, ob wir dich nicht vorsichtig ans Fühlen heranführen
sollten", überlegt Paolo. Es ist kein neuer Gedanke, aber er legt ihn nun
das erste Mal dar. "Sodass es nicht plötzlich viel und schlimm ist, sondern
es schichtweise abgearbeitet wird, verstehst du?"

Marcin richtet sich auf und blickt ihn finster an. "Lass mich einfach im Moment
in Ruhe, ja?"

Paolo nickt.

Er steht auf, um sich mit dem Menschen von der Dorfwacht zu
unterhalten. Der Fluffenstandmensch nimmt seinen Spatel wieder entgegen, um ihn
zu waschen und anschließend wieder ans Werk mit den Fluffen zu gehen. Es
ist nichts zu Bruch gegangen, was an ein Wunder grenzt. Die beiden Schüsse sind lediglich
Warnschüsse gewesen, informiert der Mensch von der Dorfwacht.

Als wieder etwas Normalität eingekehrt ist, ist Marcin verschwunden. Irgendwann
in dem Gewusel und dem ganzen Gekläre ist er wohl einfach gegangen. Wie immer. Marcin
macht das Chaos, Paolo räumt es wieder auf. Marcin ist dabei immer zu überfordert, um
einen Finger zu rühren, aber das er einfach geht, ist schon neu. Und vielleicht auch
nicht so gut. Er ist sicher immer noch aufgewühlt.

Paolo beeilt sich damit, sich von den Leuten zu verabschieden und entfernt sich ein
Stück vom Treiben. Wo mag Marcin hingegangen sein?

Marcin mag es, allein in der Natur herumzulaufen. Und normalerweise unterstützt
Paolo das sehr. Es sortiert seine Gefühle. Es ist in so vielerlei Hinsicht
gesund und gut. Aber Paolo hat viel zu wenig Ahnung darüber, wo Marcin
eigentlich hingeht.

Ein kleiner Schneefuchs fängt Paolos Blick ein. Ein Schneefuchs? Mitten im
Dorf? Ein Fabelwesen? Paolo lächelt unwillkührlich. So etwas Softes hat
er gebraucht.

Aber er gruselt sich doch ein wenig, als der Schneefuchs um seine Beine
streicht, bevor er langsam in Richtung Wald am Dorfrand spaziert. Er
blickt sich um, bevor er außer Sichtweite wäre, und wartet. Auf
ihn?

Paolo nickt und spaziert ihm nach. Vielleicht ist es ein guter Naturgeist
oder so, der ihn zu Marcin führt, auf der Fährte von Marcins dunkler Magie. Oder
der ihm ein Mittel dagegen zeigen kann. Aber als der Schneefuchs in den
Wald verschwinden will, hält Paolo doch inne. "Es tut mir leid, in den
Wald folge ich dir nicht", teilt er mit. Damit er die Gefahr des Waldes ignoriert,
bräuchte es einen stärkeren Grund als einen Schneefuchs. Auch wenn Paolo
schon oft darüber nachgedacht hat, ob es nicht gut für das Dorf wäre,
es von der Bedrohung des Vampirs zu befreien. Und ob er dafür der
Richtige sein könnte.

Die Szenerie auf dem Marktplatz hat sich nicht halb so heldenhaft angefühlt, wie
sie in Filmen über Superheld\*innen sein müsste, damit sich die
Charaktere dafür gut fühlen. Aber dies ist ja kein Superheld\*innenfilm, dies
ist die Realität. Hier würde es sich für eine privilegierte Person, wie
er es ist, nie richtig gut anfühlen, das Richtige zu tun. Allein schon, weil
Aufräumarbeiten dazugehören, oder Situationen, in denen es kein eindeutiges
Richtig und Falsch gibt. Und zum Gutfühlen ist es ohnehin nicht da, das Richtige
zu tun. Sondern dafür, Menschen zu schützen, Menschen eine sichere Umgebung
zu schaffen. Und dazu eben seine Fähigkeiten einzusetzen.

Es ist okay, dass er es mehr tut als Marcin. Er hat die Energie und Marcin
braucht sie eben.

Der Schneefuchs dreht sich um und schaut von hinter den ersten Birken in
Paolos Gesicht. Doch als dieser sich nicht rührt, kommt der Schneefuchs
wieder hinaus und lenkt seinen Pfad auf den Waldweg um den Wald herum. Paolo
folgt ihm Richtung Küste und Geschichten fallen ihm wieder ein. Es heißt,
von jenen, die die Gefahr des Waldes verschlungen hat, werden manchmal
Überbleibsel an einer Bank an der Steilküste gefunden. Schmuck zum Beispiel, oder
anderes, was die Toten bei sich getragen haben. Und sehr selten mal
eine Dose mit Asche.

Paolo folgt dem Schneefuchs zu eben jener Bank, und tatsächlich: An der
Seite der Bank hängt eine dünne Silberkette mit einem halben Herz. Paolos
Augen schwimmen, als er den Handschuh auszieht, um das Silber durch seine
Finger rinnen zu lassen. Er spürt die Legierung. Wie er sie so oft gefühlt
hat, wenn er Marcin das Gegenstück der Kette um den Hals gelegt oder
davon entfernt hat. Von Marcins schönem, zartem Hals. Wie sehr er diese
komplizierte Person doch liebt.

Und heimlich hat Paolo Angst, dass auch Marcin sich etwas antun wollen
könnte.

Er setzt sich auf die Bank, die Kette in der Hand. Eine Bewegung neben ihm lässt
ihn herumfahren. Gerade so bekommt er noch mit, wie sich der Schneefuchs in eine
menschlichere Gestalt verwandelt. Eine mit einem weißen Fellkleid, von dem Paolo
sich nicht sicher ist, ob es aus dem Körper wächst oder ein eigenständiges
Kleidungsstück ist. Schön ist es, das kann er nicht leugnen.

"Ich bin Sonja", sagt die Gestalt. "Und ich merke einer Person an, wenn sie
im Herzen gut ist und dafür zu wenig gesehen wird."
