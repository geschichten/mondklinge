\BefehlVorKapitelIBDM{EineSchlechtErzaehlteGeschichte.png}{Ein Gerät, das Ähnlichkeiten mit einer Stimmgabel hat. Aus den beiden Armen der Stimmgabel ragen aber noch zwei kürzere Arme mit Stacheln. Oben auf den Enden der Gabel sind unidentifizierbare Nupsis, die vielleicht Wolken ähneln.}{Sterben und Blutsaugen, so allgemein als Thema, wirbellose Tiere, Umweltkriese erwähnt, Fäkalausdrücke.}

Eine schlecht erzählte Geschichte
=================================

\Beitext{Luna und Paolo}

Ein kaltes, zugiges Zimmer in einem verwinkelten Holzhaus im
Geisterwald. Luna hat das kleine Fenster geöffnet. Der Wind rüttelt
am Kronleuchter, die Kerzen lodern regelmäßig auf und tauchen die
Instrumente in umheimliches Licht. Die Instrumente, die zur Perfektion
des Blutaussaugens da sind, wie Paolo nun weiß. Das Geräusch
von Kohlestift auf Zeichenpapier. Luna sitzt neben Paolos Liege auf
dem Hocker, eine verschränkte Sitzhaltung, in der sie den Zeichenblock
halten kann.

"Du malst mich nicht erst, nachdem ich tot bin?", fragt Paolo mit zitternder
Stimme.

"Ich zeichne", korrigiert Luna. "Und zwar, während ich *dir* eine Geschichte zu
erzählen gedenke. Letzteres erfordert den Umstand, dass du dabei noch lebst. Denke
ich."

Paolo antwortet nicht. Ihm ist schwindelig, und das, obwohl die Liege nicht
gekippt ist.

Wieder ist es eine Situation für Luna, die gemischt behaglich und unbehaglich
ist. Sie mag den Wind und die Kerzen. Ihren sanften Wachsgeruch. Sie mag
das Spiel mit dem Leben. Aber sie fühlt sich sehr unbehaglich damit, dass
Paolo all ihre Bilder gesehen hat. Die im unteren Stockwerk. Es fühlt sich
an, als hätte ihr jemand in den Eingeweiden herumgewühlt. Die im oberen Stockwerk
sind noch persönlicher. Dort hätte er auch Zeichnungen von Marcins Ungeheuern
gefunden. Diesen wunderschönen Kreaturen der dunklen Gefühle.

Und nun hat sie sich vorgenommen, eine Geschichte zu erzählen. Auch das geht
weit aus ihrer Comfort Zone hinaus. Aber sie möchte es gern probieren. Sie
seufzt. "Vor hunderten von Jahren, oder so, lebte in diesem Wald einst eine
Vampirkreatur, noch unerfahren."

"Du bist älter als hundert?", unterbricht Paolo.

Luna runzelt die Stirn. "Ja, das auf jeden Fall. Wie alt genau ich bin, keine
Ahnung. Mein Gedächtnis wird poröser, je weiter die Erinnerungen zurückliegen."

"Allein das", sagt Paolo. "Du bist wider die Natur. Das Leben, das du bekommst,
muss irgendwo anders abgezogen werden."

Luna schnaubt. "Hast du dir das selbst ausgedacht?"

Paolo versucht, sich in eine etwas andere Position zu legen, wenigstens
das, aber es geht nicht. "Denk doch mal selber drüber nach", fordert er
sie auf. "Guck, was du hier anrichtest! Du nimmst Leben!"

"Ja Paolo, ich nehme Leben", bestätigt Luna ungeduldig. "Das eine hat mit
dem anderen aber nichts zu tun. Kendra hat mich kürzlich freundlicherweise
darüber informiert, dass so manche Qualle unsterblich ist. Und jene sind meist
weniger mordlüstern als ich."

Paolo blickt sie halb verständnislos und halb nachdenklich an, sagt aber
nichts dazu.

"Und wenn du ein Lebewesen haben willst, dass ein bisschen eher meiner
Natur entspricht, so von Zähnen her und mehr aus der Ecke Raubtier, dann nenne ich
dir Grönlandhaie", fährt Luna fort. "Wunderschöne Fische, und von einigen
Exemplaren ist sehr wahrscheinlich, dass sie älter sind als ich. Hör mir auf
mit wider die Natur. Schau dir die Natur gründlich an! Das, was davon
noch da ist. Und wenn du gern Unmengen Energie in Weltrettungspläne stecken
möchtest, dann darein, sie zu erhalten."

Paolo erinnert sich daran, dass er die Theorie der Kipppunkte aus der Klimaforschung
auf das Vampirproblem übertragen hat. Nun ergibt es in seinem Kopf plötzlich
weniger Sinn als zu dem Zeitpunkt, zu dem er es Sonja dargelegt hat. "Es tut
mir leid", sagt er. Es fällt ihm echt nicht leicht, das zu sagen.

"Was konkret?", bohrt Luna unbarmherzig nach. "Dass du mich schon nach einem
Satz unterbrochen hast, als ich anfing, die Geschichte der Mondklinge zu
erzählen?"

"Dass ich dich für so böse gehalten habe", murmelt Paolo.

Luna hebt äußerst skeptisch die Augenbrauen. "Nach welchen Definitionen von böse
bin ich nicht böse?"

"Aber du bist nicht das Unheil der Welt." Paolo ist es eigentlich echt
nicht recht, diese Kreatur mit Anerkennung zu übergießen, während er an
eine Liege gefesselt unter ihr liegt.

"Nein, bin ich nicht", bestätigt Luna. "Wenn mich Leute einfach in Ruhe lassen
würden, könnte ich sogar recht harmlos sein. Aber sie lassen mich nicht in
Ruhe, also bin ich äußerst unharmlos." Ungeduldiger fügt sie hinzu: "Was versuchst
du da? Dass ich irgendwie denke, du hast dich verändert, du hast endlich
verstanden, worum es geht, und dich freilasse?"

Paolo fühlt sich erwischt. Das war tatsächlich seine Hoffnung dabei. Oder
etwas in der Art. Dass sie ihn anders behandeln würde, wenn sie ihn weniger
hasst. Weil er mal etwas richtig macht. "Es war unehrlich von mir, das nicht
dazuzusagen", räumt er ein. "Aber es war mir auch nicht sofort bewusst."

"Du machst also nun sogenannte Seelenstriptease vor mir", folgert Luna. "Nachdem
ich dir schon gesagt habe, dass ich nicht verhandle. Ich werde dich nicht
freilassen." Sie seufzt. "Paolo, ich weiß, dass du ein unsicherer Mensch
bist und dass du gern ein guter Mensch wärest. Und nicht weißt, ob
du es bist oder was du tun musst, um es zu sein. Und dass dich das fertig
macht und belastet."

Paolo fühlt Tränen in sich aufkommen. Er fühlt sich nun so verletzlich. Er
hätte ihr diese Dinge nicht anvertrauen sollen.

"Es tut mir leid, dass du dich schlecht fühlst", sagt Luna. "Und vielleicht
wären deine Bemühungen einer anderen Person gegenüber sogar angebracht
gewesen. Eine andere Person mag für dich in der Hinsicht auch safer
sein als ich. Aber eh du bei mir weitermachst, sage ich dir, dass es in dieser
Situation eher nicht angebracht ist. Ich brauche es nicht. Du bereust es hinterher. Es
gewinnt niemand dadurch."

Paolo weiß nicht, was er sagen soll. Er möchte sich verteidigen. Er möchte
begründen, warum er es getan hat. Also wagt er einen letzten Versuch.
"Ich", -- er muss durch Tränen sprechen --, "ich will geliebt werden. Ich
will nicht gehasst werden. Ich will nicht immer alles falsch machen."

"Du wirst geliebt", sagt Luna sanft. "Nicht von mir, aber von Angela. Das weiß
ich."

"Stimmt", schluchzt Paolo.

"Ich weiß nicht, wer dich hassen sollte. Am ehesten hat wohl Marcin Anlass, aber
meines Wissens hat er dich immer noch sehr gern", führt Luna aus und fragt sich
im nächsten Moment, ob ihr überhaupt zusteht, diese Information weiterzugeben.

"Du hasst mich nicht? Oder Sonja?", fragt Paolo skeptisch.

Luna schüttelt den Kopf. "Sonja ist misanthrop. Sie mag Menschen im allgemeinen
nicht sonderlich, aber es ist kein Hass. Es ist eher Desinteresse und allgemeine
Abscheu und Genervtheit. Es sei denn,
einer eignet sich mal für ein Spiel. Dann hat sie ein etwas fieses, aber keinesfalls
hassvolles Interesse." Sie seufzt. "Und ich bin gezwungen, nun meinen Zug
zu machen. Du bist da also zwischen zwei unsterbliche Personen geraten, die
über Jahrhunderte hinweg ihren Zwist nicht gerade in rücksichtsvoller Weise für die
Umlebenden ausleben." Sie kichert ob des unbeabsichtigten Wortspiels. "Ich
hasse dich auch nicht. Du bist mir nicht wichtig. Ich finde dich etwas nervig, aber
jetzt auch nicht so, dass das allein Mordfantasien ausgelöst hätte." Luna
grübelt einen Moment. "Wobei ich glaube, dass ich mehr Mordfantasien bei Leuten
entwickle, die ich mag. Interessante Erkenntnis."

"Es ist so böse, was ihr da macht", sagt Paolo schwach.

"Das ist es", bestätigt Luna nickend. Sie fasst den Stift anders, sodass
er nicht aufs Papier trifft, als sie mit den Fingern eine Schattierung
verreibt. "Und zur Frage, ob du alles falsch machst, liegt mir natürlich
die Antwort auf der Zunge, dass um wirklich *alles* falsch zu machen, noch
sehr viel Potenzial nach oben offen ist."

Paolo kichert bitter und schluchzt dabei gleichzeitig. "Ich möchte viel weniger
falsch machen."

"Nein", sagt Luna. "Das reicht dir nicht. Du möchtest *nichts* falsch machen. Nie
wieder. Du möchtest außerdem, dass nur Situationen vor dir liegen, die jeweils
genau eine richtige Entscheidung zulassen. Denn immer dann, wenn es zwei
ähnlich gute Möglichkeiten mit jeweils ihren Nachteilen gibt, kommst du nicht
klar, mit egal was du hinterher entschieden hast."

Paolo zittert, weint und möchte widersprechen. Er weiß nicht wie, aber es
ist so ungerecht, was Luna sagt.

Aber Luna hindert ihn daran, bevor er dazu kommt. "Lässt du mich meine Geschichte
weitererzählen? Bitte?"

"Gleich", sagt er, als er etwas realisiert. "Ich glaube, du hast Recht, mit dem,
was du sagst. Ich denke immer, warum würde ich so etwas Unrealistisches wollen? Aber
die Frage muss eher sein, wie lerne ich, nicht so etwas Unrealistisches zu wollen?"

Luna seufzt. Sanft dieses Mal. Sie nimmt sogar den Stift vom Blatt und sieht
ihn an. "Ich bin keine Therapeutin." Die Stimme untermalt mit einem verständnisvollen
Summen, das nicht zu mitleidig klingt.

"Ich wollte immer in Therapie gehen. Aber ich verstehe mich mit der Therapeutin
im Dorf nicht", sagt Paolo. "Und um weiter weg zu pendeln oder so etwas wie
eine Psycho-Kur zu machen, dafür hatte ich zu viel Angst, Marcin allein zu
lassen." Nun weint er wirklich. Tränen laufen ihm einfach über das Gesicht. Marcin
mag ihn noch, hat Luna gesagt. Aber die Beziehung ist Vergangenheit. "Ich liebe
Marcin."

"Paolo, du machst immer noch Seelenstriptease vor mir", ermahnt Luna. "Es
mag dir inzwischen sogar gut tun, aber mir nicht. Ich möchte das nicht."

Wie kann etwas, was sich gerade noch nach neuer Erkenntnis angefühlt hat, so
rasch von diesem stechenden Schmerz, falsch zu sein, durchbohrt werden? "Dann
erzähl deine bekackte Geschichte."

"Einst lebte in diesem Wald eine Vampirkreatur, noch unerfahren", setzt Luna
wieder ein. Aber wie weitererzählen? "Es ist ein guter Wald. Ein Wald, der
flüstert, ein Wald der so viel Leben enthält. So viel schönes Leben." Sie
seufzt. "Und dann kamen Menschen in diesen Wald, Menschen, die keinen Sinn
für dieses Leben hatten. Sie wollten Holz und Fleisch und gierten nach
der Macht über den Grund. Sie sagten, ihnen gehöre der Grund nun. Aber ein
Wald kann niemandem gehören."

"Außer dir", grummelt Paolo sarkastisch.

Luna schüttelt den Kopf. "Ich wohne im Wald und beschütze ihn. Ich teile ihn
nicht mit Menschen. Aber der Wald und seine anderen Bewohnenden haben kein
Verständnis davon, besessen zu werden, also besitzt sie auch niemand." Luna
seufzt abermals. "Menschen kamen in diesen Wald, um zu zerstören. Und die
Vampirkreatur wurde zum Fels, an denen sie mit ihrem Vorhaben zerschellten. Ein
Krieg entstand und einen verständnislosen Menschen nach dem anderen raffte
sie dahin. So viele, dass das Morden aufhörte, Spaß zu machen."

"Es hat dir von vornherein Spaß gemacht?", fragt Paolo.

"Ja!" Luna rollt mit den Augen. "Auf jeden!" Sie schnaubt über ihre
eigene Ausdrucksweise. "Weil es aber so viele waren, und es sich anbot, machte sich die
Vampirkreatur eine Kunst daraus. Das kennst du bestimmt: Wenn du irgendeine
Sache immer und immer wieder tust, dann perfektionierst du sie irgendwann
auch, oder?"

Paolo antwortet nicht. Er denkt einen Moment ans Kochen, aber er will
absolut nicht Morden mit Kochen vergleichen oder irgendeinen positiven
Aspekt am Morden nachempfinden. Oder sollte er? Wäre es richtig? Weil
immer das, was ihm intuitiv richtig vorkommt, doch nicht richtig ist?

"Die Vampirkreatur ließ sich immer ein paar Leute übrig, die in den Wald
kamen, um sie nicht direkt zu töten, sondern um mit ihnen zu
experimentieren", fuhr Luna fort.

"Abscheulich", kommentiert Paolo.

"Ja, schon", gibt Luna zu. Sie ist sich nicht sicher, ob sie mit
diesem Teil ihrer Vergangenheit inzwischen im Reinen ist. Das ist gerade
auch schwierig: Sie spielt wieder in dieser unnötigen Art mit
Beute. Weil sie in einer Zwickmühle steckt. Aber es erinnert sie
an damals, und es ist kein allzu schönes Gefühl. Wie sie am Anfang
festgehalten hat: Es ist einfach keine gute Option da gewesen, und diese,
die ihr als die beste erscheint, behagt ihr nicht.

Luna seufzt noch einmal und fragt sich, ob sie am Ende mehr seufzen
als erzählen wird. "Auf einem der Instrumente liegst du. Es ist ein
einfaches Gerät, um Blutdruck in Halsgegend zu erhöhen. Simpel gedacht,
aber gar nicht so wenig effektiv. Du kennst diese Sache, dass eine typische
Erstehilfemaßnahme in Fällen, wo das Blut mehr in den Kopf sollte, ist,
die Beine hochzulegen?"

Paolo nickt. Er weiß nicht mehr genau, in welchen Situationen das gut ist, was ihn
ärgert. Der Kurs, den er mit der Schulklasse gemacht hat, liegt doch gar
nicht so lange zurück.

"Ehe ich auf all die anderen eingehe, komme ich vielleicht am besten
gleich zur Mondklinge", sagt Luna. "Jene Vampirkreatur mochte Musik
sehr gerne und hatte herausgefunden, dass sie durchaus auch einen
Effekt auf hörende Wesen hat. Auf deren Kreislauf. Also entwickelte die
Vampirkreatur eine Stimmgabel, verwob darein eine zweite, und bastelte
so lange daran herum, bis ein Instrument entstand, das auf möglichst
viele Menschen den Effekt einer Extase auslöst. Herzrasen. Du hast
es erlebt."

Paolo nickt. Er findet es tatsächlich eindrucksvoll. "Es ist also kein
Instrument, um dich zu töten, sondern auch dafür da, dass du mehr
Blut aus Menschen bekommst?"

"Genau", bestätigt Luna.

"Und Sonja möchte, dass ich sie ihr beschaffe, damit du sie nicht mehr
benutzen kannst?", mutmaßt Paolo. "Damit du weniger grausig sein kannst?"

"Grausig!" Luna lächelt. "Ein schönes Wort. Danke." Sie deutet eine
Verbeugung an, eh sie ihren Stift wieder aufnimmt und weiterzeichnet. "Nein,
ich denke nicht, dass das ihre Motive sind."

Sie gleicht das Gesicht, das
sie gezeichnet hat, noch einmal mit Paolos ab. Dann legt sie den Stift
beiseite und nimmt stattdessen den Knetradierer, um die Tränenspuren
anzudeuten. Sehr sachte.

"Dieser unirdische Klang, wie ihn viele beschreiben", fährt sie fort, "hatte
einen Schneefuchs in den Wald gelockt. Die Vampirkreatur kannte es schon, dass
einige der Tiere im Wald den Klang durchaus mochten. Sie hat ihn so kreiert, dass
er sicher keine traumatische Erfahrung für die Tiere im Wald war. Aber sie merkte,
dass an dem Schneefuchs etwas anders war als an den anderen Tieren im Wald."

"Sonja", sagt Paolo leise.

"Sonja." Luna nickt.

"Ich wusste, bevor du es gesagt hast, nicht, dass sie auch unsterblich ist", gibt
Paolo zu. "Ist sie auch ein Vampir?"

"Nein, das ist sie nicht", erwidert Luna. Sie merkt, wie sie ungewöhnlich sanft
im Zusammenhang mit Sonja klingt. Das ist interessant.

"Ist sie das Gegenteil eines Vampirs? Dein Gegenstück etwa?", fragt Paolo.

"Du meinst, sie, hm, gefriert im Mondlicht, und statt Blut zu trinken, spuckt
sie es?" Luna kichert. "Nein, das trifft sicher auch nicht zu."

"Ist sie sowas wie ein Werfuchs? Sind Bisse von ihr für dich tödlich?", schlägt
Paolo vor.

Luna will erst widersprechen, aber streicht sich dann nachdenklich übers Kinn. "Spannende
Frage. Ich glaube, Sonja hat mich noch nie gebissen." Sie wendet sich wieder ihrer
Zeichnung zu. "Unwahrscheinlich trotzdem, denke ich. Du denkst zu legendenhaft, zu
wenig wissenschaftlich. Du versuchst oft, irgendwelche Aspekte aus alten Geschichten, die ihren
Weg verblümt in Fantasy-Werke gefunden haben, aufs Leben zu übertragen. Das
eine Instrument, was das Vampir töten kann: Die Mondklinge! Die eine Person, die
das Böse der Welt verkörpert: Die Vampirkreatur! Und nun, da Mondklinge out ist, suchst
du das nächste eine Element, das ein Gegenpol sein kann."

Ehe Paolo wieder in irgendwelche Gedanken abdriften kann, dass es ja klar wäre, dass
er wieder falsch denke, bittet er Luna, fortzufahren.

Luna lächelt mild. "Jedes Mal, wenn der Klang den Wald erfüllte, kam sie. Jedes
Mal ein Stückchen näher. Sie wusste, dass sie gefährlich lebt, weil sie eben
nicht nur Fuchs, sondern auch Mensch ist. Sie wusste, dass ich es fühle, aber
solange sie in Fuchsform war, habe ich sie in den Wald gelassen." Sonja kam auch
nicht, um Luna zu stören, fällt Luna ein. Sonja wollte nur nicht so alleine
sein. Und Luna hat damals keinen Sinn dafür gehabt. "Aber als sie sich
irgendwann vor meinem Haus in menschlichere Form gebracht hat, als der
Ton erscholl, habe ich sie ausgesaugt."

Paolo starrt sie entgeistert an. "Du hast Sonja ausgesaugt?"

"Immer wieder", sagt Luna. "Sie kam, um den Ton zu hören, glaube ich. Das
wird mir jetzt klar, da sie die Mondklinge stehlen will. Ich habe sie lange
nicht mehr benutzt. Ich denke, sie hat einfach Sehnsucht nach dem Klang."

Sie schweigen eine Weile. Die Geschichte ist zu Ende. Aber es ist so
unvorhergesehen für Paolo, dass er es nicht sofort realisiert.

"Was, wenn der Ton dich töten kann, wenn eine andere Person ihn
anschlägt?", überlegt Paolo.

Luna zupft die Mondklinge aus ihrer Halterung und legt den unteren
Stab, an dem die Gabeln sich verzweigen, in Paolos Hand. "Wenn du willst,
schlag sie an."

Paolo zögert. Wenn es sie tötet, dann ist er hier immer noch gefangen. Es wäre
besser, wenn sie ihn zuerst befreit. Wie lange wird er hier drin überleben? Werden
doch Menschen kommen und ihn suchen, wenn er nicht wieder auftaucht? Vielleicht,
wenn eben auch Luna nicht wieder auftaucht?

Will er Luna überhaupt noch töten? Aber sollte er nicht eine Person töten
wollen, die Marcins Schwester auf dem Gewissen hat und die ihn hier gefesselt
neben sich liegen hat? Ist es nicht die Frage, ob er oder sie? Wenn er sie
nicht tötet, tötet sie ihn. Wenn er sie tötet, sterben sie vielleicht
beide. Egal. Paolo bewegt die Mondklinge, sodass sie sehr leicht
gegen das Holz unterhalb seiner Hand stößt. Vorbereitend darauf, es gleich
mit dem ganzen Spielraum zu tun, den seine Hand hat. Aber diese sanfte Berührung
bringt sie schon so zum Schwingen, dass er sein Blut rauschen spürt, sein Griff
schwächer wird, er sie fast loslässt.

Und dann realisiert er: Es ist völlig egal, wer sie anschlägt. Sie wird nicht
dadurch anders klingen, dass er es tut, und der Klang ist es, der etwas auslöst, das
Gleiche auslösen wird wie vorhin. Er wird sich damit selbst in Extase versetzen,
so wie er sich selbst auf die Bank gelegt und ausgeliefert hat. "Nimm sie", wimmert
er. "Mach es selbst!"

Luna nimmt ihm die Mondklinge ab, atmet tief ein und aus. Sie blickt zum Fenster
in die Dunkelheit, lauscht in die Stille, bevor sie sie noch einmal anschlägt. Ihr
Fingernagel schnellt mit Schwung dagegen und der Klang durchbricht die Stille, kitzelt
selbst unter ihrer Haut. Reißt an ihrem Inneren. Der Klang des Todes.
