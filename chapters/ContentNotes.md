### Anmerkungen zu den Content Notes

Ich versuche hier eine möglichst vollständige Liste an Content Notes
zur Verfügung zu stellen, aber weiß, dass ich nicht immer alles
auf dem Schirm habe. Hinweise sind willkommen und werden ergänzt. Über
die Content Notes hinaus darf mir gern jede Frage nach Inhalten gestellt
werden und ich spoilere in privaten Konversationen nach bestem
Wissen. Es bedarf dafür keiner Begründung oder Diskussion. Ich mache das
einfach.

Ich nehme außerdem teils sehr seltene Content Notes für Personen mit auf, die
ich kenne, weil sie sich für meine Kunst interessieren.

### Für das ganze Buch

**Zentrale Themen:**

- Suizidalität.
- Blut, Blutsaugen, Beißen.
- Mord.
- Bedrohung.
- Sterben.
- Ungeheuer.
- Morbidität.
- Vielleicht toxische Beziehung.
- Manipulation.
- Erotik.
- Kink.
- Dunkle Ungeheuer.
- Essen.

\Leerzeile
\Leerzeile
**Mittleres bis häufiges Thema:**

- Sex.
- Genitalien.
- Machtgefälle.
- Psychologisches Sadismus-Masochismus-Spiel.
- Folter.
- Fesseln.
- Minderwertigkeitsgefühle.
- Erschießen.
- Leichen.


\Leerzeile
\Leerzeile
**Ein- oder zweimaliges Aufkommen:**

- Atemprobleme.
- Knochenbrüche.
- Köpfen.
- Kanibalismus - assoziierbar.
- Brandverletzungen.
- Vergiften.
- Fäkalien.
- Verwesung.
- Löcher.
- Dissoziationen.
- Schuldgefühle.
- Stalking.
- Gaslighting?
- Wirbellose Tiere - erwähnt.
- Klimawandel - erwähnt.


### Sanftes Entrinnen

Suizid, Mord, Blut, Beißen, Herzprobleme, Atemprobleme, Erotik.

### Das Ende der Welt

Knochenbruch, Suizid - nicht in diesem Kapitel durchgeführt, aber Thema.

### Silberblick

Bisher keine.

### Ketten

Erotik.

### Erkennen

Reden über Tod.

### Logik am Morgen

Reden über Tod, vielleicht toxische Beziehung.

### Probieren und Studieren

Vielleicht toxische Beziehung.

### Mondscheinhypothesen

Mord und Suizid als Gesprächsthema, Köpfen, Bedrohung, Vampir, Blut, Kanibalismus - assoziierbar, Erotik, Beißen, wirbellose Tiere - erwähnt.

### Wissenschaft des Todes

Mordversuche - thematisiert, Machtgefälle, Morbidität.

### "Jagd"

Erschießen, Morbidität.

### Brennen

Erschießen, Brandverletzungen, Blutsaugen.

### Lauden Léonide von Horstenfels

Morddrohungen, Folter - erwähnt.

### Off Limits

Vergiften, Ausbluten, Fäkalien, konsensuelles Foltern, Blut, Beißen, Fesseln.

### Die Schlange, die Schildkröte und die löchrige Flunder

Sex als Thema. Genitalien erwähnt, Löcher, Verwesung.

### Der Schneefuchs und der Schmetterling

Mord, Blut, Blutarmut, Genitalien, Erregung, Erotik, psychologisches Sadismus-Spiel, Fixieren, Ausgeliefertsein.*

### Auseinander- und Zusammen--nehmen und -setzen

Fesseln, BDSM erwähnt, ein Witz, der Ableismus auf die Schippe nimmt, und bei dem ich noch nicht entscheiden kann, ob das eher hilfreich ist oder mir nicht zusteht, emotionaler Druck, Dissoziation.

### Verloren

Schuldgefühle, Minderwertigkeitsgefühle, Gaslighting?.

### Vor-, Rück-, Nach-, An-, Ab-, Zuver- und Übersicht

Knochenbrüche, Morddrohungen.

### Die Mondklinge

Bedrohung durch Tod, Auseinandersetzung mit Tod, Leichen, Suizidalität - erwähnt, Fixierung, Ausgeliefert sein, Blutsaugen.

### Eine schlecht erzählte Geschichte

Sterben und Blutsaugen, so allgemein als Thema, wirbellose Tiere, Umweltkriese erwähnt, Fäkalausdrücke.

### Ein Scheiterhaufen

Stalking, Tötungsabsichten, Einbruch, Fesselung.

### Fiese Liebesbriefe

Kink, Lauern, legeres Reden über Mord.

### Die Verschiedenheit der Weltrettungspläne

Androhung von Folter/Mord, Gespräch über Lebensmüdigkeit.

### Das Reißen der Zeit

Erotik, Blut, Sex, Genitalien.

### Epilog

Folter erwähnt.
