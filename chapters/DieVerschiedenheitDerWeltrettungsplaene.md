\BefehlVorKapitelIBDM{DieVerschiedenheitDerWeltrettungsplaene.png}{Eine angeschnittene Quiche.}{Androhung von Folter/Mord, Gespräch über Lebensmüdigkeit.}

Die Verschiedenheit der Weltrettungspläne
=========================================

\Beitext{Die Horstenfels-WG}

Eine Woche (fast, eigentlich nur sechs Tage) später in der Villa von Léonide von
Horstenfels. Aus der Küche kriecht der Geruch von Quiche in alle Nischen des
Hauses und bis in den Garten. Marcin bereitet sie zu. Er macht es nicht so gut
wie Paolo, muss er dabei viel denken. Dabei hängt er in einem Video-Call mit
Paolo, zeigt ihm alles und lässt sich alles erklären. Der Boden mit dem
Gemüse backt bereits im Ofen, während Marcin in der Pampe rührt, die darüber
gegossen werden soll, als Paolo sich verabschiedet.

"Ich muss gleich los", sagt er. "Es war schön, mit dir zu reden. Irgendwie
ganz anders als sonst."

Marcin nickt. "Ich vermisse dich, aber ich glaube, die Trennung tut uns
ganz gut. Vor allem mir eigentlich. Wie geht es dir inzwischen damit?"

"Ich vermisse dich auch ganz furchtbar!", sagt Paolo, aber mit einem
Lächeln quer übers Gesicht. "Ich kann nicht behaupten, dass ich die
Trennung einfach wegstecke. Ich vermisse auch physische Nähe zu dir. Aber
ich glaube auch, dass es ganz gut ist, dass ich die gerade gar nicht
habe." Er seufzt. "Ich will dich nicht bedrängen."

"Ich mag gern mit dir kuscheln, wenn du wieder da bist und das dann
noch möchtest. Und wenn dir kuscheln reicht", erwidert Marcin.

Paolo reagiert einen Moment überhaupt nicht. Dann piepst irgendetwas
schrill durchs Telefon. Paolo drückt darauf herum, bis es aufhört. "Das
war der Wecker. Ich muss los!", sagt er eilig. "Aber, ja, doch,
wenn wir das beide dann noch mögen, würde ich sehr gern." In
seine Stimme hat sich ein zartes, vielleicht glückliches Weinen
geschlichen. "Grüß die anderen alle, ja? Alle!"

Bevor Marcin etwas erwidern kann, ist der Anruf beendet. Also auch
Luna, denkt Marcin. Wirklich? Aber Paolo hat betont, Marcin möge
*alle* grüßen und er weiß ganz genau, dass Luna dabei ist. Na gut.

---

Luna befindet sich mit Kendra im Garten. Die Sonne scheint. Der Feuerlöscher
steht bereit. Nicht, um Luna zu löschen, sondern die Dinge, die Lunas
entflammender Körper aus Versehen anzünden könnte.

Luna ist, bis auf einen Arm, vollständig mit einer Decke bedeckt. Auf
jenen Arm hat Kendra Markierungen gemacht und zwischen jene verschiedene
selbst gemixte Cremes gerieben, die gegen UV-Licht schützen sollten.

Das Problem ist, dass das Experiment nicht einfach sofort ein hilfreiches
Ergebnis liefert. Die Creme-Mischungen, die gar nichts taugen, haben sie
bereits ausgeschlossen. Unter den jetzigen gibt es welche, die nur eine
kurze Zeit gegen Entflammen helfen, bis sie eingesogen sind. Oder die
nach der zweiten Schicht, nachdem die erste eingesogen ist, viel länger
halten, aber eben auch nicht unbedingt dauerhaft.

Kendra macht Notizen dabei. Luna versucht, sich zu entspannen. Sie mag die
Sensorik von Creme nicht und vor allem den Geruch von Creme nicht, aber es ist
schon besser, dass sie nicht in Flammen ausbrechen wird, wenn sie heute mit den
anderen gemeinsam am Tisch sitzen wird.

Léonide sitzt draußen mit am Tisch und puzzlet. As trägt einen Strohhut
gegen die Sonne. Über Søregen herrscht seit ein paar Tagen ein
Hochdruckgebiet, das sich verirrt haben muss. Es war in Léonides Erinnerung
auf dieser Insel noch nie so lange am Stück sonnig. As hat eigentlich
eine Cappy, aber Luna hat iem gestern einen Strohhut mitgebracht, einen
ziemlich ehrwürdigen irgendwie, den sie wohl von anno dazumal in
ihrer Sammlung gehabt hat.

As gibt auf, das Puzzleteil zu finden, das as gerade sucht, weil es an
der Zeit ist. "Bleibst du da sitzen? Kann ich mir deinen Stock für
10 Minuten leihen?"

Kendra nickt. "Er ist dir vielleicht zu kurz."

Léonide nimmt ihn und drückt sich daran hoch. As nickt. "Stimmt." Aber
as nimmt ihn trotzdem mit. Er hilft iem bei der einen Stufe von
der Terrasse in die Essnische hinauf. Halber Weg in die Küche geschafft. Heute
fällt iem alles schwer. "Marcin?"

"Gleich!", ruft Marcin. Er überlegt, die Form noch zu Ende aus dem Ofen zu
nehmen, aber das sind zu viele Schritte. Es wird sicher nicht schlimm sein, wenn
sie da zwei Minuten zu lange drin ist, also klappt er den Ofen wieder zu und kommt
Léonide entgegen.

"Hast du die Zeit im Blick? Der Bus kommt bald", ermahnt Léonide.

Marcin blickt auf die Kuckkucksuhr und wird hibbelig. "Kann jemand von euch
die Quiche dann aus dem Ofen nehmen?"

Léonide nickt. "Wenn Luna bis dahin nicht präpariert ist und die Sache
übernehmen kann, werde ich es schon hinkriegen. Du bringst mir dann am
besten einen Wecker nach draußen, bevor du gehst."

---

Phillipp steigt zitternd am Busbahnhof im Dorf aus. Was für eine Fahrt. Sie
wartet, bis sich die Menge um sie herum aufgelöst hat (die eigentlich nur
aus vier Leuten bestanden hat) und außer ihr nur noch eine Person am Bahnsteig
steht. Phillipp winkt vorsichtig. Sprechen kann sie gerade nicht.

"Phillipp? Kendras Mutter?", fragt Marcin.

Phillipp nickt bloß. Sie schaltet die Noice Cancelling Kopfhörer aus,
setzt sie ab und verstaut sie in ihrem Rucksack. Es ist viel Gerödel, aber
es ist auch gut, dass das jetzt dran ist, weil es ihr Zeit gibt, mit
dem Ankommen klarzukommen. Mit der fremden Person.

Es war abgesprochen, dass Marcin sie abholt. Sie haben vorher über
Signal kommuniziert. Marcin weiß, dass Phillipp oft nicht sprechen
kann, wenn sie frisch an Bahnhöfen ankommt.

"Soll ich den Rucksack tragen?", fragt Marcin.

Aber Phillipp schüttelt den Kopf und tut es selbst.

"Sollen wir losgehen?", fragt Marcin als nächstes.

---

Später draußen am Tisch mit einer Tasse schwarzen Tees in der Hand fängt
Phillipp an, sich zu entspannen. Das einzige, was stört, ist der
Geruch nach Sonnencreme. Aber der wird auch schwächer werden. Luna
sitzt ihr Gegenüber in einem schwarzen Kleid, Typ gothic, mit
Ärmeln und Spitzenhandschuhen. Sie sitzt unter einem schwarzen Sonnenschirm. Sie
hat, wie Kendra Phillipp am Telefon erzählt hat, tatsächlich
schwarzes Haar, das nicht in der Sonne glänzt, als hätte es einen
dieser fast komplett absorbierenden Schwarztöne oder als hätte das
Haar eine nicht so haartypische Beschaffenheit. Luna ist erheblich
weniger dünn, als Phillipp sie sich ausgemalt hat, und sie schämt
sich ein wenig für ihr stereotypes Bild einer eher schmalen
Gestalt, das sie mit Vampiren verknüpft hat. Sie kann sich nur
schwer davon abhalten, ihren Blick immer wieder auf Luna zu richten, aber
diese scheint das nicht zu stören.

Als der Wecker piepst, erschreckt Phillipp sich. Marcin steht
auf und holt die Quiche aus dem Ofen. Sie riecht sehr gut und
überdeckt den Sonnencremegeruch.

"Es tut mir leid, dass ich so lange brauche, bis ich mich woanders
zurechtfinde", entschuldigt Phillipp sich, als die Teller
verteilt sind.

Alle versichern ihr, dass das doch gar kein Problem wäre. Aber
sie hätte gern am Gespräch zuvor teilgehabt. Marcin hat
von seinem Ex-Freund erzählt, der nun in einem Camp zur Aufarbeitung
psychischer Probleme ist.

"Du deckst einen Teller zu viel", richtet sich Léonide an Luna.

Lunas Gesichtsausdruck wirkt irgendwie vielsagend, sagt
Phillipp aber natürlich überhaupt nichts. Luna legt unbeirrt
Besteck zum sechsten Teller. Die Türglocke bimmelt.

"Wenn du Sonja eingeladen hast...", leitet Léonide ein, Wut
oder Bedrohlichkeit in der Stimme.

Aber Luna schüttelt den Kopf. "Angela."

Léonide wirkt erleichtert, einen Moment fast sanft. "Okay, damit
komme ich klar", sagt as. "Aber wenn du noch einmal ohne Absprache
jemanden in mein Haus einlädst, dann blute ich dich aus und
föhne dich anschließend in der Sonne mit einem Heißluftföhn."

Luna verzieht das Gesicht zu einem Ausdruck von Abscheu. Nur
für eine Moment. "*Das* könnte endgültig werden."

"Meinst du?" Léonide wirkt plötzlich weniger überzeugt.

Luna zuckt mit den Schultern und macht sich auf dem Weg zur
Tür. "Ausbluten war halt schon überraschend heftig. Wir haben
noch nicht fertig erforscht, woher mein neues Blut kam. Aber
wenn es unter anderem aus der Luftfeuchte kam, dann
kann ein Heißluftföhn oder ein Feuer schon einen entscheidend ungünstigen
Effekt haben. Und so krass mein Körper auch ist, völlig unzerstörber wird er
nicht sein", sagt sie noch, bevor sie durch die Terrassentür schreitet.

Phillipp sieht zu ihrem Kind hinüber. "Ich bin noch nicht so sicher, was
ich davon halte, dass ihr diese Experimente macht. Aber vielleicht kommt
das, weil ich mir nicht vorstellen kann, damit leben zu können, dass
Luna dann doch dabei zu Schaden kommt. Zu dauerhaftem."

"Aber sie ist so interessant!", sagt Kendra. Ihre Wissenschaftseuphorie
klingt so wunderschön durch dabei. "Sie ist genauso neugierig, wie ich. Aber
vielleicht forsche ich erstmal an ungefährlicheren Dingen an ihrem
Körper rum. Etwa erforschen wir im Moment, *wie* er heilt. Also was
für biologische oder physikalische Prozesse dahinterstecken. Dabei
machen wir erstmal immer das Gleiche."

"Anschneiden, vor allem", sagt Léonide trocken.

Phillipp möchte ihrem Kind eigentlich auch nichts verbieten, was so
logisch sinnvoll klingt. Aber ein gutes Gefühl hat sie dabei
nicht unbedingt. Luna ist schließlich auch eine Mörderin. Die
Situation ist so abgefahren!

Aber im nächsten Augenblick hat sie ganz andere Gedanken im Kopf. Luna
führt eine Person auf die Terrasse, die eine Schönheit an sich hat, die
Phillipp den Atem raubt. Das hat sie selten. Angela ist mit Sicherheit
ein gutes Stück älter als sie und trägt eine Traurigkeit oder Melancholie
in ihrem Gesicht, und zugleich eine Freundlichkeit in ihrer Haltung, die
einfach mit Phillipp sprechen.

Angela bringt Léonide ein Besuchsgeschenk mit. "Paolo meinte, Geschenke sind
nicht unbedingt willkommen bei Ihnen", sagt sie. "Ich habe Verständnis,
wenn ich es einfach wieder einpacken soll. Aber in meiner Familie gehört es
eigentlich dazu, dass wir eines mitbringen. Ich habe mir viele Gedanken
gemacht."

Léonide nimmt es grumpig entgegen. "Können wir uns auf Duzen einigen?"

"Natürlich", stimmt Angela zu.

Léonide schaut sie dabei nur flüchtig an und holt ohne Umschweife das
Geschenk aus der braunen Papiertüte. Es ist eine Kräuterteemischung. "Das
wird bei uns schon alle."

"Das freut mich!" Angela setzt sich Luna gegenüber auf den freien Platz.

Sie reden noch ein wenig darüber, wer hier welchen Tee trinkt, bis
Luna die Quiche anschneidet und verteilt.

Sie riecht immer noch gut, aber Marcin wirkt etwas unglücklich. "Ich habe
mit Paolo videotelefoniert, als ich sie gemacht habe. Aber
sie hat trotzdem einfach nicht die gleiche Konsistenz wie bei
ihm", sagt er.

"Ihr habt videotelefoniert?", fragt Angela. Sie ist gleichzeitig
traurig und glücklich dabei. "Bei mir meldet er sich im Moment
kaum mehr."

"Oh", macht Marcin.

"Du bist Paolos Elter?", fragt Phillipp.

"Mutter", konkretisiert Angela. "Also, auch Elter, klar. Ich
hätte auch einfach 'ja' sagen können."

"Ich fühle mich mit dem spezifischen Begriff 'Mutter' auch
so wohl, dass ich das Bedürfnis kenne, dass er benutzt
wird." Phillipp versucht einen motivierenden Tonfall, aber sie
ist sich nie sicher, ob sie es hinbekommt, ihre Emotionen so zu
vermimiken, dass sie sinnvoll gelesen werden können. Dann besinnt
sie sich, über welches Thema sie gerade reden und was das für
Paolos Mutter bedeuten mag. "Ist dir überhaupt wohl dabei, über
Paolo zu sprechen? Ich kann mir vorstellen, dass das Thema
recht belastend für dich ist. Oder dass wir in dem Zusammenhang
vielleicht Unterthemen aussparen sollten. Wie ist das?"

Angelas Blick wandert weiter, fast mechanisch, bis er auf Luna
haften bleibt. Luna lächelt. Angela nicht. Irgendwann
seufzt sie tief und steht auf. "Ich kann das nicht, es tut
mir leid", sagt sie. "Du hast recht, Luna, ich habe ein Bedürfnis
dazu, mich mit anderen zu treffen, und ich habe das viel zu
kurz kommen lassen, weil ich eine Familie zu versorgen hatte
und selbst genug, womit ich nicht klar komme. Aber ich kann
nicht mit dir an einem Tisch. Es tut mir leid."

"Setz dich wieder", bittet Luna. (Eigentlich ist es ein
Befehl, aber sie sagt es so sanft, dass es dadurch eher
eine Bitte ist.) Sie steht ihrerseits auf. "Lass mich an deiner
Stelle gehen. Ich verstehe dich."

Einen Moment stehen sie sich einfach gegenüber und sehen
sich an.

"Es mag sehr herzlos von mir sein, aber ich möchte trotzdem
anmerken, dass dein Kind Mordabsichten gegen Luna hatte", mischt
sich nun Lauden Léonide von Horstenfels ein.

Angela atmet noch einmal tief ein und aus, als wäre es
Schwerstarbeit, dann setzt sie sich wieder.

"Ich finde nicht, dass das mein Verhalten in irgendeiner
Form entschuldigt", merkt Luna an.

"Das nicht", sagt Angela. "Aber du bist, als du ihn gefesselt in
deiner Hütte zurückgelassen hast", -- Angelas Sprechfluss stolpert
dabei ein wenig --, "zu mir gekommen und hast mir erzählt, dass er
lebt. Und dass er nicht sterben wird."

"Es sei denn, Sonja hätte ihn nicht gerettet", murmelt Marcin.

Angela schüttelt den Kopf. "Luna hätte das nicht zugelassen. Das
hat sie mir anvertraut." Sie wendet sich Luna zu. "Setz dich
wieder."

Luna setzt sich sehr zögerlich und blickt dabei skeptisch drein.

"Du hast mein Kind gefoltert", fährt Angela fort. "Aber Paolo
hat mir auch bei seinem ersten Telefonat mit mir erklärt, dass
es überraschend keine traumatischen Auswirkungen bei ihm
haben wird, meint seine Therapeutin. Und er auch. Er hat vor allem mit
anderen psychischen Problemen zu ringen, die schon vorher da waren."

"Ich soll euch alle von Paolo grüßen", fällt Marcin ein. "Ausdrücklich
alle hier."

"Mein Kind ist keine 18. Und ich halte es schon für etwas anderes,
wenn er dir was antun möchte, als wenn du ihm was antun möchtest", sagt
Angela. Sie weiß nicht genau, worauf sie eigentlich hinaus will. Aber
irgendwie muss sie gerade Dinge loswerden.

"Das stimmt", bestätigt Luna. "Ich mag vielleicht spitzfindig
korrigieren, dass ich ihm nichts antun *wollte*. Ich wollte es
nur nicht doll genug nicht, um das Spiel anders weiterzuspielen, in
dem ich steckte. Mein Verhalten ist dennoch oder auch vielleicht
sogar deswegen ohne Frage viel kritischer zu bewerten als Paolos."

"Du redest über das Foltern von Kindern und Jugendlichen als Spiel?", fragt
Phillipp. Ihr Ton ist sehr missbilligend.

"Ungern", erwidert Luna. Sonst sagt sie nichts dazu. Es gibt Dinge,
bei denen es nicht möglich ist, sich dafür zu verteidigen. Sie
ist grausam gewesen. Es hat, was Grausamkeit betrifft, auch
noch viel Spielraum nach oben gegeben, aber
auch einigen nach unten. Luna bereut es trotzdem nicht. Auch wenn
es ihr leid um Angela tut.

"Paolo hat die Welt verbessern wollen", steigt Angela wieder
ein. "Indem er ein Vampir tötet. Oder irgendetwas großes
Held\*innenhaftes macht. Dazu hat er viele Grenzen
überschritten."

"Nichts rechtfertigt, zu foltern", erwidert Phillipp.

"Außer Konsens", sagt Kendra.

Phillip schaut ihr Kind fragend an. Kendra blickt weiter zu Luna.

Luna nickt. "Ja, Kendra hat mich mit meinem Einverständnis gefoltert. Das
ist schon okay."

"Aber Paolo war nicht einverstanden, oder?", fragt Phillipp.

Luna lächelt. "Nein. und ich hätte es auch nicht für ihn getan, hätte
er mich darum gebeten", fügt sie hinzu. "Ich mag es, wie du so klar
sagst, dass ich grausam bin und auf welche Art. Es passiert mir
zu oft, dass ich verteidigt werde."

"Es finden dich mehrere von uns sympathisch und wollen das wohl", überlegt
Léonide.

Luna nickt.

"Warum?", fragt Phillipp.

"Arx!", macht Kendra. "Luna verteidigt einen Wald. Ja, mit dem
Konzept habe ich so meine Probleme, aber wenn niemand in den
Wald kommt, tut sie einfach niemandem was. Die Sache mit dem Wald ist
bekannt. Ansonsten ist sie interessant und neugierig, hört gut zu,
ist verständnisvoll."

Angela nickt. "Verständnisvoll ist sie. Und ich glaube, ein bisschen
auch, gerade weil man zu ihr mit Lebensmüdigkeit kommen kann und sie",
Angela zögert, "weil sie dafür Raum gibt. Weil ich keine Angst haben
musste, dass es in dem Gespräch darum geht, dass ich mir auf keinen
Fall was antun soll, sondern ich einfach erstmal eine Person zum Zuhören
hatte."

Angela merkt, wie sich alle Blicke erst auf sie richten und einige
davon sich anschließend unauffällig woanders hin verlieren. Angela
nimmt sich die Gabel in die Hand und beginnt zu essen. Bis jetzt
hat das noch niemand getan. Wenn schon awkward, dann richtig, denkt
sie. "Marcin, die Konsistenz stimmt vielleicht nicht, aber die ist
richtig gut!"

Marcin lächelt. "Danke!", sagt er. Einen Moment später fügt
er hinzu: "Ich war auch viel bei Luna, um mit ihr über Themen
im Zusammenhang mit Tod und Trauer zu reden, die sonst niemand
verstanden hat. Ich weiß, dass sie meine Schwester getötet hat. Und
dass es schön war. Für beide."

Natürlich entsteht Marcins Drache,
aber er ist größer als sonst. Er legt seine Flügel um Marcins
Schultern und seinen Kopf an Marcins Hals.

"Manchmal, denke ich, dass so etwas viel eher die Welt rettet", meint
Kendra. "Wirklich zuhören. Loslassen davon, dass wir Dinge bei anderen
nicht wollen, die weh tun, und zulassen, dass sie halt da sind."

"Ich fühle mich bei dir auch sehr wohl, weil du meine Ungeheuer
magst", murmelt Marcin.

"Eine Wahlfamilie gründen", schlägt Léonide vor. "Wenn ihr mögt
zumindest."

"Auch ich, obwohl ich Schwierigkeiten mit Luna habe?", fragt
Angela.

"Auch du", sagt Léonide. "Ich glaube, für diese Schwierigkeiten
haben wir ja auch alle Verständnis."

Womit Léonide recht hat. Besonders Phillipp kommt damit noch nicht
so klar. Sie denkt darüber nach, dass sie grausames Verhalten von
Personen auch ermöglichen, wenn sie es akzeptieren. Aber immerhin
steht keine Rechtfertigung im Raum. Es ist alles sehr merkwürdig. Ihr
Kopf hat Schwierigkeiten, sich darumherum zu stülpen.

Sie speisen gemütlich. Dann gehen sie ins Wohnzimmer, wo ihnen
Marcin am Kurzflügel vorspielt. Sie genießen die Musik und die
Ungeheuer. Dann unterhalten sie sich bei Tee. Marcin ist in
Kendras Arme gekuschelt. Phillipp macht sich Gedanken, ob Angela
gern kuschelt, aber sie weiß auch überhaupt nicht, ob sie sich
bei ihr nicht bisher eher vollkommen unbeliebt gemacht hat. Es
wird sich zeigen.

Als der Abend graut und sie ein Gesellschaftsspiel ausgepackt haben,
verabschiedet Luna sich. "Es war ein schöner Urlaub mit euch. Für
eine Woche im Dorf zu schlafen, war eine neue Erfahrung für mich, die
ich nicht missen will, auch wenn sie sehr anstrengend war", sagt
sie. "Ich glaube, ich werde hier nicht wohnen bleiben. Für mich
ist und bleibt der Wald mein Daheim. Aber ich komme gern vorbei, solange
ich willkommen bin."

Der Wald. Der geliebte Geisterwald, durch den der Nebel wandert. Das
ist auch eine Welt, die es zu retten galt und gilt.
