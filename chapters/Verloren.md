\BefehlVorKapitelIBDM{Verloren.png}{Ein Schneefuchs.}{Schuldgefühle, Minderwertigkeitsgefühle, Gaslighting?.}

Verloren
========

\Beitext{Paolo}

"Ich möchte dich noch einmal dringend warnen", hat Luna zum
Abschied gesagt. "Dass ich dich von mir aus nicht töten möchte,
heißt nicht, dass ich dich verschonen werde, wenn du in meinen
Wald kommst."

---

Was Angela gesagt hat, zupft viel unbehaglicher an ihm: "Du darfst
dich alleingelassen fühlen. Dabei ist es egal, ob du Schuld daran bist, dass
dich die anderen allein lassen. Das Gefühl ist in jedem Fall da und niemand
verdient dieses Gefühl."

Aber er ist eben doch Schuld, das weiß er. Er muss besser werden. Aber
er hat das Gefühl, je mehr er versucht, besser zu werden, desto schlechter
handelt er.

---

Als er bei der Bank am Geisterwald ankommt, ist Sonja schon dort. Sie
sitzt nachdenklich auf dem kühlen Holz, den Blick aufs dunkle Meer gerichtet, lauscht
der Brandung und dem Flüstern aus den Bäumen. Sie sieht schön aus mit
ihrem im Wind wehenden Haar und Kleid, das auch aus Haar besteht.

Paolo setzt sich neben sie. Sie blickt sich kurz lächelnd zu ihm um, aber
dann schaut sie direkt wieder aufs Meer. Als würde etwas ihre Gedanken
beschäftigen und nicht loslassen.

"Hallo Sonja", sagt Paolo.

"Hallo Paolo." Sonjas Stimme ist weich und warm, ein wenig tief vibrierend, obwohl
sie eigentlich eher hoch ist, wie immer.

Paolo kommt auf die Idee, dass er die Stimme so mag, weil sie Marcins
ein wenig ähnlich ist. Aber daran will er gerade nicht denken. Er atmet
tief ein und aus. "Luna behauptet, du würdest mich manipulieren. Du würdest
wollen, dass ich den Wald betrete, damit sie mich umbringt."

Luna schnaubt sachte in ihre Traurigkeit hinein. "Sieht ihr ähnlich." Vielleicht
sagt sie das eher zu sich selbst. "Aber halt!", bremst sie sich. "Ich habe
sicher gewisse Reflexe dazu, meine Meinung über sie zu äußern. Das kann natürlich
in irgendeiner Form manipulieren. Was meinst du denn dazu? Glaubst du, dass ich
dich manipuliere?"

Paolo hat es vorhin bei Luna schon geleugnet, aber gerade fühlt er sich
freier, noch einmal darüber nachzudenken. Er schüttelt schließlich den
Kopf. "Ich bin generell in letzter Zeit verunsichert. Zum Beispiel, weil Kendra
mir in manchen Punkten erklärt hat, dass es besser ist, wenn ich
an manche Fragen noch logischer rangehe und noch mehr Faktoren mit
einbeziehe."

"Hast du es in meinem Fall versucht?", fragt Sonja.

"Ja", gesteht Paolo. "Aber Luna hätte Motive, zu behaupten, du würdest
mich manipulieren. Wenn sie wirklich verstanden hat, dass ich eine Chance
habe, sie zu töten, dann wird sie ja versuchen, mich vom Wald fern zu
halten."

Sonja nickt einfach und summt nachdenklich dazu.

"Ich dachte mal, dass sie selbst sterben möchte, weil sie mit Kendra ein
lockerflockiges Gespräch dazu hatte", fährt Paolo fort. "Kendra will sie
umbringen, das hatte ich ja schon erzählt. Aber Luna kann ja genau bestimmen,
wie Kendra es probiert. Und wenn sie ihr nichts von der Mondklinge sagt, schadet
ihr ja nicht, das Spiel mitzuspielen."

"Weiß Kendra von der Mondklinge?", fragt Sonja.

Paolo schüttelt den Kopf. "Ich bin mir sehr sicher, dass nicht", sagt
er. "Ich frage mich, ob Luna dieses Spiel nur mit Kendra spielt, weil
sie hofft, dass ich Kendra, wenn sie das gleiche Ziel wie ich hat, anvertraue, wie
durchdacht meine Pläne schon sind, und Kendra es ihr dann weitersagt."

"Meinst du?", fragt Sonja. Aber obwohl die Frage skeptisch klingt,
schwingt wieder das Verständnis darin mit, durch das sich Paolo so
ernst genommen fühlt.

Nein, Sonja manipuliert ihn nicht. Sonja hört doch bloß zu. Keine der
Hypothesen kommt von Sonja. Alle hat Paolo aufgestellt und Sonja hat
ihn bestätigt, aber auch nicht immer. Wenn er zu viel in etwas gesehen
hat, hat sie ihn auch mal gebremst.

"Ich bin nicht sicher", murmelt Paolo. "Ich bin so verunsichert
von allem!"

"Hilft es dir, auf dein Bauchgefühl zu hören?", fragt Sonja. "Kommt
dir Luna wie eine Person vor, der du trauen solltest?"

Paolo schüttelt den Kopf. "Aber du gerade auch nicht mehr", gibt
er schweren Herzens zu. "Es tut mir leid. Dass Luna es hinbekommen
hat, dass ich an dir zweifle, obwohl du mir noch nie gesagt hast,
was ich tun soll, und einfach nur an mich geglaubt hast."

Sonja sieht ihn an und lächelt warm. "Es braucht dir nicht leid
zu tun", sagt sie. "Es ist auch nicht ungesund, mal an einer
Person zu zweifeln. Das darfst du. Du darfst immer an mir zweifeln
und mir das auch sagen, wenn du willst. Ich werde damit schon
fertig."

---

Solche Kontraste, diese zwei Personen, denkt Paolo. Die Zweifel gehen
nicht völlig weg, als er sich später wieder auf den Heimweg macht. Aber
auf der anderen Seite: Luna muss ihn nicht töten. Wenn sie Sonja wirklich
für manipulativ hielte und ihn schützen wollte, hätte sie ihm am Ende
des gemeinsamen Abends nicht drohen müssen.
