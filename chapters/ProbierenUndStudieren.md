\BefehlVorKapitelIBDM{ProbierenUndStudieren.png}{Ein Wasserhahn, etwas altmodisch, auch mit den Schrauben für warmes und kaltes Wasser, die jeweils vier Nupsis haben. Ein Tropfen fällt vom Wasserhahn herab.}{Vielleicht toxische Beziehung.}

Probieren und Studieren
=======================

\Beitext{Paolo und Kendra}

"Kann ich dich mit der Küche allein lassen und schon losgehen?", fragt
Marcin.

Paolo betrachtet das in der Spüle im Wasser einweichende Geschirr. Es
ist noch früh am Morgen und sein Geduldsfaden ist sehr dünn. Er will es nicht
an Marcin auslassen. Er unterdrückt ein Seufzen. Wahrscheinlich sollte er
einfach zustimmen und sich beim Spülen allein abreagieren.

"Alternativ übernehme ich das Spülen", bietet Marcin an. "Kendra kennt den
Weg zur Schule noch nicht. Ich fände gut, dass du sie dann begleitest,
wenn sie mag. Aber du weißt auch, dass ich das Geschirr nie so wegräume, wie
es richtig wäre."

Ein Haufen Überlegungen drängen sich gleichzeitig durch Paolos Gedanken, der
lauteste davon: *Du konstrollierst Marcin*. Paolo ist gerade nicht in der
Lage, sinnvoll zu reflektieren. Gerade weil er sauer auf Kendra ist. Und
weil er weiß, dass sich manchmal so ein Vorwurf in ihn einbrennt und er sich fragt,
ob da was dran ist, nur weil er dauernd an sich selbst zweifelt, und nicht,
weil eben wirklich was dran wäre.

Für Marcin wäre es wohl sicherer, hier zu spülen, während Paolo Kendra
begleitet. Zumal Paolo Kendra nicht einschätzen kann. Vielleicht denkt sie
nach diesem Gespräch so etwas wie, jetzt erst recht. Vielleicht spricht
sie Marcin gerade deshalb auf den Ort mit der Bank an, weil Paolo
gesagt hat, sie solle es lassen. Oder sie informiert Marcin über das ganze
Gespräch mit Paolo, das sie heute morgen geführt haben.

Einen Moment schnürt Angst Paolo die Kehle zu. Angst, dass sie es tut und
Marcin ausrastet. Und Paolo dann nicht in der Nähe ist. Aber, -- das muss
sich Paolo eingestehen --, auch Angst, dass Marcin auf ihn sauer sein
könnte. Es fühlt sich beschissen für Paolo an, wenn andere ihm gegenüber
negative Gefühle haben. Das ist sicher normal. Wer kann mit so etwas schon
gut umgehen? Marcin wäre natürlich mit Grund sauer. Aber welche Wahl hätte
Paolo gehabt? Eigentlich hat er vorher gründlich abgewägt.

*Bevor er Marcins Grenzen überschritten hat*, dröhnt es durch seine
Gedanken, ob es nun stimmt oder nicht.

Paolo seufzt doch und dreht sich zu Marcin um, tritt näher an ihn heran,
legt die Hände an seine Oberarme. Weich. "Geh du ruhig", sagt er. Er
küsst Marcin auf die zarten Wangen. "Pass auf dich auf", flüstert
Paolo. "Kendra ist nicht unbedingt sehr feinfühlig." Er küsst
Marcins Nasenspitze und erntet ein niedliches Kichern. "Und wenn irgendwas
ist, ruf mich an, ja? Wenn ich dann die Person bin, an die du dich wenden
magst. Ich bin dann so schnell da, wie ich kann."

Marcin umarmt ihn. "Mach dir nicht so viele Sorgen." Dann löst er sich schon
wieder.

Paolo schaut die weichen Lippen an, die er gern zum Abschied küssen würde. Aber
vielleicht ist das auch albern. Und ja, vielleicht sorgt er sich zu viel. Es
ist für ihn schwer aushaltbar, als Marcin mit Kendra das Haus verlässt, sie einfach
gehen zu lassen. Er versucht, nicht sofort sein Handy zu checken, ob es auch
wirklich in der Nähe und nicht auf lautlos ist. Dann tut er es doch, denn davon
bekommt immerhin niemand etwas mit, es kann also niemanden stören.

Er macht sich zügig ans Spülen. So einiges bohrt in ihm, was Kendra gesagt
hat. Nicht nur, dass er Marcin kontrollieren würde. Es bohrt auch, dass sie
ihm gesagt hat, er möge doch logisch an die Sache rangehen. Er hat es ihr
zuerst gesagt. Und vielleicht ist es einfach, dass sie den Vorwurf auch
nicht auf sich sitzen lassen wollte und einem 'Ätsch, bist du doch selber'-Reflex
anheim gefallen ist. Aber der Vorwurf ist so ungerecht und bohrt massiv! Wenn
er nicht über seinen Gefühlen stehen und auf Basis von Logik entscheiden
würde, was das Richtige zu tun wäre, würde er sich Marcin gegenüber
doch einfach gehen lassen. All die Entscheidungen, dass er Kendra gebeten hat,
Marcin vor bestimmten Informationen und damit Gefühlen zu schützen, fällt er
doch nicht für sich oder für sein Ego, sondern weil ihm logisch klar ist, dass
es das Richtige sein müsste. Und vielleicht irrt er sich darin. Aber er
tut es, weil er versucht, logisch zu denken, und nicht weil er egoistisch
einfach seinen Gefühlen nachgibt. In seinen Entscheidungen steckt so
viel Verzicht auf eigenes Wohlergehen. Manchmal wünscht Paolo sich, dass
es jemand sehen würde. Eine Person in seinem Umfeld, nicht nur eine
außenstehende wie Sonja. Aber er ist froh, dass es Sonja gibt.

Vielleicht entscheidet er nicht immer richtig. Vielleicht hat Kendra
in manchen Punkten nicht ganz unrecht. Das möchte er sortieren. Er hofft,
dass Sonja bald wieder für ihn Zeit hat.

---

Kendra geht still neben Marcin her. Sie unterdrückt den Reflex, das Gespräch
mit Paolo direkt einfach offenzulegen. Weil es ein Reflex ist. Sie möchte
es durchdenken und bewusst entscheiden, was sie tun möchte. Ihre
Fragen dazu: Schätzt sie die Lage wirklich besser
ein als Paolo, der Marcin schon länger kennt? Und: Wenn sie darüber redet,
stellt sie Paolo Marcin gegenüber in ein schlechtes Licht, weil Paolo, vorsichtig
gesagt, etwas overprotective ist. Ist es die Sache wert? Lässt sich das nicht
auch anders lösen? Sollte sie Paolo nicht die Möglichkeit geben, es selbst
gerade zu rücken, zumindest wenn sich herausstellt, dass sie Recht hat?

Paolo hat sehr ablehnend gewirkt, und wütend, als er heute Morgen
aus ihrem Zimmer gegangen ist. Aber manchmal
braucht eine Erkenntnis auch ein wenig Zeit, um einzusickern. Kendra weiß
von anderen (nicht von sich selbst), dass sie oft einen Widerstand haben, was
das Einsehen betrifft, dass sie falsch liegen könnten. Kendra hat nativ
wenig Geduld dafür, aber sie hat sich antrainiert, sich Verständnis dafür
abzuringen. Also erzählt sie Marcin nicht jetzt von diesem Gespräch. Und
sie spricht auch die Klippe nicht jetzt an.

An der Schule angekommen, holt Paolo sie ein. Der Schulunterricht fühlt sich
etwas anders an als daheim. Das kann auch an neuen Lehrkräften und neuer
Umgebung liegen. Es ist eine gemischte Schule für Menschen mit und ohne
Magie, und die mit Magie erhalten täglich eine zusätzliche Stunde, in der sie
aus allen Klassen zusammengewürfelt werden. Und darüber hinaus: Kendra ist
erstaunt, weil sie sich auch mit Leuten in einem Raum sammelt, die schon über
40 sein mögen. Weil Kendra neu ist, gibt es eine Vorstellungsrunde. Sie soll
erklären, was sie mit ihren magischen Fähigkeiten bisher erlebt hat.

"Ich habe mich wahrscheinlich in mindestens zwei Nächten mitten im
Schlaf auf eine Bank und zurück ins Bett teleportiert", berichtet
sie knapp.

"Nicht wahrscheinlich, sondern absolut sicher", korrigiert Paolo. Er
fügt für die anderen erklärend hinzu: "Ich habe sie in einer der Nächte
dort getroffen und ihr bei der Gelegenheit diese Schule empfohlen."

"Was hast du nachts auf der Bank gemacht?", fragt Marcin leise. Es
wirkt etwas unwillkührlich auf Kendra.

Paolo blickt Kendra an. "Ich dachte, du hast darüber mit Marcin schon
geredet?"

Nicht alle Details, denkt Kendra. Aber sie brechen das Gespräch ab, weil
es nichts im Unterricht zu suchen hat.

---

Paolo ist spürbar nicht angetan von Kendra. Sie kann es ein bisschen
verstehen, aber fände schon ganz gut, wenn sie sich mehr Mühe geben
würden. Magie-Unterricht stellt sich als sehr experimentell heraus. Eigentlich
hat niemand Ahnung, wie man das sinnvoll unterrichtet. Ihre Lehrkraft
unterrichtet eigentlich Mathematik und gehört zufällig zu jenen mit magischer
Fähigkeit, unterrichtet dieses Fach also zusätzlich, weil sie zufällig Lehrkraft
ist und damit am ehesten ein Konzept vom Unterrichten haben könnte.

Die Lehrkraft schlägt Kendra vor, den Ort mit der Bank aufzusuchen, um
zu schauen, ob das die Teleportationsmagie triggern könnte. Kendra findet
die Idee nicht verkehrt und lässt sich den Weg erklären. Weder von
Paolo noch von Marcin. Die Magielehrkraft erkennt den Ort nach ihrer
Beschreibung und gibt ihr die Information, die sie braucht.

Kendra wandert also dreimal in der Woche nach der Schule langsam mit
ihrem Stock dorthin und genießt die Aussicht. Manchmal früher, manchmal
später. Selten trifft sie Paolo dort, der sich dann stets rasch
entfernt. Sie findet durchaus interessant, dass Paolo den Ort so
oft aufsucht, der ja angeblich für Marcin emotionale Relevanz hat. Es
ist ein besonderer Ort, auch ein wenig unheimlich, so dicht neben dem
Wald, über den Kendra erzählt wurde, dass niemand ihn betreten
könne, ohne zu sterben. Eine tödliche Gefahr lauere darin. Vielleicht
ist es das, was Paolo anzieht. Sie zieht es jedenfalls an.
