\BefehlVorKapitelIBDM{Silberblick.png}{Das gleiche Bild wie beim Kapitel Silberblick: Links eine Birke, rechts eine Bank, beides auf einer Klippe mit Blick aufs Meer. Wolken sind am Himmel. Schemenhaft sind weitere Bäume hinter der Birke.}{Folter erwähnt.}

Epilog {.epilog .unnumbered}
======

\Beitext{Kendra}

*Ein halbes Jahr später.*

An der Klippe nahe der Bank am Geisterwald. Wind weht, aber es
ist warm und trocken. Ende des Sommers oder Anfang des Herbsts. Luna
ist schon gegangen. Paolo, Kendra und Marcin haben es sich auf der
Decke gemütlich gemacht. Sie liegen dicht aneinander gekuschelt, Marcin in der
Mitte. Kendra hat sich mit Paolo abgefunden. Sie wünschte, es wäre
mehr als das. Dann könnte sie vielleicht in der Mitte liegen, wo es
noch wärmer ist.

Marcin trägt fingerlose Handschuhe und streichelt ihr sachte mit den
überraschend warmen Fingern über den Handrücken. (Auf der anderen
Seite tut er dasselbe mit Paolos Hand). Kendra genießt die
Berührung. Sie döst weg, und als sie aufwacht, sind sie nicht mehr da.

Überhaupt ist alles anders. Sie öffnet die Augen und blickt sich
um. Ihr stockt der Atem. Sie weiß nicht, ob sie begeistert oder
entgeistert sein soll.

Sie kennt das Zimmer, in dem sie liegt, auch wenn es völlig anders
eingerichtet ist als damals. Nur das Bett steht an der selben Stelle
wie ihres früher. Es ist das Zimmer, in dem sie gewohnt hat, bevor sie
ins Dorf am Geisterwald gezogen ist. Die Wohnung steht nicht leer. Sie ist
natürlich neu vermietet worden. Vorsichtig richtet sich Kendra auf.

Eigentlich ist es, worauf sie damals im Magie-Unterricht gehofft
haben. Dass sie, auf der Bank am Geisterwald sitzend, sich irgendwann
in ihre alte Wohnung zurückteleportieren würde. Aber damals hat ihre
Mutter noch hier gewohnt. Nun ist sie in einer fremden Wohnung. In einem
fremden Bett. In dem sie immerhin alleine ist. Aus dem Nachbarzimmer
dringt leises Schnarchen.

Soll sie versuchen, die Wohnung zu verlassen? Soll sie liegen
bleiben und hoffen, dass sie sich wieder zur Bank teleportiert? In
ihren bisherigen Teleportationseskapaden hat sie sich dabei
immer halb wie in einem alben realistischen Traum gefühlt. Das
Gefühl hat sie auch jetzt, aber mehr Wissen. Sie legt sich
zurück auf den Rücken unter die warme Decke, falls sie nicht
mehr Teleportieren kann, sobald sie von der Realität der Lage
überzeugt ist.

Was für eine unangenehme Situation. Aber Kendra ist gewohnt, ein
Vampir zu foltern. Sie bringt so schnell nichts aus dem Konzept. Sie
atmet tief ein und aus, stellt sich vor, was sie tun muss, um wieder
zurückzufahren, oder wie ihre damalige einzige Schulfreundin gucken
würde, wenn sie mitten in der Nacht bei ihr mit dieser Geschichte
klingeln würde, -- und schläft bei den Gedanken ein.
