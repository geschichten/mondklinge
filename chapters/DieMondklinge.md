\BefehlVorKapitelIBDM{DieMondklinge.png}{Ein Pilz mit Stiel und Hut. Die Krempe ist markant und dunkel. Der Hut hat fast die Form eines abgerundeten Kegels.}{Bedrohung durch Tod, Auseinandersetzung mit Tod, Leichen, Suizidalität - erwähnt, Fixierung, Ausgeliefert sein, Blutsaugen.}

Die Mondklinge
==============

\Beitext{Luna und Paolo}

Natürlich ist Paolo im Wald. Luna weiß es, sobald sie den Waldboden mit ihren Füßen
berührt. Es ist ein alter Wald. Einer, in dem die Bäume noch über große Strecken miteinander
kommunizieren. Und der Farn und die Pilze, vor allem die Pilze. Sie sind oft besonders
gesprächig, aber manchmal verfangen sich ihre Gedanken auch in Kreisen. Luna seufzt
innerlich. Ihre eigenen Gedanken verdröseln sich zu einem überraschend komplexen
Knoten. Sonja hat seit jeher Schwierigkeiten damit gehabt, damit zurechtzukommen,
Luna unterlegen zu sein. Dabei geht es vor allem darum, dass Luna physisch
stärker ist als sie. Und vielleicht auch darum, dass Luna sich nicht so leicht
verunsichern lässt und mit einer Selbstverständlichkeit sich einen Wald geclaimt
hat, die Sonja einfach nicht hat. Nun stellt sich Luna die Frage, ob Sonja ihr
im Manipulieren überlegen ist. Das ist allerdings schwer zu messen, weil Luna es nicht
drauf anlegt. Hätte sie es tun sollen? Hätte sie versuchen sollen, Paolo im Vorfeld
eher mit Tricks als mit Androhung von Gewalt aus dem Wald zu halten?

Sie beeilt sich nicht. Sie weiß, was sie tun wird. Sie mag es nicht. Aber manchmal
gibt es eben nur schlechte Optionen und dann muss daraus die beste gewählt werden.
Sie mag es nicht, und doch fühlt sie wieder dieses angenehme Ziepen des Ungeheuerlichen
in sich bei dem Gedanken, über ein fremdes Leben bestimmen zu können. Damit
spielen zu können.

---

Im Haus nahe der Klippe im Geisterwald. Nebel wabert um es herum. Es ist zugewuchert, als
wäre es jahrelang nicht bewohnt gewesen, aber innen ist alles sauber und die Einrichtung
ist unversehrt. Paolo kommt sich hier sehr verkehrt vor. Als würde er in etwas sehr
Privates eindringen. Es ist ein verwinkeltes, selbstgebautes Haus aus Holz. Alle, die
es bisher erwähnt haben, haben es immer Hütte genannt, aber es ist definitiv mehr
als eine Hütte. Das einzige,
was es unwohnlich macht, ist die Temperatur. Es ist so kalt wie außerhalb davon. Beim
Herumleuchten mit seiner Taschenlampe, die immerhin sehr hell ist, findet Paolo Kerzenhalter
an der Wand, mit cremefarbenen, verschieden weit abgebrannten Kerzen darin, einige dazwischen
auch bunt. Es hängen außerdem Bilder an den Wänden,
mit Kohle oder Kohlestiften gezeichnete Bilder in schwarzen Bilderrahmen, die so viel
verschiedenes Persönliches zeigen. Eine Bilderreihe zeigt die gleiche Person aber
in verschiedenen Altern, etwa alle 10 Jahre, vom Kleinkind bis zu einer alten, runzlichen
Person. Auf dem letzten Bild ist die Person tot. Überhaupt gibt es viele Bilder von
Leichen. Und von toten Bäumen, toten Tieren, zerfledderten Schmetterlingen. Es
ist ein Haus des Todes. Den Tod so zu feiern, denkt Paolo, das ist
schon, wie er zu Sonja gesagt hat, eigentlich nicht gesund für diese Welt.

Paolo bewegt sich vorsichtig die schmale, verwinkelte Treppe hinauf. Oben wird es
sicher noch persönlicher, aber er rechnet damit, dass er die Mondklinge am ehesten hier
finden wird. Auf halber Treppe geht eine schmale Tür ab, die Paolo fast übersehen
hätte, weil sich ihre Maserung kaum von der der Wand um sie herum unterscheidet. Er
muss sich ein wenig bücken, um hineinzutreten. Dies wird es sein, denkt er.

Es ist ein Raum, mit einem einzigen kleinen Fenster. Er ist niedrig. Paolo kann gerade
so aufrecht stehen. Und er ist voller interessanter Instrumente, die Paolo am
ehesten Folter zugeordnet hätte, aber er erkennt keines wieder. Weder aus Geschichtsmuseen
noch aus Horrorfilmen. Er nähert sich einer Art Liege, aber bevor er sie anfasst, blickt
er sich vorsichtshalber noch einmal um. Für einen Augenblick glaubt er, dass Luna im
Eingang zum Raum steht, klar und deutlich. Aber als er seine Taschenlampe genau dorthin
richtet, ist da nichts. Er weiß, dass diese Vampirkreatur jeden Augenblick auftauchen
kann. Deshalb ist es auch naheliegend, dass sein Gehirn ihm für einen Moment vormacht,
dass das jetzt der Fall wäre. Er wendet sich wieder der Liege zu. Sein Puls rast. Er
stellt sich vor, dass Luna ihn bis in ihr Zimmer in der Villa des Laudens von Horstenfels
hören kann. Er versucht kontrolliert zu atmen, um sich zu beruhigen.

Luna lehnt sich hinter dem Schrank an die Wand, dem sie gerade die Mondklinge entnommen
hat. Paolo hat sie dabei kurz gesehen. Aber er tut so, als ob das nicht der Fall
wäre? Nein. Er macht sich vor, dass er es sich nur eingebildet hat, weil sie so schnell
und lautlos wieder weg gewesen ist. Wow. Einfach, wow. Er weiß doch, wie schnell sie ist. "Du bist
nicht allein", sagt sie, unsachlich. "Und in meinem Wald." Sie riecht Paolos Angst, hört
seinen Herzschlag flattern. Und offensichtlich hört sie seine Schritte, die er versucht, so
leise wie möglich Richtung Ausgang zu lenken. Immerhin das.

Einen Moment überlegt sie, ob sie ihn doch entkommen lassen soll, aber dann ist sie
doch plötzlich bei ihm, als er die Tür erreicht, greift ihm am Handgelenk, das sie
auf seinem Rücken verdreht, um ihn zurück in den Raum zu schubsen. Sie spürt seinen
Angstschweiß unter ihren Fingern. Riecht seine Lebendigkeit. Sie lässt ihn los und
schließt die Tür hinter sich. Dann, gerade so schnell, dass die Kerzen vom Wind nicht
ausgehen, entzündet sie alle 12 Kerzen am Kronleuchter. Sie ist zu langsam, denkt
sie grinsend. Paolo ist wieder mit der Hand an der Tür und sucht nach der
versteckten Möglichkeit, sie von innen zu öffnen, als sie
fertig ist. Luna bremst die Bewegung des Kronleuchters ab und drängt sich im nächsten
Moment zwischen Tür und Paolo. Sie greift Paolos Kinn mit der Hand und schiebt in zurück
in den Raum. Wieder lässt sie ihn los. Er tritt drei Schritte rückwärts von
ihr weg. "Vorsicht, dass du dir nicht die Haare am Leuchter entzündest", rät Luna.

Paolo bleibt stehen und zittert. "Warum lässt du mich wieder los?", fragt er.

Luna zuckt mit den Schultern. "Damit du dich freier bewegen kannst", sagt sie
halb überzeugt. "Nicht komplett frei, obviously. Aber vielleicht möchtest
du die Liege ja weiter untersuchen. Möchtest du darauf Platz nehmen und sie
ausprobieren?"

Paolo blickt sich einen Moment um, aber fixiert anschließend Luna wieder. "Warum
sollte ich das tun?"

Luna zuckt noch einmal mit den Schultern. Es ist beinahe langweilig mit
Paolo. "Du warst damit beschäftigt, bevor du realisiert hast, dass ich wirklich
hier bin." Aber dieser Moment des Grauens, der sich in Paolos Haltung widerspiegelt,
hat durchaus etwas. Luna lächelt ein schmales Lächeln.

"Du spielst wohl gern mit Beute", mutmaßt Paolo.

"Schon", gibt Luna zu.

"Aber du vergisst dabei, dass das der Beute mehr Zeit gibt, einen Angriff gegen
dich zu planen oder zu entkommen!" Paolo blickt sich abermals um. Dieses Mal
etwas gründlicher, und als er sich wieder Luna zuwendet, steht diese direkt vor
ihm. Sein Herz setzt gefühlt für einen Schlag aus. Er taumelt rückwärts, aber
Luna packt ihn am Kinn.

"Nicht in den Kronleuchter stolpern", raunt sie. Sie dreht ihn am Kinn so, dass er
eher in Richtung Liege ausweicht, als sie ihn loslässt. "Und nein, das vergesse
ich nicht."

"Warum gehst du das Risiko ein, dass ich etwas gegen dich ausrichten
könnte?", fragt Paolo. Vielleicht ist es Unfug von ihm, das zu fragen. Er
hat doch keine Ahnung von solchen Situationen. Die Täter\*innen reden
lassen, um sich zu schützen, um abzulenken, heißt es. Aber vielleicht
ist nicht die optimale Strategie, ihnen Tipps zu geben. Paolo ist aber nicht
im Stande dazu, über andere Strategien nachzudenken.

"Wenn es denn ein Risiko wäre", grummelt Luna. "Dann hätte es einen etwas
höheren Spaßfaktor für mich."

"Was, wenn ich hier die eine Waffe finde, die dich töten kann?", fragt
Paolo. Vielleicht kann er so herausfinden, ob sie hier ist. Vielleicht
kann er es an Lunas Gesicht ablesen, wenn sie sich erschreckt.

"Wie unwahrscheinlich mag das sein?" Luna streicht sich das Haar zurück. Schon
wieder. Sie muss sich eine neue Spange besorgen. "Dass du eine bisher
unbekannte Waffe findest, mit der ich mich noch nicht zu töten versucht
hätte, ausgerechnet in dem Zimmer, in dem die Dinge lagern, mit
denen ich ausgiebig herumexperimentiert habe."

Nichts! Nichts ist an Lunas Mine ablesbar. Weiß sie nicht, dass die Mondklinge
sie töten kann? Weiß sie nicht, dass sie in ihrem Haus ist? Weiß sie es, aber tut
so, als wäre nichts? Vielleicht versucht sie, ihn davon wegzudrängen. Vielleicht
ist sie oben in ihrem Schlafzimmer. Oder im Schrank, der am anderen Ende
des Raums steht, als die Bank es tut, auf die sie ihn zu drängen versucht. Paolo
merkt, wie er die Luft anhält und holt zitternd Atem.

"Meinst du die Mondklinge?", fragt Luna.

Einfach sachliche Neugierde, interpretiert Paolo. Er überlegt, nicht zu
antworten, aber merkt erst verspätet, dass er längst genickt hat. Er
versucht, seine Panikreaktion durch Bedrohlichkeit auszugleichen: "Sie
ist hier im Raum, nicht wahr?" Ob das Fangfrage genug ist?

Luna nickt und lächelt. "Das ist sie." Sie macht eine einladende Geste
auf die Liege hinter Paolo. "Möchtest du dich nicht setzen?"

Paolo schüttelt energisch den Kopf. "Ich liefere mich dir doch nicht
freiwillig ans Messer!"

"An die Zähne, wenn", korrigiert Luna und zeigt ihr Gebiss mit einem
Grinsen, das nicht dazu gedacht ist, fröhlich zu wirken.

Paolo versucht, an Luna vorbeizugehen. Wenn sie weiter so mit ihm spielt,
muss er doch irgendwann Erfolg haben, zum Schrank zu kommen.

Aber in dem Moment, in dem er den Abstand zwischen sich und Liege zu vergrößern
versucht, steht Luna wieder direkt vor ihm, so plötzlich, als könnte sie sich
beamen, legt einen Finger unter sein Kinn. "Ich habe Zeit", raunt sie.

"Irgendwann wird mich jemand suchen kommen", wendet Paolo ein. Reflexartig macht
er wieder einen Schritt zurück Richtung Liege. Er realisiert, dass Luna sich
ihm nicht nähert, um ihn näher dahin zu drängen, sondern nur, um ihm den Weg
abzuschneiden, wenn er versucht, Distanz zwischen sich und Liege zu
bringen. Mit jedem Mal, das er versucht, wegzukommen, kommt er
ein Stück näher dran. Er ist also im Moment am sichersten, wenn er stehen bleibt,
wo er ist, und sich nicht umwendet.

"Nicht völlig ausgeschlossen", stimmt Luna zu. "Ich habe Kendra Bescheid gesagt,
dass ich morgen früh nicht wieder in der Villa sein werde, wenn du hier bist. Sie
könnte es weitersagen. Aber morgen ist Sonntag und sie schläft an Sonntagen oft
aus."

"Warum hast du ihr gesagt, dass ich hier bin?", fragt Paolo, einfach, um
zu reden. "Willst du, dass sie mir folgt, damit du auch sie töten kannst?"

Luna schüttelt den Kopf. "Kendra ist eine der sehr wenigen Personen, die es
über die Jahrhunderte geschafft haben, mich so zu interessieren, dass sie
sicher vor mir in meinen Wald spazieren könnte. Sie weiß es natürlich nicht."

Paolo weiß darauf nichts zu sagen. Er verharrt ruhig und lässt Luna nicht aus den
Augen. Sie macht eine weitere Geste zur Liege hin und tritt nun, auch ohne
einen Fluchtversuch seitens Paolo einen halben Schritt näher auf ihn zu.

"Wenn du so fixiert darauf bist, dass ich auf dieser Liege Platz nehme, warum
überwindest du mich nicht einfach?", fragt er. Es erschließt sich ihm nicht. "Du
bist stärker als ich. Oder hast du doch eine Schwachstelle?"

"Ich finde jeden Moment und jedes bisschen, dass sich ein Opfer ergibt, stets
sehr interessant", sagt Luna. "Aber vielleicht wird das Ganze auch ein
bisschen reizvoll für dich, wenn ich dir ein Angebot mache. Wenn du dich
aus eigener Kraft auf die Liege legst, zeige ich dir die Mondklinge."

Paolo schluckt. "Aber du fixierst mich vorher?", fragt er geistesgegenwärtig.

"Hinterher", verspricht Luna. "Was deine Chancen, dich zu wehren, ein sehr
winziges bisschen verbessert. Aber nach jeder realitätsspiegelnden Rundung bleibt die
Wahrscheinlichkeit, dass du am Ende nicht fixiert auf dieser Liege liegst,
nach wievor null."

"Du wirkst ehrlich", stellt Paolo verdattert fest. "Es wäre ohne den Zusatz
wahrscheinlicher, dass ich nachgebe."

Luna lächelt, weil sie spürt, dass sie ihn gleich so weit hat. Sie fühlt sich
innerlich aufgeregt. "Ich bemühe mich stets, ehrlich zu sein. Manchmal
lasse ich allerdings bewusst Informationen weg. Ich behaupte nicht, fair
zu sein."

"Dass dich die Mondklinge töten kann?", fragt Paolo. "Ist das Information, die
du weglässt?"

"Oder, dass sie mich nicht töten kann", neckt Luna. "Eins von beidem."

"Ich tippe auf meine Variante", sagt Paolo. Er seufzt. "Aber wenn du stets
ehrlich bist, dann frage ich doch mal direkt: Wenn ich mich freiwillig
auf die Liege lege, sind das dann meine besten Chancen, dir die Mondklinge
abzunehmen? Auch wenn sie klein sind? Richtig klein."

Luna gibt ein Geräusch zwischen Kichern und Schnauben von sich. "Ich würde
'freiwillig' korrigieren durch 'durch eigene Kraft', einfach, weil es präziser
ist. Wenn ich dich erpresse oder ködere, legst du dich da nicht allzu freiwillig
drauf", hält sie fest. "Und nach meiner Einschätzung hast du recht. Immer noch
quasi keine Chance, aber die beste, die du kriegen kannst."

"Du spielst dieses Spiel echt gut", meint Paolo. Er weiß nicht genau, warum
er das sagt. Er zögert nur noch einen Moment, dann legt er sich auf die
Liege. Er fühlt sich innerlich so wabbelig wie noch nie in seinem Leben. Nicht
einmal, als er Luna vorhin im Augenwinkel im Zimmer gesehen hat und dachte, es
wäre vorbei. Es ist etwas anderes, überwältigt zu werden, als sich selbst
auszuliefern.

Trotzdem versucht er, sich bereit zu halten. Sobald er die Mordwaffe sieht,
aufzuschnellen, sie ihr abzunehmen, auch wenn er sich dabei daran verletzt, und
zu versuchen, abzuhauen. Er hat noch die UV-Taschenlampe in der Jacke, mit der
er sie anstrahlen wird. Er steckt die Hand in die Tasche, wo sie sich befindet,
und schließt sie um den Griff, den Daumen am Schalter, als er sich auf
die Liege niederlässt.

Luna bewegt sich rasch wie ein flackernder Schatten durch den Raum, erinnert
Paolo dabei an Marcins Monster. Sie holt sich dabei einen Dreibeinigen Hocker,
auf dem sie direkt neben Paolos Oberkörper Platz nimmt. "Danke für dieses
Geschenk", sagt sie leise und warm.

Paolos Atem zittert vor Anspannung, als Luna ein silbrig glänzendes
Metallinstrument aus ihrer Kleidung hervorzieht. Es sieht nicht aus wie
eine Waffe. Und es ist nicht aus Edelmetall, sondern aus Stahl. Das fühlt
Paolo. Er kann seit einer Weile auch andere Metalle identifizieren, aber
nicht verändern.

Das ist der Moment. Paolo knipst die Lampe an und will sie aus der Tasche
ziehen, um Luna zu überraschen, aber Luna lässt ihren Zeigefinger mit dem
Fingernagel gegen das Instrument schellen und Paolo erstarrt. Er kann nichts
mehr hören außer diesen unwirklichen Ton. Sein Blut rauscht, sein Puls
rast, wie bei einem wirklich schlimmen Schreck oder wie in Extase. Sein
Sichtfeld verschwimmt. Er merkt, wie Luna das Gerät in ein dafür vorgesehenen
Platz der Liege steckt, seine Hand mit der UV-Lampe aus der Tasche zieht, deren
Licht einmal durch ihr Gesicht streicht, bevor sie zu Boden fällt. Ihr
Lichtkegel richtet sich leider nicht zufällig auf Luna sondern strahlt irgendwo
an die Wand. Luna fasst sich ins Gesicht, um mögliche Flammen zu löschen,
aber da ist schon nichts mehr.

Der Ton hält noch ein wenig an. Luna schließt die Hand- und Fußschellen, die
Oberarm- und Oberschenkelschellen um Paolo, und ist gerade damit beschäftigt, seine
Hüfte zu fixieren, als er wieder an Bewegungsfähigkeit zurückgewinnt. Aber es
ist zu spät. Mit aller Kraft wehrt er sich gegen die Fesselung, Aber die Liege
ist aus sehr stabilem Holz und die Fesseln aus weichen, aber nicht weniger stabilen
Gurten.

"Ich will nicht!", schreit Paolo. Endlich ergreift ihn die Panik.

Luna genießt den Moment. Das Zappeln. Die Verzweiflung. Die Einsicht, die in
ihr Opfer einsickert, dass es nichts mehr tun kann, dass sein Schicksal in ihrer
Macht liegt. Ganz allein in ihrer.

"Können wir handeln?", fragt Paolo. "Kann ich irgendwas geben, was du willst,
außer mein Leben, damit du mich freilässt?"

"Nein", sagt Luna schlicht. "Ich lasse dich nicht frei. Darüber verhandele
ich nicht."

Paolos Atem zittert, als Luna sich in aller Gelassenheit nach der UV-Lampe
bückt, ohne in ihren Lichtstrahl zu fassen. Sie setzt sich wieder auf den
Hocker neben Paolo, zupft sich umständlich den Handschuh von der linken
Hand und leuchtet anschließend mit der Lampe ihren linken Mittelfinger an. Es
sieht unverschämt hübsch aus, wie er entflammt. "Keine schlechte Idee, sowas
dabei zu haben", sagt sie, bevor sie die Lampe ausschaltet und auf einen Tisch
legt. Einen Tisch an der anderen Wand des Raums, aber sie tut es so schnell, wie
andere Leute brauchen, um bloß den Arm auszustrecken.

Paolo fängt an zu weinen. Aber nichts weiter passiert. Schließlich, als
er wieder einen klaren Gedanken fassen kann, fragt er: "Worauf wartest
du?"

Luna zuckt mit den Schultern. "Darauf, dass du dich ein bisschen beruhigst", sagt
sie. "Ich dachte, ich erzähle dir, wozu die Liege da ist, weil dich das vorhin
so interessiert hat."

"Okay", Paolo schnieft. "Ich kann mich ja ohnehin nicht wehren." Und in diesem
Moment fällt ihm ein, dass er wieder selbst Schuld an seiner Lage ist: Sie hat
es ihm vorhin gesagt, dass die Mondklinge ihr nichts anhaben kann. Indirekt mit
diesem Satz, dass es so unwahrscheinlich wäre, dass sie in diesem Raum etwas
töten könnte.

"Wenn es dich nicht interessiert, erzähle ich es nicht", wendet Luna ein. "Du
hast wenig Spielraum für irgendwas, aber hier hast du welchen."

Paolo ringt mit sich, aber bittet sie schließlich doch um die Erklärung. Vielleicht
lenkt es ihn ab. Und sie. Aber eigentlich hat er keine Hoffnung mehr.

Luna bückt sich und zieht einen Stift aus einem der Beine der Liege. Sie
zeigt ihn Paolo. "Mit diesen Stiften, die ziemlich stabil sind, lässt sich
die Liege in ihrer Höhe verstellen. Wobei", -- Luna zieht den Stift auf der
anderen Seite aus dem Bein --, "sie nicht insgesamt tiefer oder höher wird, sondern
sie lässt sich dadurch kippen."

Sie kippt die Liege mit dem Kopfteil abschüssig. Paolo rauscht das Blut in den
Kopf. Er schnappatmet. "Wegen Blutfluss?", fragt er.

"Genau", stimmt Luna zu. "Ich habe einiges getan, um zu optimieren, mehr Blut
aus Menschen saugen zu können, bevor sie kollabieren."

Aber entgegen Paolos Erwartungen kippt Luna die Liege wieder horizontal und
befestigt sie im alten Zustand, der wahrscheinlich der bequemste ist. "Warum
machst du sie wieder gerade?", fragt er verwirrt.

"Ich dachte, ich erzähle dir noch von der Mondklinge", sagt Luna. "Das ist eine
längere Geschichte. Ich weiß nicht wie gut ich darin bin, Geschichten zu
erzählen, aber dies ist eine, die sich vielleicht als Abschiedsgeschichte eignet."
