\BefehlVorKapitelIBDM{DieSchlangeDieSchildkroeteUndDieLoechrigeFlunder.png}{Der Panzer einer Schildkröte, in der sich ein Skelettkopf verkrochen hat. Es sind dunkle Schlieren angedeutet, die aus dem Panzer hervorquellen.}{Sex als Thema. Genitalien erwähnt, Löcher, Verwesung.}

Die Schlange, die Schildkröte und die löchrige Flunder
======================================================

\Beitext{Paolo und Kendra}

Spät in der Nacht, Léonides ehemaliges Arbeitszimmer, nun halb umfunktioniert
in Kendras Arbeits- und Schlafzimmer. Kendras Bett ist noch nicht geliefert
worden. Daher liegt ein hohes 1,40 mal 2 Meter Luftbett auf mehreren Kisten
und Paneelen, damit sie einen Ersatz hat, von dem sie leicht aufstehen kann. Dort
liegt sie neben Marcin. Es ist fast dunkel, nur das Nachtlicht leuchtet sachte,
und sie haben ihre Brillen längst abgesetzt. Trotzdem sieht Marcin für Kendra
immer noch kissable aus. Sie grinst über ihren eigenen Ausdruck. Ob sie
es noch einmal wagen darf?

"Hast du ein schlechtes Gewissen?", fragt Marcin.

Weil sie ihn küssen will? Kann er Gedanken lesen? Vielleicht ist es auch
recht offensichtlich, dass sie das möchte, Kendra weiß es nicht. "Weshalb?"

"Wegen Luna", antwortet Marcin.

Kendra hadert einen Moment mit sich, bevor sie verneint.

Marcin runzelt die Stirn. "Mir kommt dadurch schon die Frage auf, wer
von uns allen das krasseste Monster ist."

"Ich weiß einfach nicht, was mir ein schlechtes Gewissen bringen soll!", verteidigt
Kendra sich. "Ich glaube, wir sind da über Grenzen gegangen, die für sie nicht
so gut waren. Ich kann mich aber auch ohne negative Gefühle dazu entscheiden, dass
ich so etwas nicht wieder tue. Oder eben doch, wenn *sie* es will."

Marcin berührt sie an der Wange. Vielleicht, um sie zu beruhigen. "Ich
mag dich. Ich wollte dich nicht verletzen. Oder kritisieren."

"Ich habe mich schon kritisiert gefühlt", gibt Kendra zu. "Und ich
verstehe noch nicht, wie das keine Kritik sein kann."

"Es war nicht in Ordnung von mir", sagt Marcin. "Es basiert auf dem Ableismus: Wer
nicht richtig fühlt, ist ein Monster. Aber ich habe angefangen, mich halbwegs
damit zu akzeptieren, eines zu sein. Ich habe eine gewisse Liebe zu meinen
Monstern. Und manchmal habe ich mich ein kleines bisschen selbst lieb
deswegen. Wenn ich dich so bezeichnet habe, dann also, weil ich weiß, dass
mein Umfeld, mit dem ich groß geworden bin, es wertend tun würde, während ich
es liebevoll tue." Er seufzt. "Es ist trotzdem nicht okay. Aber so war das
gemeint."

Kendra nimmt Marcins Hand, mit der er ihre Wange berührt, in ihre, führt sie
zu ihrem Mund, aber sieht Marcin noch einen Moment an, bevor sie die
Hand küsst. Marcin zieht sie nicht zurück. "Damit kann ich gut leben und
es fühlt sich sogar lieb an." Kendra mag seine Ungeheuer schließlich auch. Vielleicht
mag sie doch eines sein, wenn Marcin sie dafür mag. Sie küsst die Hand noch einmal.

"Soll ich dich küssen?", fragt Marcin.

"Soll?", fragt Kendra.

"Vielleicht unglückliche Wortwahl", sagt Marcin. "Beim letzten Mal hast du
mich geküsst. Vielleicht kann ich mich besser fallen lassen, wenn ich aktiver
bin? Ich finde es spontan nicht unbedingt realistisch, aber ich würde es
ausprobieren."

Wieder flattert etwas in Kendra. "Na dann, los!", sagt sie. Irgendetwas
ist irritierend, aber sie weiß nicht was. Vielleicht wird es sich lösen,
wenn sie sich küssen, und wenn nicht, wird sie es genauer zu verstehen
versuchen.

Marcin rückt näher zu ihr, sie rückt näher an Marcin heran. Er ist nervös, das
merkt sie. Ob er noch nie aktiv geküsst hat? Das erste Mal, als sich ihre
Lippen berühren, passiert das nur sehr kurz. Kendra spürt den Drang, wieder
leidenschaftlich zu werden, aber sie lässt Marcin machen.

Marcin küsst sie auf verschiedene Arten. Mit festen Lippen, mit Zunge, ohne, sehr
langsam und weich und irgendwann auch wieder leidenschaftlich. Letzteres so lange,
bis Kendra sich vergisst und sich fallen lässt. Es ist längst etwas, was sie
eigentlich als Teil einer Beziehung mit ihm haben will. Wenn er mag. Das würde
wahrscheinlich bedeuten, ihn Paolo auszuspannen.

Kendra ist verwirrt von ihren Gefühlen und kehrt aus dem Gedankenuniversum zurück,
das nur Genuss, Leidenschaft und zärtliche Gier kennt. Marcin hört auf, sie zu
küssen. Irgendetwas stimmt nicht, steht zwischen ihnen. Sie sind sich sehr
nah. Marcin hat wieder zärtlich ein Bein über sie gelegt. Sein durchgestreckter
Fuß berührt sie am Oberschenkel. Es ist schön so, aber es gibt Unausgesprochenes. Sie
sehen sich an. Kendras Atem zittert noch. Sie sollte es ihm jetzt sagen.

"Ich hatte eine Erkenntnis", sagt sie also.

"Ich auch", sagt Marcin.

Kendra fühlt sich unwillkührlich aufgeregt. Was ist das bloß für ein fieses,
vereinnahmendes Gefühl? Sie möchte eigentlich nicht so sehr hoffen, dass
Marcins Erkenntnis ihrer ähnelt. Sie möchte, dass er frei ist. "Hast du
einen Vorzug, wer zuerst darüber redet?"

"Ich brauche etwas länger, glaube ich. Wenn deine recht kurz ist, dann
du zuerst", bittet er.

Kendra nickt. "Ich verliebe mich in dich. Oder so", sagt sie. "Ich mag es sehr, dir
auf diese Art nah zu sein." Muss sie noch etwas dazu sagen? Dass sie darüber
nachgedacht hat, ihn Paolo auszuspannen? "Ich hätte Lust auf Sex." Ihr wird
einen Moment sehr heiß, als sie es sagt.

Marcin schweigt verdächtig lang, aber rührt sich nicht. Schwarze Schlieren wabern
um ihn herum, aber manifestieren sich zu nichts. "Ich nicht", sagt er. "Ich
finde fair, dir das zu sagen, bevor ich dich ausfrage, wie sich das anfühlt."

"Lust auf Sex?", fragt Kendra.

Marcin nickt sachte. Ihre Stirnen (Gestirn? Wie ist der Plural?) berühren sich
dabei.

"Ich habe noch eine Vorwegfrage, bevor ich dir die
Frage beantworte", sagt Kendra. "Du hast nicht gezielt Lust
auf Sex mit mir, oder du möchtest eher sicher keinen haben? Ich weiß, das manche
Menschen welchen wollen, die keine Lust empfinden, deshalb frage ich."

"Ich weiß es noch nicht", antwortet Marcin. "Ich kann mir das nicht vorstellen. Vielleicht
würde ich es aus Neugierde tun, aber zum einen ist mir das im Moment zu viel und
zum anderen würde ich das schon mit Paolo absprechen wollen, wenn."

"Okay", sagt Kendra. Das Gefühl Enttäuschung testet wieder, ob es angebracht ist, gefühlt
zu werden, und ist dieses Mal erfolgreich. Sie will das nicht, aber Gefühle passieren
nun mal, und dann muss mit ihnen gedealt werden. "Du möchtest wissen, wie sich Lust
auf Sex anfühlt", wiederholt sie.

"Ja", sagt Marcin. "Ich kann mir das nicht vorstellen. Hast du irgendwie so einen
inneren Drang, zum Beispiel mein Genital anzufassen? Oder dass ich deines berühre?"

"Ja, schon, genau", sagt Kendra. Ihr ist nun eher unbehaglich warm. Sie überlegt,
wie sie es besser beschreiben kann, aber Marcins Beschreibung trifft es einfach
sehr genau. "Ich spüre beim Küssen, oft, nicht immer, so eine Leidenschaft, die ich sehr
mag. So eine fühle ich auch beim, trocken gesagt, Interagieren mit Genitalien. Wenn
ich die Person dahinter halt als attraktiv empfinde. Kann es sein, dass du mich nicht
als sexuell attraktiv empfindest?"

Marcin nickt. Dieses Mal noch vorsichtiger, sodass sich ihr Gestirn dabei nicht
berührt. Seine Augen werden feucht.

"Hey, das ist okay!", beruhigt Kendra eilig. "Du schuldest mir nichts. Wirklich!" Sie
fügt hinzu: "Oder vermisst du dadurch etwas?"

Wieder wabern die dunklen Schlieren um ihn herum. Manchmal kann Kendra sein Gesicht
kaum erkennen, so dicht sind sie. Sollte sie sich Sorgen machen?

Schließlich schält sich langsam eine Form aus den Schatten, eine Flunder, die über Marcins
Augen liegt. Ihre seitlichen Flossen bewegen sich schlängelnd, als wären sie im
Wasser. Die Flunder ist das erste Tier, an dem Kendra hellere Stellen beobachtet, die
nicht die Augen sind. Sie ist löchrig und dort, wo das verweste Fleisch herausschaut, ist
sie gräulich bleicher. Kendra mag sie sofort.

"Ich mag Kuscheln", sagt Marcin. Er tastet mit der Hand Kendras Schulter hinauf und
berührt sie wieder an der Wange. "Und irgendwann wollte Paolo mehr. Und ich wollte auch,
aber ich glaube, ich wollte, weil ich dachte", -- er zögert, atmet, redet ruhiger weiter --,
"weil ich dachte, dass das mein Ich sein müsste, wenn ich mich nicht so kontrolliere. Ich dachte,
der Grund dafür, dass ich keine Lust empfinde und sich die Leidenschaft für mich aufgesetzt
anfühlt, ist meine Kontrolle. Dass ich es nie ganz passieren lassen darf, also passiert
es quasi nicht. Wie, wenn du Kuchen backen willst, aber du gezwungen bist, den Zucker
komplett rauszulassen."

"Dann würde ich eine Quiche backen", sagt Kendra, ehe sie nachdenken kann, ob das eine gute
Idee ist.

Marcin kichert und streichelt ihr mit den zarten Fingern über die Wange. Es fühlt sich
schön an. "Aber du weißt halt nicht, ob deine Kuchen missglücken, weil du nicht backen
kannst, solange sie schon deshalb missglücken, dass du die wichtigste Zutat weglassen
musst."

"Ich verstehe, was du meinst", sagt Kendra. "Du hast dich nie ganz auf Sex- und Küssensdinge
einlassen können, weil du Kontrolle brauchtest, während Loslassen dabei eine so wichtige
Zutat wie Zucker im Kuchen sein könnte. Und nun, da du mit mir ein Risikospiel spielst,
in dem du loslassen darfst, findest du heraus, dass es nicht daran liegt. Sondern dass
es dir auch so nicht behagt."

Marcin gibt einen zittrigen, zustimmenden Laut von sich. Die Flunder wird einen Moment
qualmiger, manifestiert sich aber wieder. "Es tut mir leid."

"Was jetzt genau?", fragt Kendra. "Weil ich nicht finde, dass dir was leid tun muss."

"Dass ich dir sozusagen einen Korb gebe", sagt Marcin. "Klar ist das mein Recht, aber
es tut mir trotzdem leid, dass ich das tue."

Kendra streichelt Marcin über den Oberarm. Sie seuzft. "Ich wünschte, du hättest deshalb
keine negativen Gefühle", sagt sie. "Und ich wünschte, ich könnte irgendwas Überzeugenderes
sagen als, dass das in Ordnung so ist." Sie wünschte, sie könnte wahrheitsgemäß behaupten, sie
wäre nicht enttäuscht. "Ich mag dich, wie du bist. Möchtest du diese Haltung, die wir haben,
noch etwas auflösen? Oder ganz losgelassen werden?"

"Nein, wenn es für dich okay ist, mag ich das so", widerspricht Marcin. "Ich könnte
verstehen, wenn es dir zu dicht ist im Zusammenhang mit meinen Grenzen. Mein Bein
hat da vielleicht was anzüglich Deutbares."

"Ich mag es auch so", widerspricht Kendra. "Und ich kann es einfach als nicht anzüglich
einordnen. Es ist gemütlich und zärtlich. Magst du Zärtlichkeit?"

"Ja, sehr!", betont Marcin. Wieder verliert die Flunder an Schärfe, und dieses Mal
verschliert sie in der Luft, bis sie sich ganz auflöst. Stattdessen entsteht wieder
der kleine Drache, der zwischen ihre Körper kriecht, als Marcin Kendra enger in den
Arm nimmt, und sich dort aufwärmt.

"Haben die verschiedenen Wesen eine Bedeutung?", fragt Kendra. "Spiegeln sie bestimmte
Emotionen wieder?" Marcin hat vorhin schon einmal so etwas in der Richtung gesagt.

"Der Drache ist eine bestimmte Art Traurigkeit", antwortet Marcin. "Ich habe ihn sehr
lieb."

"Was bedeuten Schlange, Schildkröte und Flunder?"

"Ich bin nicht ganz sicher", sagt Marcin. "Ich glaube, die Flunder steht für Erkenntnisse,
die in meine Vergangenheit im Nachhinein ein Gefühl von Abscheu ergänzen, das da eigentlich
hingehört hätte."

"Würdest du zustimmen, dass Paolo dir gegenüber übergriffig war?", fragt Kendra. "Ich
frage vielleicht zu persönliche Fragen. Es tut mir leid. Ignorier mich, wenn das besser
ist."

"Ich finde gut, dass du fragst!", versichert Marcin. "Ich verdränge schon viel zu lange,
darüber nachzudenken. Wegen der Emotionen und der Ungeheuer, verstehst du?"

"Ja." Kendra versucht einen weichen Tonfall, der ein wenig von Marcins Brust geschluckt
wird. "Ich finde bisher die Anwesenheit jedes deiner Ungeheuer angenehm."

Marcin streichelt Kendra so zärtlich über den Rücken, dass sie ein leises Seufzen
nicht unterdrücken kann. Es ist einfach nur behaglich. Sie hat es geschafft, aufzuhören,
ihre Leidenschaftswünsche auf Marcin zu projizieren.

"Ich glaube, so ganz gesund war die Beziehung mit Paolo nicht", sagt er. "*Ist*", korrigiert
er. "Aber er hat mich nie zu Körperlichem bedrängt. Nie!"

"Damit meinst du, du hast aktiv 'ja' gesagt?", fragt Kendra.

"Und ich habe es auch provoziert, sozusagen", fügt Marcin hinzu. "Ich", er zögert einen
Moment, "ich weiß, was ihn heiß macht." Er vergräbt seinen Kopf in Kendras Haar. "Ich
weiß nicht, was ich dazu fühlen soll", nuschelt er. "Ich habe versucht, zu sein, wie
ich sein möchte, aber ich bin eben anders."

Dieses Mal streichelt Kendra ihm über den Rücken. Sortiert das lange Haar. Hält ihn
zärtlich fest, während er weint.

"Es fällt mir so schwer, nicht so sein zu wollen, wie Leute mich brauchen", gibt
er zu. "Du musst unglaublich gut darin sein, mir Sicherheit zu geben, dass ich
mich traue, dir gegenüber zuzugeben, dass ich nicht will, wie du mich eigentlich
gern hättest."

"Ich will dich nicht anders haben, als du bist", sagt Kendra energisch. "Wenn du
mich zufällig attraktiv empfunden hättest, ja, ich kann nicht leugnen, dass das nett
gewesen wäre, aber ich finde viel, viel wichtiger und interessanter, *dich*
kennenzulernen. Du bist aufregend und interessant und individuell und, und schön. Mit
deinem ganzen Sein."

"Du kennst mich doch noch kaum", wendet Marcin ein.

"Ich kenne auch den Geisterwald kaum, von innen erst recht nicht, aber egal was
darin ist, ich habe ihn sehr gern", sagt Kendra.

Marcin schweigt einen Moment und streichelt ihr noch einmal zärtlich über
den Rücken. "Wow, ist das eine schöne Metapher", sagt er schließlich. Er
möchte den Moment eigentlich noch etwas genießen, aber sein Hirn drängt
ihn zu den unbeantworteten Fragen. "Die Schildkröte ist, denke ich, das
Gefühl von Schutzbedürftigkeit. Ich war kurz vor der Realisierung, die ich
dir offenbart habe, und ich habe mich zerschunden gefühlt, und danach, als
bräuchte ich ein neues Daheim."

Kendra zieht die Arme etwas fester um ihn. "Wenn du möchtest, kann ich für
dich Familie sein. Hilft das?"

"Ich glaube, schon", nuschelt Marcin. "Für mich ist Angela wie eine Mutter,
glaube ich. Und ich liebe Bran sehr, auch wenn sie nicht mehr ist. Ich
glaube, ich möchte keinen Schwesterersatz. Aber ich mag gern, wenn du Teil
meiner Familie sein magst, dich als Famlilienmitglied zu haben."

"Das klingt schön", sagt Kendra. "Und ehrlich gesagt, viel schöner sogar als
eine andere Form von Beziehung."

Marcin streichelt ihr durchs Haar. "Und die Schlange", sagt er. "Das ist
das schwierigste. Ich glaube, sie ist dann aufgetaucht, wenn ich dann
doch 'nein' sagen wollte, und nicht konnte, weil ich mir nicht eingestehen
wollte, dass ich es sagen wollte. Sie hat an meiner statt 'nein' gesagt."

"Es klingt schon ein wenig, als hättest du ein reichlich beschissenes
Leben", murmelt Kendra. "Brauchst du Support, was Paolo angeht? Ich
nehme an, du willst darüber tatsächlich mit ihm reden?" Und als Marcin
zustimmt, fügt sie hinzu: "Würde es dir helfen, wenn ich dabei wäre?"

"Ich weiß es noch nicht", sagt Marcin. "Ich bin gerade emotional
erschöpft. Ich brauche eine Pause, ich kann gerade nicht mehr klar genug
darüber nachdenken. Wärest du da für mich, wenn ich
mir morgen einen Plan ausdenken möchte?"

"Natürlich, Marcin", sagt sie. Sie fühlt in sich ein starkes Gefühl von
Zuneigung zu ihm aufkommen. Aber es ist nicht die Verliebtheit von vorhin. Es
ist etwas anderes. "Ich habe dich lieb. Wenn ich darf."

Marcin berührt ihre Stirn mit seinen Lippen. "Darfst du", flüstert er
dagegen. "Auch wenn ich gerade noch nicht fassen kann, was es bedeutet."
