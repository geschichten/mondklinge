\BefehlVorKapitelIBDM{DasReissenDerZeit.png}{Ein anskizzierter Birkenwald im Nebel. Ohne Bank.}{Erotik, Blut, Sex, Genitalien.}

Das Reißen der Zeit
===================

\Beitext{Luna und Sonja}

Geruch von modrigem Holz, vom ersten Grün der frischen Brennesseln. Der
Klang des Windes in den Zweigen. Eine Woche ist Luna nicht hier gewesen. Sie
hat es vermisst. Ein Wald, der über zweihundert Jahre ihr Zuhause war, und
sie kann ihn kaum eine Woche verlassen ohne dieses Sehnen.

Er ist trocken. Für seine Verhältnisse zumindest. Das Laub auf dem Boden
knistert anders. Und doch ist es so vertraut, wie Luna es braucht.

Vor ihrer Holzhütte liegt Sonja auf dem Boden. Sie rührt sich kaum, als
Luna ankommt. Sie ist vielleicht auf der halben Verwandlung zwischen
Fuchs und menschlicherer Form stehen geblieben und liegt dort in diesem
wunderschönen Zwischenkörper mit zerzausten Haaren.

Luna liest sie vom Boden auf und trägt sie behutsam durchs Haus in ihr Bett,
legt sich neben sie, streicht ihr über das Fell. "Was brauchst du?"

"Wasser", kommt es nuschelnd aus Sonja heraus. "Was zu essen. Wärme. An
Wärme mangelt es hier."

Luna kümmert sich um alles davon. Sie reicht Sonja die Flasche zu trinken
aus ihrer schwarzen, bestickten Umhängetasche und legt auch die
Brotdose mit dem Stück Quiche daneben, das sie sich eigentlich für sich
mitgebracht hat. Die Küchenzeile ist unten in einem kleinen
Steinanbau hinter dem Haus. Sie setzt einen Kessel mit Wasser auf, um es
später in irgendwas zu gießen, was als Wärmflaschenersatz taugt, denn sie
hat so etwas nicht.

Bis das Wasser kocht und ihr etwas einfällt, geht sie zurück zu
Sonja ans Bett. Diese mümmelt an der Quiche herum, ohne Rücksicht
darauf, dass sie das ganze Bett zukrümelt.

"Warst du einfach die ganze Woche hier?", fragt Luna.

Ein Zittern durchläuft Sonjas Körper. Er ist ein klein wenig
menschlicher geworden. Sie nickt. "Kannst du mich nicht in den
Arm nehmen und aufwärmen?"

Luna berührt Sonjas Nacken mit ihrer kalten Hand. "Ich glaube nicht,
dass das taugt."

Sonjas Körper durchrinnt ein Zittern. "Es ist so kalt", flüstert
sie. "Und ich bin so nörgelig. Das ist so ungerecht dir gegenüber."

Als Luna den Kessel pfeifen hört und von Sonja ablässt, greift diese
mit diesen langen, fellig knochigen Fingern nach ihrem Arm. "Du
wirst warm, wenn du Blut trinkst, oder?"

"Aber die Person, die gerade zum Aussaugen zur Verfügung steht,
ist die, die ich gerade aufpäppeln möchte", wendet Luna ein. "Das
wirkt kontraproduktiv auf mich."

"Saug mich ruhig aus", flüstert Sonja leise. "Wenn ich zurückkomme,
geht es mir entspannt und gut."

"Ich hol den Kessel hoch und überlege es mir", verspricht Luna.

Als sie wenig später mit dem Kessel wieder in ihrem Schlafzimmer ankommt,
hat Sonja die Krümel aus dem Bett auf den Boden gefegt und liegt in zwei
Wolldecken eingewickelt nun in fast menschlicher Form da und bibbert. Es
ist ein herzzerreißender Anblick.

Luna stellt den Kessel ab und kriecht zu Sonja ins Bett. "Bist du dir
sicher, dass du das willst?", flüstert sie. "Ich bin gerade nicht
scharf darauf, dich auszusaugen, aber wenn es ist, was du willst..."

"Ich will mich dir nicht aufdrängen", sagt Sonja. "Ich dränge mich
dir auf, oder?"

Luna streicht Sonja übers Haupt. "Ich habe dich gerade gern hier", sagt
sie sanft.

"Wenn du mich aussaugst und ich in deinem Bett wieder zu mir komme..." Sonja
beendet den Satz nicht. "Ich kann nicht klar denken. Ich hätte gern deinen
warmen Körper an mir. Ich bin gerade auch nicht ausdrücklich scharf drauf
ausgesaugt zu werden, aber ich stelle es mir auch schön vor. Würdest
du es hassen?"

Luna schüttelt den Kopf. "Ganz und gar nicht. Dann also mal als Mittel
zum Zweck."

Luna hebt behutsam die Decken an, um sich zu Sonja darunter zu schieben, und
nimmt deren geschwächten Körper sanft in den Arm. Sie streicht ihr Haar
vom Hals. Sonjas Hals liegt trotzdem nicht nackt dar, eine dünnhaarige Fellschicht
wächst darauf. Das ist auch mal neu, denkt Luna. Sie streicht mit ihren
Lippen darüber. Sonja fiepst sacht, aber so unbeschreiblich kraftlos im
Vergleich zu sonst. Dieses Häufchen Elend. Luna seufzt fast, als sie
die Zähne in Sonjas Hals versenkt und zu trinken beginnt.

Es trifft sie überraschend, dass es so gut ist, dass es ihr so gut
tut. Sonja wehrt sich nicht ein bisschen, lässt sich einfach dahinein
fallen, als wäre es das normalste der Welt, von einem Vampir ausgesaugt
zu werden. Lunas Finger streicheln Sonja zärtlich durchs Fell auf ihrem
Rücken, bis aus ihm irgendwann ein Todesseufzen klingt, dass Luna
tief berührt.

Luna hätte, während sie Sonjas Leiche im Arm hält und aufwärmt, Zeit
gehabt, sich darüber Gedanken zu machen, warum es sich nun so gut
anfühlt, diese Kreatur in den Armen zu halten. Sie sind zufällig zwei
unsterbliche Wesen, die in der selben Gegend leben. Sie haben nie
zusammengefunden. Luna weiß nicht mehr, warum. Ihr Gedächtnis
wird poröser, je weiter es in die Vergangenheit hineinreicht. Und soweit
sie sich erinnern kann, waren Sonja und sie schon immer da und auch
schon immer eher im Zwist. Sie haben wohl damals gegenseitig ihre
Handlungsweisen nicht besonders gut gefunden. Sie haben sich beide
über die Zeit verändert, aber Gefühle sind geblieben. Irgendwie so.

Aber Luna setzt sich nicht weiter damit auseinander. Dann lässt sich
dieser Moment besser genießen.

Eine ganze Weile später, -- sie hat einfach mit Sonja im weichen
*eigenen* Bett gelegen und sich entspannt --, informiert sie ein tiefer
Atemzug des Körpers in ihren Armen darüber, dass Sonja
wieder da ist. Mit einem zweiten Atemzug wandelt sich Sonjas Körper wieder
in eine sehr menschliche Gestalt. Sie öffnet die Augen. Braun. Das
hat Luna noch nie beobachtet.

"Ich bin immer noch hier", sagt Sonja. Vielleicht verwundert. "Und du
bist warm."

Luna lächelt und streicht Sonjas Haar nach hinten. Das Gesicht ist
nun unbehaart. "Geht es dir besser?"

Sonja lächelt auch und macht ein zustimmendes Geräusch. "Ich kann
noch nicht so ganz fassen, dass ich hier sein darf."

"Ist dir nun warm genug?", fragt Luna.

"Es ist schon mächtig kalt bei dir", sagt Sonja und fügt
leiser hinzu: "Ich sollte vielleicht nicht immer das erste sagen, was ich
denke."

Luna schmiegt sich mehr an Sonja, um Körperwärme zu teilen. "Es wäre
wohl effektiver, wenn ich mich ausziehe, oder?" Sie entfernt sich
wieder ein Stück von Sonja und fängt an, ihre Schnürung im Rücken zu
lösen.

"Ich helfe dir, wenn du mich lässt", bietet Sonja etwas verdutzt an.

Luna dreht sich um, und lässt sich helfen. Als es locker genug ist,
zieht sie es sich über den Kopf. Sie entledigt sich außerdem ihrer
Strümpfe und ihrer Unterwäsche. Alles landet einfach neben ihrem
Bett. Dann schmiegt sie ihren Körper wieder gegen Sonjas.

"Auch wieder als Mittel zum Zweck?", fragt Sonja.

"Wozu sonst?", fragt Luna. "Funktioniert es? Wirst du wärmer?"

Sonja nickt. Sie überlegt, ob sie sich umdreht, damit Lunas
Körper ihren Rücken wärmen kann. Aber dann sind die Lippen
weiter weg. Und sie will Luna küssen. So sehr. Aber nur, wenn
sie es will. "Magst du küssen?", fragt sie vorsichtig.

"Damit dir noch wärmer wird?", fragt Luna belustigt.

Sonjas Atem verhakt sich einen Moment. Ehrlich sein. Sie
schüttelt den Kopf. "Das wäre vielleicht ein Nebeneffekt", sagt
sie. "Magst du küssen als Beschäftigung? Zum Selbstzweck?"

"Meistens nicht so sehr", sagt Luna. Sie sagt es selten, stellt
sie fest. Sie hat es noch nie für sich getan, immer nur für
andere. Und sich dann manchmal genommen, was sie wollte. "Es
sei denn, sie bluten."

Sonja beißt sich auf die Lippen. Es sieht anstrengend aus. Sie
blutet nicht, als sie wieder locker lässt. "Ich habe irgendwie
große Hemmungen, Verletzungen zuzufügen", sagt sie. "Egal ob mir
oder anderen."

Luna streichelt ihr über den Kopf und von dort in den Nacken. "Das
ist in Ordnung."

"Findest du nicht, dass es eine Schwäche ist?", fragt Sonja.

"Vielleicht eine wie nicht über Tafeln kratzen können", überlegt
Luna. "Manche Leute können das einfach nicht. Es ist ja meistens
auch nicht besonders sinnvoll. Also auch wirklich nicht schlimm, das
nicht zu können."

Sonja nickt. "Ich würde es gern gerade können."

"Darf ich dir weh tun?", fragt Luna.

Sonjas Atem reagiert auf die Worte, was Luna ein Lächeln
entlockt. "Es tut mir nicht weh, wenn du mich beißt. Oder
kaum."

Mit der Hand, die in Sonjas Nacken liegt, fährt Luna über
Sonjas Hals wieder nach vorn, bis ihr Daumennagel auf Sonjas
Unterlippe zum Liegen kommt. "Wenn ich den Fingernagel nehme,
vielleicht schon. Willst du das?"

"Grah!", sagt Sonja. "Du bist so fies und so gut! Ja ich will
das!" Sie spricht ein klein wenig undeutlich, um den Kontakt
zwischen Daumen und Lippe nicht zu lösen.

Luna bohrt ihren Fingernagel durch Sonjas Lippenhaut. Sonja wimmert
und zieht zischend die Luft ein. Luna zieht eine Blutline von
Sonjas Lippe das Kinn hinunter und beugt sich anschließend vor,
um sie von unten zur Lippe hin aufzulecken. Dann saugt sie das
Blut aus der Lippe, bis diese heilt, versenkt sich dabei in den
Kuss hinein. Sie fühlt Sonjas rascheren Atem auf ihrem Gesicht. Sie
mag, dass Sonja genießt. Aber als Sonja wieder ganz verheilt und
das Blut aufgeleckt ist, erzeugt Luna wieder Abstand zwischen ihren
Gesichtern.

Sonja atmet rasch und schwelgt. So sehr. Wenn das hier bloß
nie enden würde. "Ich liebe dich", flüstert sie.

Luna streichelt ihr wieder sanft über den Kopf. "Das irritiert
mich, offen gestanden. Für mich passt das Spiel, das du da
mit mir gespielt hast, das ich auch nur ungern Spiel nenne,
nicht dazu."

Sonjas Körper sackt in sich zusammen. "Du hast es so haushoch
gewonnen", murrt sie. "Du gewinnst immer. Du wirst immer
gewinnen."

"Wir sollten über Machtgefälle reden", antwortet Luna.

Sonja öffnet die Augen weit und runzelt die Stirn. "Ich
liebe es und ich hasse es. Dass wir eins haben", sagt
sie. "Und jedes Mal, wenn ich versuche, etwas daran zu
ändern, dass wir eins haben, fühle ich mich hinterher mies,
weil ich auf die falsche Art brutal bin. Dir verzeihen immer
alle, mir nicht. Aber ich sage mir dann, ich will auch gar nicht,
dass sie mir verzeihen. Aber vielleicht magst du mich lieber, wenn
sie es tun. Aber ich glaube, es ist schlecht, dass ich will, dass
sie mir verzeihen, damit du mich magst. Ich bin so verwirrt."

Luna schmunzelt. "Ich auch", sagt sie. "Vielleicht, weil
ich dich mag, obwohl du den ganzen Mist abziehst."

Sonja streckt eine Hand aus und streichelt Lunas Wange. Dann
zieht sie sie wieder weg. "Magst du das überhaupt?"

"Dich mögen oder das Streicheln?", fragt Luna.

"Letzteres", sagt Sonja und korrigiert sich eilig. "Beides."

"Ich mag das Streicheln. Sehr", antwortet Luna. "Das Mögen verwirrt
mich noch. Aber, hm. Ich mag es auch. Das ist ziemlich eindeutig."

Sonja lächelt. "Das ist schön!" Sie streckt die Hand wieder aus
und streichelt Lunas Wange dieses Mal etwas länger.

Luna schließt die Augen und genießt. Viel weicher noch sagt
sie: "Ich habe in der Drogerie noch eine Schmetterlingsspange
gekauft. Möchtest du eine haben? Und falls ja, die, die ich
seit etwa einem Jahrzehnt trage oder die Neue?"

"Oh", macht Sonja. "Die Entscheidung ist schwierig."

"Wir können auch wöchentlich wechseln oder so etwas", schlägt
Luna vor.

Sonja blickt sie an, als wäre gerade etwas Wundervolles
passiert. Sie nickt. "Das möchte ich." Leiser und ängstlicher
fügt sie hinzu: "Warum bist du so lieb zu mir?"

Luna streichelt Sonja wieder über das Haar, über den oberen Rücken,
beobachtet, wie Sonjas Körper darauf reagiert. Er
wird weicher und entspannter. "Das ist gar keine so einfache Frage. Vielleicht
verwöhne ich gern."

"Hast du dich in mich verliebt?", fragt Sonja.

Irgendwas in Luna springt auf die Frage an wie ein
Frosch, der zu lange nicht im Wasser war. Unwillkürlich
rückt sie näher an Sonja heran und verdreht dabei ihren
unteren Arm, auf dem Sonja liegt so, dass sie im nächsten
Augenblick unter ihr liegen würde. Sie hält die Bewegung
auf, als sie sie realisiert. "Ich glaube, schon."

Sonja grinst fast feixend. "Du bist in mich verlihiebt!" Sie
sagt es wie einen albernen Singsang. "Du willst mich! Du
willst mich benutzen!"

"Moment", sagt Luna. "Ich würde gern", -- sie unterdrückt
es 'vorher' zu sagen, und weiß nicht einmal, ob es mehr
ein sprachlicher oder ein psychischer Reflex wäre, es
zu sagen --, "mit dir zu Ende über unser Machtgefälle
reden. Ist das in Ordnung für dich?"

Sonja zuckt mit den Schultern. "Es klingt sinnvoll. Aber
ich weiß auch nicht, was ich dazu sagen soll. Du bist halt
stärker als ich."

"Schon", sagt Luna. "Aber ich glaube irgendwie, das eigentliche
Machtgefälle zwischen uns kommt durch, hm, wie erkläre ich das. Das,
was du angesprochen hast mit der Beliebtheit."

"Ich will gar nicht beliebt sein", sagt Sonja. "Ich will, dass
du mich liebst. Und küsst." Wieso hat sie das gesagt. Es ist
schon wieder zu lange her. "Ich denke, das Gespräch ist wichtig,
aber ich kann nicht klar denken, weil ich dich küssen will. 
Gern auch blutig."

Luna gluckst. "Meinst du, du kannst besser mit mir reden, wenn
ich dich frisch geküsst habe? Oder macht es das schlimmer?"

"Ich weiß es nicht. Probier es aus, wenn du magst", schlägt
Sonja vor.

Luna fädelt die Finger von unten in Sonjas Nackenhaar. "Ich
beiße dieses Mal. Ja?"

Sonja nickt. Sie ist schon kaum mehr in dieser Welt, weil allein
die Vorstellung sie zerlegt. Auf so wunderschöne Weise.

Luna nimmt ihre Oberlippe in den Mund und schiebt ihre
Zähne von innen durch die Lippenhaut im Mund. Sonja zittert,
fiepst und gibt sich einfach dem Gefühl hin, der Zunge unter
ihrer Lippe in ihrem Mund, die das Blut daraus leckt. Sie
küsst zurück. Es ist ein so schöner Kuss! Was soll das nur
werden mit ihnen.

Luna löst sich wieder. "Und? Besser?", fragt sie.

Sonja versucht, sich zu sammeln. Sie blinzelt einige Male. "Die
Frage ist fies", sagt sie. "Du musst mich küssen und dann einfach
mit reden anfangen. Wenn ich drüber nachdenken soll, ob ich
küssen möchte, dann will ich es auch. Dann ist der Drang wieder
da."

Luna zieht sie fest in eine Umarmung, umklammert sie eng und
küsst sie ein weiteres mal. Dieses Mal ratscht sie mit ihren Zähnen
zuvor über Sonjas Lippenhaut. Es kommt viel Blut heraus. Sonja
fragt sich einen Moment, ob ihr wegen des erneuten Blutverlusts
schwindelig wird, aber das ist Unsinn. Es ist, weil sie fast
hyperventilierend in den Armen eines Ungeheuers liegt, das sie
verzehrt.

Luna lässt ihr etwas Raum zum Atmen. Aber dann tut sie, worum
Sonja sie gebeten hat. Sie redet über Machtgefälle. "Es geht nicht
direkt darum, dass du unbeliebter bist als ich. Sondern dass
deine Taten schlechter bewertet werden als meine. Meine wirken
durch die Regeln, die ich vor Jahrzehnten schon etabliert habe, irgendwie
nicht so falsch wie deine. Deine sind unkoordinierter. Aber eigentlich
nicht schlimmer oder?"

"Ich weiß es nicht", sagt Sonja. "Ich denke immer, du machst schon
alles richtiger."

"Und das, denke ich, ist das Machtgefälle, das dich dazu bringt,
gegen mich spielen zu wollen", mutmaßt Luna. "Irgendwie kommen Leute
auf den Gedanken, dass du Schuld daran wärest, dass ich Paolo etwas
angetan habe, weil ja klar war, dass ich es tun würde."

"Hm", macht Sonja. "Ja. Ich bin da schon eher Schuld dran als
du."

"Nein!", widerspricht Luna. "Bist du nicht! Du hast mir in diesem
Spiel gezeigt, dass ich es mir zu einfach mache."

"Das wollte ich", sagt Sonja. "Aber wie ich es gemacht habe, war
schlecht und du hast gewonnen."

"Du hast es gemacht, wie du es konntest", vermutet Luna.

"Es war kein Plan", erklärt Sonja. "Ich spaziere manchmal als Schneefuchs
durchs Dorf, um Essen zu stibitzen, und dann beobachte ich diese
Menschen. Und dann war da diese Sache zwischen Paolo und, wie heißt
der Mensch mit den langen Haaren und den schwarzen Ungeheuern?"

"Marcin", antwortet Luna.

"Genau. Marcin", wiederholt Sonja. "Du magst Marcin, oder?"

Luna nickt einfach. Sie fragt sich, ob sie Sonja zurück zu ihrem
Faden schieben sollte, oder ob sie es selber schafft.

"Das kann ich verstehen", räumt Sonja ein. "Durch die Finsternis,
die Marcin in diese Welt wabern lässt, die voller Universum ist, weißt
du? Menschen schränken sich immer so ein mit ihrem, das ist gut, das
ist schlecht. Und so und genau so und nicht anders machen wir
Trauer."

"Du sprichst nicht mehr in funktionierenden Sätzen", informiert
Luna. Sie versucht, geduldig zu klingen.

"Entschuldigung", sagt Sonja. Sie schließt einen Moment die
Augen, um sich zu sammeln. "Marcin fühlt und dann passieren die
Ungeheuer und die sind schön." Sie holt noch einmal Luft. "Sind sie!"

"Ja, sie sind wunderschön", sagt Luna.

"Ich mag nicht, wie Menschen das einschränken wollen", fährt
Sonja fort. "Und Paolo hat das ganz doll gemacht. Und ich war
einfach so bedient von seinem heldentümlichen Drang, das
Richtige zu tun, indem er die Gefühle abschneidet. Und dann
habe ich das Falsche mit ihm gemacht."

"Du hast, wenn ich das richtig verstehe, ihm zugehört und
sein Geschwurbel bestätigt", fasst Luna zusammen.

"Und wenn seines nicht gereicht hat, habe ich auch so
Sachen reingestreut, wie, dass die Mondklinge dich
töten könnte", gibt Sonja zu.

Luna kichert. "Einfallsreich."

"Aber du bist mir doch böse deswegen, oder?", fragt Sonja.

Luna schüttelt den Kopf. "Ich war es", gibt sie zu. "Aber
ich bin es nicht mehr." Sie seufzt. "Ich hätte das Machtgefälle
zwischen uns schon länger mal reflektieren sollen. Ich verstehe,
dass ich durch meine Art dich in die Enge getrieben habe,
irgendwie zu reagieren. Du denkst weniger strukturiert
als ich über das nach, was du tust, aber du machst nichts
gedankenlos. Es hat einen Sinn. Sich gegen Machtgefälle zu wehren, sieht oft nicht so
galant aus, wie die Gewalt, die Personen ausüben, die Macht
haben."

Sonja kichert. "Nun faselst du."

"Stimmt vielleicht", gibt Luna zu.

"Aber es ist hilfreich", sagt Sonja.

"Wirklich?", fragt Luna. "Ich habe das Gefühl, überhaupt nicht
hilfreich zu sein. Ich habe das Gefühl, ich kann Dinge überhaupt
nicht so darlegen, wie du sie brauchst."

"Du liegst so da, wie ich dich brauche", sagt Sonja. Dann
atmet sie erschreckt und kichernd ein. "Brauchen ist übertrieben. Aber
ich liege hier so gern mit dir."

Luna streichelt ihr über das Haar. Wie so oft. "Ich liege hier
auch sehr gern mit dir", murmelt sie.

Sonja nähert sich ihrem Gesicht, um sie noch einmal zu küssen, aber
weicht dann doch wieder zurück. Sie schauen sich in die Augen.

"Wenn du möchtest, beute ich dich jetzt aus", schlägt Luna vor. Sie
lächelt ob des unvermittelten Einatmens. "Zärtlich", fügt sie
hinzu. "Langsamer als sonst und vielleicht dieses Mal ohne Aussaugen. Es sei
denn, du willst ausdrücklich nochmal."

"Ohne Aussaugen?", fragt Sonja. "Was bleibt dann für dich?"

"Dein Atem. Dein Fiepsen. Deine Verzweiflung, weil ich dich
hinhalte", schlägt Luna vor.

"Aber was hast du überhaupt vor, wenn es nicht blutet?", bohrt
Sonja nach. Sie ist hibbelig. Voll Vorfreude. Aber sie möchte
auch nicht, dass Luna etwas tut, was sie eigentlich nicht will
und nur tut, um Sonja zu verwöhnen.

"Ich weiß es noch nicht genau", sagt Luna. "Ich dachte, ich beiße
dich ein bisschen. An verschiedenen Stellen." Sie lässt eine
Hand durch das Fellkleid auf Lunas Vorderseite wandern. "Hier
zum Beispiel." Und bekommt als Antwort das Beben des Brustkorbs
in den Fingern mit.

"Ja", haucht Sonja, ehe sie sich besinnen kann, die Sache mit dem
Verwöhnen anzusprechen. Sie kann es auch kurz darauf nicht, weil
Luna sie zärtlich und genussvoll in die Schulter beißt.

Als Luna sich ihren Weg ihren Oberkörper hinunterbahnt, ist Sonjas
Körper warm genug, dass sie es schafft, das Fellkleid einzuziehen, sodass
nur eine dünne, zarte Fellschicht ihren Körper kleidet.

"Sechs Brüste", sagt Luna zwischen zwei zarten Bissen. "Das gibt
mir einiges zu tun und auszukosten."

Es sind sechs flache Brüste, die unter dem Fellkleid nicht zu sehen
sind, wenn es lang ist. Luna streicht über ihre Ränder und sachte
über die Brustwarzen, nur sehr zart. Sonja entfleucht fast jedes
Mal ein Seufzen, und dann ein Wimmern, wenn Luna hineinbeißt. Nicht
mittig, eher an den Seiten entlang. Den Bauch hinab. Sie beißt in
Sonjas Oberschenkelinnenseiten. So dicht an ihrer Vulvina, denkt
Sonja. Sie atmet schneller, wünscht es sich und wünscht es sich
doch nicht, weil es nicht Lunas Ding ist. Oder doch? Sonja öffnet
*natürlich* unwillkürlich die Beine.

"Can I eat you out?", fragt Luna.

"Fragst du das auf Englisch, weil du mich da tatsächlich beißen
willst?", fragt Sonja bebend.

"Ich", Luna zögert, "ich wollte gern, dass es sprachlich beides
abdeckt und dass du im Zweifel zu beidem konsentest. Aber
eigentlich dachte ich schon eher an lecken."

Sonja lässt sich gehen. "Ich würde sehr gern. Und ich mag die
Angst, dass du es vielleicht doch tust."

Luna schiebt ihren Kopf zwischen Sonjas Beine und leckt zwischen
den Vulvalippen hindurch. Sie tut es ein zweites Mal. Aber dann
verharrt sie.

Sonja ist von dieser Berührung so fassungslos, dass sie, als nichts
weiter passiert, ihre eigenen überraschend freien Hände in Lunas
Haar gräbt und den Kopf gegen ihre Vulvina zieht. Es passiert
nichts. Luna öffnet nicht einmal den Mund. Es ist seltsam. Sonja
lässt los. "Es tut mir leid", sagt sie.

Luna legt sich wieder neben sie. Ihr Gesichtsausdruck ist
seltsam nachdenklich. Sie wirkt nicht so entspannt wie vorhin. Ob
sie es wirklich nur tut, weil sie Sonja einen Gefallen tun will?

"Ich habe dich bedrängt und eine Grenze überschritten. Es tut
mir leid!", wiederholt Sonja.

"Ich hätte mich schon losmachen können, wenn ich gewollt hätte",
erwidert Luna. "Ich fand das Gefühl auch ganz interessant, dass
du mal mich festhältst."

"Aber du wolltest nicht", murmelt Sonja. "Wie fühlst du dich jetzt? Was
magst du eigentlich? Magst du Sex überhaupt?"

Luna wendet sich Sonja wieder zu und streichelt ihr das Haar. "Manchmal
schon", sagt Luna.

Sie wirkt so verunsichert auf Sonja. "Darf ich dich vorsichtig anfassen?"

Luna nickt.

Sonja streckt eine Hand aus und streichelt ihr über die Wange. Eine Weile. Luna
schließt wieder die Augen. Dass sie das mag, weiß Sonja schon. Dann streichelt
sie Lunas Hals hinab, über ihre Schulter. Auf ihre Vorderseite und an den
Seiten ihrer Brust entlang. Eine relativ große Brust. "Magst du das?"

Luna nickt wieder. "Das ist ganz nett", sagt sie.

Sonja streichelt in der Gegend der Brustwarze über die dort dunklere
Haut. Berührt die Brustwarze selbst zart. Sie wird fester. "Und das?"

Luna antwortet nicht. Hält die Augen weiter geschlossen.

Sonja hört verunsichert auf, die Hand zu bewegen.

"Mach ruhig noch ein bisschen weiter", bittet Luna. "Ich kann mir
ja nur ein Urteil bilden, wenn ich es fühle, oder?"

Sonja grinst und atmet dabei, sodass Luna hören kann, dass ein leichtes
Kichern darin steckt. Sie macht weiter. Sie merkt, wie Lunas Atem etwas
schneller geht. Sie fühlt sich nun weniger unsicher. Ob es Lunas erste
Erfahrung dieser Art ist? "Hat noch nie jemand deine Brüste gestreichelt?"

"Doch schon", widerspricht sie. "Aber da ging es nicht um mich."

Die Worte rühren in Sonja fast etwas zu Tränen. "Ich möchte gern
heute alles, was ich tue, für dich tun."

"Danke", sagt Luna. Sie seufzt und fügt hinzu: "Aber diese Sache mit
dem Streicheln dort ist nicht so gut. Es überreizt mich gerade."

Sonja zieht ratlos die Hand weg. "Soll ich etwas anderes probieren?"

"Kannst du mich doll in den Nippel kneifen?", schlägt Luna vor. "Ich
will wissen, wie das ist."

Sonja sieht Luna einen Moment entgeistert an und ist ganz froh, dass
Luna es nicht sieht, weil sie die Augen geschlossen hat. "In Ordnung." Sie
fühlt sich nicht unbedingt sehr behaglich dabei, als sie die Finger um
Lunas eine Brustwarze legt und dann plötzlich sehr feste zudrückt. Ob
Luna sich sonst so beim Sex fühlt? Dinge tun, die andere wollen, die sie
nicht so richtig überzeugen, aber trotzdem voll Zuneigung und Willen?

Luna zieht zischend die Luft ein. Ihr ganzer Körper zuckt.

"Ist es gut?", fragt Sonja.

Luna schüttelt den Kopf und öffnet die Augen. "Aber es macht, dass ich
mich auf dich stürzen und dich beißen will."

"Dann tu es." Sonja nimmt die Hände wieder weg.

Luna schüttelt den Kopf. Und doch fädelt sie ihre obere Hand wieder in
Sonjas Nackenhaar und zieht ihn näher zu sich.

Sonja atmet sofort erregt ein. "Wir können auch sowas die ganze
Nacht machen, wenn du es magst."

"Möchtest du mich zwischen den Beinen anfassen?", fragt Luna.

Sonja hätte genickt, weil sie vor Aufregung kaum sprechen kann, aber
Luna fixiert ihren Kopf. "Gern. Wenn du willst!"

Luna schiebt Sonjas Hand zwischen ihre Beine, aber lässt Sonjas Finger
selbst ihren Weg an ihr Genital finden. Sie ist feucht. Sonja fragt sich,
ob sie nicht eigentlich eher mit etwas anderem gerechnet hätte. Sie fährt
mit ihren Fingern zwischen die Vulvalippen und beobachtet dabei Lunas
Gesicht. Sie versucht, irgendwie abzulesen, ob es ihr gefällt. Was ihr
genau gefällt. Luna schließt wieder die Augen. Sie sieht entspannt aus und
atmet sehr ruhig. Es ist anders als bei dem Experiment mit der Brust.

"Magst du es?", fragt Sonja.

"Es ist schon ganz schön", antwortet Luna leise.

Sonja findet heraus, dass Luna zumindest mehr Anzeichen von Entspannung
zeigt, wenn sie es besonders sanft tut. Aber als sie schneller wird,
zieht Luna ihre Finger wieder zwischen den Beinen weg. Sie streichelt
Sonja über den Rücken, während die andere Hand Sonja weiterhin im Nackenhaar
gepackt hält. "Darf ich dich noch einmal blutig küssen?"

Sonja nickt. "Immer", haucht sie. "Also, fast immer zumindest."

Luna zieht Sonja fest an sich, umklammert sie und beißt Sonja in
die Lippe. Sie tut es nicht nur einmal, dieses Mal, sondern widerholt
alle Varianten von vorhin. Sie küsst Sonja so lange, bis Sonja nicht
mehr denken kann und sich einfach hingibt, vergisst, dass sie
eigentlich mehr auf Luna dabei achten möchte. Aber als Luna aufhört,
lächelt diese Vampirkreatur.

"Ich glaube, für mich funktioniert es nicht, wenn du nicht unverschämt
genießt", sagt sie. "Und es ist schwer für mich, wenn ich dabei etwas
mache, was ich selbst nicht kenne. Dich lecken zum Beispiel. Ich weiß einfach
nicht, was ich tun müsste, damit es gefällt. Und selbst wenn ich wüsste,
dass es dir gefällt, ist es nichts, was ich kontrollieren könnte. Ich
fühle mich dabei hilflos."

Sonja kommt allmählich wieder zu Atem und streichelt Luna wieder
über die Wange. "Ich habe es gern, wenn du Kontrolle über mich
hast." Sonjas Stimme bebt immer noch ein wenig.

Luna kratzt ihr zärtlich über den Rücken. "Magst du mit mir Sex
mit Strap-On versuchen?"

Sonjas Atem stolpert. "Ich, ja, also, wenn du willst, schon", sagt
sie. "Also, sehr gern. Für mich ist die Vorstellung schön! Aber
möchtest du wirklich?"

"Ich würde es sehr gern probieren." Luna klingt dieses Mal ziemlich
überzeugt.

Sonja nickt. "Hast du denn einen?"

Luna grinst und lässt Sonja für einen Augenblick los. Sie legt sich
nackt halb übers Bett und sucht auf der anderen Seite darunter
in einer Kiste. Sie braucht nicht lange, bis sie ihn hervorholt. "Hebst
du die Beine an?"

Sonja schlägt stirnrunzelnd die Decke weg und streckt die Beine in
die Höhe. "Willst du ihn dir nicht erst anziehen?"

"Ich wollte ihn *dir* anziehen", korrigiert Luna. Sie ist doch
wieder verunsichert. "Möchtest du unter den Umständen doch nicht?"

Sonja antwortet nicht sofort. Sie ist überrascht. "Aber du
willst mich dominieren und nicht irgendwie tauschen?"

Luna grinst. "Ich möchte dich ausbeuten", raunt sie. "Ich möchte
etwas in mir drin fühlen. Ich dachte, da bietet es sich an, dir
einen Strap-On anzuziehen."

Sonja schnappatmet und stimmt zu.

Und Luna eröffnet das Spiel gefühlt jetzt erst so richtig. Sie
zieht ihn Sonja mit geübt wirkenden Griffen an, zurrt ihn
zweckmäßig fest und schiebt ihren Körper langsam auf Sonjas.

Es ist schön, so schön, ihr so zu gehören. Von ihr ausgenutzt
zu werden. Dabei erregt zu sein, aber in diesem Punkte ignoriert
zu werden. (Nur in diesem.)

Irgendwann später liegen sie sich außer Atem in den Armen. Luna
küsst zärtlich Sonjas Gesicht. "Danke", flüstert sie. "Wie
war es für dich?"

"Gut, das weißt du doch", sagt Sonja. Luna hat so sehr auf sie
dabei geachtet, jedes Seufzen in sich eingesaugt und darauf
reagiert. "Wie war es für dich?", fragt sie ängstlich. "Ich
hatte nicht den Eindruck, dass du einen Orgasmus hattest."

"Hatte ich nicht", bestätigt Luna. "Brauche ich auch nicht. Ich
genieße anderes an Sex als Orgasmen. Irgendwann bin ich einfach
irgendwie gesättigt."

"Soll ich gehen?", fragt Sonja.

Luna schüttelt den Kopf. "Ich habe das Gefühl, ich könnte dich
ein Jahrzehnt in den Armen halten und es würde mir nicht zu
viel werden. Stimmt bestimmt nicht. Aber im Moment genieße
ich dich sehr. Willst du denn gehen?"

Sonja schmiegt sich mehr in Lunas Umarmung. "Ich möchte
im Moment nicht einmal übers Gehen nachdenken. Ich glaube, ich
war noch nie so glücklich!"

Irgendeine Erinnerung in Luna wird wach, die eigentlich schon längst
durch die Porösität ihres Gedächtnisses entfleucht ist. Hat
Sonja ihr das vielleicht schon einmal gesagt? Vor ein paar
Jahrhunderten? Ist es doch nicht das erste Mal mit ihnen, dass
sie etwas so klar mögend miteinander tun? Sie
weiß es nicht. Es ist auch nicht wichtig. Der
Augenblick gerade ist gut. Sehr gut.

Sie hören, wie leichter Regen auf das Holzdach tröpfelt. Endlich,
endlich kann der Wald wieder Wasser aufnehmen. Das Holzhaus
im Geisterwald hüllt sie zusammen mit dem Regen in eine urgemütliche
Kulisse aus Klang und Geruch. Vielleicht braucht Luna für
Sonja doch irgendwann noch einen Ofen. Oder wenigstens eine Wärmflasche.

Morgen früh wird Nebel sie wieder einhüllen und durch den Geisterwald
wandern. Die grünen Sprossen werden sprießen und der Wald wird
seine Geschichten über Tod und Leben vor sich hinflüstern.
