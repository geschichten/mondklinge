\BefehlVorKapitelIBDM{FieseLiebesbriefe.png}{Ein zweimal gefaltetes Papier mit Blumenmuster am Rand. Lesbar ist ein Teil der Überschrift: Sonja, mein S. Alles weitere ist vom gefalteten Papier verdeckt.}{Kink, Lauern, legeres Reden über Mord.}

Fiese Liebesbriefe
==================

\Beitext{Sonja}

Im Geisterwald vorm verwinkelten Holzhaus mit seinen schrägen Türmchen, eingewuchert
in Efeu und Moos und eingebettet in vertrauten Nebel. Sonjas Atem füllt
die Stille. Sie ist hier hergerannt, in Schneefuchsform, und steht nun
in menschlicherer ein paar Meter vor der Tür. Sie verschnauft, bis sie sich
beruhigt, bis sie nur noch das Blut durch ihren Körper rauschen spürt.

Zwei Dinge sind anders als sonst: Sie wird nicht aus dem Nichts angegriffen -- immer
noch nicht -- und sie hat Angst. Sonja hat selten Angst und sie weiß nicht,
warum jetzt.

Sie nähert sich dem Haus noch einen Schritt. Es ist der eine Schritt, der sie an
einen Ort führt, an dem sie noch nie gewesen ist. Sie war noch nie so dicht an
diesem Haus. Sie wurde immer vorher abgefangen. Luna hat ihre Fingernägel in
ihre Haut gebohrt, Spuren hinterlassend, und hat ihr ins Ohr geraunt, dass
es ihr privates Reich wäre, und dass Sonja darin nichts verloren hätte. Die
Drohung darin nicht überhörbar, dass sie schon einen Weg finden würde, Sonja
*wirklich* etwas anzutun, wenn sie das Haus je beträte.

Sonja versteht es. Sie hat ihr eigenes Reich, das Luna nie betreten hat, und
das ist gut so. Sonja hat ein anderes Verhältnis zu Privatsphäre, aber es ist
gut, einen Ort zu haben, an dem sie vor Luna sicher ist. Wenn sie mal wieder
etwas angezettelt hat, wofür Luna sie jagt, dann kann sie sich doch Momente
aussuchen, in denen sie ausruhen kann, in denen sie weiß, dass Luna vielleicht
vor der Tür lauert, aber nicht hineinkommen wird.

Aber sie gesteht sich ein, dass sie Luna gar nicht so wichtig ist. Luna hat
höchstens einmal für ein paar Stunden gelauert. Und sie selten woanders gejagt
als im Wald.

Sonja tritt noch näher an das Haus heran. Sie zittert. Sofort wächst mehr Fell aus
ihrem Körper, um sie zu wärmen. Aber eigentlich will sie das gar nicht. Es
macht sie weniger zugänglich. Und gleichzeitig kann sie mit kürzerem Haar besser
laufen. Sie mag die Vorstellung, von Luna gejagt zu werden, aber gerade
reagiert ihr Körper kaum darauf.

Gerade ist sie nicht für ein kinky Spiel hier. Sondern für den Anfang eines
Redemption Arcs, denkt sie, und kichert. Ob sie je eine Chance hat, dass
Luna sie mögen wird, wenn sie sich selbst so wenig ernst dabei nehmen kann,
wenn sie versucht, anständig zu sein?

Wie so eine anständige Person pocht sie an die Holztür. Es ist ein schönes
Gefühl, das zu tun, und gleichzeitig hat sie so große Angst. Denn sie fürchtet,
dass der Optimalfall nicht eintreten wird, dass Luna einfach öffnet und sie
hereinbittet.

Sie behält recht. Niemand öffnet die Tür. Auch nicht, als sie ein zweites
Mal klopft, aber dieses Mal hört sie Paolo aus dem Fenster um Hilfe
schreien. Aus dem offen stehenden Fester im mittleren Stockwerk, das niedriger
ist als die anderen. Dem mit den Folterinstrumenten. Aus dem Sonja so oft
die Stimmen der Sterbenden gehört hat. Und die Mondklinge.

Paolo ist also noch am Leben. Sonja atmet zitternd ein und aus. Sie weiß nicht,
was sie von dieser Situation halten soll. Luna weiß, dass sie hier ist. So
etwas entgeht dieser Vampirkreatur nicht. (Es sei denn sie ist selbst
nicht hier, aber sie würde ihr Opfer doch nicht allein lassen.)

Sonja hat das Gefühl, dass sie hier auf eine Probe gestellt wird. Und wenn
sie alles richtig macht, überlebt Paolo. Und das ist, was von ihr
erwartet wird in diesem Redemption Arc.

Und es geht ihr auf den Keks. Sie möchte nicht geprüft werden. Das ist nicht,
wie sie gemocht werden will. Sie möchte verdingst nochmal auch ihren Raum haben.

Sie überlegt, in das Haus hineinzuspazieren und es nach Lauden Léonide von
Horstenfels' Rezept zu versuchen: Sie wird Luna einfach sagen, was sie will. Und
wenn Paolo dabei draufgeht, dann ist das so.

Sie klopft noch einmal energischer an die Tür, bevor sie die Klinke
hinunterdrückt. Aber die Tür gibt nicht nach. Sie ist von innen
verriegelt.

Paolo schreit lauter um Hilfe. Er ist nicht geknebelt. Aber das ist auch nicht
Lunas Stil.

"Ich komme ja!", ruft Sonja zu ihm hinauf. Sie geht ein paar Schritte zurück.

Wenn die Tür verriegelt ist, will Luna
sicher nicht, dass sie sie aufbricht. Vielleicht hat sie das Fenster
bewusst offen stehen gelassen.

*Vielleicht ist es eine Falle.*

Dann rennt sie halt in eine Falle, denkt Sonja. Sie begibt sich in Schneefuchsform, weil
sie auf diese Weise leichter den schrägen, alten Baum neben dem Fenster hinaufklettern
kann. Das hat sie immerhin schon ein paar Mal gemacht. Um zu lauschen. Aber den Sprung hat sie nie
gewagt. Sie erinnert sich resigniert daran, dass heute schon ein Sprung in eine Falle geführt
hat. Grah! Egal.

Sie springt. Sie landet überraschend sicher im Eingang des kleinen Fensters und lässt
sich in den Raum gleiten. Er ist kühl und relativ dunkel. Ein paar Kerzen sind ausgebrannt,
einige hat der Wind dann doch ausgepustet. Vier brennen noch.

Sonja blickt sich um. "Ist Luna nicht hier?", fragt sie.

Paolo schüttelt den Kopf. Er hat aufgehört zu schreien. "Sie hat gesagt, vielleicht
kommst du mich retten." Schluchzend fügt er hinzu: "Aber dass du es nicht für mich
tun würdest."

Sonja blickt fassungslos in Paolos Gesicht. Es ist keine Falle, es ist eine
Demütigung. Sie hat eigentlich echt keine Lust, diese nervige Person zu
retten. Urx. "Sondern für sie?", fragt sie. "Luna denkt, so etwas würde ich
für sie tun?"

Paolo schüttelt wieder den Kopf. "Sie hat mir das vor allem erklärt, damit ich
nicht wieder denke, du hättest Gefühle für mich", sagt er. "Sie hat einen
Brief für dich hinterlassen." Paolos Blick wandert auf seinen mit Gurten
umschlungenen Bauch.

Tatsächlich lugt ein weißes Stück Zeichenpapier, zweimal gefaltet, unter dem Gurt
hervor. Sonja geht die paar Schritte auf Paolo zu und zupft es hervor. Es ist
erst nicht so freiwillig, aber als Sonja etwas energischer daran zieht (sie
weiß, was das Papier aushält), erklingt wieder dieser unbeschreibliche Klang
der Mondklinge. Nicht so laut dieses Mal. Sie hält einen Moment inne und genießt,
wie er unter ihrer Haut entlangrinnt, eh sie den Brief entfaltet.

> Sonja, mein Sonnenschein,

Sonja schnaubt. Welch ironische Anrede. Sie klingt so positiv. Aber Sonja weiß doch,
wie wenig Luna Sonnenschein bekommt.

> wenn du diesen Brief in Händen hältst, werde ich es wissen. Hinweis: Mondklinge.

Sonja schnaubt wieder. Deshalb also war sie irgendwie am Brief befestigt. Unter
der Decke, in die Luna Paolo gegen die Kälte gewickelt hat.

> Ich mache mich dann mal auf zu einem längeren Urlaub und lege damit
> Paolos Leben in deine Hände. Du hast nie einen Mord begangen, richtig?
> Dann ist hier deine Gelegenheit. Du müsstest ihn nur gefesselt da liegen
> lassen. Aber du kannst es natürlich auch direkter oder kreativer angehen.

Wow, denkt Sonja. Was ist das für eine Vampirkreatur? (Und warum kribbelt
es in Sonja voll Zuneigung?)

> Ich habe durchaus eine Meinung dazu, ob du es tun oder lassen solltest. Und
> nach meinen jüngsten Erfahrungen mit dir habe ich den Eindruck, dass sie
> dich interessiert. Also hier ist sie: Ich denke, du solltest ihn
> gehen lassen. Er könnte noch einiges an Leben vor sich haben. Auch wenn es
> sich echt unbehaglich anfühlt, dass eine Person überleben könnte, die mein
> Haus von innen gesehen hat. (Die anderen Räume.) Oder mich zeichnen
> gesehen hat.

Wow, denkt Sonja wieder. Sie hat Luna schon zeichnen gesehen. Und Luna
weiß es. Vielleicht mag Luna sie doch.

Welch Liebesbrief. Ist es als Liebesbrief gedacht? Aber die nächsten
Worte machen es zumindest für Sonjas Begriffe zu einem:

> Du bist mir nicht egal, Sonja. Ich habe schon immer ein Faible
> für Monster gehabt. Wenn du was mit mir zu tun haben willst, sag
> das doch einfach. Ich sauge dich vielleicht nicht täglich
> aus, aber ein- bis zweimal in der Woche wäre schon drin. Was
> meinst du?
>
> Luna

Sonja legt den Brief zur Seite, atmet einen genussvollen Zug
Wald- und Meeresluft ein und lässt ihn wieder entweichen. Mit einem
Lächeln auf den Lippen, das auch ein wenig mit Frust verwoben
ist. "Na, dann wollen wir mal herausfinden, wie ich dich
losmache", sagt sie. "Luna hat nicht irgendwo eine Anleitung?"

"Was stand in dem Brief?", fragt Paolo, statt zu antworten. Unhilfreich. Er
wirkt ein wenig verwirrt.

"Es war ein fieser Liebesbrief, und alles weitere geht dich einen
feuchten Dreck an", sagt Sonja. "Ich frage mich, ob hier irgendwo
ein stabiles Seil ist."

"Im Schrank", informiert Paolo und versucht einen Finger so zu
verdrehen, dass er in eine Richtung zeigt. "Ich weiß nicht, warum
mich Luna darüber informiert hat. Sie meinte, daran sei eine
gemütliche Schlaufe, um Menschen kopfüber aufzuhängen."

"Sie erzählt gern von ihren Methoden, mehr Blut aus Leuten zu
bekommen", erklärt Sonja und tritt dem Schrank entgegen.

"Schon", weiß Paolo. "Aber warum erzählt sie mir *nur* vom Seil? Es
wirkte etwas aufgesetzt. Und warum suchst du dann ausgerechnet eines?"

Sonja schüttelt grinsend den Kopf. "Dieses Biest!", flucht
sie. Die Information hat Luna doch genau zu diesem Zweck
dargelegt: "Ich brauche eines, mit dem ich dich aus dem Fenster abseilen
kann. Damit ich dich nicht noch einmal durch die Räume
gehen lassen muss, die ihr etwas bedeuten. Und vor allem, weil ich
selber nicht dadurch gehen werde, ohne dass sie mich einlädt."

---

Paolo ist erschöpft und kann sich nur schwer aufrecht
halten, weshalb Sonja beschließt, ihn
aus dem Wald zu begleiten. Sie muss wie automatisch immer wieder
denken, dass ausgerechnet dieser Mensch es geschafft haben wird, lebend
aus dem Wald zu kommen. Es sei denn, Luna taucht doch noch auf.

Luna taucht nicht auf. Sie kommen an der Bank an, an der sie sich
so oft getroffen haben. Aber die Klippe ist nicht verlassen.

Paolos Mutter Angela wartet auf sie. "Ab hier übernehme ich", bestimmt
sie.

Sonja nickt bloß. Sie bleibt da, bis Paolo seiner Mutter in die Arme
fallen kann. Die alte, schöne Person stützt Paolo. Sie weint nicht,
aber Sonja merkt, wie sehr sie liebt. Es ist ein sehr persönlicher Moment,
in den Sonja nicht gehört. Also geht sie.

Es war kein gutes Spiel, überlegt sie. Sie hat Luna in eine Zwickmühle
gebracht, um herauszufinden, was die Vampirkreatur entscheidet: Paolo
töten oder ihn nicht töten. Und Luna hat sie mit einen sehr guten Zug
matt gesetzt, den Sonja nicht hat kommen sehen. Sie hat Sonja in
eine Lage gebracht, in der der Mord zu viel an ihr haften würde. Sie hat
Sonja quasi gespiegelt, was Sonja mit Luna gemacht hat.

Sonja geht zurück in den Wald. In Lunas Wald. Den sie nun für eine Weile
für sich hat. Sie ist sicher nicht so gut darin, ihn zu bewachen und zu
schützen, wie Luna das ist, aber sie wird es ein wenig versuchen.
