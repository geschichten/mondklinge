Vorwort {.unnumbered}
=======

Das Buch begann ich am 4.11.2022 mit dem Kapitel "Mondscheinhypothesen". Ich war mir zu dem
Zeitpunkt nicht sicher, ob das Kapitel nicht eher eine alleinstehende Kurzgeschichte
werden sollte. Aber dann habe ich
es kombiniert mit einer Challenge. Mein Herzwesen Evanesca (Katherina Ushachov)
schrieb in etwa "Von dir würde ich sogar ein Buch
mit folgendem Klappentext lesen und keine Angst haben, dass es schrecklich wird.", und lieferte
mir dies:

> Die blutjunge 17-jährige Kendra entdeckt, dass sie Zauberkräfte hat. An ihrer
> neuen Schule fühlt sie sich zum geheimnisvollen Paolo mit den silbernen Augen
> hingezogen, obwohl er sie ständig verspottet. Doch als eine dunkle Bedrohung
> sowohl die Schule als auch die Welt zu vernichten droht, müssen sie
> zusammenarbeiten. Werden sie es schaffen?

Ähm, nun. *Das* nenne ich tatsächlich mal challenging. *Blutjung* muss ich immerhin nicht
wörtlich nutzen, aber *17*? *Silberne Augen*? *Hingezogen* zu einer Person, die
sie *verspottet*?

Ich glaube, der Klappentext wird hinterher nur mit viel Mühe auf das Werk gepresst werden
können. Aber es ist nicht vollkommen unmöglich.

Auch den Titel hat sich Evanesca ausgedacht. "Im Bann der Mondklinge" impliziert
also auch, dass es eine Mondklinge geben muss. So viele Dinge, die in mein
Versmaß (Metapher für meinen Schreibstil) zu quetschen waren, ohne dass es sich
wie Reim-dich-oder-fress-dich anfühlt...

Wie es dann dazu gekommen ist, dass ich ein zweites Kapitel geschrieben habe, das
alleinstehen könnte, weiß ich nicht. Ich war immer noch nicht davon überzeugt, daraus
wirklich einen zusammenhängenden Roman zu bauen, aber am 11.11.2022 habe ich mit
dem ersten Kapitel "Sanftes Entrinnen" nachgelegt.

Am 10.12.2022, nachdem ich mit einigen Menschen über mein mäßig überzeugendes Konzept
gesprochen habe, hatte ich dann genug Draht zur Geschichte, um mit einem zusammenhängenden
Gedöns anzufangen und daraus was Ganzeres zu stricken. Alle weiteren Kapitel
schrieb ich halbwegs chronologisch bis zum 2.1.2023.

Während ich dieses Werk produziere, befinde ich mich in den Tiefen eines autistischen
BurnOuts, der meine klare Denkfähigkeit und mein Durchhaltungstum belastet. Das hat zum Beispiel Einfluss
auf die Kapitellänge, oder darauf, dass es nicht wirklich einen komplexen Plot
oder so etwas gibt. Und dass es vielleicht das schlechteste Werk ist, dass ich seit
Ende der Schulzeit schreibe. Aber es geht hier nicht darum, Qualität zu produzieren, sondern
darum, zu copen. Mein Kopf ist permanent kreativ. Irgendwo muss ich damit hin, sonst
fühle ich mich noch miserabler. Nicht aus Leistungsdruck heraus, sondern weil die
Kreativität ansonsten ungelenkt irgendwo hintrudelt, wo sie mich zermürbt.

Eigentlich will ich gerade andere Texte überarbeiten. Aber ich kann mir aktuell wirklich
nicht aussuchen, was ich tun kann. Mein Hirn fordert ein, was es braucht. Also: Nehmt
mit diesem Schrieb Vorlieb oder lasst es, wie es euch behagt.

Die Perspektive ist ein bisschen chaotisch. Manchmal wechsele ich innerhalb von Szenen
den Point of View. Manchmal schreibe ich allwissend.

In diesem Buch kommt das Neopronomen 'as/sain/iem/as' vor. Das ist etwas, was in unserer
Welt im Umgang mit vielen nicht-binären Menschen durchaus üblich ist -- und nicht
auf nicht-binäre Menschen begrenzt. Menschen dürfen sich ein Pronomen aussuchen, mit dem
sie referenziert werden möchten. In meinem Umfeld gibt es Menschen, die aus
verschiedenen Gründen irgendwann eines für sich gewählt haben. Neopronomen
sind fester Bestandteil in vielen vor allem queeren Communities. Es besteht
unter den Menschen, die es betrifft, weitgehend Konsens, dass das Konzept ein
Gutes ist. Pronomen werden zunehmend in Signaturen oder in Profilen angegeben.

Das Pronomen 'as/sain/iem/as' ist für mich darüber hinaus besonders, weil es
eine Zeit mein liebstes für mich war. Ich mag es aktuell immer noch gern für
mich. Und es hat mich sehr berührt, dass ein Charakter aus Eleanor Bardilacs
Roman "Knochenasche rottet nicht" dieses Pronomen hat.

![map of Søregen in grayscale](/home/kaulquake/Fischtopf/Geschichten/ImBannDerMondklinge/SoeregenMapRand.png)
