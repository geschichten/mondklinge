\BefehlVorKapitelIBDM{LogikAmMorgen.png}{Eine leere Schale, an der ein Löffel lehnt.}{Reden über Tod, vielleicht toxische Beziehung.}

Logik am Morgen
===============

\Beitext{Paolo und Kendra}

Paolo wacht morgens um fünf auf und kann nicht mehr schlafen. Marcin ist
an ihn gekuschelt. Sie haben heute Nacht das Bett geteilt, zum Kuscheln,
mehr nicht. Marcin wacht nicht auf davon, dass Paolo sich aus dem Bett
verzieht.

Paolo nimmt, um Marcin in Ruhe schlafen zu lassen, seine Anziehsachen mit
ins Bad, wäscht sich kurz und bekleidet sich. Er hat eigentlich geplant,
Kendra eine möglichst große Auswahl an Frühstücksoptionen zur Verfügung zu
stellen, sodass sie auf jeden Fall haben kann, was sie gewohnt ist, oder
auch, was sie sich außer der Reihe diesen Morgen wünschen könnte. Er
hatte Würstchen und Spiegelei zu machen geplant, Croissant mit
Marmelade, Vollkornbrot, selbstgemachte Gemüsestreichpaste, Reis
mit gebratenem Obst und Nüssen (er weiß nicht einmal, ob das irgendwo auf
der Welt so gefrühstückt wird, aber Marcin hat manchmal Morgende, an
denen er nur genau das runterkriegt) und schließlich søreger Kräuterhafer
mit Butterkruste. Da Marcin vegetarisch ist, haben sie vegetarische
Würstchen da, aber wie Paolo befürchtet hat, sind sie nicht vegan. Und
die Butterkruste ist es natürlich auch nicht.

Paolo nimmt das schmale Ringbuch mit veganen Rezepten aus dem Kochbuchregal
und sucht nach Würstchen. Er wird fündig: Es gibt ein Rezept auf Basis von
roten Bohnen und Glutenmehl, vermengt mit allerlei Gewürzen. Er kommt auf die
Idee, außerdem Baked Beans zu machen. Das ist vielleicht auch ein guter
Ei-Ersatz. Leise macht er sich ans Werk.

---

Kendras Wecker düdelt in eine Tiefschlafphase hinein, die Kendra zu Ende genießen
möchte. Sie drückt Snooze. Das macht sie selten, und wenn, dann höchstens einmal. Aber
in den paar Wachsekunden nach dem Düdeln realisiert sie, dass sie heute jedes bisschen
Schlaf haben will, das sie noch kriegen kann. Sie öffnet noch einmal mühsam die
Augen, um den Wecker auf die spätest mögliche Zeit umzustellen, mit der sie
rechtzeitig in der Schule sein kann. Ohne Frühstück, nur mit Katzenwäsche.

Sie wacht dann aber nicht vom Weckerdüdeln auf, sondern vom Knarzen der Treppe. Beim
ersten Mal ignoriert sie es noch. Beim zweiten Mal rappelt sie sich murrend aus dem
Bett und öffnet die Tür, bevor die Schritte wieder ganz verklungen sind.

Paolo dreht sich zu ihr um. "Habe ich dich geweckt?"

Kendra zuckt die Schultern. "Ja und? Wird eh Zeit." Dann besinnt sie sich. "Ich
bin ein Morgenmufflon. Ich kriege das mit der Freundlichkeit am Morgen nicht
so richtig hin. Es tut mir leid."

"Ich kann dir Frühstück hochbringen", verspricht Paolo. "Vielleicht entschädigt das
fürs Wecken."

Kendra hebt eine Augenbraue. "Werde ich hier vollumfänglich verwöhnt? Ist das eine
Fünfsterne-Untermiete? Was sage ich, fünfzehn Sterne?"

Paolo grinst, antwortet aber nicht.

Als Kendra angezogen ist und ihre Schulsachen in eine Umhängetasche getan hat,
klopft Paolo wieder. Kendra öffnet und starrt etwas fassungslos auf ein Tablett
befüllt mit Köstlichkeiten, das sich vielleicht eher an fünf Personen hätte richten
sollen. Es duftet allerdings gut genug, dass Kendra für einen Moment denkt, dass
sie das alles schon alleine verdrückt bekommen würde. "Knoblauch zum Frühstück?" Ihr
Blick wandert skeptisch über die Dinge. Was davon riecht nach Knoblauch?

"Es ist alles vegan und du darfst dir eins davon aussuchen", teilt Paolo mit. "Oder
auch zwei. Oder du nimmst dir einen der leeren Teller und stellst dir was zusammen. Und, darf
ich reinkommen und bei dir frühstücken?"

Kendra fühlt sich nicht, als hätte sie das Recht, das abzulehnen, und öffnet die
Tür. Paolo tritt ein, wartet, bis sie Klammotten vom Nachttisch geräumt hat, die
sie da drauf platziert hat, und stellt das Tablett ab. Sie nimmt auf dem Bett Platz,
er auf dem einen Stuhl.

"Ja, ich habe das Talent, innerhalb einer Nacht dieses Chaos zu veranstalten", sagt
Kendra. "Ein weniger brauchbares Talent, als es deine Kochkünste sind. Du hast das
alles gemacht, oder?"

Paolo nickt. "Wir haben nicht so viel Zeit. Ich habe zwar nichts gegen Anerkennung
für meine Kochkünste, aber Hauptsache ist, es schmeckt. Und ich würde gern mit dir
über anderes reden."

"Schieß los!", fordert Kendra auf. Sie kann sich nicht entscheiden und nimmt tatsächlich
von allem etwas auf einen Teller. Sie registriert dabei, dass Paolo zufrieden lächelt.

"Über Marcin", sagt Paolo. "Es ist vielleicht besser, wenn er nicht erfährt, wo wir
uns begegnet sind."

"Dafür mag es zu spät sein", meint Kendra prompt. "Und was nun?"

Paolo runzelt die Stirn. "Ich dachte, ich hätte mitbekommen müssen, wenn er bei
dir hier oben gewesen wäre."

"Kontrollierst du ihn?" Kendra ist schneller damit, das zu sagen, als sie realisieren
kann, dass das eine beschissene Frage ist.

Paolo sieht sie entsprechend alarmiert an.

"Es tut mir leid, das war nicht okay", sagt Kendra sofort.

"Ich..." Paolo wirkt nicht beruhigt und in Gedanken verheddert. "Wir
haben heute Nacht in einem Bett geschlafen", sagt er schließlich. "Das geht dich
eigentlich auch einen Scheißdreck an. Aber ich hätte halt deshalb gemerkt, wenn
er lange weggewesen wäre. Nicht, weil ich ihn beobachte."

Kendra betrachtet ihn lange. Irgendwie hat sie jetzt mehr Sorge als zuvor. Vielleicht
ist da nichts. Aber Paolo ist hier, um mit ihr über Marcin zu reden, und fühlt sich
genötigt, sich zu verteidigen, als es zur Frage kommt, ob er ihn kontrolliere. "Warum
soll Marcin nicht davon erfahren, wo wir uns getroffen haben?"

"Marcins Schwester Bran, die vor dir hier das Zimmer bewohnt hat, ist gestorben. Der
Ort, wo wir uns getroffen haben, steht mit ihr in Verbindung.", erklärt Paolo. "Marcin ist emotional nicht
der stabilste, und wenn er zu viel Negatives fühlt, manifestieren sich seine
Emotionen in Form böser und gefährlicher Kreaturen. Das ist seine Magie." Paolo
seufzt. "Ich weiß nicht, ob Marcin schon davon erzählt hat. Es ist natürlich
eigentlich seine Sache, außer dass seine Magie andere gefährden kann. Und du erfährst
früher oder später ohnehin davon."

Kendra nickt und erinnert sich an den kleinen Drachen. Und dann an Marcins Bilder. Vielleicht
waren manche davon doch keine Fotobearbeitungen sondern echte Aufnahmen seiner
Magie. "Auf Basis welcher Überlegungen beschließt du, dass Marcin nicht hätte wissen sollen,
wo wir uns getroffen haben?"

Paolo runzelt die Stirn. "Ist das nicht klar?"

Kendra schüttelt den Kopf. "Du kennst Marcin sicher besser als ich. Aber auf mich
wirkte Marcin im Gespräch spontan erst einmal nicht so, als würde ihn die Sache mit
dem Tod seiner Schwester übermäßig emotional fühlen lassen", erklärt Kendra. Sie ist sich
nicht völlig sicher, klar. Aber Marcin hat es ohne Umschweife, ohne besonders gewählte
Worte, einer ihm fremden Person in einem Chat geschrieben. "Wenn ich einer emotional
instabilen Person Informationen vorenthalten soll, würde ich diese Entscheidung gern
auf Basis einer gut durchdachten Begründung fällen."

"Was denkst *du* denn?", fragt Paolo. "Seine Schwester ist gestorben! Seine Schwester!"

Kendra zuckt die Schultern. "Ich weiß. Von ihm."

"Er ist hier nachts zu dir rauf, um dir mal eben vom Mord an seiner Schwester zu
erzählen?", rückversichert sich Paolo.

Kendra hält gedanklich kurz inne. Dass es Mord war, hat bisher noch niemand von
beiden gesagt. Sie überlegt kurz, Paolo zu korrigieren, dass sie gechattet haben, aber
im Prinzip geht es Paolo nichts an. "Wie und wann er es mir gesagt hat, ist, denke
ich, unsere Sache. Ich mag daran erinnern, dass wir schon einen gemeinsamen Nachhauseweg
hierher hatten."

"Stimmt" Paolo runzelt kurz die Stirn. "Aber
denk doch mal logisch!", fordert er. "Wenn deine Schwester sterben
würde, was wäre emotional mit dir los? Würdest du das einfach wegstecken?"

"Ehrlich gesagt, wahrscheinlich", antwortet Kendra. "Aber ich habe keine Schwester."

"Wenn du eine hättest, wüsstest du es", erwidert Paolo.

Kendra schüttelt den Kopf. "Es kommt überhaupt nicht drauf an, was ich fühlen
würde", sagt sie. "Es geht hier um Marcin und dessen Gefühle. Leute trauern
sehr individuell. Es lässt sich nicht einfach von sich auf andere schließen."

"Wenn du wirklich liebst, trauerst du auch", hält Paolo fest. Er wirkt wütend.

Kendra *ist* wütend. "Das ist ableistischer Shit", sagt sie, dennoch sachlich. "Ich
fuße meine Entscheidung, ob ich einer emotional instabilen Person Informationen
vorenthalte, jedenfalls nicht auf wilden von dir auf *alle* anderen übertragenen
Hypothesen. Dafür, dass ich bei so etwas mitspiele, brauche ich schon etwas
mehr wissenschaftliche Grundlage. Basierend auf Daten und auf logischem Denken, wie
du gesagt hast."

Paolo steht auf. "Er ist gefährlich!", betont er. "Seine Magie ist gefährlich! Es
können Leute bei einem Ausbruch von ihm verletzt werden oder vielleicht sogar sterben. Und
dir ist das einfach egal?"

Kendra schüttelt den Kopf. "Überhaupt nicht", sagt sie. "Aber soweit ich die
Lage verstehe, führt vorenthaltene Information, wenn sie dann irgendwann doch
verraten wird, auch bei vielen Menschen zu emotionalen Reaktionen. Entsprechend
wäge ich ab. Wenn von Marcin zu erwarten wäre, dass er auf irgendeine Information
im Zusammenhang mit seiner Schwester stark emotional reagieren würde, wäre
es für mich vielleicht etwas anderes, aber dafür sehe ich keine Anhaltspunkte. Und
solange ich diese nicht sehe, werde ich kein Spiel mit vorenthaltenen Informationen
mitspielen."

"Es ist kein Spiel!", faucht Paolo. Aber er wirkt nachdenklich.

Kendra lässt ihn einfach gehen. Es ist kaum mehr Zeit, das Frühstück zu genießen. Und
sie weiß nicht so genau, was sie mit den Resten machen soll. Also trägt sie sie in die
Küche.
